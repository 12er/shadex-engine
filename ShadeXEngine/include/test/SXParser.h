#ifndef _SXPARSER_SXPARSER_CPP_
#define _SXPARSER_SXPARSER_CPP_

/**
 * parser test cases
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>
#include <string>
using namespace std;

namespace sxparser {

	void testReadFile(const string logname);
	void testParseNumbers(const string logname);
	void testParseStrings(const string logname);
	void testNodes(const string logname);
	void testParseSXdata(const string logname);
	void testParseXMLdata(const string logname);
	void testParsePLYdata(const string logname);
	void testParseColladaData(const string logname);
	void testParseColladaSkeleton(const string logname);
	void testBitmap(const string logname);

}

#endif