#ifndef _TEST_SXWIDGET_H_
#define _TEST_SXWIDGET_H_

/**
 * SX widget test cases
 * (c) 2012 by Tristan Bauer
 */
#include <export/Export.h>
#include <string>
#include <sx/XUnit.h>
#include <QWidget>
using namespace std;

namespace sxwidget {

	class CloseWidget: public QObject {
		Q_OBJECT
	private:
		QWidget *widget;
		sx::XUnit *unit;
	public:
		CloseWidget(QWidget *widget, sx::XUnit &unit);
	public slots:
		void closeWidget();
	};

	void testSXWidget(const string logname);

}

#endif