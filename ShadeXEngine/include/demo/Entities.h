#ifndef _DEMO_ENTITIES_H_
#define _DEMO_ENTITIES_H_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <export/Export.h>
#include <sx/SXMath.h>
#include <sx/SX.h>
#include <string>
#include <fstream>
using namespace std;

namespace sx {

	enum RenderStage {
		STAGE_USESMOKE ,
		STAGE_USESMOKELIGHTSHAFT ,
		STAGE_COMMON ,
		STAGE_USELIGHTSHAFT ,
	};

	class Renderer {
	public:
		static Renderer *renderer;

		virtual XTag *getConfiguration() = 0;
		virtual int getImageWidth() = 0;
		virtual int getImageHeight() = 0;
		virtual void setEmitSmoke(bool emitSmoke) = 0;
		virtual void setPreferredBottomPosition(const Vector &pos) = 0;
		virtual Vector getPreferredBottomPosition() const = 0;
		virtual void addTerrainSamplePoint(const Vector &inputPoint, Vector &terrainPoint) = 0;
		virtual void addSmokeInteractor(const Vector &position, const Vector &velocity) = 0;
		virtual void setFireflyAttractionPoint(const Vector &pos) = 0;
		virtual Vector getFireflyAttractionPoint() const = 0;
		virtual void setBlendLightshafts(float lightshaftfactor) = 0;
		virtual void setLightdir(const Vector &lightdir) = 0;
		virtual Vector getLightdir() = 0;
		virtual void setRenderStage(RenderStage renderStage) = 0;
		virtual RenderStage getRenderStage() const = 0;
	};
	
	/**
	 * entity of the demo
	 */
	class Entity {
	public:
		virtual ~Entity();

		/**
		 * per frame update of the entity
		 */
		virtual void calculate(SXRenderArea &area) = 0;

	};

	/**
	 * render stage attributes
	 */
	struct RenderStageData {
	public:
		RenderStageData();
		RenderStageData(const RenderStageData &rsd);
		RenderStageData &operator = (const RenderStageData &rsd);

		float backgroundSound;
		float time;
		RenderStage renderstage;
		bool emitsmoke;
		Vector smokeposition;
		Vector fireflyattractor;
		float lightshaftintensity;
		float sunrotationspeed;
	};

	/**
	 * views the scene
	 */
	class Viewer: public Entity {
	private:
		Vector *position;
		Vector *view;
		Vector *up;
		Matrix *projectionview;
		Matrix *mainprojectionview;
		Matrix *viewM;
		Matrix *normalview;
		Matrix *proj2world;

		//audio
		AudioPass *audioPass;
		AudioObject *backgroundMusic;
		UniformFloat *musicVolume;
		
		//user control related attributes
		float pressedControlKey;

		float switchedMode;
		bool demoMode;

		float switchedEmitSmoke;
		bool emitSmoke;

		float switchedMoveSun;
		bool moveSun;

		float runtime;
		float demotime;
		bool started;

		vector<pair<Vector,pair<Vector,float>>> userdefinedPath;
		vector<RenderStageData> userdefinedRenderStageData;
		
		//automatic camera related attributes
		UniformFloat *SXengineTime;
		UniformFloat *SXengineLogo;
		UniformFloat *glow;
		UniformFloat *showlogo;
		Matrix *SXengineCoordTransform;
		Texture *SXengineTexture;
		float maxLogoTime;
		bool displaylogo;
		vector<pair<Vector,pair<Vector,float>>> path;
		Vector lastpos;
		float lasttime;
		float currenttimedelta;
		float consumeNode;

		vector<RenderStageData> renderStageData;
		RenderStageData currentRenderStageData;
		Vector currentLightdir;

		//automatic camera
		void processPath(SXRenderArea &area);
		//processes user input
		void processUserInput(SXRenderArea &area);
		void printRenderStage();
		void printPath();
		void saveConfiguration();
	public:
		Viewer();
		~Viewer();
		bool isDemoMode() const;
		void calculate(SXRenderArea &area);
	};

	/**
	 * the sky of the scene
	 */
	class Sky: public Entity {
	private:
		/**
		 * direction from the sunlight in worldspace,
		 * points away from the sun
		 */
		Vector *lightdir;
		UniformFloat *ambient;
		Vector *position;
		Matrix *model;
		UniformFloat *daynightLightshafts;
	public:
		Sky();
		~Sky();
		void setBlendLightshafts(float lightshaftfactor);
		void setLightdir(const Vector &lightdir);
		Vector getLightdir();
		void calculate(SXRenderArea &area);
	};

	/**
	 * terrain
	 */
	class Terrain: public Entity {
	private:
		Vector *viewerPosition;
		Matrix *model;
		Matrix *projheightmap;
		Vector terrainPosition;

		BufferedMesh *inputPointsMesh;
		BufferedMesh *terrainPointsMesh;
		vector<Vector> inputPoints;
		vector<Vector *> terrainPoints;
	public:
		Terrain();
		~Terrain();
		void addSamplePoint(const Vector &inputPoint, Vector &terrainPoint);
		void calculate(SXRenderArea &area);
	};

	class Grass: public Entity {
	private:
		Vector grassPosition;
		Vector *viewerPosition;
		Matrix *model;
		Vector *wave1;
		Vector *wave2;
	public:
		Grass();
		~Grass();
		void calculate(SXRenderArea &area);
	};

	class Firefly: public Entity {
	private:
		int id;
		float size;
		float speed;
		float wingspeed;
		Vector fireflyposition;
		Vector fireflydirection;
		Vector attractor;
		float changeAttractor;
		UniformMatrix *model;
		UniformMatrix *normal;
		UniformMatrix *left;
		UniformMatrix *right;
		UniformMatrix *behind;
		UniformMatrix *middle;
		UniformMatrix *mmodel;
		UniformMatrix *mnormal;
		UniformMatrix *mleft;
		UniformMatrix *mright;
		UniformMatrix *mbehind;
		UniformMatrix *mmiddle;
		Vector terrainPosition;
	public:
		Firefly(int id);
		~Firefly();
		void setPosition(const Vector &pos);
		void setDirection(const Vector &dir);
		void setSize(float size);
		void calculate(SXRenderArea &area);
	};

	/**
	 * water
	 */
	class Water: public Entity {
	private:
		Vector *viewerPosition;
		Vector *view;
		Vector *up;
		Matrix *reflectionprojview;
		Matrix *reflectionview;
		Matrix *reflectionnormalview;
		Matrix *model;
		UniformFloat *waterheight;
		UniformFloat *abovewater;
		Vector *wave1;
		Vector *wave2;
	public:
		Water();
		~Water();
		void calculate(SXRenderArea &area);
	};

	struct SmokeInteractionCandidate {
	public:
		SmokeInteractionCandidate();
		SmokeInteractionCandidate(const SmokeInteractionCandidate &candidate);
		SmokeInteractionCandidate &operator = (const SmokeInteractionCandidate &candidate);

		friend bool operator < (const SmokeInteractionCandidate &c1, const SmokeInteractionCandidate &c2);

		float distance;
		Vector position;
		Vector velocity;
	};

	/**
	 * smoke
	 */
	class Smoke: public Entity {
	private:
		Matrix *castboxTransform;
		Matrix *castboxInvTransform;
		Matrix *castboxBox2Tex;
		Matrix *castboxWorld2Tex;
		Matrix *castboxTex2World;
		UniformFloat *dx;
		UniformFloat *insideCastbox;
		UniformFloat *emitSmoke;
		UniformFloat *decay;
		Vector *randacceleration;
		Vector *smokeboxpos;
		vector<Vector *> smokeInteractorPositions;
		vector<Vector *> smokeInteractorVelocities;
		vector<SmokeInteractionCandidate> smokeInteractionCandidates;
		Vector preferredBottomPosition;

		int currentTarget;
		bool simulateSmokeDynamics;
	public:
		Smoke();
		~Smoke();
		void setSimulateSmokeDynamics(bool simulateSmokeDynamics);
		void setEmitSmoke(bool emitSmoke);
		void setPreferredBottomPosition(const Vector &pos);
		Vector getPreferredBottomPosition() const;
		void addSmokeInteractor(const Vector &position, const Vector &velocity);
		int getCurrentTarget() const;
		void calculate(SXRenderArea &area);
	};

}

#endif