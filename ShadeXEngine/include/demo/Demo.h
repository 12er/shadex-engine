#ifndef _DEMO_DEMO_H_
#define _DEMO_DEMO_H_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SX.h>
#include <demo/Entities.h>
#include <vector>
using namespace std;

namespace sx {

	class Demo: public SXRenderListener, public Renderer {
	private:

		int imageWidth;
		
		int imageHeight;
		
		bool useScreenDimensions;

		Viewer *viewer;

		Smoke *smoke;

		Sky *sky;

		Terrain *terrain;

		RenderStage renderStage;

		vector<Entity *> entities;

		Vector fireflyAttractionPoint;

		XTag *configuration;

		Demo(const Demo &);

		Demo &operator = (const Demo &);

		void loadEntities();

	public:

		Demo(const string &mapconfigfile);

		~Demo();

		XTag *getConfiguration();

		int getImageWidth();

		int getImageHeight();

		void setImageWidth(int width);

		void setImageHeight(int height);

		void setUseScreenDimensions(bool use);

		void setRenderStage(RenderStage renderStage);

		RenderStage getRenderStage() const;

		void setEmitSmoke(bool emitSmoke);

		void setPreferredBottomPosition(const Vector &pos);

		Vector getPreferredBottomPosition() const;

		void addTerrainSamplePoint(const Vector &inputPoint, Vector &terrainPoint);

		void addSmokeInteractor(const Vector &position, const Vector &velocity);

		void setFireflyAttractionPoint(const Vector &pos);

		Vector getFireflyAttractionPoint() const;

		void setBlendLightshafts(float lightshaftfactor);

		void setLightdir(const Vector &lightdir);

		Vector getLightdir();

		void create(SXRenderArea &area);

		void reshape(SXRenderArea &area);

		void render(SXRenderArea &area);

		void stop(SXRenderArea &area);

	};

}

#endif