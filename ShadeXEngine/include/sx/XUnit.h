#ifndef _SX_XUNIT_H_
#define _SX_XUNIT_H_

/**
 * Testing framework
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>
#include <sx/Log4SX.h>

namespace sx {

	/**
	 * collects information about the test cases
	 */
	class XUnit {
	private:

		/**
		 * Logger for the test cases. User defined
		 * comments, and the outcome of the tests
		 * is passed to the logger.
		 */
		Logger *logger;

		/**
		 * true iff every test has been working
		 */
		bool passed;

		/**
		 * copy constructor, not used
		 */
		XUnit(const XUnit &);

		/**
		 * assignment operator, not used
		 */
		XUnit &operator = (const XUnit &);

		/**
		 * intern member of the class
		 */
		template<class X>
		void _INTERN_ASSERTEQUALS_(X v1, X v2) {
			(*logger) << Annotation("assertEquals: ");
			if(v1 == v2) {
				(*logger) << Level(L_HARMLESS) << "success" << Logger::newLine();
			} else {
				(*logger) << Level(L_ERROR) << "fail: param1 is " << v1 << ", but param2 is " << v2 << Logger::newLine();
				passed = false;
			}
		}

	public:

		/**
		 * Constructs a XUnit object with
		 * a logger.
		 *
		 * @param logger must be a valid Logger, messages about the outcome of the tests are passed to the logger
		 */
		EXL XUnit(Logger *logger);

		/**
		 * deconstructor
		 */
		EXL ~XUnit();
		
		/**
		 * Appends a markup to the test's log with markup message comment.
		 *
		 * @param comment the added markup has markup message comment
		 */
		EXL void addMarkup(string comment);

		/**
		 * Appends an annotation to the test's log with annotation message annotation.
		 *
		 * @param annotation the annotation message
		 */
		EXL void addAnnotation(string annotation);

		/**
		 * the test is passed, iff value is true
		 */
		EXL void assertTrue(bool value);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(char v1, char v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(unsigned char v1, unsigned char v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(short v1, short v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(unsigned short v1, unsigned short v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(int v1, int v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(unsigned int v1, unsigned int v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(long v1, long v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(unsigned long v1, unsigned long v2);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(float v1, float v2);

		/**
		 * the test is passed, iff the absolute value of the difference
		 * of v1 and v2 is smaller or equal to epsilon
		 */
		EXL void assertEquals(float v1, float v2, float epsilon);

		/**
		 * the test is passed, iff v1 and v2 have the same value
		 */
		EXL void assertEquals(double v1, double v2);

		/**
		 * the test is passed, iff the absolute value of the difference
		 * of v1 and v2 is smaller or equal to epsilon
		 */
		EXL void assertEquals(double v1, double v2, double epsilon);

		/**
		 * the test is passed, iff v1 and v2 are made of exactly the same
		 * character sequences
		 */
		EXL void assertEquals(string v1, string v2);

		/**
		 * Waits for the user to input text in the console.
		 * The test is passed, iff the user inputs the string 'y'.
		 * 
		 * @param message Text displayed at the console to ask the user for an input. The string (y/n) is appended to message.
		 */
		EXL void assertInputYes(string message);

	};

}

#endif