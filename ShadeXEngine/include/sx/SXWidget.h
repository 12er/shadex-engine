#ifndef _SX_SXWIDGET_H_
#define _SX_SXWIDGET_H_

/**
 * ShadeX Engine Widget for the Qt framework
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>
#include <sx/SX.h>
#include <sx/SXMath.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <QWidget>
#include <QTimer>
#include <QtOpenGL/QGLWidget>
#include <boost/unordered_map.hpp>
#include <boost/timer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <map>
#include <vector>
using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::posix_time::to_simple_string;
using namespace std;
using namespace boost;

namespace sx {

	class SXWidget;

	/**
	 * widget used by SXWidget to render
	 * with renderlisteners
	 */
	class SXInternalWidget: public QGLWidget {
		
		Q_OBJECT

	private:

		/**
		 * the father SXWidget
		 */
		SXWidget *sxWidget;

		/**
		 * mapping of qt special keys to
		 * sx special keys
		 */
		static map<int,int> keys_qt2sx;

		/**
		 * init function for keys_qt2sx mapping
		 */
		static map<int,int> initKeys_qt2sx();

		void keyPressEvent(QKeyEvent * e);

		void keyReleaseEvent(QKeyEvent * e);

		void mousePressEvent(QMouseEvent * e);

		void mouseReleaseEvent(QMouseEvent * e);

		void mouseMoveEvent(QMouseEvent * e);

		void wheelEvent(QWheelEvent * e);

		/**
		 * copy constructor, not used
		 */
		SXInternalWidget(const SXInternalWidget &);

		/**
		 * assignment operator, not used
		 */
		SXInternalWidget &operator = (const SXInternalWidget &);

	public:

		/**
		 * constructor, takes father SXWidget as a parameter
		 */
		SXInternalWidget(SXWidget *sxWidget);

		/**
		 * deconstructor
		 */
		~SXInternalWidget();

		/**
		 * initializes opengl for the render listeners
		 */
		void initializeGL();

		/**
		 * calls resize and create in the render listeners
		 */
		void resizeGL(int width, int height);

		/**
		 * calls render and stop in renderlisteners
		 */
		void paintGL();

	};

	/**
	 * a Widget offering an environment for SXRenderListeners
	 */
	class SXWidget: public QWidget, public SXRenderArea {

		Q_OBJECT

		friend class SXInternalWidget;

	private:

		/**
		 * true iff resources have been
		 * created, which have not been
		 * deconstructed
		 */
		bool constructed;

		/**
		 * true if listeners are processed currently
		 */
		bool processingListeners;

		/**
		 * true if stopAfterRender should be
		 * called after all listeners are processed
		 */
		bool callStopAfterRender;

		/**
		 * render listeners
		 */
		vector<SXRenderListener *> listeners;

		/**
		 * render listeners, which have not been initialized yet
		 */
		vector<SXRenderListener *> initListeners;

		/**
		 * timer, makes SXWidget render
		 * periodically
		 */
		QTimer timer;

		/**
		 * measurement of the time since creation of the
		 * renderer in seconds
		 */
		boost::timer timeMeasurement;

		/**
		 * the starting time of the last frame
		 */
		double lastTimeRendered;

		/**
		 * the starting time of the current frame
		 */
		double currentTimeRendering;

		/**
		 * x coordinate of mouseposition
		 */
		int mouseX;

		/**
		 * y coordinate of mouseposition
		 */
		int mouseY;

		/**
		 * x coordinate of the difference of the
		 * mousepointer shortly before set with setMousePosition
		 * and the position set with setMousePosition
		 */
		double mouseDeltaX;

		/**
		 * y coordinate of the difference of the
		 * mousepointer shortly before set with setMousePosition
		 * and the position set with setMousePosition
		 */
		double mouseDeltaY;

		/**
		 * last time the mouse was set to a position
		 * with setMousePointer
		 */
		double lastSetMouseTime;

		/**
		 * history of positions set with setMousePointer
		 */
		vector<Vector> setMouseDeltas;

		/**
		 * map of currently pressed keys
		 */
		unordered_map<int,int> keys;

		/**
		 * map of currently pressed mouse buttons
		 */
		unordered_map<int,int> mouseKeys;

		/**
		 * called for each frame once, sets the starting times
		 * of the last and the current time
		 */
		void updateTime();

		/**
		 * adds a key to map keys in SXWidget
		 */
		void addKey(int key);

		/**
		 * removes a key from map keys in SXWidget
		 */
		void removeKey(int key);

		/**
		 * adds a mousekey to map mouseKeys in SXWidget
		 */
		void addMouseKey(MouseButton key);

		/**
		 * removes a mousekey from map mouseKeys in SXWidget
		 */
		void removeMouseKey(MouseButton key);

		/**
		 * area providing rendering facilities
		 */
		SXInternalWidget *internalWidget;

		/**
		 * copy constructor, not used
		 */
		SXWidget(const SXWidget &);

		/**
		 * assignment operator, not used
		 */
		SXWidget &operator = (const SXWidget &);

	signals:

		/**
		 * fires after method stopRendering()
		 * deallocated all render listeners
		 */
		EXW void finishedRendering();

		/**
		 * Fires when the user rotates the
		 * mousewheel. The value of parameter
		 * degrees is equal to the angle the
		 * user rotated the mousewheel away
		 * from its position before in degrees.
		 * A positive value indicates, that the
		 * user rotated the wheel was rotated
		 * away from the user.
		 */
		EXW void wheelRotates(float degrees);

	public:

		/**
		 * constructor
		 */
		EXW SXWidget();

		/**
		 * deconstructor
		 */
		EXW ~SXWidget();

		/**
		 * Makes the SXWidget render repeatedly. Parameter
		 * time specifies the amount of time from one frame
		 * to another in seconds.
		 */
		EXW void renderPeriodically(float time);

		/**
		 * Makes the SXWidget stop rendering repeatedly.
		 */
		EXW void stopRenderPeriodically();

		/**
		 * Adds a listener. The listener is removed, when the widget
		 * is removed.
		 */
		EXW void addRenderListener(SXRenderListener &l);

		/**
		 * Removes a listener, if it's part of the widget. Otherwise the listener is not removed.
		 */
		EXW void deleteRenderListener(SXRenderListener &l);

		/**
		 * removes all listeners, if the widget has not stopped rendering yet
		 */
		EXW void deleteRenderListeners();

		/**
		 * @see sx::SXRenderArea::getTime()
		 */
		EXW double getTime() const;

		/**
		 * @see sx::SXRenderArea::getDeltaTime()
		 */
		EXW double getDeltaTime() const;

		/**
		 * @see sx::SXRenderArea::getMouseX()
		 */
		EXW int getMouseX() const;

		/**
		 * @see sx::SXRenderArea::getMouseY()
		 */
		EXW int getMouseY() const;

		/**
		 * @see sx::SXRenderArea::getMouseDeltaX()
		 */
		EXW double getMouseDeltaX() const;
		
		/**
		 * @see sx::SXRenderArea::getMouseDeltaY()
		 */
		EXW double getMouseDeltaY() const;

		/**
		 * @see sx::SXRenderArea::hasKey(int)
		 */
		EXW bool hasKey(int key) const;

		/**
		 * @see sx::SXRenderArea::hasMouseKey(MouseButton)
		 */
		EXW bool hasMouseKey(MouseButton key) const;

		/**
		 * @see sx::SXRenderArea::getWidth()
		 */
		EXW int getWidth() const;

		/**
		 * @see sx::SXRenderArea::getHeight()
		 */
		EXW int getHeight() const;

		/**
		 * @see sx::SXRenderArea::stopRendering()
		 */
		EXW void stopRendering();

		/**
		 * @see sx::SXRenderArea::setMousePointer(int,int)
		 */
		EXW void setMousePointer(int x, int y);

		/**
		 * @see sx::SXRenderArea::setShowCursor(bool)
		 */
		EXW void setShowCursor(bool showCursor);

	};

}

#endif