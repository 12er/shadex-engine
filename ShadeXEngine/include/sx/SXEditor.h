#ifndef _SX_SXEDITOR_H_
#define _SX_SXEDITOR_H_

/**
 * ShadeX Engine Editor
 * (c) 2012 by Tristan Bauer
 */
#include <export/Export.h>
#include <QWidget>
#include <QSplitter>
#include <QTabWidget>
#include <QToolButton>
#include <QLabel>
#include <QStackedWidget>
#include <QLayout>
#include <sx/SXWidget.h>
#include <QLineEdit>
#include <QDialog>
#include <QGraphicsScene>
#include <QCompleter>
#include <string>
#include <boost/unordered_map.hpp>
using namespace boost;
using namespace std;

namespace sx {

	enum LayoutExpansion {
		EXPAND_VERTICALLY,
		EXPAND_HORIZONTALLY
	};

	class SequenceLayout: public QLayout
	{
		Q_OBJECT
	private:
		QList<QLayoutItem *> items;
		LayoutExpansion expansion;
		int margin_bottom;
		int margin_top;
		int margin_left;
		int margin_right;

		SequenceLayout(const SequenceLayout &);
		SequenceLayout &operator = (const SequenceLayout &);
	public:
		SequenceLayout(LayoutExpansion expansion);

		void setMargin(int margin_bottom, int margin_top, int margin_left, int margin_right);
		void setMargin(int margin);

		void addItem(QLayoutItem *);
		int count() const;
		QLayoutItem *itemAt(int index) const;
		QLayoutItem *takeAt(int index);
		void setGeometry(const QRect &);
		QSize sizeHint() const;
		QSize minimumSize() const;
		bool hasHeightForWidth() const;
		int heightForWidth(int) const;
	};

	class ClickableLabel: public QLabel {
		Q_OBJECT
	private:
		ClickableLabel(const ClickableLabel &);
		ClickableLabel &operator = (const ClickableLabel &);
	protected:
		 void mousePressEvent(QMouseEvent *ev);
	public:
		ClickableLabel(QWidget * parent = 0, Qt::WindowFlags f = 0);
		ClickableLabel(const QString & text, QWidget * parent = 0, Qt::WindowFlags f = 0);
	signals:
		void clicked();
	};

	class Warning: public QDialog {
		Q_OBJECT
	private:
		void initGui(const QString &msg);

		Warning(const Warning &);
		Warning &operator = (const Warning &);
	public:
		Warning(const QString &title, const QString &text, QWidget *w);
	};

	struct SXLogicObject {
		string ID;
		unordered_map<int,unordered_map<int,int> > guiParts;
	};

	struct SXLogicPass: public SXLogicObject {
	};

	struct SXLogicObjects {
	private:
		static unordered_map<int,int> nextIDs;
	public:
		static unordered_map<string,SXLogicObject *> objects;
		static unordered_map<string,unordered_map<int,int> > effects;
		static unordered_map<string,SXLogicPass *> passes;
		static unordered_map<string,unordered_map<int,int> > renderobjects;
		static unordered_map<string,unordered_map<int,int> > meshes;
		static unordered_map<string,unordered_map<int,int> > shaders;
		static unordered_map<string,unordered_map<int,int> > rendertargets;
		static unordered_map<string,unordered_map<int,int> > textures;
		static unordered_map<string,unordered_map<int,int> > matrices;
		static unordered_map<string,unordered_map<int,int> > vectors;
		static unordered_map<string,unordered_map<int,int> > floats;

		static bool checkAddPass(const string &ID);
		static void addPass(const string &ID, int diagramID, int guiID);
		static void changePass(const string &oldID, const string &newID, int diagramID, int guiID);
		static void removePass(const string &ID, int diagramID, int guiID);
	
		static void updateNextID(int diagramID, int existingID);
		static int getNextID(int diagramID);
	};

	class GraphEditPane;

	struct ConfigParts {
		int ID;
		QWidget *tools;
		GraphEditPane *graphEdit;
		QWidget *detailEdit;
		QToolButton *closeDetailButton;
		QWidget *detailEditPane;
	};

	enum EditMode {
		MODE_NONE,
		MODE_ADDPASS,

		MODE_EDITPASS,
		
		MODE_MOVE,
	};

	class ChooseIDDialog: public QDialog {
		Q_OBJECT
	private:
		EditMode editMode;
		QLineEdit *input;
		QCompleter *completer;
		QString ID;

		void initGui();
		ChooseIDDialog(const ChooseIDDialog &);
		ChooseIDDialog &operator = (const ChooseIDDialog &);
	private slots:
		void choose();
	public:
		ChooseIDDialog(EditMode mode, QWidget *w = 0);
		const QString getID() const;
	};

	class ChangeIDInput: public QWidget {
		Q_OBJECT
	private:
		GraphEditPane *scene;
		EditMode editMode;
		ClickableLabel *label;
		QDialog *inputWidget;
		QLineEdit *input;
		QCompleter *completer;
		QString ID;
		int diagramID;
		int guiID;

		void initGui();
		ChangeIDInput(const ChangeIDInput &);
		ChangeIDInput &operator = (const ChangeIDInput &);
	private slots:
		void awaitInput();
		void takeInput();
		void discardInput();
	public:
		ChangeIDInput(EditMode mode, const QString &ID, int diagramID, int guiID, GraphEditPane *scene = 0);
		void setID(const QString &ID);
		const QString getID() const;
	public slots:
		void showLabel();
		void showInput();
	signals:
		void changedGui();
	};

	class ChangeUniformInput: public QWidget {
		Q_OBJECT
	private:
		GraphEditPane *scene;
		EditMode editMode;
		ClickableLabel *label;
		QDialog *inputWidget;
		QLineEdit *input;
		QLabel *assignmentLabel;
		QLineEdit *valueInput;
		QToolButton *addValueButton;
		QToolButton *removeValueButton;
		QCompleter *completer;
		QCompleter *valueCompleter;
		QString ID;
		int diagramID;
		int guiID;

		void initGui();
		ChangeUniformInput(const ChangeUniformInput &);
		ChangeUniformInput &operator = (const ChangeUniformInput &);
	private slots:
		void awaitInput();
		void takeInput();
		void discardInput();
	public:
		ChangeUniformInput(EditMode mode, const QString &ID, int diagramID, int guiID, GraphEditPane *scene = 0);
		void setID(const QString &ID);
		const QString getID() const;
	public slots:
		void showLabel();
		void showInput();
	signals:
		void changedGui();
	};

	class GraphicWidget: public QWidget {
		Q_OBJECT
	protected:
		int guiID;
		GraphEditPane *scene;

		GraphicWidget(int guiID, GraphEditPane *scene, QWidget *parent = 0);
	protected slots:
		void startMove();
	private:
		GraphicWidget(const GraphicWidget &);
		GraphicWidget &operator = (const GraphicWidget &);
	public:
		virtual void setID(const QString &ID) = 0;
		virtual const QString getID() const = 0;
		int getGuiID() const;
	public slots:
		void pack();
		virtual void removeDiagramItem() = 0;
	};

	class PassWidget: public GraphicWidget {
		Q_OBJECT
	private:
		ClickableLabel *titleLabel;
		ChangeIDInput *IDgui;

		void initGui();
		PassWidget(int guiID, const PassWidget &);
		PassWidget &operator = (const PassWidget &);
	public:
		PassWidget(const QString &ID, int guiID, GraphEditPane *scene, QWidget *parent = 0);
		void setID(const QString &ID);
		const QString getID() const;
	protected:
		void paintEvent(QPaintEvent *);
	public slots:
		void removeDiagramItem();
	};

	class GraphEditPane: public QGraphicsScene {
		Q_OBJECT
	private:
		int diagramID;
		unordered_map<int,GraphicWidget *> nodes;
		QWidget *parentWidget;
		QGraphicsView *view;
		QString nextID;
		EditMode editMode;
		QPointF movePoint;

		GraphEditPane(const GraphEditPane &);
		GraphEditPane &operator = (const GraphEditPane &);
	public:
		GraphEditPane(int diagramID, const QSize &size,QWidget *parent = 0);
		int getDiagramID() const;
		void setNextID(const QString &ID);
		void setEditMode(EditMode mode);
		void addPass(const QPointF &point, const QString &ID, int guiID);
		void addNode(GraphicWidget &node);
		void removeNode(int guiID);
		void setGraphicsView(QGraphicsView *view);
		QGraphicsView *getGraphicsView();
	protected:
		void mousePressEvent(QGraphicsSceneMouseEvent *event);
		void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
		void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
		void keyPressEvent(QKeyEvent *keyEvent);
	public slots:
		void removeDiagramItems();
	signals:
		void removingDiagramItems();
	};

	class SXEditor: public QWidget {
		Q_OBJECT
	private:
		QStackedWidget *mainGuis;

		QWidget *start;

		QSplitter *splitter;
		int nextConfigID;
		QTabWidget *configurations;
		QVector<ConfigParts> configParts;
		QAction *addPassAction;
		SXWidget *renderer;

		SXEditor(const SXEditor &);
		SXEditor &operator = (const SXEditor &);
		void initGui();
	private slots:
		void createDialog();
		void openDialog();
		void setAddPass();
	public:
		SXEditor();
		~SXEditor();
		void addConfiguration(const QString &path = "", bool load = false);
		QSize sizeHint() const;
		ConfigParts &currentConfig();
	public slots:
		void removeConfiguration(int index);
	};

	class CreateConfigDialog: public QDialog {
		Q_OBJECT
	private:
		QLineEdit *input;

		void initGui();
		CreateConfigDialog(const CreateConfigDialog &);
		CreateConfigDialog &operator = (const CreateConfigDialog &);
	private slots:
		void create();
	public:
		CreateConfigDialog(QWidget *w);
		const QString getText() const;
	};

}

#endif