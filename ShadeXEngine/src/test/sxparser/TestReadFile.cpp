#ifndef _TEST_SXPARSER_TESTREADFILE_CPP_
#define _TEST_SXPARSER_TESTREADFILE_CPP_

/**
 * test cases with files
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/SXParser.h>
#include <sstream>
using namespace sx;
using namespace std;

namespace sxparser {

	void testReadFile(const string logname) {
		XUnit unit(new FileLogger(logname));
		unit.addMarkup("Reading Files");

		unit.addAnnotation("Read an invalid file - ");
		try {
			readFile("someshitthatdoesnotexist");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addAnnotation("Read an empty file - ");
		try {
			string content = readFile("../data/test/sxparser/files/0.txt");
			unit.assertEquals(content,"");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("Read a file made of one word - ");
		try {
			string content = readFile("../data/test/sxparser/files/1.txt");
			unit.assertEquals(content,"hello");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("Read a file with multiple lines - ");
		try {
			string content = readFile("../data/test/sxparser/files/2.txt");
			unit.assertEquals(content,
				"here\n"
				"we have\n"
				"some content!"
				);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("Read a large file with 8192 lines - ");
		stringstream testvalue;
		for(unsigned int i=0 ; i<8192 ; i++) {
			testvalue << "A lot of content is in here! A lot of content is in here! A lot of content is in here!\n";
		}
		try {
			string content = readFile("../data/test/sxparser/files/3.txt");
			unit.assertEquals(content,
				testvalue.str()
				);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

	}

}

#endif