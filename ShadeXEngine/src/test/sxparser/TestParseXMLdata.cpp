#ifndef _TEST_SXPARSER_TESTPARSEXMLDATA_CPP_
#define _TEST_SXPARSER_TESTPARSEXMLDATA_CPP_

/**
 * XML parser test cases
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXParser.h>
#include <sx/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <boost/foreach.hpp>
using namespace sx;
using namespace boost;

namespace sxparser {

	void testParseXMLdata(XUnit &unit) {
		//test detect wrong closing tags
		unit.addMarkup("parseXMLdata - incorrect closing tags");
		try {
			XTag * tag = parseXMLFile("../data/test/sxparser/files/xml1.xml");
			delete tag;
			unit.addAnnotation("closing tag - failed to detect wrong closing tag - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		//test detect attribute id uniqueness test
		unit.addMarkup("parseXMLdata - finding mutliple attribute ID occurence");
		try {
			XTag * tag = parseXMLFile("../data/test/sxparser/files/xml2.xml");
			delete tag;
			unit.addAnnotation("closing tag - failed to detect an attribute ID occuring for a second time in a tag - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		//test valid xml files
		unit.addMarkup("tests for parseXMLdata - valid xml files");
		try {
			XTag *tag = parseXMLFile("../data/test/sxparser/files/xml3.xml");
			unit.addAnnotation("xml - node count - ");
			unit.assertEquals(tag->nodes.size(),(unsigned int)9);
			unit.addAnnotation("xml - attribute count - ");
			unit.assertEquals(tag->stringAttribs.size(),(unsigned int)2);
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(tag->name,"te:s.t");
			unit.addAnnotation("xml - attributes - ");
			unit.assertEquals(tag->getStrAttribute("at"),"fuck");
			unit.addAnnotation("xml - attributes - ");
			unit.assertEquals(tag->getStrAttribute("bat"),"luck");
			vector<XTag *> tags;
			tag->getTags("*",tags);
			if(tags.size() < 4) {
				throw Exception("Too less child tags");
			}
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(tags[0]->name,"jack");
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(tags[1]->name,"kack");
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(tags[2]->name,"myxml");
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(tags[3]->name,"p");
			XTag *kack = tag->getFirst("kack");
			unit.addAnnotation("xml - attributes - ");
			unit.assertEquals(kack->getStrAttribute("jack"),"fack");
			unit.addAnnotation("xml - attributes - ");
			unit.assertEquals(kack->getStrAttribute("sack"),"dack");
			XTag *myxml = tag->getFirst("myxml");
			unit.addAnnotation("xml - attributes - ");
			unit.assertEquals(myxml->getStrAttribute("sacker"),"sack");
			XTag *somexml = myxml->getFirst("somexml");
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(somexml->name,"somexml");
			unit.addAnnotation("xml - text - ");
			unit.assertEquals(somexml->getDirectTexts(),"some text");
			
			delete tag;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		//test parsing a large xml file
		unit.addMarkup("tests for parseXMLdata - parse a large xml file");
		try {
			XTag *tag = parseXMLFile("../data/test/sxparser/files/xml4.xml");
			unit.addAnnotation("xml - node count - ");
			unit.assertEquals(tag->nodes.size(),(unsigned int)7);
			unit.addAnnotation("xml - attribute count - ");
			unit.assertEquals(tag->stringAttribs.size(),(unsigned int)0);
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(tag->name,"level");
			XTag *entities = tag->getFirst("entities");
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(entities->name,"entities");
			unit.addAnnotation("xml - node count - ");
			unit.assertEquals(entities->nodes.size(),(unsigned int)3575);
			vector<XTag *> tags;
			entities->getTags("*",tags);
			if(tags.size() < 1400) {
				throw Exception("not enough child nodes");
			}
			XTag *palmtree = tags[1399];
			//XTag *palmtree = dynamic_cast<XTag *>(entities->nodes[2799]);
			unit.addAnnotation("xml - tagname - ");
			unit.assertEquals(palmtree->name,"entity");
			unit.addAnnotation("xml - attribute - ");
			unit.assertEquals(palmtree->getStrAttribute("type"),"palmtree");
			vector<XTag *> components;
			palmtree->getTags("position.*",components);
			palmtree->getTags("zrotation",components);
			if(components.size() < 4) {
				throw Exception("not enough child nodes");
			}
			unit.addAnnotation("xml - value - ");
			unit.assertEquals(components[0]->getDirectTexts(),"713.669");
			unit.addAnnotation("xml - value - ");
			unit.assertEquals(components[1]->getDirectTexts(),"-574.782");
			unit.addAnnotation("xml - value - ");
			unit.assertEquals(components[2]->getDirectTexts(),"24.3361");
			unit.addAnnotation("xml - value - ");
			unit.assertEquals(components[3]->getDirectTexts(),"5.8757");

			delete tag;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

	void testParseXMLdata(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testParseXML",new ListLogger());
			Logger::setDefaultLogger("testParseXML");

			testParseXMLdata(unit);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif