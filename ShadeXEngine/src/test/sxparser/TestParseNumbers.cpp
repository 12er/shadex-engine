#ifndef _TEST_SX_TESTPARSENUMBERS_CPP_
#define _TEST_SX_TESTPARSENUMBERS_CPP_

/**
 * number parser test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXParser.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <vector>
using namespace sx;
using namespace std;

namespace sxparser {

	void testParseNumbers(const string logname) {
		XUnit unit(new FileLogger(logname));
		unit.addMarkup("Parsing lists of numbers");

		unit.addAnnotation("parse an empty numberlist - size is zero - ");
		try {
			vector<double> numbers = parseNumbers("");
			unit.assertEquals(numbers.size(),(unsigned int)0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("parse a numberlist with one number - ");
		try {
			vector<double> numbers = parseNumbers("	 \n	\r\n3.14159		 ");
			unit.assertEquals(numbers.size(),(unsigned int)1);

			unit.addAnnotation("numberlist with one number - check 1. number - ");
			unit.assertEquals(numbers[0],3.14159);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("parse a numberlist with multiple numbers - ");
		try {
			vector<double> numbers = parseNumbers("	 \n	\r\n3.14159 1.13999e-14	\n -6 7		  135.4456	-1.50996e7	-9");
			unit.assertEquals(numbers.size(),(unsigned int)7);

			unit.addAnnotation("numberlist with one number - check 1. number - ");
			unit.assertEquals(numbers[0],3.14159);

			unit.addAnnotation("numberlist with one number - check 2. number - ");
			unit.assertEquals(numbers[1],0.0000000000000113999);

			unit.addAnnotation("numberlist with one number - check 3. number - ");
			unit.assertEquals(numbers[2],-6.0);

			unit.addAnnotation("numberlist with one number - check 4. number - ");
			unit.assertEquals(numbers[3],7.0);

			unit.addAnnotation("numberlist with one number - check 5. number - ");
			unit.assertEquals(numbers[4],135.4456);

			unit.addAnnotation("numberlist with one number - check 6. number - ");
			unit.assertEquals(numbers[5],-15099600.0);

			unit.addAnnotation("numberlist with one number - check 7. number - ");
			unit.assertEquals(numbers[6],-9.0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("parse a numberlist with an error - ");
		try {
			vector<double> numbers = parseNumbers("1 2 3 4 2 4 5z 4 2 1");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}
	}
	
}

#endif