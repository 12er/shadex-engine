#ifndef _TEST_SXPARSER_TESTPARSESXDATA_CPP_
#define _TEST_SXPARSER_TESTPARSESXDATA_CPP_

/**
 * SX parser test cases
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXParser.h>
#include <sx/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <boost/foreach.hpp>
using namespace sx;
using namespace boost;

namespace sxparser {

	void testParseSXdata(XUnit &unit) {
		//test detect attribute id uniqueness test
		unit.addMarkup("parseSXdata - finding mutliple attribute ID occurence");
		try {
			XTag * tag = parseSXFile("../data/test/sxparser/files/sx1.c");
			delete tag;
			unit.addAnnotation("attributes - failed to detect an attribute ID occuring for a second time in a tag - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		try {
			XTag * tag = parseSXFile("../data/test/sxparser/files/sx2.c");
			delete tag;
			unit.addAnnotation("attributes - failed to detect an attribute ID occuring for a second time in a tag - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		//test valid sx files
		unit.addMarkup("tests for parseSXdata - valid sx files");
		try {
			XTag *tag = parseSXFile("../data/test/sxparser/files/sx3.c");
			unit.addAnnotation("sx - node count - ");
			unit.assertEquals(tag->nodes.size(),(unsigned int)4);
			unit.addAnnotation("sx - attribute count - ");
			unit.assertEquals(tag->stringAttribs.size(),(unsigned int)2);
			unit.addAnnotation("sx - attribute count - ");
			unit.assertEquals(tag->realAttribs.size(),(unsigned int)2);
			unit.addAnnotation("sx - tagname - ");
			unit.assertEquals(tag->name,"te:s.t");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(tag->getStrAttribute("at"),"fuck");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(tag->getStrAttribute("bat"),"luck");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(tag->getRealAttribute("at"),32.1);
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(tag->getRealAttribute("id"),1.2);
			vector<XTag *> tags;
			tag->getTags("*",tags);
			if(tags.size() < 4) {
				throw Exception("Too less child tags");
			}
			unit.addAnnotation("sx - tagname - ");
			unit.assertEquals(tags[0]->name,"jack");
			unit.addAnnotation("sx - tagname - ");
			unit.assertEquals(tags[1]->name,"kack");
			unit.addAnnotation("sx - tagname - ");
			unit.assertEquals(tags[2]->name,"myxml");
			unit.addAnnotation("sx - tagname - ");
			unit.assertEquals(tags[3]->name,"p");
			XTag *kack = tag->getFirst("kack");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(kack->getStrAttribute("jack"),"fack");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(kack->getStrAttribute("sack"),"dack");
			XTag *myxml = tag->getFirst("myxml");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(myxml->getStrAttribute("sacker"),"sack");
			unit.addAnnotation("sx - attributes - ");
			unit.assertEquals(myxml->getDirectTexts(),"heyho");
			XTag *somexml = myxml->getFirst("somexml");
			unit.addAnnotation("sx - tagname - ");
			unit.assertEquals(somexml->name,"somexml");
			unit.addAnnotation("sx - text - ");
			unit.assertEquals(somexml->getDirectTexts(),"some text");
			
			delete tag;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		//test large sx files
		unit.addMarkup("tests for parseSXdata - large sx files");
		try {
			XTag *tag = parseSXFile("../data/test/sxparser/files/sx4.c");
			vector<XTag *> tags;
			tag->getTags("data.section",tags);
			if(tags.size() < 2374) {
				throw Exception("searched nodes are don't exist");
			}
			XTag *section = tags[2373];
			unit.addAnnotation("large sx - test name - ");
			unit.assertEquals(section->name,"section");
			unit.addAnnotation("large sx - test tag name - ");
			unit.assertEquals(section->getFirst("name")->getDirectTexts(),"12er");
			unit.addAnnotation("large sx - test tag position - ");
			unit.assertEquals(section->getFirst("position")->getRealAttribute("x"),1337.0);
			unit.addAnnotation("large sx - test tag position - ");
			unit.assertEquals(section->getFirst("position")->getRealAttribute("y"),12321.0);
			unit.addAnnotation("large sx - test tag position - ");
			unit.assertEquals(section->getFirst("position")->getRealAttribute("z"),13.0);
			unit.addAnnotation("large sx - test tag other - ");
			unit.assertEquals(section->getFirst("other")->getRealAttribute("a1"),1.0);
			unit.addAnnotation("large sx - test tag other - ");
			unit.assertEquals(section->getFirst("other")->getRealAttribute("a2"),3.0);
			unit.addAnnotation("large sx - test tag other - ");
			unit.assertEquals(section->getFirst("other")->getRealAttribute("a3"),2.0);
			unit.addAnnotation("large sx - test tag other - ");
			unit.assertEquals(section->getFirst("other")->getRealAttribute("a4"),4.0);

			delete tag;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

	void testParseSXdata(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testParseSX",new ListLogger());
			Logger::setDefaultLogger("testParseSX");

			testParseSXdata(unit);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif