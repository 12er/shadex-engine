#ifndef _TEST_SX_TESTPARSECOLLADADATA_CPP_
#define _TEST_SX_TESTPARSECOLLADADATA_CPP_

/**
 * collada parser testcases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXParser.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <vector>
#include <sstream>
using namespace sx;
using namespace std;

namespace sxparser {

	void testParseColladaData(const string logname) {
		XUnit unit(new FileLogger(logname));

		try {
			Logger::addLogger("testColladaData",new ListLogger());
			Logger::setDefaultLogger("testColladaData");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
			return;
		}

		unit.addMarkup("Parsing nonexistent file - ");

		unit.addAnnotation("read a file, which does not exist - ");

		try {
			parseColladaFile("i don't exist");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addMarkup("Parsing a file, which is not in Collada format - ");

		unit.addAnnotation("open an existing file, which is not a Collada file - ");

		try {
			parseColladaFile("../data/test/sxparser/files/xml4.xml");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addMarkup("Parsing file collada0.dae in Collada format without bones");

		XMesh *mesh = 0;

		unit.addAnnotation("parse file - ");
		try {
			double vertices[] = {
				//triangle
				//0
				7,6,1,
				//1
				13,12,-5,
				//2
				19,18,-11,

				//quad
				//1
				13,12,-5,
				//3
				25,24,-17,
				//4
				31,30,-23,

				//1
				13,12,-5,
				//4
				31,30,-23,
				//2
				19,18,-11,

				//hexagon
				//3
				25,24,-17,
				//4
				31,30,-23,
				//6
				43,42,-35,

				//3
				25,24,-17,
				//6
				43,42,-35,
				//8
				55,54,-47,
				
				//3
				25,24,-17,
				//8
				55,54,-47,
				//7
				49,48,-41,

				//3
				25,24,-17,
				//7
				49,48,-41,
				//5
				37,36,-29,
			};
			double normals[] = {
				//triangle
				//0
				0.8018,0.5345,-0.2673,
				//0
				0.8018,0.5345,-0.2673,
				//0
				0.8018,0.5345,-0.2673,

				//quad
				//0
				0.8018,0.5345,-0.2673,
				//2
				0.6462,0.5744,-0.5026,
				//1
				0.6838,0.5698,-0.4558,

				//0
				0.8018,0.5345,-0.2673,
				//1
				0.6838,0.5698,-0.4558,
				//0
				0.8018,0.5345,-0.2673,

				//hexagon
				//2
				0.6462,0.5744,-0.5026,
				//1
				0.6838,0.5698,-0.4558,
				//2
				0.6462,0.5744,-0.5026,

				//2
				0.6462,0.5744,-0.5026,
				//2
				0.6462,0.5744,-0.5026,
				//4
				0.6175,0.5764,-0.5352,

				//2
				0.6462,0.5744,-0.5026,
				//4
				0.6175,0.5764,-0.5352,
				//3
				0.6281,0.5758,-0.5234,


				//2
				0.6462,0.5744,-0.5026,
				//3
				0.6281,0.5758,-0.5234,
				//2
				0.6462,0.5744,-0.5026,
			};
			double texcoords[] = {
				//triangle
				//0
				1,2,
				//1
				3,4,
				//1
				3,4,

				//quad
				//1
				3,4,
				//1
				3,4,
				//1
				3,4,

				//1
				3,4,
				//1
				3,4,
				//1
				3,4,

				//hexagon
				//1
				3,4,
				//1
				3,4,
				//4
				9,10,

				//1
				3,4,
				//4
				9,10,
				//4
				9,10,

				//1
				3,4,
				//4
				9,10,
				//3
				7,8,

				//1
				3,4,
				//3
				7,8,
				//2
				5,6,

			};
			double other[] = {
				//triangle
				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,

				//quad
				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,

				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,

				//hexagon
				//0
				1.5,1,-0.5,
				//0
				1.5,1,-0.5,
				//1
				3,2.5,-2,

				//0
				1.5,1,-0.5,
				//1
				3,2.5,-2,
				//1
				3,2.5,-2,

				//0
				1.5,1,-0.5,
				//1
				3,2.5,-2,
				//1
				3,2.5,-2,

				//0
				1.5,1,-0.5,
				//1
				3,2.5,-2,
				//1
				3,2.5,-2,
			};
			double mybuffer[] = {
				//triangle
				//0
				0.5392,0.647,0.5392,4,
				//1
				0.575,0.7318,0.3659,8,
				//2
				0.5817,0.7528,0.308,12,

				//quad
				//1
				0.575,0.7318,0.3659,8,
				//1
				0.575,0.7318,0.3659,8,
				//1
				0.575,0.7318,0.3659,8,

				//1
				0.575,0.7318,0.3659,8,
				//1
				0.575,0.7318,0.3659,8,
				//2
				0.5817,0.7528,0.308,12,

				//hexagon
				//1
				0.575,0.7318,0.3659,8,
				//1
				0.575,0.7318,0.3659,8,
				//0
				0.5392,0.647,0.5392,4,

				//1
				0.575,0.7318,0.3659,8,
				//0
				0.5392,0.647,0.5392,4,
				//0
				0.5392,0.647,0.5392,4,

				//1
				0.575,0.7318,0.3659,8,
				//0
				0.5392,0.647,0.5392,4,
				//0
				0.5392,0.647,0.5392,4,

				//1
				0.575,0.7318,0.3659,8,
				//0
				0.5392,0.647,0.5392,4,
				//0
				0.5392,0.647,0.5392,4,
			};

			mesh = parseColladaFile("../data/test/sxparser/files/collada0.dae");
			unit.assertTrue(true);

			unit.addAnnotation("facesize - ");
			unit.assertEquals(mesh->faceSize, (unsigned int)3);

			unit.addAnnotation("has 5 buffers - ");
			unit.assertEquals(mesh->buffers.size(),(unsigned int)5);

			unit.addAnnotation("has buffer vertices - ");
			map<string,XBuffer *>::iterator fVertices = mesh->buffers.find("vertices");
			if(fVertices == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer vertices",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer normals - ");
			map<string,XBuffer *>::iterator fNormals = mesh->buffers.find("normals");
			if(fNormals == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer normals",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer texcoords - ");
			map<string,XBuffer *>::iterator fTexcoords = mesh->buffers.find("texcoords");
			if(fTexcoords == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer texcoords",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer other - ");
			map<string,XBuffer *>::iterator fOther = mesh->buffers.find("other");
			if(fOther == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer other",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer mybuffer - ");
			map<string,XBuffer *>::iterator fMybuffer = mesh->buffers.find("mybuffer");
			if(fMybuffer == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer mybuffer",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("name of vertices - ");
			unit.assertEquals((*fVertices).second->name,"vertices");

			unit.addAnnotation("name of normals - ");
			unit.assertEquals((*fNormals).second->name,"normals");

			unit.addAnnotation("name of texcoords - ");
			unit.assertEquals((*fTexcoords).second->name,"texcoords");

			unit.addAnnotation("name of other - ");
			unit.assertEquals((*fOther).second->name,"other");

			unit.addAnnotation("name of mybuffer - ");
			unit.assertEquals((*fMybuffer).second->name,"mybuffer");

			unit.addAnnotation("attributeSize of vertices - ");
			unit.assertEquals((*fVertices).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of normals - ");
			unit.assertEquals((*fNormals).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of texcoords - ");
			unit.assertEquals((*fTexcoords).second->attributeSize,(unsigned int)2);

			unit.addAnnotation("attributeSize of other - ");
			unit.assertEquals((*fOther).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of mybuffer - ");
			unit.assertEquals((*fMybuffer).second->attributeSize,(unsigned int)4);

			unit.addAnnotation("size of vertices - ");
			unit.assertEquals((*fVertices).second->vertexAttributes.size(),(unsigned int)sizeof(vertices)/sizeof(double));

			unit.addAnnotation("size of normals - ");
			unit.assertEquals((*fNormals).second->vertexAttributes.size(),(unsigned int)sizeof(normals)/sizeof(double));

			unit.addAnnotation("size of texcoords - ");
			unit.assertEquals((*fTexcoords).second->vertexAttributes.size(),(unsigned int)sizeof(texcoords)/sizeof(double));

			unit.addAnnotation("size of other - ");
			unit.assertEquals((*fOther).second->vertexAttributes.size(),(unsigned int)sizeof(other)/sizeof(double));
			
			unit.addAnnotation("size of mybuffer - ");
			unit.assertEquals((*fMybuffer).second->vertexAttributes.size(),(unsigned int)sizeof(mybuffer)/sizeof(double));
		
			for(unsigned int i=0 ; i<(*fVertices).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare vertices at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fVertices).second->vertexAttributes[i],vertices[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fNormals).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare normals at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fNormals).second->vertexAttributes[i],normals[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fTexcoords).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare texcoords at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fTexcoords).second->vertexAttributes[i],texcoords[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fOther).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare other at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fOther).second->vertexAttributes[i],other[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fMybuffer).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare mybuffer at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fMybuffer).second->vertexAttributes[i],mybuffer[i],0.001);
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
		if(mesh != 0) {
			delete mesh;
		}

		unit.addMarkup("Parsing file collada1.dae in Collada format with bones");

		mesh = 0;
		unit.addAnnotation("parse file - ");
		try {
			double vertices[] = {
				//quad 0
				//0
				5,4,3,
				//1
				11,10,-3,
				//3
				23,22,-15,

				//0
				5,4,3,
				//3
				23,22,-15,
				//2
				17,16,-9,

				//quad 1
				//2
				17,16,-9,
				//3
				23,22,-15,
				//4
				29,28,-21,

				//2
				17,16,-9,
				//4
				29,28,-21,
				//5
				35,34,-27,

			};
			double normals[] = {
				//quad 0
				//0
				0.8944,0.4472,0,
				//1
				0.7071,0.5657,-0.4243,
				//2
				0.6554,0.5735,-0.4915,

				//0
				0.8944,0.4472,0,
				//2
				0.6554,0.5735,-0.4915,
				//1
				0.7071,0.5657,-0.4243,

				//quad 1
				//1
				0.7071,0.5657,-0.4243,
				//2
				0.6554,0.5735,-0.4915,
				//0
				0.8944,0.4472,0,

				//1
				0.7071,0.5657,-0.4243,
				//0
				0.8944,0.4472,0,
				//1
				0.7071,0.5657,-0.4243,

			};
			double body[] = {
				//quad 0
				//0
				0.1,
				//1
				1,
				//1
				1,

				//0
				0.1,
				//1
				1,
				//not set
				0,

				//quad 1
				//not set
				0,
				//1
				1,
				//3
				3,

				//not set
				0,
				//3
				3,
				//2
				2,

			};
			double arm_right[] = {
				//quad 0
				//0
				0.1,
				//1
				1,
				//3
				3,

				//0
				0.1,
				//3
				3,
				//2
				2,

				//quad 1
				//2
				2,
				//3
				3,
				//5
				5,

				//2
				2,
				//5
				5,
				//4
				4,

			};

			mesh = parseColladaFile("../data/test/sxparser/files/collada1.dae");
			unit.assertTrue(true);

			unit.addAnnotation("facesize - ");
			unit.assertEquals(mesh->faceSize, (unsigned int)3);

			unit.addAnnotation("has 5 buffers - ");
			unit.assertEquals(mesh->buffers.size(),(unsigned int)5);

			unit.addAnnotation("has buffer vertices - ");
			map<string,XBuffer *>::iterator fVertices = mesh->buffers.find("vertices");
			if(fVertices == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer vertices",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer normals - ");
			map<string,XBuffer *>::iterator fNormals = mesh->buffers.find("normals");
			if(fNormals == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer normals",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer body - ");
			map<string,XBuffer *>::iterator fBody = mesh->buffers.find("body");
			if(fBody == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer body",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer arm_left - ");
			map<string,XBuffer *>::iterator fArm_left = mesh->buffers.find("arm_left");
			if(fArm_left == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer arm_left",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer arm_right - ");
			map<string,XBuffer *>::iterator fArm_right = mesh->buffers.find("arm_right");
			if(fArm_right == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer arm_right",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("name of vertices - ");
			unit.assertEquals((*fVertices).second->name,"vertices");

			unit.addAnnotation("name of normals - ");
			unit.assertEquals((*fNormals).second->name,"normals");

			unit.addAnnotation("name of body - ");
			unit.assertEquals((*fBody).second->name,"body");

			unit.addAnnotation("name of arm_left - ");
			unit.assertEquals((*fArm_left).second->name,"arm_left");

			unit.addAnnotation("name of arm_right - ");
			unit.assertEquals((*fArm_right).second->name,"arm_right");

			unit.addAnnotation("attributeSize of vertices - ");
			unit.assertEquals((*fVertices).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of normals - ");
			unit.assertEquals((*fNormals).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of body - ");
			unit.assertEquals((*fBody).second->attributeSize,(unsigned int)1);

			unit.addAnnotation("attributeSize of arm_left - ");
			unit.assertEquals((*fArm_left).second->attributeSize,(unsigned int)1);

			unit.addAnnotation("attributeSize of arm_right - ");
			unit.assertEquals((*fArm_right).second->attributeSize,(unsigned int)1);


			unit.addAnnotation("size of vertices - ");
			unit.assertEquals((*fVertices).second->vertexAttributes.size(),(unsigned int)sizeof(vertices)/sizeof(double));

			unit.addAnnotation("size of normals - ");
			unit.assertEquals((*fNormals).second->vertexAttributes.size(),(unsigned int)sizeof(normals)/sizeof(double));

			unit.addAnnotation("size of body - ");
			unit.assertEquals((*fBody).second->vertexAttributes.size(),(unsigned int)sizeof(body)/sizeof(double));

			unit.addAnnotation("size of arm_left - ");
			unit.assertEquals((*fArm_left).second->vertexAttributes.size(),(unsigned int)sizeof(body)/sizeof(double));

			unit.addAnnotation("size of arm_right - ");
			unit.assertEquals((*fArm_right).second->vertexAttributes.size(),(unsigned int)sizeof(arm_right)/sizeof(double));

			for(unsigned int i=0 ; i<(*fVertices).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare vertices at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fVertices).second->vertexAttributes[i],vertices[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fNormals).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare normals at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fNormals).second->vertexAttributes[i],normals[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fBody).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare body at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fBody).second->vertexAttributes[i],body[i],0.001);
			}

			for(unsigned int i=0 ; i<(*fArm_left).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare arm_left at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fArm_left).second->vertexAttributes[i],0.0);
			}

			for(unsigned int i=0 ; i<(*fArm_right).second->vertexAttributes.size() ; i++) {
				stringstream str;
				str << "compare arm_right at index " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals((*fArm_right).second->vertexAttributes[i],arm_right[i],0.001);
			}

		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
		if(mesh != 0) {
			delete mesh;
		}

		unit.addMarkup("Parsing file collada2.dae in Collada format without bones");

		mesh = 0;
		unit.addAnnotation("parse file - ");
		try {
			mesh = parseColladaFile("../data/test/sxparser/files/collada2.dae");
			unit.assertTrue(true);

			unit.addAnnotation("facesize - ");
			unit.assertEquals(mesh->faceSize, (unsigned int)3);

			unit.addAnnotation("has 3 buffers - ");
			unit.assertEquals(mesh->buffers.size(),(unsigned int)3);

			unit.addAnnotation("has buffer vertices - ");
			map<string,XBuffer *>::iterator fVertices = mesh->buffers.find("vertices");
			if(fVertices == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer vertices",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer normals - ");
			map<string,XBuffer *>::iterator fNormals = mesh->buffers.find("normals");
			if(fNormals == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer normals",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer texcoords - ");
			map<string,XBuffer *>::iterator fTexcoords = mesh->buffers.find("texcoords");
			if(fTexcoords == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer texcoords",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("name of vertices - ");
			unit.assertEquals((*fVertices).second->name,"vertices");

			unit.addAnnotation("name of normals - ");
			unit.assertEquals((*fNormals).second->name,"normals");

			unit.addAnnotation("name of texcoords - ");
			unit.assertEquals((*fTexcoords).second->name,"texcoords");

			unit.addAnnotation("attributeSize of vertices - ");
			unit.assertEquals((*fVertices).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of normals - ");
			unit.assertEquals((*fNormals).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of texcoords - ");
			unit.assertEquals((*fTexcoords).second->attributeSize,(unsigned int)2);

			unit.addAnnotation("size of vertices - ");
			unit.assertEquals((*fVertices).second->vertexAttributes.size(),(unsigned int)194760);

			unit.addAnnotation("size of normals - ");
			unit.assertEquals((*fNormals).second->vertexAttributes.size(),(unsigned int)194760);

			unit.addAnnotation("size of texcoords - ");
			unit.assertEquals((*fTexcoords).second->vertexAttributes.size(),(unsigned int)129840);

			vector<double> &vVertices = (*fVertices).second->vertexAttributes;
			vector<double> &vNormals = (*fNormals).second->vertexAttributes;
			vector<double> &vTexcoords = (*fTexcoords).second->vertexAttributes;

			Vector v1(0.09445f,-11.1467f,-0.281136f);
			Vector v1_b((float)vVertices[21663],(float)vVertices[21664],(float)vVertices[21665]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test first vertex of face at index 1337 - ");
				unit.assertEquals(v1_b[i], v1[i], 0.001f);
			}

			Vector v2(0.140123f,-10.8528f,-0.386951f);
			Vector v2_b((float)vVertices[21669],(float)vVertices[21670],(float)vVertices[21671]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test third vertex of face at index 1337 - ");
				unit.assertEquals(v2_b[i], v2[i], 0.001f);
			}

			Vector v3_b((float)vVertices[21672],(float)vVertices[21673],(float)vVertices[21674]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test third vertex of face at index 1337 - ");
				unit.assertEquals(v3_b[i], v1[i], 0.001f);
			}

			Vector v4_b((float)vVertices[21675],(float)vVertices[21676],(float)vVertices[21677]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test fourth vertex of face at index 1337 - ");
				unit.assertEquals(v4_b[i], v2[i], 0.001f);
			}

			Vector n1(0.348826f,-0.522721f,-0.777856f);
			Vector n1_b((float)vNormals[21663],(float)vNormals[21664],(float)vNormals[21665]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test first normal of face at index 1337 - ");
				unit.assertEquals(n1_b[i],n1[i],0.001f);
			}

			Vector t1(0.948417f,0.991532f);
			Vector t1_b((float)vTexcoords[14442],(float)vTexcoords[14443]);
			for(unsigned int i=0 ; i<2 ; i++) {
				unit.addAnnotation("test first texcoord of face at index 1337 - ");
				unit.assertEquals(t1_b[i],t1[i],0.001f);
			}

		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
		if(mesh != 0) {
			delete mesh;
		}

		unit.addMarkup("Parsing file collada3.dae in Collada format with bones");

		mesh = 0;
		unit.addAnnotation("parse file - ");
		try {
			mesh = parseColladaFile("../data/test/sxparser/files/collada3.dae");
			unit.assertTrue(true);

			unit.addAnnotation("facesize - ");
			unit.assertEquals(mesh->faceSize, (unsigned int)3);

			unit.addAnnotation("has 16 buffers - ");
			unit.assertEquals(mesh->buffers.size(),(unsigned int)16);

			unit.addAnnotation("has buffer vertices - ");
			map<string,XBuffer *>::iterator fVertices = mesh->buffers.find("vertices");
			if(fVertices == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer vertices",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer normals - ");
			map<string,XBuffer *>::iterator fNormals = mesh->buffers.find("normals");
			if(fNormals == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer normals",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("has buffer shoulder_left - ");
			map<string,XBuffer *>::iterator fShoulder_left = mesh->buffers.find("shoulder_left");
			if(fShoulder_left == mesh->buffers.end()) {
				throw Exception("Error: does not contain buffer shoulder_left",EX_SYNTAX);
			}
			unit.assertTrue(true);

			unit.addAnnotation("name of vertices - ");
			unit.assertEquals((*fVertices).second->name,"vertices");

			unit.addAnnotation("name of normals - ");
			unit.assertEquals((*fNormals).second->name,"normals");

			unit.addAnnotation("name of shoulder_left - ");
			unit.assertEquals((*fShoulder_left).second->name,"shoulder_left");

			unit.addAnnotation("attributeSize of vertices - ");
			unit.assertEquals((*fVertices).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("attributeSize of normals - ");
			unit.assertEquals((*fNormals).second->attributeSize,(unsigned int)3);

			unit.addAnnotation("upperarm_right of shoulder_left - ");
			unit.assertEquals((*fShoulder_left).second->attributeSize,(unsigned int)1);

			unit.addAnnotation("size of vertices - ");
			unit.assertEquals((*fVertices).second->vertexAttributes.size(),(unsigned int)9000);

			unit.addAnnotation("size of normals - ");
			unit.assertEquals((*fNormals).second->vertexAttributes.size(),(unsigned int)9000);

			unit.addAnnotation("size of shoulder_left - ");
			unit.assertEquals((*fShoulder_left).second->vertexAttributes.size(),(unsigned int)3000);

			vector<double> &vVertices = (*fVertices).second->vertexAttributes;
			vector<double> &vNormals = (*fNormals).second->vertexAttributes;
			vector<double> &vShoulder_left = (*fShoulder_left).second->vertexAttributes;

			Vector v1(-0.223942f,1.07768f,1.68456f);
			Vector v1_p((float)vVertices[5886],(float)vVertices[5887],(float)vVertices[5888]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test first vertex of face at index 654 - ");
				unit.assertEquals(v1_p[i],v1[i],0.001f);
			}

			Vector n1(-0.204505f,0.975249f,0.0838954f);
			Vector n1_p((float)vNormals[5886],(float)vNormals[5887],(float)vNormals[5888]);
			for(unsigned int i=0 ; i<3 ; i++) {
				unit.addAnnotation("test first normal of face at index 654 - ");
				unit.assertEquals(n1_p[i],n1[i],0.001f);
			}

			double w1 = 0.0432156;
			double w1_p = vShoulder_left[1962];
			unit.addAnnotation("test weight of bone shoulder_left of the first vertex of face at index 654 - ");
			unit.assertEquals(w1,w1_p,0.000001);


		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
		if(mesh != 0) {
			delete mesh;
		}

	}

}

#endif