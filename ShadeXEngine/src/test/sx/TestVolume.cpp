#ifndef _TEST_SX_TESTVOLUME_CPP_
#define _TEST_SX_TESTVOLUME_CPP_

/**
 * 3d texture test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <vector>
#include <cmath>
#include <boost/date_time/posix_time/posix_time.hpp>
using namespace sx;
using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::posix_time::to_simple_string;
using namespace std;

namespace sxengine {

	class TestVolume1: public SXRenderListener {
	private:
		XUnit *unit;
		ListLogger *logger;
		float time;

		BufferedMesh *mesh;
		Shader *project;
		Shader *present;
		UniformVector *weight;
		UniformFloat *presentweight;
		Texture *t1;
		Texture *t2;
		Texture *t3;
		Texture *targettex;
		Volume *targetvolume;
		Volume *targetvolume2;
		RenderTarget *target;

		TestVolume1(const TestVolume1 &);
		TestVolume1 &operator = (const TestVolume1 &);
	public:

		TestVolume1(XUnit &unit,ListLogger &logger) {
			this->unit = &unit;
			this->logger = &logger;
			time = 0;
		}

		~TestVolume1() {
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("store files");

			float pi4 = ((float)Pi)/4.0f;
			float e4 = 2.71828182845f/4.0f;
			float g4 = 1.6180339887f/4.0f;
			float texArray[] = {
			//layer0
				pi4,e4,g4,pi4,
				g4,g4,g4,e4,
				pi4,pi4,e4,e4,
				pi4,pi4,pi4,pi4,
			//layer1
				g4,e4,e4,e4,
				e4,g4,pi4,e4,
				e4,g4,g4,e4,
				g4,g4,g4,g4,
			//layer2
				e4,pi4,pi4,g4,
				e4,g4,g4,e4,
				pi4,g4,e4,g4,
				g4,g4,e4,e4
			};
			vector<float> texBuffer(texArray,texArray+48);
			Volume *wv1 = new Volume("v");
			wv1->setWidth(2);
			wv1->setHeight(2);
			wv1->setDepth(3);
			wv1->setFloatBuffer(texBuffer);
			wv1->load();
			unit->addAnnotation("load write-volume1 - ");
			unit->assertTrue(wv1->isLoaded());
			unit->addAnnotation("write-volume1 pixelformat - ");
			unit->assertEquals((int)wv1->getPixelFormat(),(int)FLOAT_RGBA);

			unit->addAnnotation("save write-volume1 to files - ");
			try {
				wv1->save("../data/test/sxengine/files/output/wv1_layer.png");
				unit->assertTrue(true);
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			Volume *wv2 = new Volume("v");
			wv2->addPath("../data/test/sxengine/files/output/wv1_layer0.png");
			wv2->addPath("../data/test/sxengine/files/output/wv1_layer1.png");
			wv2->addPath("../data/test/sxengine/files/output/wv1_layer2.png");
			wv2->load();
			unit->addAnnotation("load write-volume2 - ");
			unit->assertTrue(wv2->isLoaded());
			unit->addAnnotation("write-volume2 pixelformat - ");
			unit->assertEquals((int)wv2->getPixelFormat(),(int)FLOAT16_RGBA);

			unit->addAnnotation("save write-volume2 to files - ");
			try {
				wv2->save("../data/test/sxengine/files/output/wv2_layer.png");
				unit->assertTrue(true);
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			delete wv1;
			delete wv2;

			unit->addAnnotation("load written files for comparison - ");
			try {
				float layer0[] = {
					pi4,e4,g4,pi4,
					g4,g4,g4,e4,
					pi4,pi4,e4,e4,
					pi4,pi4,pi4,pi4
				};
				float layer1[] = {
					g4,e4,e4,e4,
					e4,g4,pi4,e4,
					e4,g4,g4,e4,
					g4,g4,g4,g4
				};
				float layer2[] = {
					e4,pi4,pi4,g4,
					e4,g4,g4,e4,
					pi4,g4,e4,g4,
					g4,g4,e4,e4
				};

				Bitmap m1l0("../data/test/sxengine/files/output/wv1_layer0.png");
				Bitmap m1l1("../data/test/sxengine/files/output/wv1_layer1.png");
				Bitmap m1l2("../data/test/sxengine/files/output/wv1_layer2.png");
				Bitmap m2l0("../data/test/sxengine/files/output/wv2_layer0.png");
				Bitmap m2l1("../data/test/sxengine/files/output/wv2_layer1.png");
				Bitmap m2l2("../data/test/sxengine/files/output/wv2_layer2.png");
				unit->assertTrue(true);

				float *d1 = (float *)m1l0.getData();
				float *d2 = (float *)m1l1.getData();
				float *d3 = (float *)m1l2.getData();
				float *d4 = (float *)m2l0.getData();
				float *d5 = (float *)m2l1.getData();
				float *d6 = (float *)m2l2.getData();

				for(unsigned int i=0 ; i<2*2*4 ; i++) {
					stringstream s1;
					stringstream s2;
					stringstream s3;
					stringstream s4;
					stringstream s5;
					stringstream s6;
					s1 << "wv1 layer0 color " << i << " - ";
					unit->addAnnotation(s1.str());
					unit->assertEquals(d1[i],layer0[i],0.00001f);
					s2 << "wv1 layer1 color " << i << " - ";
					unit->addAnnotation(s2.str());
					unit->assertEquals(d2[i],layer1[i],0.00001f);
					s3 << "wv1 layer2 color " << i << " - ";
					unit->addAnnotation(s3.str());
					unit->assertEquals(d3[i],layer2[i],0.00001f);
					s4 << "wv2 layer0 color " << i << " - ";
					unit->addAnnotation(s4.str());
					unit->assertEquals(d4[i],layer0[i],0.0003f);
					s5 << "wv2 layer1 color " << i << " - ";
					unit->addAnnotation(s5.str());
					unit->assertEquals(d5[i],layer1[i],0.0003f);
					s6 << "wv2 layer2 color " << i << " - ";
					unit->addAnnotation(s6.str());
					unit->assertEquals(d6[i],layer2[i],0.0003f);
				}
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			unit->addMarkup("loading phase");

			float meshVArray[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList(meshVArray,meshVArray + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices", meshVList, 4);
			mesh->addBuffer("texcoords",meshTList, 2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("load vertices - ");
			unit->assertTrue(mesh->isLoaded());

			project = new Shader("project");
			present = new Shader("present");
			project->addShaderFile("../data/test/sxengine/files/volumeproject.vp",VERTEX);
			project->addShaderFile("../data/test/sxengine/files/volumeproject.fp",FRAGMENT);
			present->addShaderFile("../data/test/sxengine/files/present.vp",VERTEX);
			present->addShaderFile("../data/test/sxengine/files/present.fp",FRAGMENT);
			project->load();
			present->load();
			unit->addAnnotation("load project shader - ");
			unit->assertTrue(project->isLoaded());
			unit->addAnnotation("load present shader - ");
			unit->assertTrue(present->isLoaded());

			weight = new UniformVector("weight");
			presentweight = new UniformFloat("presentweight");

			t1 = new Texture("v1");
			t2 = new Texture("v2");
			t3 = new Texture("v3");
			targettex = new Texture("target1");
			t1->setPath("../data/test/sxengine/files/v1.jpg");
			t2->setPath("../data/test/sxengine/files/v2.jpg");
			t3->setPath("../data/test/sxengine/files/v3.jpg");
			targettex->setWidth(3);
			targettex->setHeight(3);
			t1->load();
			t2->load();
			t3->load();
			targettex->load();
			unit->addAnnotation("load texture 1: success - ");
			unit->assertTrue(t1->isLoaded());
			unit->addAnnotation("load texture 2: success - ");
			unit->assertTrue(t2->isLoaded());
			unit->addAnnotation("load texture 3: success - ");
			unit->assertTrue(t3->isLoaded());
			unit->addAnnotation("load targettex: success - ");
			unit->assertTrue(targettex->isLoaded());

			targetvolume = new Volume("target1");
			targetvolume2 = new Volume("t2");
			targetvolume->setWidth(3);
			targetvolume->setHeight(3);
			targetvolume->setDepth(7);
			targetvolume->setPixelFormat(BYTE_RGBA);
			targetvolume2->setWidth(3);
			targetvolume2->setHeight(3);
			targetvolume2->setDepth(7);
			targetvolume2->setPixelFormat(FLOAT_RGBA);
			targetvolume->load();
			targetvolume2->load();
			unit->addAnnotation("load targetvolume: success - ");
			unit->assertTrue(targetvolume->isLoaded());
			unit->addAnnotation("volume1 has pixelformat byte - ");
			unit->assertEquals((int)targetvolume->getPixelFormat(),(int)BYTE_RGBA);
			unit->addAnnotation("load targetvolume2: success - ");
			unit->assertTrue(targetvolume2->isLoaded());
			unit->addAnnotation("volume2 has pixelformat float - ");
			unit->assertEquals((int)targetvolume2->getPixelFormat(),(int)FLOAT_RGBA);
			
			targetvolume2->setUniformName("target2","resource");
			targetvolume->setUniformName("volume1","present");
			targetvolume2->setUniformName("volume2","present");

			target = new RenderTarget("bla");
			target->setWidth(3);
			target->setHeight(3);
			target->load();
			unit->addAnnotation("load successfully - ");
			unit->assertTrue(target->isLoaded());
		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
			time += area.getDeltaTime();
			if(time > 9) {
				area.stopRendering();
			}

			(*weight) = Vector(1.0f,0,0);
			//presentweight determines which volume
			//or texture is shown on the display
			if(time < 6.0f) {
				//presentweight makes the presentprogram faids for the
				//first six seconds from targetvolume1 to targetvolume2
				(*presentweight) << max(0.0f,min(1.0f,time - 3.0f));
			} else if(time < 7.0f) {
				//presentweight makes the presentprogram show texture v1
				//from second 6 to second 7
				(*presentweight) << 2.0f;
			} else if(time < 8.0f) {
				//presentweight makes the presentprogram show texture v2
				//from second 7 to second 8
				(*presentweight) << 3.0f;
			} else {
				//presentweight makes the presentprogram show texture v3
				//from second 7 to second 9
				(*presentweight) << 4.0f;
			}

			project->use();
			weight->use(*project,"resource");
			t1->use(*project,"resource");
			t2->use(*project,"resource");
			t3->use(*project,"resource");

			//render to first layer
			vector<Volume *> volumes;
			volumes.push_back(targetvolume2);
			volumes.push_back(targetvolume);
			target->beginRender(*project,volumes,0,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render to second layer
			(*weight) = Vector(0,1.0f,0);
			weight->use(*project,"resource");

			target->beginRender(*project,volumes,1,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render to third layer
			(*weight) = Vector(0,0,1.0f);
			weight->use(*project,"resource");

			target->beginRender(*project,volumes,2,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render to fourth layer
			(*weight) = Vector(0.5f,0,0.5f);
			weight->use(*project,"resource");

			target->beginRender(*project,volumes,3,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render to fifth layer
			(*weight) = Vector(0.5f,0.5f,0);
			weight->use(*project,"resource");

			target->beginRender(*project,volumes,4,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render to sixth layer
			(*weight) = Vector(0,0.5f,0.5f);
			weight->use(*project,"resource");

			target->beginRender(*project,volumes,5,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render to sixth layer
			(*weight) = Vector(0.33f,0.33f,0.33f);
			weight->use(*project,"resource");

			target->beginRender(*project,volumes,6,"resource");
			target->setViewport();
			RenderTarget::clearTarget();

			mesh->render(*project);

			target->endRender();

			//render volumes and textures
			present->use();
			presentweight->use(*present,"present");
			t2->use(*present,"present");
			targetvolume->use(*present,"present");
			t1->use(*present,"present");
			targetvolume2->use(*present,"present");
			t3->use(*present,"present");

			RenderTarget::setViewport(0,0,area.getWidth(),area.getHeight());
			RenderTarget::clearTarget();

			mesh->render(*present);

		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete mesh;
			delete project;
			delete present;
			delete weight;
			delete presentweight;
			delete t1;
			delete t2;
			delete t3;
			delete targettex;
			delete targetvolume;
			delete targetvolume2;
			delete target;
		}

	};

	class TestVolume2: public SXRenderListener {
	private:
		XUnit *unit;
		ListLogger *logger;
		float time;

		UniformFloat *timevar;
		BufferedMesh *mesh;
		RenderObject *obj1;
		UniformFloat *lcoordinate;
		Texture *tex1;
		Texture *tex2;
		Texture *tex3;

		Volume *vol1;
		Volume *vol2;
		Shader *pass1shader;
		RenderTarget *fbo1;
		Pass *pass1;

		Volume *vol3;
		Volume *vol4;
		Shader *pass2shader;
		UniformFloat *startLayer;
		UniformFloat *endLayer;
		UniformFloat *useTextures;
		Pass *pass2;

		Texture *tex4;
		Texture *tex5;
		Texture *tex6;
		RenderObject *obj2;
		RenderObject *obj3;
		RenderObject *obj4;
		UniformMatrix *transform1;
		UniformMatrix *transform2;
		UniformMatrix *transform3;
		Shader *pass3shader;
		Shader *obj3shader;
		Pass *pass3;

		Volume *filevolume;
		
		Shader *screenshader;
		RenderTarget *screen;
		Pass *screenpass;

		TestVolume2(const TestVolume2 &);
		TestVolume2 &operator = (const TestVolume2 &);
	public:
		TestVolume2(XUnit &unit,ListLogger &logger) {
			this->unit = &unit;
			this->logger = &logger;
			time = 0;
		}

		~TestVolume2() {
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("loading phase");

			timevar = new UniformFloat("time");
			timevar->load();
			unit->addAnnotation("load UniformFloat time - ");
			unit->assertTrue(timevar->isLoaded());

			float meshVArray[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList(meshVArray,meshVArray + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices", meshVList, 4);
			mesh->addBuffer("texcoords",meshTList, 2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("load vertices - ");
			unit->assertTrue(mesh->isLoaded());

			obj1 = new RenderObject("obj1");
			obj1->setMesh(*mesh);
			obj1->load();
			unit->addAnnotation("load obj1 - ");
			unit->assertTrue(obj1->isLoaded());

			tex1 = new Texture("tex1");
			tex2 = new Texture("tex2");
			tex3 = new Texture("tex3");
			tex1->setPath("../data/test/sxengine/files/ShadeXEngine.jpg");
			tex2->setPath("../data/test/sxengine/files/background.jpg");
			tex3->setPath("../data/test/sxengine/files/v1.jpg");
			tex1->load();
			tex2->load();
			tex3->load();
			unit->addAnnotation("load texture 1 - ");
			unit->assertTrue(tex1->isLoaded());
			unit->addAnnotation("load texture 2 - ");
			unit->assertTrue(tex2->isLoaded());
			unit->addAnnotation("load texture 3 - ");
			unit->assertTrue(tex3->isLoaded());

			lcoordinate = new UniformFloat("layercoordinate");
			lcoordinate->load();
			unit->addAnnotation("load layercoordinate - ");
			unit->assertTrue(lcoordinate->isLoaded());

			//objects of pass 1
			//pass 1 tests the layercoordinate in action

			vol1 = new Volume("vol1");
			vol2 = new Volume("v2");
			vol1->setWidth(500);
			vol1->setHeight(300);
			vol1->setDepth(3);
			vol1->setPixelFormat(BYTE_RGBA);
			vol2->setWidth(500);
			vol2->setHeight(300);
			vol2->setDepth(3);
			vol2->setPixelFormat(FLOAT_RGBA);
			vol1->load();
			vol2->load();
			unit->addAnnotation("load volume 1 - ");
			unit->assertTrue(vol1->isLoaded());
			unit->addAnnotation("load volume 2 - ");
			unit->assertTrue(vol2->isLoaded());
			vol2->setUniformName("vol2","pass1");

			pass1shader = new Shader("pass1shader");
			pass1shader->addShaderFile("../data/test/sxengine/files/pass1shader.vp",VERTEX);
			pass1shader->addShaderFile("../data/test/sxengine/files/pass1shader.fp",FRAGMENT);
			pass1shader->load();
			unit->addAnnotation("load pass1shader - ");
			unit->assertTrue(pass1shader->isLoaded());

			fbo1 = new RenderTarget("fbo1");
			fbo1->setWidth(500);
			fbo1->setHeight(300);
			fbo1->load();
			unit->addAnnotation("load fbo1 - ");
			unit->assertTrue(fbo1->isLoaded());

			vector<Volume *> pass1volumes;
			pass1volumes.push_back(vol1);
			pass1volumes.push_back(vol2);
			pass1 = new Pass("pass1");
			pass1->setShader(pass1shader);
			pass1->addUniform(*tex1);
			pass1->addUniform(*tex2);
			pass1->addUniform(*tex3);
			pass1->addRenderObject(*obj1);
			pass1->setLayerCoordinate(lcoordinate);
			pass1->setOutput(*fbo1,pass1volumes);
			pass1->load();
			unit->addAnnotation("load pass1 - ");
			unit->assertTrue(pass1->isLoaded());

			//objects of pass2
			//pass 2 tests rendering into a fraction of the layers

			lcoordinate->setUniformName("lcoord","pass2");

			vol3 = new Volume("vol3");
			vol4 = new Volume("vol4");
			vol3->setWidth(500);
			vol3->setHeight(300);
			vol3->setDepth(6);
			vol4->setWidth(500);
			vol4->setHeight(300);
			vol4->setDepth(6);
			vol3->load();
			vol4->load();
			unit->addAnnotation("load vol3 - ");
			unit->assertTrue(vol3->isLoaded());
			unit->addAnnotation("load vol4 - ");
			unit->assertTrue(vol4->isLoaded());

			pass2shader = new Shader("pass2shader");
			pass2shader->addShaderFile("../data/test/sxengine/files/pass2shader.vp",VERTEX);
			pass2shader->addShaderFile("../data/test/sxengine/files/pass2shader.fp",FRAGMENT);
			pass2shader->load();
			unit->addAnnotation("load pass2shader - ");
			unit->assertTrue(pass2shader->isLoaded());

			startLayer = new UniformFloat("startLayer");
			startLayer->load();
			unit->addAnnotation("load UniformFloat startLayer - ");
			unit->assertTrue(startLayer->isLoaded());

			endLayer = new UniformFloat("endLayer");
			endLayer->load();
			unit->addAnnotation("load UniformFloat endLayer - ");
			unit->assertTrue(startLayer->isLoaded());

			useTextures = new UniformFloat("useTextures");
			useTextures->load();
			unit->addAnnotation("load UniformFloat useTextures - ");
			unit->assertTrue(useTextures->isLoaded());

			vector<Volume *> pass2volumes;
			pass2volumes.push_back(vol3);
			pass2volumes.push_back(vol4);
			pass2 = new Pass("pass2");
			pass2->setShader(pass2shader);
			pass2->addUniform(*tex1);
			pass2->addUniform(*tex2);
			pass2->addUniform(*tex3);
			pass2->addUniform(*useTextures);
			pass2->addRenderObject(*obj1);
			pass2->setLayerCoordinate(lcoordinate);
			pass2->setStartLayer(startLayer);
			pass2->setEndLayer(endLayer);
			pass2->setOutput(*fbo1,pass2volumes);
			pass2->load();
			unit->addAnnotation("load pass2 - ");
			unit->assertTrue(pass2->isLoaded());

			//objects of pass3

			lcoordinate->setUniformName("lcoord","pass3");

			tex4 = new Texture("tex4");
			tex5 = new Texture("t5");
			tex6 = new Texture("tex6");
			tex4->setPath("../data/test/sxengine/files/tex4.jpg");
			tex5->setPath("../data/test/sxengine/files/tex5.jpg");
			tex6->setPath("../data/test/sxengine/files/tex6.jpg");
			tex4->load();
			tex5->load();
			tex6->load();
			unit->addAnnotation("load tex4 - ");
			unit->assertTrue(tex4->isLoaded());
			unit->addAnnotation("load tex5 - ");
			unit->assertTrue(tex5->isLoaded());
			unit->addAnnotation("load tex6 - ");
			unit->assertTrue(tex6->isLoaded());
			tex4->setUniformName("tex1","obj4");
			tex5->setUniformName("tex2","obj4");
			tex6->setUniformName("tex3","obj4");
			tex5->setUniformName("tex5","pass3");
			tex5->setUniformName("tex5","obj3");

			pass3shader = new Shader("pass3shader");
			obj3shader = new Shader("obj3shader");
			pass3shader->addShaderFile("../data/test/sxengine/files/pass3shader.vp",VERTEX);
			pass3shader->addShaderFile("../data/test/sxengine/files/pass3shader.fp",FRAGMENT);
			obj3shader->addShaderFile("../data/test/sxengine/files/obj3shader.vp",VERTEX);
			obj3shader->addShaderFile("../data/test/sxengine/files/obj3shader.fp",FRAGMENT);
			pass3shader->load();
			obj3shader->load();
			unit->addAnnotation("load pass3shader - ");
			unit->assertTrue(pass3shader->isLoaded());
			unit->addAnnotation("load obj3shader - ");
			unit->assertTrue(obj3shader->isLoaded());

			transform1 = new UniformMatrix("transform1");
			transform2 = new UniformMatrix("transform2");
			transform3 = new UniformMatrix("transform3");
			transform1->load();
			transform2->load();
			transform3->load();
			unit->addAnnotation("load transform1 - ");
			unit->assertTrue(transform1->isLoaded());
			unit->addAnnotation("load transform2 - ");
			unit->assertTrue(transform2->isLoaded());
			unit->addAnnotation("load transform3 - ");
			unit->assertTrue(transform3->isLoaded());
			(*transform1) = Matrix().translate(Vector(-0.666f,0,0)) * Matrix().scale(Vector(0.3f,0.8f,1));
			(*transform2) = Matrix().scale(Vector(0.3f,0.8f,1));
			(*transform3) = Matrix().translate(Vector(0.666f,0,0)) * Matrix().scale(Vector(0.3f,0.8f,1));
			transform1->setUniformName("transform","obj2");
			transform2->setUniformName("transform","obj3");
			transform3->setUniformName("transform","obj4");

			obj2 = new RenderObject("obj2");
			obj3 = new RenderObject("obj3");
			obj4 = new RenderObject("obj4");
			obj2->setMesh(*mesh);
			obj2->addUniform(*transform1);
			obj3->setShader(obj3shader);
			obj3->setMesh(*mesh);
			obj3->addUniform(*transform2);
			obj3->addUniform(*tex4);
			obj3->addUniform(*tex5);
			obj3->addUniform(*tex6);
			obj4->setMesh(*mesh);
			obj4->addUniform(*transform3);
			obj2->load();
			obj3->load();
			obj4->load();
			unit->addAnnotation("load obj2 - ");
			unit->assertTrue(obj2->isLoaded());
			unit->addAnnotation("load obj3 - ");
			unit->assertTrue(obj3->isLoaded());
			unit->addAnnotation("load obj4 - ");
			unit->assertTrue(obj4->isLoaded());

			pass3 = new Pass("pass3");
			pass3->setShader(pass3shader);
			pass3->addRenderObject(*obj2);
			pass3->addRenderObject(*obj3);
			pass3->addRenderObject(*obj4);
			pass3->addUniform(*tex1);
			pass3->addUniform(*tex2);
			pass3->addUniform(*tex3);
			pass3->setLayerCoordinate(lcoordinate);
			pass3->setOutput(*fbo1,pass2volumes);
			pass3->load();
			unit->addAnnotation("load pass3 - ");
			unit->assertTrue(pass3->isLoaded());

			//load volume from files
			filevolume = new Volume("vol4");
			filevolume->load();
			unit->addAnnotation("not able to load filevolume without any specs - ");
			unit->assertTrue(!filevolume->isLoaded());
			delete filevolume;

			//first path not supposed to be loadable
			filevolume = new Volume("vol4");
			filevolume->addPath("../data/test/sxengine/files/screenshader.vp.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex4.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex5.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex6.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex4.jpg");
			filevolume->load();
			unit->addAnnotation("not able to load filevolume with an invalid path - ");
			unit->assertTrue(!filevolume->isLoaded());
			delete filevolume;

			//fourth path not supposed to be loadable
			filevolume = new Volume("vol4");
			filevolume->addPath("../data/test/sxengine/files/tex4.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex5.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex6.jpg");
			filevolume->addPath("../data/test/sxengine/files/screenshader.vp.jpg");
			filevolume->addPath("../data/test/sxengine/files/tex4.jpg");
			filevolume->load();
			unit->addAnnotation("not able to load filevolume with an invalid path - ");
			unit->assertTrue(!filevolume->isLoaded());
			delete filevolume;

			vector<string> paths;
			paths.push_back("../data/test/sxengine/files/vol1.jpg");
			paths.push_back("../data/test/sxengine/files/vol2.jpg");
			paths.push_back("../data/test/sxengine/files/vol3.jpg");
			filevolume = new Volume("vol4");
			filevolume->setPaths(paths);
			filevolume->addPath("../data/test/sxengine/files/vol4.jpg");
			filevolume->addPath("../data/test/sxengine/files/vol5.jpg");
			filevolume->addPath("../data/test/sxengine/files/vol6.jpg");
			filevolume->load();
			unit->addAnnotation("load filevolume - ");
			unit->assertTrue(filevolume->isLoaded());
			unit->addAnnotation("filevolume width - ");
			unit->assertEquals((int)filevolume->getWidth(),(int)170);
			unit->addAnnotation("filevolume height - ");
			unit->assertEquals((int)filevolume->getHeight(),(int)150);
			unit->addAnnotation("filevolume depth - ");
			unit->assertEquals((int)filevolume->getDepth(),(int)6);

			//objects of pass screenpass

			screenshader = new Shader("screenshader");
			screenshader->addShaderFile("../data/test/sxengine/files/screenshader.vp",VERTEX);
			screenshader->addShaderFile("../data/test/sxengine/files/screenshader.fp",FRAGMENT);
			screenshader->load();
			unit->addAnnotation("load screenshader - ");
			unit->assertTrue(screenshader->isLoaded());

			screen = new RenderTarget("screen");
			screen->setWidth(500);
			screen->setHeight(300);
			screen->setRenderToDisplay(true);
			screen->load();
			unit->addAnnotation("load screen rendertarget - ");
			unit->assertTrue(screen->isLoaded());

			screenpass = new Pass("screenpass");
			screenpass->addUniform(*timevar);
			screenpass->addUniform(*vol3);
			screenpass->addUniform(*vol1);
			screenpass->addUniform(*vol4);
			screenpass->addUniform(*vol2);
			screenpass->addRenderObject(*obj1);
			screenpass->setShader(screenshader);
			screenpass->setOutput(*screen,vector<Texture *>());
			screenpass->load();
			unit->addAnnotation("load screenpass - ");
			unit->assertTrue(screenpass->isLoaded());
		}

		void reshape(SXRenderArea &area) {
			screen->setWidth(area.getWidth());
			screen->setHeight(area.getHeight());
			screen->load();
		}

		void render(SXRenderArea &area) {
			time += area.getDeltaTime();
			if(time > 20) {
				area.stopRendering();
			}
			(*timevar) << time;

			pass1->render();

			if(time < 12) {
				//render pass2 for the first
				//12 seconds

				//clear whole volumes with one color
				(*startLayer) << 0.0f;
				(*endLayer) << 5.0f;
				(*useTextures) << 0.0f;
				pass2->render();

				//shift targetinterval of voumes
				(*startLayer) << fmod(time,6.0f);
				(*endLayer) << fmod(time+2.0f,6.0f);
				(*useTextures) << 1.0f;
				pass2->render();
			} else if(time < 16) {
				//after the first 12 seconds
				//render pass 3
				pass3->render();
			}

			if(time >= 16) {
				//after the first 12 seconds
				//render pass 3 with filevolume
				//instead of vol4
				screenpass->removeUniform("vol4");
				screenpass->addUniform(*filevolume);
			}
			screenpass->render();
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");

			delete timevar;
			delete mesh;
			delete obj1;
			delete lcoordinate;
			delete tex1;
			delete tex2;
			delete tex3;

			delete vol1;
			delete vol2;
			delete pass1shader;
			delete fbo1;
			delete pass1;

			delete vol3;
			delete vol4;
			delete pass2shader;
			delete startLayer;
			delete endLayer;
			delete useTextures;
			delete pass2;

			delete tex4;
			delete tex5;
			delete tex6;
			delete obj2;
			delete obj3;
			delete obj4;
			delete transform1;
			delete transform2;
			delete transform3;
			delete pass3shader;
			delete obj3shader;
			delete pass3;

			delete filevolume;

			delete screenshader;
			delete screen;
			delete screenpass;
		}

	};

	void testVolume(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testvolume",new ListLogger());
			Logger::setDefaultLogger("testvolume");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			int argc = 1;
			char **argv = new char *[1];
			char *argvcontent = "testvolume";
			argv[0] = argvcontent;

			QApplication *app1 = new QApplication(argc,argv);

			SXWidget *widget1 = new SXWidget();
			widget1->renderPeriodically(0.01f);
			widget1->setMinimumSize(600,400);
			QObject::connect(widget1,SIGNAL(finishedRendering()),widget1,SLOT(close()));
			widget1->addRenderListener(*new TestVolume1(unit,logger));
			widget1->show();

			unit.addAnnotation("execute QApp 1 - ");
			unit.assertEquals(app1->exec(),0);
			delete widget1;
			delete app1;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");

			QApplication *app2 = new QApplication(argc,argv);

			SXWidget *widget2 = new SXWidget();
			widget2->renderPeriodically(0.01f);
			widget2->setMinimumSize(600,400);
			QObject::connect(widget2,SIGNAL(finishedRendering()),widget2,SLOT(close()));
			widget2->addRenderListener(*new TestVolume2(unit,logger));
			widget2->show();

			unit.addAnnotation("execute QApp 2 - ");
			unit.assertEquals(app2->exec(),0);
			delete widget2;
			delete app2;

			delete argv;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif