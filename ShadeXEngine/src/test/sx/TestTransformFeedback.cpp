#ifndef _TEST_SX_TESTTRANSFORMFEEDBACK_CPP_
#define _TEST_SX_TESTTRANSFORMFEEDBACK_CPP_

/**
 * transform feedback test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <vector>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestTransformFeedback1: public SXRenderListener {
	private:
		XUnit *unit;
		ListLogger *list;
		float time;

		BufferedMesh *mesh1;
		BufferedMesh *mesh2;
		BufferedMesh *mesh3;
		Shader *program1;
		Shader *program2;
		Shader *program3;
		UniformMatrix *transform1;

		TestTransformFeedback1(const TestTransformFeedback1 &);
		TestTransformFeedback1 &operator = (const TestTransformFeedback1 &);
	public:

		TestTransformFeedback1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
			time = 0;
		}

		~TestTransformFeedback1() {
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("loading phase");

			program1 = new Shader("program1");
			program1->addShader(
				"#version 400 core			\n"
				"in vec4 vertices;				\n"
				"in vec3 colors;				\n"
				"out vec3 col;					\n"
				"								\n"
				"void main() {					\n"
				"	gl_Position = vertices;	\n"
				"	col = colors;				\n"
				"}								\n"
				,VERTEX);
			program1->addShader(
				"#version 400 core			\n"
				"in vec3 col;					\n"
				"out vec4 frag1;				\n"
				"								\n"
				"void main() {					\n"
				"	frag1 = vec4(col,1);		\n"
				"}								\n"
				,FRAGMENT);
			program1->load();
			unit->addAnnotation("load program1 - ");
			unit->assertTrue(program1->isLoaded());
			unit->addAnnotation("program1 fragmendshading - ");
			unit->assertTrue(program1->isFragmentShading());
			unit->addAnnotation("program1 transformfeedback - ");
			unit->assertTrue(!program1->isTransformFeedback());

			program2 = new Shader("program2");
			program2->addShader(
				"#version 400 core				\n"
				"in vec4 vertices;				\n"
				"in vec3 colors;				\n"
				"out vec4 gVertices;			\n"
				"out vec3 gColors;				\n"
				"								\n"
				"void main() {					\n"
				"	gVertices = vertices;		\n"
				"	gColors = colors;			\n"
				"}								\n"
				,VERTEX);
			program2->addShader(
				"#version 400 core				\n"
				"layout (triangles) in;			\n"
				"layout (triangle_strip,max_vertices=6) out;	\n"
				"in vec4 gVertices[];			\n"
				"in vec3 gColors[];				\n"
				"out vec4 outVertices;			\n"
				"out vec3 outColors;			\n"
				"								\n"
				"void main() {					\n"
				"	vec4 p1 = gVertices[1] + vec4((gVertices[2].xyz-gVertices[1].xyz)/3.0,0);		\n"
				"	vec4 p2 = gVertices[1] + vec4((gVertices[2].xyz-gVertices[1].xyz)*2.0/3.0,0);	\n"
				"	vec3 c1 = gColors[1] + (gColors[2] - gColors[1])/3.0;			\n"
				"	vec3 c2 = gColors[1] + (gColors[2] - gColors[1])*2.0/3.0;		\n"
				"	outVertices = gVertices[0];	\n"
				"	outColors = gColors[0];		\n"
				"	EmitVertex();				\n"
				"	outVertices = gVertices[1];	\n"
				"	outColors = gColors[1];		\n"
				"	EmitVertex();				\n"
				"	outVertices = p1;			\n"
				"	outColors = c1;				\n"
				"	EmitVertex();				\n"
				"	EndPrimitive();				\n"
				"	outVertices = gVertices[0];	\n"
				"	outColors = gColors[0];		\n"
				"	EmitVertex();				\n"
				"	outVertices = p2;			\n"
				"	outColors = c2;				\n"
				"	EmitVertex();				\n"
				"	outVertices = gVertices[2];	\n"
				"	outColors = gColors[2];		\n"
				"	EmitVertex();				\n"
				"	EndPrimitive();				\n"
				"}								\n"
				,GEOMETRY);
			program2->addTransformFeedbackBuffer("outVertices");
			program2->addTransformFeedbackBuffer("outColors");
			program2->load();
			unit->addAnnotation("load program2 - ");
			unit->assertTrue(program2->isLoaded());
			unit->addAnnotation("program2 fragmendshading - ");
			unit->assertTrue(!program2->isFragmentShading());
			unit->addAnnotation("program2 transformfeedback - ");
			unit->assertTrue(program2->isTransformFeedback());
			const vector<string> &tfbuffers = program2->getTransformFeedbackBuffers();
			unit->addAnnotation("program2 transformfeedback buffer 1 - ");
			unit->assertEquals(tfbuffers[0],"outVertices");
			unit->addAnnotation("program2 transformfeedback buffer 2 - ");
			unit->assertEquals(tfbuffers[1],"outColors");

			program3 = new Shader("program3");
			program3->addShader(
				"#version 400 core				\n"
				"in vec4 vertices;				\n"
				"in vec3 colors;				\n"
				"out vec3 outColors;			\n"
				"out vec4 outVertices;			\n"
				"uniform mat4 transform1;		\n"
				"								\n"
				"void main() {					\n"
				"	outColors = colors;			\n"
				"	outVertices = transform1 * vertices;	\n"
				"}								\n"
				,VERTEX);
			program3->addTransformFeedbackBuffer("outVertices");
			program3->addTransformFeedbackBuffer("outColors");
			program3->load();
			unit->addAnnotation("load program3 - ");
			unit->assertTrue(program3->isLoaded());
			unit->addAnnotation("program3 fragmendshading - ");
			unit->assertTrue(!program3->isFragmentShading());
			unit->addAnnotation("program3 transformfeedback - ");
			unit->assertTrue(program3->isTransformFeedback());
			const vector<string> &tf2buffers = program3->getTransformFeedbackBuffers();
			unit->addAnnotation("program3 transformfeedback buffer 1 - ");
			unit->assertEquals(tf2buffers[0],"outVertices");
			unit->addAnnotation("program3 transformfeedback buffer 2 - ");
			unit->assertEquals(tf2buffers[1],"outColors");

			float mesh1VArray[] = {
			0.5f,0,0,1,			0.4f,0.4f,0,1,		0.25f,0.25f,0,1,
			0.25f,0.25f,0,1,	0.4f,0.4f,0,1,		0,0.5f,0,1,
			0,0.5f,0,1,			-0.4f,0.4f,0,1,		-0.25f,0.25f,0,1,
			-0.25f,0.25f,0,1,	-0.4f,0.4f,0,1,		-0.5f,0,0,1,
			-0.5f,0,0,1,		-0.4f,-0.4f,0,1,	-0.25f,-0.25f,0,1,
			-0.25f,-0.25f,0,1,	-0.4f,-0.4f,0,1,	0,-0.5f,0,1,
			0,-0.5f,0,1,		0.4f,-0.4f,0,1,		0.25f,-0.25f,0,1,
			0.25f,-0.25f,0,1,	0.4f,-0.4f,0,1,		0.5f,0,0,1
			};
			float mesh1TArray[] = {
			1,1,1,		1,0,0,		0,0,1,
			0,0,1,		1,0,0,		0,1,0,
			0,1,0,		0,0,1,		1,0,0,
			1,0,0,		0,0,1,		0,1,0,
			0,1,0,		0,1,0,		1,0,0,
			1,0,0,		0,1,0,		0,0,1,
			0,0,1,		0,0,1,		1,0,0,
			1,0,0,		0,0,1,		1,1,1
			};
			vector<float> mesh1VList(mesh1VArray,mesh1VArray + 96);
			vector<float> mesh1TList(mesh1TArray,mesh1TArray + 72);
			mesh1 = new BufferedMesh("mesh1");
			mesh1->addBuffer("vertices", mesh1VList, 4);
			mesh1->addBuffer("colors", mesh1TList, 3);
			mesh1->setOutputIdentifier("vertices","outVertices");
			mesh1->setOutputIdentifier("colors","outColors");
			mesh1->setFaceSize(3);
			mesh1->setMaxVertexCount(72);
			mesh1->load();
			unit->addAnnotation("mesh1 load vertices - ");
			unit->assertTrue(mesh1->isLoaded());
			unit->addAnnotation("mesh1 vertex count - ");
			unit->assertEquals(mesh1->getVertexCount(),(unsigned int)24);
			unit->addAnnotation("mesh1 maximum vertex count - ");
			unit->assertEquals(mesh1->getMaxVertexCount(),(unsigned int)72);

			vector<float> mesh2VList;
			vector<float> mesh2TList;
			mesh2 = new BufferedMesh("mesh2");
			mesh2->addBuffer("vertices","outVertices",mesh2VList,4);
			mesh2->addBuffer("colors","outColors",mesh2TList,3);
			mesh2->setFaceSize(3);
			mesh2->setMaxVertexCount(71);
			mesh2->load();
			unit->addAnnotation("mesh2 load vertices - ");
			unit->assertTrue(mesh2->isLoaded());
			unit->addAnnotation("mesh2 vertex count - ");
			unit->assertEquals(mesh2->getVertexCount(),(unsigned int)0);
			unit->addAnnotation("mesh2 maximum vertex count - ");
			unit->assertEquals(mesh2->getMaxVertexCount(),(unsigned int)71);

			float mesh3VArray[] = {
				0,0,-0.1f,1,				0,0.5f,-0.1f,1,		-0.125f,0.125f,-0.1f,1,
				-0.125f,0.125,-0.1f,1,		-0.5f,0,-0.1f,1,	0,0,-0.1f,1,
				0,0,-0.1f,1,				0,-0.5f,-0.1f,1,	0.125f,-0.125f,-0.1f,1,
				0.125f,-0.125f,-0.1f,1,		0.5f,0,-0.1f,1,		0,0,-0.1f,1
			};
			float mesh3TArray[] = {
				0,0,1,		0,1,0,			1,0,1,
				1,0,1,		1,1,0,			0,0,1,
				0,0,1,		1,0.5f,0.5f,	0,1,0.5f,
				0,1,0.5f,	1,0,0,			0,0,1
			};
			vector<float> mesh3VList(mesh3VArray,mesh3VArray+48);
			vector<float> mesh3TList(mesh3TArray,mesh3TArray+36);
			mesh3 = new BufferedMesh("mesh3");
			mesh3->addBuffer("vertices",mesh3VList,4);
			mesh3->addBuffer("colors",mesh3TList,3);
			mesh3->setOutputIdentifier("vertices","outVertices");
			mesh3->setOutputIdentifier("colors","outColors");
			mesh3->setFaceSize(3);
			mesh3->setMaxVertexCount(5);
			mesh3->load();
			unit->addAnnotation("mesh3 load vertices - ");
			unit->assertTrue(mesh3->isLoaded());
			unit->addAnnotation("mesh3 vertex count - ");
			unit->assertEquals(mesh3->getVertexCount(),(unsigned int)12);
			unit->addAnnotation("mesh3 maximum vertex count - ");
			unit->assertEquals(mesh3->getMaxVertexCount(),(unsigned int)12);

			transform1 = new UniformMatrix("transform1");
			transform1->load();
			unit->addAnnotation("load transform1 - ");
			unit->assertTrue(transform1->isLoaded());

			program2->use();
			mesh2->beginCapture(*program2);
			mesh1->render(*program2);
			mesh3->render(*program2);
			mesh2->endCapture(*program2);
			unit->addAnnotation("program2 written vertexcount - ");
			unit->assertEquals(mesh2->countCreatedVertices(),(unsigned int)69);
			unit->addAnnotation("mesh2 new vertexCount - ");
			unit->assertEquals(mesh2->getVertexCount(),(unsigned int)69);

			VertexBuffer *vbo = mesh3->getBuffer("verticio");
			unit->addAnnotation("try getting non-existent buffer in mesh3 - ");
			unit->assertTrue(vbo == 0);

			vbo = mesh3->getBuffer("vertices");
			unit->addAnnotation("get buffer vertices in mesh3 - ");
			unit->assertTrue(vbo != 0);

			float *content = vbo->unlock();
			unit->assertEquals(content[16],-0.5f,0.0001f);
			unit->assertEquals(content[17],0.0f,0.0001f);
			unit->assertEquals(content[18],-0.1f,0.0001f);
			unit->assertEquals(content[19],1.0f,0.0001f);
			content[18] = -0.2f;
			vbo->lock();

			vbo = mesh2->getBuffer("colors");
			unit->addAnnotation("get buffer colors in mesh3 - ");
			unit->assertTrue(vbo != 0);

			content = vbo->unlockWrite();
			content[162] = 0.8f;
			content[163] = 1.0f;
			content[164] = 0.1f;
			vbo->lock();
	
			vbo = mesh3->getBuffer("vertices");
			unit->addAnnotation("get buffer vertices in mesh3 - ");
			unit->assertTrue(vbo != 0);

			const float *rContent = vbo->unlockRead();
			unit->assertEquals(rContent[18],-0.2f,0.0001f);
			vbo->lock();

			vbo = mesh2->getBuffer("colors");
			unit->addAnnotation("get buffer colors in mesh2 - ");
			unit->assertTrue(vbo != 0);

			rContent = vbo->unlockRead();
			unit->assertEquals(rContent[162],0.8f,0.0001f);
			unit->assertEquals(rContent[163],1.0f,0.0001f);
			unit->assertEquals(rContent[164],0.1f,0.0001f);
			vbo->lock();

		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
			time += area.getDeltaTime();
			if(time > 4) {
				area.stopRendering();
			}

			transform1->rotate(Vector(0,0,1),sx::Tau*area.getDeltaTime()*0.1f);

			RenderTarget::setViewport(0,0,area.getWidth(),area.getHeight());
			RenderTarget::clearTarget();

			program3->use();
			transform1->use(*program3,"resource");
			mesh1->beginCapture(*program3);
			mesh2->render(*program3);
			mesh1->endCapture(*program3);

			mesh2->beginCapture(*program3);
			mesh1->render(*program3);
			mesh2->endCapture(*program3);

			program1->use();
			mesh2->render(*program1);
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");

			delete mesh1;
			delete mesh2;
			delete mesh3;
			delete program1;
			delete program2;
			delete program3;
			delete transform1;
		}

	};

	void testTransformFeedback(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testtransformfeedback",new ListLogger());
			Logger::setDefaultLogger("testtransformfeedback");
			ListLogger &list = dynamic_cast<ListLogger &>(Logger::get());

			int argc = 1;
			char **argv = new char *[1];
			char *argvcontent = "testtransformfeedback";
			argv[0] = argvcontent;

			QApplication *app1 = new QApplication(argc,argv);

			SXWidget *widget1 = new SXWidget();
			widget1->renderPeriodically(0.01f);
			widget1->setMinimumSize(600,400);
			QObject::connect(widget1,SIGNAL(finishedRendering()),widget1,SLOT(close()));
			widget1->addRenderListener(*new TestTransformFeedback1(unit,list));
			widget1->show();

			unit.addAnnotation("execute app1 - ");
			unit.assertEquals(app1->exec(),0);
			delete widget1;
			delete app1;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		}  catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif