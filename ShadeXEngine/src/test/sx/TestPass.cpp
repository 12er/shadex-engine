#ifndef _TEST_SX_TESTPASS_CPP_
#define _TEST_SX_TESTPASS_CPP_

/**
 * pass test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <vector>
#include <cmath>
#include <boost/date_time/posix_time/posix_time.hpp>
using namespace sx;
using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::posix_time::to_simple_string;
using namespace std;

namespace sxengine {

	class TestPass1: public SXRenderListener {
	private:
		int width;
		int height;

		RenderTarget *target;
		Pass *pass;
		Shader *shader;
		Shader *shader1;
		Shader *shader3;
		BufferedMesh *mesh;
		Texture *tex1;
		Texture *tex2;
		Texture *tex3;
		Texture *tex4;
		Texture *tex5;
		Texture *tex6;
		RenderObject *obj1;
		RenderObject *obj2;
		RenderObject *obj3;
		RenderObject *obj4;
		RenderObject *obj5;
		RenderObject *obj6;
		RenderObject *obj7;
		RenderObject *obj8;
		UniformMatrix *mat1;
		UniformMatrix *mat2;
		UniformVector *vec1;
		UniformVector *vec2;
		UniformFloat *float1;
		UniformFloat *float2;

		XUnit *unit;
		ListLogger *list;
		timer timeMeasurement;
	public:

		TestPass1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			unit->addMarkup("loading phase");
			
			target = new RenderTarget("target");
			target->setRenderToDisplay(true);
			target->setWidth(width);
			target->setHeight(height);
			target->load();
			unit->addAnnotation("load target - ");
			unit->assertTrue(target->isLoaded());
			
			pass = new Pass("main");
			
			shader = new Shader("shader");
			shader->addShaderFile("../data/test/sxengine/files/pass.vp",VERTEX);
			shader->addShaderFile("../data/test/sxengine/files/pass.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			unit->addAnnotation("load shader - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader->isLoaded());

			shader1 = new Shader("shader1");
			shader1->addShaderFile("../data/test/sxengine/files/pass.vp",VERTEX);
			shader1->addShaderFile("../data/test/sxengine/files/pass.fp",FRAGMENT);
			list->getMarkups().clear();
			shader1->load();
			unit->addAnnotation("load shader1 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader1->isLoaded());

			shader3 = new Shader("shader3");
			shader3->addShaderFile("../data/test/sxengine/files/pass.vp",VERTEX);
			shader3->addShaderFile("../data/test/sxengine/files/pass.fp",FRAGMENT);
			list->getMarkups().clear();
			shader3->load();
			unit->addAnnotation("load shader3 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader3->isLoaded());

			float meshVArray[] = {
			-1,-1,0,1,
			-0.875f,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-0.875f,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList(meshVArray,meshVArray + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices",meshVList,4);
			mesh->addBuffer("texcoords",meshTList,2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("load mesh - ");
			unit->assertTrue(mesh->isLoaded());
			
			tex1 = new Texture("textures.tex1");
			tex1->setPath("../data/test/sxengine/files/background.jpg");
			tex1->load();
			unit->addAnnotation("load tex1 - ");
			unit->assertTrue(tex1->isLoaded());

			tex2 = new Texture("textures.tex2");
			tex2->setUniformName("vec1","main");
			tex2->setPath("../data/test/sxengine/files/ShadeXEngine.jpg");
			tex2->load();
			unit->addAnnotation("load tex2 - ");
			unit->assertTrue(tex2->isLoaded());

			tex3 = new Texture("tex.tex3");
			tex3->setPath("../data/test/sxengine/files/t3.jpg");
			tex3->load();
			unit->addAnnotation("load tex3 - ");
			unit->assertTrue(tex3->isLoaded());

			tex4 = new Texture("t.tex4");
			tex4->setPath("../data/test/sxengine/files/t4.jpg");
			tex4->load();
			unit->addAnnotation("load tex4 - ");
			unit->assertTrue(tex4->isLoaded());

			tex5 = new Texture("t.tex1");
			tex5->setPath("../data/test/sxengine/files/t3.jpg");
			tex5->load();
			unit->addAnnotation("load tex5 - ");
			unit->assertTrue(tex5->isLoaded());

			tex6 = new Texture("tex.tex2");
			tex6->setPath("../data/test/sxengine/files/t4.jpg");
			tex6->load();
			unit->addAnnotation("load tex6 - ");
			unit->assertTrue(tex6->isLoaded());

			obj1 = new RenderObject("obj1");

			obj2 = new RenderObject("obj2");

			obj3 = new RenderObject("obj3");

			obj4 = new RenderObject("obj4");

			obj5 = new RenderObject("obj5");

			obj6 = new RenderObject("obj6");

			obj7 = new RenderObject("obj7");

			obj8 = new RenderObject("obj8");

			mat1 = new UniformMatrix("m.mat1");
			
			mat2 = new UniformMatrix("m.something");
			mat2->setUniformName("mat2","main");
			
			vec1 = new UniformVector("m.vec1");

			vec2 = new UniformVector("m.vec2");
			vec2->setUniformName("mat1","main");
			
			float1 = new UniformFloat("f.float1");

			float2 = new UniformFloat("f.float2");
			float2->setUniformName("vec1","main");


			unit->addMarkup("add/remove uniforms");

			pass->addUniform(*mat1);
			pass->addUniform(*mat2);
			pass->addUniform(*vec1);
			list->getMarkups().clear();
			pass->addUniform(*vec2);
			try {
				unit->addAnnotation("add 4 uniforms - ");
				unit->assertEquals(list->getUintEntry("Pass::addUniform",0),(unsigned int)3);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			pass->addUniform(*float1);
			list->getMarkups().clear();
			pass->addUniform(*float2);
			try {
				unit->addAnnotation("add 6 uniforms - ");
				unit->assertEquals(list->getUintEntry("Pass::addUniform",0),(unsigned int)4);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			pass->addUniform(*tex1);
			list->getMarkups().clear();
			pass->addUniform(*tex2);
			try {
				unit->addAnnotation("add 8 uniforms - ");
				unit->assertEquals(list->getUintEntry("Pass::addUniform",0),(unsigned int)5);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			pass->removeUniform("mat2");
			pass->removeUniform("vec1");
			list->getMarkups().clear();
			pass->removeUniform("tex1");
			try {
				unit->addAnnotation("remove three uniforms - ");
				unit->assertEquals(list->getUintEntry("Pass::removeUniform",0),(unsigned int)2);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			list->getMarkups().clear();
			pass->removeUniforms();
			try {
				unit->addAnnotation("remove all uniforms - ");
				unit->assertEquals(list->getUintEntry("Pass::removeUniforms",0),(unsigned int)0);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete pass;


			unit->addMarkup("render - discard");

			obj1->setMesh(*mesh);
			obj2->setMesh(*mesh);
			obj3->setMesh(*mesh);
			obj3->setShader(shader3);
			vector<Texture *> targets;

			pass = new Pass("main");
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render empty pass - ");
				unit->assertEquals(list->getStringEntry("Pass::render-discard",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			pass->addRenderObject(*obj1);
			pass->addRenderObject(*obj2);
			pass->addRenderObject(*obj3);
			pass->setOutput(*target,targets);
			pass->setShader(shader);
			pass->setVisible(false);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render invisible pass - ");
				unit->assertEquals(list->getStringEntry("Pass::render-discard",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete pass;

			pass = new Pass("main");
			pass->addRenderObject(*obj1);
			pass->addRenderObject(*obj2);
			pass->addRenderObject(*obj3);
			pass->setShader(shader);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass without rendertarget - ");
				unit->assertEquals(list->getStringEntry("Pass::render-discard",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			pass->setOutput(*target,targets);
			pass->removeRenderObject(obj3->getID());
			vector<RenderObject *> *temp = &pass->getTempRenderObjects();
			temp->push_back(obj1);
			temp->push_back(obj2);
			pass->setShader(0);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass without shaders - ");
				unit->assertEquals(list->getStringEntry("Pass::render-discard",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			temp = &pass->getTempRenderObjects();
			temp->push_back(obj1);
			temp->push_back(obj2);
			temp->push_back(obj3);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass with shader in temp object - ");
				list->getStringEntry("Pass::render-discard",0);
				unit->assertTrue(false);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(true);
			}

			pass->addRenderObject(*obj3);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass with shader in renderobject - ");
				list->getStringEntry("Pass::render-discard",0);
				unit->assertTrue(false);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(true);
			}

			pass->removeRenderObjects();
			pass->setShader(shader);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass with shader - ");
				list->getStringEntry("Pass::render-discard",0);
				unit->assertTrue(false);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(true);
			}
			delete pass;


			unit->addMarkup("distribution of texture slots");

			pass = new Pass("main");
			shader->freeTextureSlots();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			unit->addAnnotation("shader - lock four texture slots - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)3);
			shader->lockTextureSlots();
			shader1->freeTextureSlots();
			shader1->createTextureSlot();
			shader1->createTextureSlot();
			unit->addAnnotation("shader - lock three texture slots - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)2);
			shader1->lockTextureSlots();
			shader3->freeTextureSlots();
			shader3->createTextureSlot();
			shader3->createTextureSlot();
			shader3->createTextureSlot();
			shader3->createTextureSlot();
			unit->addAnnotation("shader - lock five texture slots - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)4);
			shader3->lockTextureSlots();

			obj4->setMesh(*mesh);
			obj4->setShader(shader1);
			obj4->addUniform(*tex3);
			obj4->addUniform(*tex4);

			obj5->setMesh(*mesh);
			obj5->setShader(shader1);
			obj5->addUniform(*tex3);
			obj5->addUniform(*tex4);

			obj6->setMesh(*mesh);
			obj6->setShader(shader3);
			obj6->addUniform(*tex3);
			obj6->addUniform(*tex4);

			obj7->setMesh(*mesh);
			obj7->addUniform(*tex3);
			obj7->addUniform(*tex4);

			obj8->setMesh(*mesh);
			obj8->addUniform(*tex3);
			obj8->addUniform(*tex4);

			pass->addRenderObject(*obj4);
			pass->addRenderObject(*obj5);
			pass->addRenderObject(*obj6);
			pass->addRenderObject(*obj7);
			pass->addRenderObject(*obj8);
			temp = &pass->getTempRenderObjects();
			temp->push_back(obj4);
			temp->push_back(obj5);
			temp->push_back(obj6);
			temp->push_back(obj7);
			temp->push_back(obj8);
			pass->addUniform(*tex5);
			pass->addUniform(*tex6);
			pass->setShader(shader);
			pass->setOutput(*target,targets);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass, control slots - ");
				list->getStringEntry("Pass::render-discard",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->assertTrue(true);
			}
			unit->addAnnotation("control slots shader - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)6);
			unit->addAnnotation("control slots shader1 - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)5);
			unit->addAnnotation("control slots shader3 - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)7);
			shader->freeTextureSlots();
			shader1->freeTextureSlots();
			shader3->freeTextureSlots();
			unit->addAnnotation("control locked slots of shader - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)4);
			unit->addAnnotation("control locked slots of shader1 - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)3);
			unit->addAnnotation("control locked slots of shader3 - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)5);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			shader1->unlockTextureSlots();
			shader1->freeTextureSlots();
			shader3->unlockTextureSlots();
			shader3->freeTextureSlots();
			unit->addAnnotation("unlocked slots of shader - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)0);
			unit->addAnnotation("unlocked slots of shader1 - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)0);
			unit->addAnnotation("unlocked slots of shader3 - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)0);

			//repeat rendering, but this time without a pass shader
			shader->freeTextureSlots();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			unit->addAnnotation("shader - lock four texture slots - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)3);
			shader->lockTextureSlots();
			shader1->freeTextureSlots();
			shader1->createTextureSlot();
			shader1->createTextureSlot();
			unit->addAnnotation("shader - lock three texture slots - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)2);
			shader1->lockTextureSlots();
			shader3->freeTextureSlots();
			shader3->createTextureSlot();
			shader3->createTextureSlot();
			shader3->createTextureSlot();
			shader3->createTextureSlot();
			unit->addAnnotation("shader - lock five texture slots - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)4);
			shader3->lockTextureSlots();

			temp = &pass->getTempRenderObjects();
			temp->push_back(obj4);
			temp->push_back(obj5);
			temp->push_back(obj6);
			temp->push_back(obj7);
			temp->push_back(obj8);
			pass->setShader(0);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("render pass without shader, control slots - ");
				list->getStringEntry("Pass::render-discard",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->assertTrue(true);
			}
			unit->addAnnotation("control slots shader - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)4);
			unit->addAnnotation("control slots shader1 - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)5);
			unit->addAnnotation("control slots shader3 - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)7);
			shader->freeTextureSlots();
			shader1->freeTextureSlots();
			shader3->freeTextureSlots();
			unit->addAnnotation("control locked slots of shader - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)4);
			unit->addAnnotation("control locked slots of shader1 - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)3);
			unit->addAnnotation("control locked slots of shader3 - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)5);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			shader1->unlockTextureSlots();
			shader1->freeTextureSlots();
			shader3->unlockTextureSlots();
			shader3->freeTextureSlots();
			unit->addAnnotation("unlocked slots of shader - ");
			unit->assertEquals(shader->createTextureSlot(),(unsigned int)0);
			unit->addAnnotation("unlocked slots of shader1 - ");
			unit->assertEquals(shader1->createTextureSlot(),(unsigned int)0);
			unit->addAnnotation("unlocked slots of shader3 - ");
			unit->assertEquals(shader3->createTextureSlot(),(unsigned int)0);
			delete pass;


			unit->addMarkup("test deleting buffers");

			pass = new Pass("main");
			pass->setOutput(*target,targets);
			pass->addRenderObject(*obj4);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("default deletion of buffers - ");
				unit->assertEquals(list->getStringEntry("Pass::render-clear",0),"clear(depth|color)");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			pass->setClearColorBuffer(true);
			pass->setClearDepthBuffer(true);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("deletion of color and depth buffers - ");
				unit->assertEquals(list->getStringEntry("Pass::render-clear",0),"clear(depth|color)");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			pass->setClearColorBuffer(true);
			pass->setClearDepthBuffer(false);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("deletion of color buffer - ");
				unit->assertEquals(list->getStringEntry("Pass::render-clear",0),"clear(color)");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			pass->setClearColorBuffer(false);
			pass->setClearDepthBuffer(true);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("deletion of depth buffer - ");
				unit->assertEquals(list->getStringEntry("Pass::render-clear",0),"clear(depth)");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			pass->setClearColorBuffer(false);
			pass->setClearDepthBuffer(false);
			list->getMarkups().clear();
			pass->render();
			try {
				unit->addAnnotation("deleting no buffers - ");
				list->getStringEntry("Pass::render-clear",0);
				unit->assertTrue(false);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(true);
			}
	
			//start measuring time
			ptime time(second_clock::local_time());
			timeMeasurement.restart();

			area.stopRendering();
		}

		void reshape(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();
		}

		void render(SXRenderArea &area) {
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete target;
			delete pass;
			delete shader;
			delete shader1;
			delete shader3;
			delete mesh;
			delete tex1;
			delete tex2;
			delete tex3;
			delete tex4;
			delete tex5;
			delete tex6;
			delete obj1;
			delete obj2;
			delete obj3;
			delete obj4;
			delete obj5;
			delete obj6;
			delete obj7;
			delete obj8;
			delete mat1;
			delete mat2;
			delete vec1;
			delete vec2;
			delete float1;
			delete float2;
		}

	};

	class TestPass2: public SXRenderListener {
	private:
		int width;
		int height;

		RenderTarget *target1;
		RenderTarget *target2;
		Shader *shader1;
		Shader *shader2;
		Shader *shader3;
		Texture *tex1;
		Texture *tex2_1;
		Texture *tex2_2;
		Texture *tex3_1;
		Texture *tex3_2;
		Texture *frag1;
		Texture *frag2;
		UniformVector *translate1;
		UniformVector *translate2;
		UniformVector *translate3;
		UniformVector *translate4;
		UniformVector *translate5;
		UniformVector *translate6;
		BufferedMesh *mesh;
		BufferedMesh *quad;
		RenderObject *obj1;
		RenderObject *obj2;
		RenderObject *obj3;
		RenderObject *obj4;
		RenderObject *obj5;
		RenderObject *obj6;
		RenderObject *objquad;
		Pass *pass1;
		Pass *pass2;

		XUnit *unit;
		ListLogger *list;
		timer timeMeasurement;
	public:

		TestPass2(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();


			unit->addMarkup("loading phase");

			target1 = new RenderTarget("target1");
			target1->setWidth(600);
			target1->setHeight(600);
			target1->load();
			unit->addAnnotation("load target1 - ");
			unit->assertTrue(target1->isLoaded());

			frag1 = new Texture("f.frag1");
			frag1->setUniformName("tex1","pass2");
			frag1->setWidth(600);
			frag1->setHeight(600);
			frag1->setPixelFormat(FLOAT_RGBA);
			frag1->load();
			unit->addAnnotation("load frag1 - ");
			unit->assertTrue(frag1->isLoaded());

			frag2 = new Texture("f.frag2");
			frag2->setUniformName("tex2","pass2");
			frag2->setWidth(600);
			frag2->setHeight(600);
			frag2->setPixelFormat(BYTE_RGBA);
			frag2->load();
			unit->addAnnotation("load frag2 - ");
			unit->assertTrue(frag2->isLoaded());
			
			target2 = new RenderTarget("target2");
			target2->setWidth(width);
			target2->setHeight(height);
			target2->setRenderToDisplay(true);
			target2->load();
			unit->addAnnotation("load target2 - ");
			unit->assertTrue(target2->isLoaded());

			shader1 = new Shader("shader1");
			shader1->addShaderFile("../data/test/sxengine/files/pass1.vp",VERTEX);
			shader1->addShaderFile("../data/test/sxengine/files/pass1.fp",FRAGMENT);
			list->getMarkups().clear();
			shader1->load();
			unit->addAnnotation("load shader1 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader1->isLoaded());

			shader2 = new Shader("shader2");
			shader2->addShaderFile("../data/test/sxengine/files/pass1extra.vp",VERTEX);
			shader2->addShaderFile("../data/test/sxengine/files/pass1extra.fp",FRAGMENT);
			list->getMarkups().clear();
			shader2->load();
			unit->addAnnotation("load shader2 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader2->isLoaded());

			shader3 = new Shader("shader3");
			shader3->addShaderFile("../data/test/sxengine/files/pass2.vp",VERTEX);
			shader3->addShaderFile("../data/test/sxengine/files/pass2.fp",FRAGMENT);
			list->getMarkups().clear();
			shader3->load();
			unit->addAnnotation("load shader3 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader3->isLoaded());

			tex1 = new Texture("tex1.tex1");
			tex1->setPath("../data/test/sxengine/files/pass1.jpg");
			tex1->load();
			unit->addAnnotation("load tex1 - ");
			unit->assertTrue(tex1->isLoaded());

			tex2_1 = new Texture("tex2_1.tex2");
			tex2_1->setPath("../data/test/sxengine/files/pass2_1.jpg");
			tex2_1->load();
			unit->addAnnotation("load tex2_1 - ");
			unit->assertTrue(tex2_1->isLoaded());

			tex2_2 = new Texture("tex2_2.tex2");
			tex2_2->setPath("../data/test/sxengine/files/pass2_2.jpg");
			tex2_2->load();
			unit->addAnnotation("load tex2_2 - ");
			unit->assertTrue(tex2_2->isLoaded());

			tex3_1 = new Texture("tex3_1.tex3");
			tex3_1->setPath("../data/test/sxengine/files/pass3_1.jpg");
			tex3_1->load();
			unit->addAnnotation("load tex3_1 - ");
			unit->assertTrue(tex3_1->isLoaded());

			tex3_2 = new Texture("tex3_2.tex3");
			tex3_2->setPath("../data/test/sxengine/files/pass3_2.jpg");
			tex3_2->load();
			unit->addAnnotation("load tex3_2 - ");
			unit->assertTrue(tex3_2->isLoaded());

			translate1 = new UniformVector("t1.translate");
			*translate1 = Vector( 0.5f , 1.5f , 0 );

			translate2 = new UniformVector("t2.translate");
			*translate2 = Vector( 1.5f , 1.5f , 0 );

			translate3 = new UniformVector("t3.translate");
			*translate3 = Vector( 0.5f , 0.5f , 0 );

			translate4 = new UniformVector("t4.translate");
			*translate4 = Vector( 1.5f , 0.5f , 0);

			translate5 = new UniformVector("t5.translate");
			*translate5 = Vector( 0.9f , 1.1f , 0 );

			translate6 = new UniformVector("t6.translate");
			*translate6 = Vector( 1.1f , 0.9f , 0);

			float meshVArray1[] = {
			-1,-1,0,1,
			-0.875f,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-0.875f,0,1
			};
			float meshVArray2[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList1(meshVArray1,meshVArray1 + 24);
			vector<float> meshVList2(meshVArray2,meshVArray2 + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices",meshVList1,4);
			mesh->addBuffer("texcoords",meshTList,2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("load mesh - ");
			unit->assertTrue(mesh->isLoaded());

			quad = new BufferedMesh("quad");
			quad->addBuffer("vertices",meshVList2,4);
			quad->addBuffer("texcoords",meshTList,2);
			quad->setFaceSize(3);
			quad->load();
			unit->addAnnotation("load quad - ");
			unit->assertTrue(quad->isLoaded());

			obj1 = new RenderObject("obj1");
			obj1->setMesh(*mesh);
			obj1->setShader(shader2);
			obj1->addUniform(*translate1);
			obj1->addUniform(*tex2_1);
			obj1->addUniform(*tex3_1);
			obj1->addUniform(*translate1);

			obj2 = new RenderObject("obj2");
			obj2->setMesh(*mesh);
			obj2->addUniform(*translate2);
			obj2->addUniform(*tex2_2);
			obj2->addUniform(*translate2);

			obj3 = new RenderObject("obj3");
			obj3->setMesh(*mesh);
			obj3->addUniform(*translate3);
			obj3->addUniform(*tex2_1);
			obj3->addUniform(*translate3);

			obj4 = new RenderObject("obj4");
			obj4->setMesh(*mesh);
			obj4->setShader(shader2);
			obj4->addUniform(*translate4);
			obj4->addUniform(*tex2_2);
			obj4->addUniform(*tex3_2);
			obj4->addUniform(*translate4);

			obj5 = new RenderObject("obj5");
			obj5->setMesh(*mesh);
			obj5->addUniform(*translate5);
			obj5->addUniform(*tex2_1);
			obj5->addUniform(*translate5);

			obj6 = new RenderObject("obj6");
			obj6->setMesh(*mesh);
			obj6->setShader(shader2);
			obj6->addUniform(*translate6);
			obj6->addUniform(*tex2_2);
			obj6->addUniform(*tex3_2);
			obj6->addUniform(*translate6);

			objquad = new RenderObject("quad");
			objquad->setMesh(*quad);

			pass1 = new Pass("pass1");
			vector<Texture *> output1;
			output1.push_back(frag1);
			output1.push_back(frag2);
			pass1->setOutput(*target1,output1);
			pass1->setShader(shader1);
			pass1->addUniform(*tex1);
			pass1->addRenderObject(*obj1);
			pass1->addRenderObject(*obj2);
			pass1->addRenderObject(*obj3);
			pass1->addRenderObject(*obj4);

			pass2 = new Pass("pass2");
			pass2->setOutput(*target2,vector<Texture *>());
			pass2->setShader(shader3);
			pass2->addUniform(*frag1);
			pass2->addUniform(*frag2);
			pass2->addRenderObject(*objquad);
	
			//start measuring time
			ptime time(second_clock::local_time());
			timeMeasurement.restart();
		}

		void reshape(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			target2->setWidth(width);
			target2->setHeight(height);
			target2->load();
		}
		
		void render(SXRenderArea &area) {
			double millies = timeMeasurement.elapsed();

			//plink with temp renderobjects
			double plink = sin(millies * Tau);
			if(plink > 0) {
				vector<RenderObject *> &temp = pass1->getTempRenderObjects();
				temp.push_back(obj5);
				temp.push_back(obj6);
			}
			pass1->render();

			pass2->render();

			if(millies > 6) {
				//leave window after five seconds
				area.stopRendering();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete shader1;
			delete shader2;
			delete shader3;
			delete tex1;
			delete tex2_1;
			delete tex2_2;
			delete tex3_1;
			delete tex3_2;
			delete frag1;
			delete frag2;
			delete translate1;
			delete translate2;
			delete translate3;
			delete translate4;
			delete translate5;
			delete translate6;
			delete target1;
			delete target2;
			delete mesh;
			delete quad;
			delete obj1;
			delete obj2;
			delete obj3;
			delete obj4;
			delete obj5;
			delete obj6;
			delete objquad;
			delete pass1;
			delete pass2;
		}

	};

	void testPass(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testpass",new ListLogger());
			Logger::setDefaultLogger("testpass");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			int argc = 1;
			char **argv = new char *[1];
			char *argvcontent = "testpass";
			argv[0] = argvcontent;

			QApplication *app1 = new QApplication(argc,argv);

			SXWidget *widget1 = new SXWidget();
			widget1->renderPeriodically(0.01f);
			widget1->setMinimumSize(600,400);
			QObject::connect(widget1,SIGNAL(finishedRendering()),widget1,SLOT(close()));
			widget1->addRenderListener(*new TestPass1(unit,logger));
			widget1->show();

			unit.addAnnotation("execute QApp 1 - ");
			unit.assertEquals(app1->exec(),0);
			delete widget1;
			delete app1;

			QApplication *app2 = new QApplication(argc,argv);

			SXWidget *widget2 = new SXWidget();
			widget2->renderPeriodically(0.01f);
			widget2->setMinimumSize(600,400);
			QObject::connect(widget2,SIGNAL(finishedRendering()),widget2,SLOT(close()));
			widget2->addRenderListener(*new TestPass2(unit,logger));
			widget2->show();

			unit.addAnnotation("execute QApp 2 - ");
			unit.assertEquals(app2->exec(),0);
			delete widget2;
			delete app2;

			delete argv;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif