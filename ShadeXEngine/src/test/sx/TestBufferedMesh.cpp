#ifndef _TEST_SX_TESTBUFFEREDMESH_CPP_
#define _TEST_SX_TESTBUFFEREDMESH_CPP_

/**
 * buffered mesh test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <boost/date_time/posix_time/posix_time.hpp>
using namespace sx;
using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::posix_time::to_simple_string;

namespace sxengine {

	class TestBufferedMesh1: public SXRenderListener {
	private:
		unsigned int width;
		unsigned int height;

		Shader *shader;
		BufferedMesh *mesh;

		XUnit *unit;
		ListLogger *list;
		timer timeMeasurement;
		TestBufferedMesh1(const TestBufferedMesh1 &);
		TestBufferedMesh1 &operator = (const TestBufferedMesh1 &);
	public:
		TestBufferedMesh1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = (unsigned int)area.getWidth();
			this->height = (unsigned int)area.getHeight();

			unit->addMarkup("test saving mesh");

			float vArray1[] = {
				0,0,0,
				1,0,0,
				0,1,0,
				0,1,0,
				1,0,0,
				1,1,0,
				0,1,1,
				0,1,2,
				0,2,2
			};
			vector<float> vVector1(vArray1,vArray1+27);
			float nArray1[] = {
				-1,-1,1,
				1,-1,1,
				-1,1,1,
				-1,1,1,
				1,-1,1,
				1,1,1,
				1,0,0,
				1,0,0,
				1,0,0
			};
			vector<float> nVector1(nArray1,nArray1+27);
			float tArray1[]= {
				0.5f,0.5f,
				0.75f,0.5f,
				0.5f,0.6f,
				0.5f,0.6f,
				0.75f,0.5f,
				0.75f,0.6f,
				0,0,
				0.5f,0,
				0.5f,0.5f
			};
			vector<float> tVector1(tArray1,tArray1+18);

			BufferedMesh *smesh1 = new BufferedMesh("mesh.someid");
			smesh1->setMaxVertexCount(9);
			smesh1->addBuffer("vertices",vector<float>(),3);
			smesh1->addBuffer("normals",vector<float>(),3);
			smesh1->addBuffer("texcoords",vector<float>(),2);
			smesh1->setFaceSize(3);
					
			smesh1->load();
			unit->addAnnotation("load smesh1 - ");
			unit->assertTrue(smesh1->isLoaded());

			unit->addAnnotation("smesh1 vertexCount - ");
			unit->assertEquals(smesh1->getVertexCount(),(unsigned int)0);

			unit->addAnnotation("smesh1 maxVertexCount - ");
			unit->assertEquals(smesh1->getMaxVertexCount(),(unsigned int)9);

			smesh1->setVertexCount(8);
			unit->addAnnotation("smesh1 set vertexCount - ");
			unit->assertEquals(smesh1->getVertexCount(),(unsigned int)6);

			smesh1->setVertexCount(10432234);
			unit->addAnnotation("smesh1 set vertexCount - ");
			unit->assertEquals(smesh1->getVertexCount(),(unsigned int)9);

			VertexBuffer *vb1 = smesh1->getBuffer("vertices");
			VertexBuffer *nb1 = smesh1->getBuffer("normals");
			VertexBuffer *tb1 = smesh1->getBuffer("texcoords");
			if(vb1 != 0 && nb1 != 0 && tb1 != 0) {
				float *buffer = vb1->unlockWrite();
				for(unsigned int i=0 ; i<9*3 ; i++) {
					buffer[i] = vArray1[i];
				}
				vb1->lock();
				buffer = nb1->unlockWrite();
				for(unsigned int i=0 ; i<9*3 ; i++) {
					buffer[i] = nArray1[i];
				}
				nb1->lock();
				buffer = tb1->unlockWrite();
				for(unsigned int i=0 ; i<9*2 ; i++) {
					buffer[i] = tArray1[i];
				}
				tb1->lock();
			} else {
				unit->addAnnotation("smesh1 has buffers vertices, normals and texcoords - ");
				unit->assertTrue(false);
			}

			try {
				unit->addAnnotation("save smesh1 - ");
				smesh1->save("../data/test/sxengine/files/output/omesh1.dae");
				smesh1->save("../data/test/sxengine/files/output/omesh1.c");
				unit->assertTrue(true);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			delete smesh1;

			BufferedMesh *smesh2 = new BufferedMesh("mesh");
			smesh2->addBuffers("../data/test/sxengine/files/output/omesh1.dae");
			smesh2->load();
			unit->addAnnotation("read written mesh into smesh2 - ");
			unit->assertTrue(smesh2->isLoaded());

			unit->addAnnotation("smesh2 facesize - ");
			unit->assertEquals(smesh2->getFaceSize(),(unsigned int)3);
			
			unit->addAnnotation("smesh2 vertexCount - ");
			unit->assertEquals(smesh2->getVertexCount(),(unsigned int)9);

			unit->addAnnotation("smesh2 maxVertexCount - ");
			unit->assertEquals(smesh2->getMaxVertexCount(),(unsigned int)9);

			VertexBuffer *rvb2 = smesh2->getBuffer("vertices");
			VertexBuffer *rnb2 = smesh2->getBuffer("normals");
			VertexBuffer *rtb2 = smesh2->getBuffer("texcoords");
			if(rvb2 != 0 && rnb2 != 0 && rtb2 != 0) {
				const float *buffer = rvb2->unlockRead();
				for(unsigned int i=0 ; i<9*3 ; i++) {
					unit->addAnnotation("smesh2 compare vertices - ");
					unit->assertEquals(buffer[i],vArray1[i],0.00001f);
				}
				rvb2->lock();
				buffer = rnb2->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					Vector n1(nArray1[3*i],nArray1[3*i+1],nArray1[3*i+2]);
					n1.normalize();
					for(unsigned int j=0 ; j<3 ; j++) {
						unit->addAnnotation("smesh2 compare normals - ");
						unit->assertEquals(buffer[3*i+j],n1[j],0.00001f);
					}
				}
				rnb2->lock();
				buffer = rtb2->unlockRead();
				for(unsigned int i=0 ; i<9*2 ; i++) {
					unit->addAnnotation("smesh2 compare texcoords - ");
					unit->assertEquals(buffer[i],tArray1[i],0.00001f);
				}
				rnb2->lock();
			} else {
				unit->addAnnotation("smesh2 has buffers vertices, normals and texcoords - ");
				unit->assertTrue(false);
			}

			delete smesh2;

			try {
				ShadeX shadeX;
				shadeX.addResources("../data/test/sxengine/files/output/omesh1.c");
				shadeX.load();
				BufferedMesh &sxmesh2 = shadeX.getBufferedMesh("mesh.someid");

				unit->addAnnotation("sxmesh2 facesize - ");
				unit->assertEquals(sxmesh2.getFaceSize(),(unsigned int)3);
				
				unit->addAnnotation("sxmesh2 vertexCount - ");
				unit->assertEquals(sxmesh2.getVertexCount(),(unsigned int)9);

				unit->addAnnotation("sxmesh2 maxVertexCount - ");
				unit->assertEquals(sxmesh2.getMaxVertexCount(),(unsigned int)9);

				rvb2 = sxmesh2.getBuffer("vertices");
				rnb2 = sxmesh2.getBuffer("normals");
				rtb2 = sxmesh2.getBuffer("texcoords");
				if(rvb2 != 0 && rnb2 != 0 && rtb2 != 0) {
					const float *buffer = rvb2->unlockRead();
					for(unsigned int i=0 ; i<9*3 ; i++) {
						unit->addAnnotation("sxmesh2 compare vertices - ");
						unit->assertEquals(buffer[i],vArray1[i],0.00001f);
					}
					rvb2->lock();
					buffer = rnb2->unlockRead();
					for(unsigned int i=0 ; i<9*3 ; i++) {
						unit->addAnnotation("sxmesh2 compare normals - ");
						unit->assertEquals(buffer[i],nArray1[i],0.00001f);
					}
					rnb2->lock();
					buffer = rtb2->unlockRead();
					for(unsigned int i=0 ; i<9*2 ; i++) {
						unit->addAnnotation("sxmesh2 compare texcoords - ");
						unit->assertEquals(buffer[i],tArray1[i],0.00001f);
					}
					rnb2->lock();
				} else {
					unit->addAnnotation("sxmesh2 has buffers vertices, normals and texcoords - ");
					unit->assertTrue(false);
				}

			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			float b2Array3[] = {
				0.2f,
				0.9f,
				0.1f,
				0.322f,
				0.524f,
				0.765f,
				0.2112f,
				0.12323f,
				0.0454f
			};
			float b4Array3[] = {
				0.923f,
				0.6554f,
				0.22343f,
				0.555f,
				0.4523f,
				0.8343f,
				0.77723f,
				0.66555f,
				0.529292f
			};
			vector<float> b2Vector3(b2Array3,b2Array3+9);
			vector<float> b4Vector3(b4Array3,b4Array3+9);

			BufferedMesh *smesh3 = new BufferedMesh("mesh.anotherid");
			smesh3->addBuffer("vertices",vVector1,3);
			smesh3->addBuffer("bone2",b2Vector3,1);
			smesh3->addBuffer("bone4",b4Vector3,1);
			smesh3->setFaceSize(3);
			smesh3->load();
			unit->addAnnotation("load smesh3 - ");
			unit->assertTrue(smesh3->isLoaded());

			Matrix skeleton_m = Matrix().translate(Vector(0,0,1));
			Matrix bone1_m = Matrix().translate(Vector(0,0,1));
			Matrix bone2_m = Matrix().translate(Vector(0,0,1));
			Matrix bone3_m = Matrix().translate(Vector(0,-1,1));
			Matrix bone4_m = Matrix().translate(Vector(0,1,1));
			Matrix bone5_m = Matrix().translate(Vector(0,-1,1));
			Matrix bone6_m = Matrix().translate(Vector(0,1,1));
			Matrix invskeleton_m = skeleton_m;
			Matrix invbone1_m = bone1_m;
			Matrix invbone2_m = bone2_m;
			Matrix invbone3_m = bone3_m;
			Matrix invbone4_m = bone4_m;
			Matrix invbone5_m = bone5_m;
			Matrix invbone6_m = bone6_m;
			invskeleton_m.inverse();
			invbone1_m.inverse();
			invbone2_m.inverse();
			invbone3_m.inverse();
			invbone4_m.inverse();
			invbone5_m.inverse();
			invbone6_m.inverse();

			Bone sb5;
			sb5.ID = "bone5";
			sb5.parentTransform = bone5_m;

			Bone sb6;
			sb6.ID = "bone6";
			sb6.parentTransform = bone6_m;

			Bone sb3;
			sb3.ID = "bone3";
			sb3.parentTransform = bone3_m;
			sb3.bones.push_back(sb5);
			sb3.bones.push_back(sb6);

			Bone sb4;
			sb4.ID = "bone4";
			sb4.parentTransform = bone4_m;

			Bone sb2;
			sb2.ID = "bone2";
			sb2.parentTransform = bone2_m;
			sb2.bones.push_back(sb3);
			sb2.bones.push_back(sb4);

			Bone sb1;
			sb1.ID = "bone1";
			sb1.parentTransform = bone1_m;
			sb1.bones.push_back(sb2);

			vector<Bone> bones1;
			bones1.push_back(sb1);
			smesh3->setSkeleton(bones1);
			smesh3->setSkeletonMatrix(skeleton_m);
			
			try {
				unit->addAnnotation("save smesh3 - ");
				smesh3->save("../data/test/sxengine/files/output/omesh2.dae");
				smesh3->save("../data/test/sxengine/files/output/omesh2.c");
				unit->assertTrue(true);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			delete smesh3;

			BufferedMesh *smesh4 = new BufferedMesh("mesh");
			smesh4->addBuffers("../data/test/sxengine/files/output/omesh2.dae");
			smesh4->load();
			unit->addAnnotation("read written mesh into smesh4 - ");
			unit->assertTrue(smesh4->isLoaded());

			unit->addAnnotation("smesh4 facesize - ");
			unit->assertEquals(smesh4->getFaceSize(),(unsigned int)3);
			
			unit->addAnnotation("smesh4 vertexCount - ");
			unit->assertEquals(smesh4->getVertexCount(),(unsigned int)9);

			unit->addAnnotation("smesh4 maxVertexCount - ");
			unit->assertEquals(smesh4->getMaxVertexCount(),(unsigned int)9);
			VertexBuffer *rvb4 = smesh4->getBuffer("vertices");
			VertexBuffer *rb1b4 = smesh4->getBuffer("bone1");
			VertexBuffer *rb2b4 = smesh4->getBuffer("bone2");
			VertexBuffer *rb3b4 = smesh4->getBuffer("bone3"); 
			VertexBuffer *rb4b4 = smesh4->getBuffer("bone4");
			VertexBuffer *rb5b4 = smesh4->getBuffer("bone5");
			VertexBuffer *rb6b4 = smesh4->getBuffer("bone6");
			if(rvb4 != 0 && rb1b4 != 0 && rb2b4 != 0 && rb3b4 != 0 && rb4b4 != 0 && rb5b4 != 0 && rb6b4 != 0) {
				const float *buffer = rvb4->unlockRead();
				for(unsigned int i=0 ; i<9*3 ; i++) {
					unit->addAnnotation("smesh4 compare vertices - ");
					unit->assertEquals(buffer[i],vArray1[i],0.00001f);
				}
				rvb4->lock();
				buffer = rb1b4->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					unit->addAnnotation("smesh4 compare bone1 weights - ");
					unit->assertEquals(buffer[i],0.0f,0.00001f);
				}
				rb1b4->lock();
				buffer = rb3b4->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					unit->addAnnotation("smesh4 compare bone3 weights - ");
					unit->assertEquals(buffer[i],0.0f,0.00001f);
				}
				rb3b4->lock();
				buffer = rb5b4->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					unit->addAnnotation("smesh4 compare bone5 weights - ");
					unit->assertEquals(buffer[i],0.0f,0.00001f);
				}
				rb5b4->lock();
				buffer = rb6b4->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					unit->addAnnotation("smesh4 compare bone6 weights - ");
					unit->assertEquals(buffer[i],0.0f,0.00001f);
				}
				rb6b4->lock();
				buffer = rb2b4->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					unit->addAnnotation("smesh4 compare bone2 weights - ");
					unit->assertEquals(buffer[i],b2Array3[i],0.00001f);
				}
				rb2b4->lock();
				buffer = rb4b4->unlockRead();
				for(unsigned int i=0 ; i<9 ; i++) {
					unit->addAnnotation("smesh4 compare bone4 weights - ");
					unit->assertEquals(buffer[i],b4Array3[i],0.00001f);
				}
				rb4b4->lock();
			} else {
				unit->addAnnotation("smesh4 has buffer vertices - ");
				unit->assertTrue(false);
			}

			Matrix inv1 = invbone1_m * invskeleton_m;
			Matrix inv2 = invbone2_m * invbone1_m * invskeleton_m;
			Matrix inv3 = invbone3_m * invbone2_m * invbone1_m * invskeleton_m;
			Matrix inv4 = invbone4_m * invbone2_m * invbone1_m * invskeleton_m;
			Matrix inv5 = invbone5_m * invbone3_m * invbone2_m * invbone1_m * invskeleton_m;
			Matrix inv6 = invbone6_m * invbone3_m * invbone2_m * invbone1_m * invskeleton_m;
			try {
				Skeleton skeleton("skeleton");
				skeleton.addBones(*smesh4);
				skeleton.load();
				if(!skeleton.isLoaded()) {
					throw Exception();
				}

				const Bone &bone1 = skeleton.getBone("bone1");
				const Bone &bone2 = skeleton.getBone("bone2");
				const Bone &bone3 = skeleton.getBone("bone3");
				const Bone &bone4 = skeleton.getBone("bone4");
				const Bone &bone5 = skeleton.getBone("bone5");
				const Bone &bone6 = skeleton.getBone("bone6");

				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 1 - ");
					unit->assertEquals(bone1.inverseBindPoseMatrix[i],inv1[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 2 - ");
					unit->assertEquals(bone2.inverseBindPoseMatrix[i],inv2[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 3 - ");
					unit->assertEquals(bone3.inverseBindPoseMatrix[i],inv3[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 4 - ");
					unit->assertEquals(bone4.inverseBindPoseMatrix[i],inv4[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 5 - ");
					unit->assertEquals(bone5.inverseBindPoseMatrix[i],inv5[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 6 - ");
					unit->assertEquals(bone6.inverseBindPoseMatrix[i],inv6[i],0.00001f);
				}

			} catch(Exception &e) {
				unit->addAnnotation("couldn't load smesh4's skeleton - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			delete smesh4;

			try {
				ShadeX shadeX;
				shadeX.addResources("../data/test/sxengine/files/output/omesh2.c");
				shadeX.load();
				BufferedMesh &sxmesh4 = shadeX.getBufferedMesh("mesh.anotherid");
				Skeleton &sxskeleton = shadeX.getSkeleton("mesh.anotherid.skeleton");

				unit->addAnnotation("sxmesh4 facesize - ");
				unit->assertEquals(sxmesh4.getFaceSize(),(unsigned int)3);
				
				unit->addAnnotation("sxmesh4 vertexCount - ");
				unit->assertEquals(sxmesh4.getVertexCount(),(unsigned int)9);

				unit->addAnnotation("smesh4 maxVertexCount - ");
				unit->assertEquals(sxmesh4.getMaxVertexCount(),(unsigned int)9);
				rvb4 = sxmesh4.getBuffer("vertices");
				rb2b4 = sxmesh4.getBuffer("bone2");
				rb4b4 = sxmesh4.getBuffer("bone4");
				if(rvb4 != 0 && rb2b4 != 0 && rb4b4 != 0) {
					const float *buffer = rvb4->unlockRead();
					for(unsigned int i=0 ; i<9*3 ; i++) {
						unit->addAnnotation("sxmesh4 compare vertices - ");
						unit->assertEquals(buffer[i],vArray1[i],0.00001f);
					}
					rvb4->lock();
					buffer = rb2b4->unlockRead();
					for(unsigned int i=0 ; i<9 ; i++) {
						unit->addAnnotation("sxmesh4 compare bone2 weights - ");
						unit->assertEquals(buffer[i],b2Array3[i],0.00001f);
					}
					rb2b4->lock();
					buffer = rb4b4->unlockRead();
					for(unsigned int i=0 ; i<9 ; i++) {
						unit->addAnnotation("sxmesh4 compare bone4 weights - ");
						unit->assertEquals(buffer[i],b4Array3[i],0.00001f);
					}
					rb4b4->lock();
				} else {
					unit->addAnnotation("sxmesh4 has buffer vertices - ");
					unit->assertTrue(false);
				}
				const Bone &bone1 = sxskeleton.getBone("bone1");
				const Bone &bone2 = sxskeleton.getBone("bone2");
				const Bone &bone3 = sxskeleton.getBone("bone3");
				const Bone &bone4 = sxskeleton.getBone("bone4");
				const Bone &bone5 = sxskeleton.getBone("bone5");
				const Bone &bone6 = sxskeleton.getBone("bone6");

				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 1 in sxkeleton - ");
					unit->assertEquals(bone1.inverseBindPoseMatrix[i],inv1[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 2 in sxkeleton - ");
					unit->assertEquals(bone2.inverseBindPoseMatrix[i],inv2[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 3 in sxkeleton - ");
					unit->assertEquals(bone3.inverseBindPoseMatrix[i],inv3[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 4 in sxkeleton - ");
					unit->assertEquals(bone4.inverseBindPoseMatrix[i],inv4[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 5 in sxkeleton - ");
					unit->assertEquals(bone5.inverseBindPoseMatrix[i],inv5[i],0.00001f);
				}
				for(unsigned int i=0 ; i<16 ; i++) {
					unit->addAnnotation("compare inverse bind pose matrix of bone 6 in sxkeleton - ");
					unit->assertEquals(bone6.inverseBindPoseMatrix[i],inv6[i],0.00001f);
				}
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			unit->addMarkup("loading phase");

			shader = new Shader("s1");
			shader->addShaderFile("../data/test/sxengine/files/mesh.vp",VERTEX);
			shader->addShaderFile("../data/test/sxengine/files/mesh.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			unit->addAnnotation("loaded shader - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					string msg = list->getStringEntry("Shader::load",i);
					unit->addAnnotation(msg);
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader->isLoaded());
			float meshAttribArray1[] = {
				-1,0,0,1,
				0,1,0,1,
				0,0,0,1,
				0,0,0,1,
				1,0,0,1,
				0,-1,0,1
			};
			vector<float> meshAttrib1(meshAttribArray1,meshAttribArray1 + 24);
			float meshAttribArray2[] = {
				1,1,0,
				0,1,0,
				1,0,0,
				1,0,0,
				1,0,1,
				0,0,1
			};
			vector<float> meshAttrib2(meshAttribArray2,meshAttribArray2 + 18);
			float incompatibleMeshArray[] = {
				1,2,3,
				2,3,4,
				4,3,2,
				3,3,4,
				5,5,4
			};
			vector<float> incompatibleMesh(incompatibleMeshArray,incompatibleMeshArray+15);

			mesh = new BufferedMesh("fail");
			mesh->setFaceSize(3);
			mesh->addBuffer("a",meshAttrib1,3);
			mesh->addBuffer("b",meshAttrib2,4);
			mesh->addBuffer("c",incompatibleMesh,3);
			list->getMarkups().clear();
			mesh->load();
			unit->addAnnotation("incompatible buffers - ");
			unit->assertTrue(!mesh->isLoaded());
			delete mesh;

			mesh = new BufferedMesh("fail");
			mesh->setFaceSize(3);
			mesh->addBuffer("a",incompatibleMesh,4);
			mesh->addBuffers("../data/test/sxengine/files/mesh.ply",true);
			list->getMarkups().clear();
			mesh->load();
			unit->addAnnotation("incompatible to file buffer - make small buffer as large as the buffers loaded from a file - ");
			unit->assertTrue(mesh->isLoaded());

			unit->addAnnotation("number of buffers - ");
			unit->assertEquals((unsigned int)mesh->getBuffers().size(),(unsigned int)3);
			delete mesh;
			
			mesh = new BufferedMesh("fail");
			mesh->setFaceSize(3);
			mesh->addBuffers("../data/test/sxengine/files/incompatible.ply");
			mesh->addBuffers("../data/test/sxengine/files/mesh.ply",true);
			list->getMarkups().clear();
			mesh->load();
			unit->addAnnotation("incompatible file buffers - ");
			unit->assertTrue(!mesh->isLoaded());
			try {
				unit->addAnnotation("incompatible file buffers - release one buffer - ");
				unit->assertEquals(list->getUintEntry("BufferedMesh::unload",0) , (unsigned int)1);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete mesh;

			mesh = new BufferedMesh("buffer");
			mesh->setFaceSize(3);
			mesh->addBuffers("../data/test/sxengine/files/mesh.ply");
			mesh->load();
			unit->addAnnotation("first load - success - ");
			unit->assertTrue(mesh->isLoaded());
			mesh->addBuffers("../data/test/sxengine/files/mesh.ply");
			mesh->addBuffers("../data/test/sxengine/files/mesh.ply");
			list->getMarkups().clear();
			mesh->load();
			unit->addAnnotation("second load - fail - ");
			unit->assertTrue(!mesh->isLoaded());
			try {
				unit->addAnnotation("duplicate attribute - release two buffers - ");
				unit->assertEquals(list->getUintEntry("BufferedMesh::unload",0) , (unsigned int)3);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			mesh->addBuffer("vertices", meshAttrib1, 4);
			mesh->addBuffer("colors", meshAttrib2, 3);
			mesh->addBuffers("../data/test/sxengine/files/mesh.ply",true);
			list->getMarkups().clear();
			mesh->load();
			try {
				unit->addAnnotation("successful load - three buffers - ");
				unit->assertEquals(list->getUintEntry("BufferedMesh::load",0) , (unsigned int)4);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			unit->addAnnotation("loaded mesh - ");
			unit->assertTrue(mesh->isLoaded());

			//start measuring time
			ptime time(second_clock::local_time());
			timeMeasurement.restart();
		}

		void reshape(SXRenderArea &area) {
			this->width = (unsigned int)area.getWidth();
			this->height = (unsigned int)area.getHeight();
		}

		void render(SXRenderArea &area) {
			//prepare surface
			RenderTarget::setViewport(0,0,width,height);
			RenderTarget::clearTarget();

			shader->use();
			mesh->render(*shader);

			double millies = timeMeasurement.elapsed();
			if(millies > 5) {
				//leave window after five seconds
				area.stopRendering();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");

			delete shader;
			list->getMarkups().clear();
			delete mesh;
			try {
				unit->addAnnotation("destructor - ");
				unit->assertEquals(list->getUintEntry("BufferedMesh::unload",0) , (unsigned int)4);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
		}

	};

	void testBufferedMesh(const string log) {
		XUnit unit(new FileLogger(log));
		try {
			Logger::addLogger("testbufferedmesh",new ListLogger());
			Logger::setDefaultLogger("testbufferedmesh");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			int argc = 1;
			char **argv = new char *[1];
			char *argvarg = "testbufferedmesh";
			argv[1] = argvarg;
			QApplication app(argc,argv);

			SXWidget widget;
			QObject::connect(&widget,SIGNAL(finishedRendering()),&widget,SLOT(close()));
			widget.setMinimumSize(600,400);
			widget.renderPeriodically(0.01f);
			widget.addRenderListener(*new TestBufferedMesh1(unit,logger));
			widget.show();

			unit.addAnnotation("execute QApp - ");
			unit.assertEquals(app.exec(),0);

			delete argv;

			unit.addMarkup("image validation");
			unit.assertInputYes("Could you see two triangles with shades of red, green, blue, yellow, pink?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif