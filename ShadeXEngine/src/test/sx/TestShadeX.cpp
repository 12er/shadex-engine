#ifndef _TEST_SX_TESTSHADEX_CPP_
#define _TEST_SX_TESTSHADEX_CPP_

/**
 * ShadeX test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QPushButton>
#include <QGridLayout>
#include <cmath>
#include <boost/date_time/posix_time/posix_time.hpp>
using namespace sx;
using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::posix_time::to_simple_string;
using namespace std;
using namespace sx;

namespace sxengine {

	//TODO:
	//TEST DECONSTRUCTION
	//TEST COUNT OF RESOURCES
	class TestShadeX1: public SXRenderListener {
	private:
		XUnit *unit;
		ListLogger *list;
		TestShadeX1(const TestShadeX1 &);
		TestShadeX1 &operator = (const TestShadeX1 &);
	public:

		TestShadeX1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test loading resources from a file");

			try {
				ShadeX *s1 = new ShadeX();

				s1->addResources("../data/test/sxengine/files/resources1.c");
				list->getMarkups().clear();
				try {
					s1->load();
					unit->addAnnotation("at least one resourse can't be loaded - ");
					unit->assertTrue(false);
				} catch(Exception &e) {
					unit->addAnnotation("at least one resourse can't be loaded - ");
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(true);
				}
				//unit->addAnnotation("number of resources - ");
				//unit->assertEquals(list->getUintEntry("ShadeX::load()/resources",0),(unsigned int)3);
				unit->addAnnotation("number of shaders - ");
				unit->assertEquals(list->getUintEntry("ShadeX::load()/shaders",0),(unsigned int)3);
				unit->addAnnotation("number of shaders to be loaded in one program - ");
				unit->assertEquals(list->getUintEntry("effect1.shader1",0),(unsigned int)2);
				unit->addAnnotation("number of shaders to be loaded in one program - ");
				unit->assertEquals(list->getUintEntry("effect1.shader2",0),(unsigned int)5);
				unit->addAnnotation("number of shaders to be loaded in one program - ");
				unit->assertEquals(list->getUintEntry("effect1.shader3",0),(unsigned int)0);				
				Shader &shader1 = s1->getShader("effect1.shader1");
				unit->addAnnotation("loaded shader - ");
				unit->assertTrue(shader1.isLoaded());
				Shader &shader2 = s1->getShader("effect1.shader2");
				unit->addAnnotation("loaded shader - ");
				unit->assertTrue(shader2.isLoaded());
				Shader &shader3 = s1->getShader("effect1.shader3");
				unit->addAnnotation("shader not loaded - ");
				unit->assertTrue(!shader3.isLoaded());
				UniformFloat &float1 = s1->getUniformFloat("float.float1");
				unit->addAnnotation("float - ");
				unit->assertEquals(float1.value,3.14159f);
				UniformVector &vec0 = s1->getUniformVector("vector.v0");
				unit->addAnnotation("vec0 - ");
				unit->assertTrue(vec0.equals(Vector()));
				UniformVector &vec1 = s1->getUniformVector("vector.v1");
				unit->addAnnotation("vec1 - ");
				unit->assertTrue(vec1.equals(Vector(1)));
				UniformVector &vec2 = s1->getUniformVector("vector.v2");
				unit->addAnnotation("vec2 - ");
				unit->assertTrue(vec2.equals(Vector(1,3,0,1)));
				UniformVector &vec3 = s1->getUniformVector("vector.v3");
				unit->addAnnotation("vec3 - ");
				unit->assertTrue(vec3.equals(Vector(1,3,3,1)));
				UniformVector &vec4 = s1->getUniformVector("vector.v4");
				unit->addAnnotation("vec4 - ");
				unit->assertTrue(vec4.equals(Vector(1,3,3,7)));
				UniformMatrix &mat0 = s1->getUniformMatrix("matrix.m0");
				unit->addAnnotation("mat0 - ");
				unit->assertTrue(mat0.equals(Matrix()));
				UniformMatrix &mat1 = s1->getUniformMatrix("matrix.m1");
				unit->addAnnotation("mat1 - ");
				unit->assertTrue(mat1.equals(Matrix(5)));
				UniformMatrix &mat2 = s1->getUniformMatrix("matrix.m2");
				unit->addAnnotation("mat2 - ");
				unit->assertTrue(mat2.equals(Matrix(
					1 , 2,
					3 , 4
					)));
				UniformMatrix &mat3 = s1->getUniformMatrix("matrix.m3");
				unit->addAnnotation("mat3 - ");
				unit->assertTrue(mat3.equals(Matrix(
					1 , 2 , 3 ,
					4 , 5 , 6 ,
					7 , 8 , 9
					)));
				UniformMatrix &mat4 = s1->getUniformMatrix("matrix.m4");
				unit->addAnnotation("mat4 - ");
				unit->assertTrue(mat4.equals(Matrix(
					1 , 2 , 3 , 4 ,
					5 , 6 , 7 , 8 ,
					9 , 10 , 11 , 12 ,
					13 , 14 , 15 , 16
					)));
				BufferedMesh &mesh = s1->getBufferedMesh("some.mesh");
				unit->addAnnotation("some.mesh - loaded - ");
				unit->assertTrue(mesh.isLoaded());
				unit->addAnnotation("some.mesh - facesize - ");
				unit->assertEquals(mesh.getFaceSize(),(unsigned int)3);
				unit->addAnnotation("some.mesh - facesize - ");
				unit->assertEquals(mesh.getVertexCount(),(unsigned int)6);
				Texture &tex1 = s1->getTexture("tex.tex1");
				unit->addAnnotation("tex1 - loaded - ");
				unit->assertTrue(tex1.isLoaded());
				unit->addAnnotation("tex1 - width - ");
				unit->assertEquals((unsigned int)tex1.getWidth(),(unsigned int)600);
				unit->addAnnotation("tex1 - height - ");
				unit->assertEquals((unsigned int)tex1.getHeight(),(unsigned int)400);
				unit->addAnnotation("tex1 - pixelformat - ");
				unit->assertEquals((unsigned int)tex1.getPixelFormat(),(unsigned int)FLOAT_RGBA);
				Texture &tex2 = s1->getTexture("tex.tex2");
				unit->addAnnotation("tex2 - loaded - ");
				unit->assertTrue(tex2.isLoaded());
				unit->addAnnotation("tex2 - width - ");
				unit->assertEquals((unsigned int)tex2.getWidth(),(unsigned int)50);
				unit->addAnnotation("tex2 - height - ");
				unit->assertEquals((unsigned int)tex2.getHeight(),(unsigned int)20);
				unit->addAnnotation("tex2 - pixelformat - ");
				unit->assertEquals((unsigned int)tex2.getPixelFormat(),(unsigned int)BYTE_RGBA);
				Texture &tex3 = s1->getTexture("tex.tex3");
				unit->addAnnotation("tex3 - loaded - ");
				unit->assertTrue(tex3.isLoaded());
				unit->addAnnotation("tex3 - width - ");
				unit->assertEquals((unsigned int)tex3.getWidth(),(unsigned int)400);
				unit->addAnnotation("tex3 - height - ");
				unit->assertEquals((unsigned int)tex3.getHeight(),(unsigned int)300);
				unit->addAnnotation("tex3 - pixelformat - ");
				unit->assertEquals((unsigned int)tex3.getPixelFormat(),(unsigned int)BYTE_RGBA);
				RenderTarget &tg1 = s1->getRenderTarget("target.t1");
				unit->addAnnotation("target1 - loaded - ");
				unit->assertTrue(tg1.isLoaded());
				unit->addAnnotation("target1 - width - ");
				unit->assertEquals((unsigned int)tg1.getWidth(),(unsigned int)320);
				unit->addAnnotation("target1 - height - ");
				unit->assertEquals((unsigned int)tg1.getHeight(),(unsigned int)210);
				unit->addAnnotation("target1 - renderToDisplay - ");
				unit->assertTrue(!tg1.isRenderingToDisplay());
				RenderTarget &tg2 = s1->getRenderTarget("target.t2");
				unit->addAnnotation("target2 - loaded - ");
				unit->assertTrue(tg2.isLoaded());
				unit->addAnnotation("target2 - width - ");
				unit->assertEquals((unsigned int)tg2.getWidth(),(unsigned int)500);
				unit->addAnnotation("target2 - height - ");
				unit->assertEquals((unsigned int)tg2.getHeight(),(unsigned int)400);
				unit->addAnnotation("target2 - renderToDisplay - ");
				unit->assertTrue(tg2.isRenderingToDisplay());
				RenderObject &obj1 = s1->getRenderObject("object.o1");
				Shader *o1Shader = obj1.getShader();
				if(o1Shader != 0) {
					unit->addAnnotation("obj1 shader - ");
					unit->assertEquals(o1Shader->getID(),"effect1.shader1");
					unit->addAnnotation("obj1 shader - loaded - ");
					unit->assertTrue(o1Shader->isLoaded());
				} else {
					unit->addAnnotation("no obj shader - ");
					unit->assertTrue(false);
				}
				Mesh *o1Mesh = obj1.getMesh();
				if(o1Mesh != 0) {
					unit->addAnnotation("obj1 mesh - ");
					unit->assertEquals(o1Mesh->getID() , "some.mesh");
					unit->addAnnotation("obj1 mesh - loaded - ");
					unit->assertTrue(o1Mesh->isLoaded());
				} else {
					unit->addAnnotation("no obj mesh - ");
					unit->assertTrue(false);
				}
				UniformFloat *failfloat = 0;
				try {
					failfloat = new UniformFloat("object.o1.unloaded");
					s1->addResource("object.o1.unloaded",*failfloat);
					unit->addAnnotation("should not take float, because of id - ");
					unit->assertTrue(false);
				} catch(Exception &e) {
					delete failfloat;
					failfloat = 0;
					unit->addAnnotation("id of unloaded taken -> unloaded is part of object - ");
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(true);
				}
				unit->addAnnotation("obj1 - set uniform tex name - ");
				unit->assertEquals(tex1.getUniformName("object.o1"),"otex1");
				unit->addAnnotation("obj1 - set uniform float name - ");
				unit->assertEquals(float1.getUniformName("object.o1"),"ofloat");
				unit->addAnnotation("obj1 - set uniform vec name - ");
				unit->assertEquals(vec2.getUniformName("object.o1"),"ovec1");
				unit->addAnnotation("obj1 - set uniform vec name - ");
				unit->assertEquals(vec3.getUniformName("object.o1"),"ovec2");
				unit->addAnnotation("obj1 - set uniform mat name - ");
				unit->assertEquals(mat3.getUniformName("object.o1"),"omat1");
				unit->addAnnotation("obj1 - set uniform mat name - ");
				unit->assertEquals(mat4.getUniformName("object.o1"),"omat2");
				unit->addAnnotation("obj1 - set uniformset mat name - ");
				unit->assertEquals(mat2.getUniformName("object.o1"),"omat3");
				unit->addAnnotation("obj1 - set uniformset mat name - ");
				unit->assertEquals(mat1.getUniformName("object.o1"),"omat4");
				unit->addAnnotation("obj1 - set uniformset vec name - ");
				unit->assertEquals(vec4.getUniformName("object.o1"),"omat3");
				unit->addAnnotation("obj1 - uniformset 1337 size - ");
				unit->assertEquals(obj1.getUniformSet(1337).size(),(unsigned int)2);
				unit->addAnnotation("obj1 - uniformset 7331 size - ");
				unit->assertEquals(obj1.getUniformSet(7331).size(),(unsigned int)1);
				Pass &pass1 = s1->getPass("pass1");
				Pass &pass2 = s1->getPass("pass2");
				Pass &pass3 = s1->getPass("passWithoutObj1");

				list->getMarkups().clear();
				pass1.removeRenderObject("notexisting");
				try {
					unit->addAnnotation("number of renderobjects in pass1 - ");
					unit->assertEquals(list->getUintEntry("Pass::removeRenderObject",0),(unsigned int)1);
				} catch(Exception &e) {
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(false);
				}

				list->getMarkups().clear();
				pass2.removeRenderObject("notexisting");
				try {
					unit->addAnnotation("number of renderobjects in pass2 - ");
					unit->assertEquals(list->getUintEntry("Pass::removeRenderObject",0),(unsigned int)1);
				} catch(Exception &e) {
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(false);
				}

				list->getMarkups().clear();
				pass3.removeRenderObject("notexisting");
				try {
					unit->addAnnotation("number of renderobjects in pass3 - ");
					unit->assertEquals(list->getUintEntry("Pass::removeRenderObject",0),(unsigned int)0);
				} catch(Exception &e) {
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(false);
				}

				unit->addAnnotation("obj1 - visibility - ");
				unit->assertTrue(obj1.isVisible());

				RenderObject &obj2 = s1->getRenderObject("object.o2");
				unit->addAnnotation("obj2 - visibility - ");
				unit->assertTrue(!obj2.isVisible());

				unit->addAnnotation("pass1 - shader - id - ");
				Shader *pass1shader = pass1.getShader();
				if(pass1shader != 0) {
					unit->assertEquals(pass1shader->getID(),"effect1.shader2");
					unit->addAnnotation("pass1 - shader - is loaded - ");
					unit->assertTrue(pass1shader->isLoaded());
				} else {
					unit->addAnnotation("pass1 should have a shader - ");
					unit->assertTrue(false);
				}
				failfloat = 0;
				try {
					failfloat = new UniformFloat("pass1.unloaded");
					s1->addResource("pass1.unloaded",*failfloat);
					unit->addAnnotation("should not take float, because of id - ");
					unit->assertTrue(false);
				} catch(Exception &e) {
					delete failfloat;
					failfloat = 0;
					unit->addAnnotation("id of unloaded taken -> unloaded isn't part of pass1 - ");
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(true);
				}
				failfloat = 0;
				try {
					failfloat = new UniformFloat("pass1.outputtex");
					s1->addResource("pass1.outputtex",*failfloat);
					unit->addAnnotation("should not take float, because of id - ");
					unit->assertTrue(false);
				} catch(Exception &e) {
					delete failfloat;
					failfloat = 0;
					unit->addAnnotation("id of outputtex taken -> outputtex isn't part of pass1 - ");
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(true);
				}
				unit->addAnnotation("pass1 - set uniform tex name - ");
				unit->assertEquals(tex1.getUniformName("pass1"),"pfloat");
				unit->addAnnotation("pass1 - set uniform float name - ");
				unit->assertEquals(float1.getUniformName("pass1"),"ptex1");
				unit->addAnnotation("pass1 - set uniform vec name - ");
				unit->assertEquals(vec2.getUniformName("pass1"),"pvec2");
				unit->addAnnotation("pass1 - set uniform vec name - ");
				unit->assertEquals(vec3.getUniformName("pass1"),"pvec1");
				unit->addAnnotation("pass1 - set uniform mat name - ");
				unit->assertEquals(mat3.getUniformName("pass1"),"pmat2");
				unit->addAnnotation("pass1 - set uniform mat name - ");
				unit->assertEquals(mat4.getUniformName("pass1"),"pmat1");
				unit->addAnnotation("pass1 - tex.tex2 - set target name - ");
				unit->assertEquals(tex2.getUniformName("pass1"),"out1");
				Texture &tex4 = s1->getTexture("tex.tex4");
				unit->addAnnotation("pass1 - tex.tex4 - set target name - ");
				unit->assertEquals(tex4.getUniformName("pass1"),"out2");
				unit->addAnnotation("pass1 - visibility - ");
				unit->assertTrue(pass1.isVisible());
				unit->addAnnotation("pass1 - visibility - ");
				unit->assertTrue(pass1.isVisible());
				unit->addAnnotation("pass1 - clears color buffer - ");
				unit->assertTrue(pass1.isClearingColorBuffer());
				unit->addAnnotation("pass1 - clears depth buffer - ");
				unit->assertTrue(pass1.isClearingDepthBuffer());

				unit->addAnnotation("pass2 - not visible - ");
				unit->assertTrue(!pass2.isVisible());
				unit->addAnnotation("pass2 - doesn't clear color buffer - ");
				unit->assertTrue(!pass2.isClearingColorBuffer());
				unit->addAnnotation("pass2 - doesn't clear depth buffer - ");
				unit->assertTrue(!pass2.isClearingDepthBuffer());

				Effect &effect1 = s1->getEffect("effect.e1");
				unit->addAnnotation("effect1 - visibility - ");
				unit->assertTrue(effect1.isVisible());
				unit->addAnnotation("effect1 - count effects - ");
				unit->assertEquals(effect1.getObjects().size(),(unsigned int)5);
				if(effect1.getObjects().size() == 5) {
					Pass *p1 = dynamic_cast<Pass *>(effect1.getObjects()[0].object);
					Pass *p2 = dynamic_cast<Pass *>(effect1.getObjects()[1].object);
					Pass *p3 = dynamic_cast<Pass *>(effect1.getObjects()[2].object);
					Pass *p4 = dynamic_cast<Pass *>(effect1.getObjects()[3].object);
					Pass *p5 = dynamic_cast<Pass *>(effect1.getObjects()[4].object);
					unit->addAnnotation("effect1 - passid - ");
					unit->assertEquals(p1->getID(),"pass2");
					unit->addAnnotation("effect1 - passid - ");
					unit->assertEquals(p2->getID(),"pass2");
					unit->addAnnotation("effect1 - passid - ");
					unit->assertEquals(p3->getID(),"pass1");
					unit->addAnnotation("effect1 - passid - ");
					unit->assertEquals(p4->getID(),"pass2");
					unit->addAnnotation("effect1 - passid - ");
					unit->assertEquals(p5->getID(),"pass1");
					Shader *s = p3->getShader();
					if(s != 0) {
						unit->addAnnotation("effect1 - pass1 - shaderID - ");
						unit->assertEquals(s->getID(),"effect1.shader2");
					} else {
						unit->addAnnotation("effect1 - pass1 - expected shader not found - ");
						unit->assertTrue(false);
					}
					unit->addAnnotation("effect1 - pass2 - visibility - ");
					unit->assertTrue(!p1->isVisible());
				}

				Effect &effect2 = s1->getEffect("effect.e2");
				unit->addAnnotation("effect2 - visibility - ");
				unit->assertTrue(!effect2.isVisible());

			} catch(Exception &e) {
				unit->addAnnotation("loading from file tests could not be completed - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			unit->addMarkup("test working with shaders");

			try {
				ShadeX *s1 = new ShadeX();

				Shader &shader1 = s1->getShader("shader1");
				shader1.addShaderFile("../data/test/sxengine/files/shader1.vp",VERTEX);
				Shader &shader2 = s1->getShader("shader2");
				Shader &shader3 = s1->getShader("shader1");
				list->getMarkups().clear();
				shader3.addShaderFile("../data/test/sxengine/files/shader1.fp",FRAGMENT);
				try {
					s1->load();
					unit->addAnnotation("at least one resourse can't be loaded - ");
					unit->assertTrue(false);
				} catch(Exception &e) {
					unit->addAnnotation("at least one resourse can't be loaded - ");
					unit->addAnnotation(e.getMessage());
					unit->assertTrue(true);
				}
				unit->addAnnotation("number of resources - ");
				unit->assertEquals(list->getUintEntry("ShadeX::load()/resources",0),(unsigned int)2);
				unit->addAnnotation("number of shaders - ");
				unit->assertEquals(list->getUintEntry("ShadeX::load()/shaders",0),(unsigned int)2);
				Shader &shader4 = s1->getShader("shader1");
				unit->addAnnotation("shader1 loaded - ");
				unit->assertTrue(shader4.isLoaded());
				Shader &shader5 = s1->getShader("shader2");
				unit->addAnnotation("shader2 loaded - ");
				unit->assertTrue(!shader5.isLoaded());
				shader5.addShaderFile("../data/test/sxengine/files/shader1.vp",VERTEX);
				shader5.addShaderFile("../data/test/sxengine/files/shader1.fp",FRAGMENT);
				s1->load();
				Shader &shader6 = s1->getShader("shader1");
				unit->addAnnotation("shader1 loaded, second time - ");
				unit->assertTrue(shader6.isLoaded());
				Shader &shader7 = s1->getShader("shader2");
				unit->addAnnotation("shader2 loaded, second time - ");
				unit->assertTrue(shader7.isLoaded());
				list->getMarkups().clear();
				s1->clear("shader1");
				unit->addAnnotation("number of resources after deleting shader1 - ");
				unit->assertEquals(list->getUintEntry("ShadeX::clear()/resources",0),(unsigned int)1);
				unit->addAnnotation("number of shaders after deleting shader1 - ");
				unit->assertEquals(list->getUintEntry("ShadeX::clear()/shaders",0),(unsigned int)1);
				list->getMarkups().clear();
				s1->clear();
				unit->addAnnotation("number of resources after clear - ");
				unit->assertEquals(list->getUintEntry("ShadeX::clear()/resources",0),(unsigned int)0);
				unit->addAnnotation("number of shaders after clear - ");
				unit->assertEquals(list->getUintEntry("ShadeX::clear()/shaders",0),(unsigned int)0);
				delete s1;
			} catch(Exception &e) {
				unit->addAnnotation("shader tests could not be completed - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			unit->addMarkup("test working with transformations");

			try {
				ShadeX *s1 = new ShadeX();
				s1->addResources("../data/test/sxengine/files/ShadeX/matrices.c");
				s1->load();

				Vector vr1(1,2,3,4);
				UniformVector *v1 = &s1->getUniformVector("vec.v1");
				for(unsigned int i=0 ; i<4 ; i++) {
					stringstream amsg;
					amsg << "vector vec.v1 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*v1)[i],vr1[i],0.0001f);
				}

				Matrix mr2(
					1,2,3,4,
					5,6,7,8,
					9,10,11,12,
					13,14,15,16
					);
				Matrix mr3(
					1,2,3,
					4,5,6,
					7,8,9
					);
				Matrix mr4(
					1,2,
					3,4
					);
				Matrix mr5(
					2
					);
				Matrix mr1;
				mr1 = mr2 * mr3 * mr4 * mr5;
				UniformMatrix *m1 = &s1->getUniformMatrix("mat.m1");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m1 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.rotate(Vector(-1,0.5f,0.3f,1),1.5345f);
				m1 = &s1->getUniformMatrix("mat.m2");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m2 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.translate(Vector(4, 1, 3, 1));
				m1 = &s1->getUniformMatrix("mat.m3");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m3 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.scale(Vector(55, 6, 36, 1));
				m1 = &s1->getUniformMatrix("mat.m4");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m4 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.shear(Vector(3, -1, 2.5f, 1));
				m1 = &s1->getUniformMatrix("mat.m5");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m5 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.orthographicPerspeciveMatrix(-533,210,2,113,0.2f,621);
				m1 = &s1->getUniformMatrix("mat.m6");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m6 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.viewMatrix(Vector(3,5.23f,10.1f),Vector(0.4f,2.5f,0.13f),Vector(1.5f,0.5f,1.32f));
				m1 = &s1->getUniformMatrix("mat.m7");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m7 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr1.perspectiveMatrix(2.321f,1920,1200,0.3f,654);
				m1 = &s1->getUniformMatrix("mat.m8");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m8 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}

				mr2.translate(Vector(4,2,3));
				mr3.rotate(Vector(0.5f,0.75f,2.1f),3.982f);
				mr4.translate(Vector(9,-8,12));
				mr5.perspectiveMatrix(1.5f,1024,800,1,500);
				mr1 = mr2 * mr3 * mr4 * mr5;
				m1 = &s1->getUniformMatrix("mat.m9");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "matrix mat.m9 - compare " << i << ". component - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals((*m1)[i],mr1[i],0.0001f);
				}
			} catch(Exception &e) {
				unit->addAnnotation("matrix tests could not be completed - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			area.stopRendering();
		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
		}

		void stop(SXRenderArea &area) {
		}

	};

	class TestShadeX2: public SXRenderListener {
	private:
		ShadeX shadeX;
		XUnit *unit;
		ListLogger *list;

		//animation transformations of the bones
		Matrix *in_body;
		Matrix *in_head;
		Matrix *in_shoulder_left;
		Matrix *in_shoulder_right;
		Matrix *in_upperarm_left;
		Matrix *in_upperarm_right;
		Matrix *in_underarm_left;
		Matrix *in_underarm_right;
		Matrix *in_pelvis_left;
		Matrix *in_pelvis_right;
		Matrix *in_thigh_left;
		Matrix *in_thigh_right;
		Matrix *in_calf_left;
		Matrix *in_calf_right;

		//3x3 submatrices of the inverse bind pose and the bind pose matrices
		//used to alight the coordinate axes of the bone coordinate spaces
		//to the coordinate system of the object/model space
		Matrix bbm_body;
		Matrix inv_bbm_body;
		Matrix bbm_head;
		Matrix inv_bbm_head;
		Matrix bbm_shoulder_left;
		Matrix inv_bbm_shoulder_left;
		Matrix bbm_shoulder_right;
		Matrix inv_bbm_shoulder_right;
		Matrix bbm_upperarm_left;
		Matrix inv_bbm_upperarm_left;
		Matrix bbm_upperarm_right;
		Matrix inv_bbm_upperarm_right;
		Matrix bbm_underarm_left;
		Matrix inv_bbm_underarm_left;
		Matrix bbm_underarm_right;
		Matrix inv_bbm_underarm_right;
		Matrix bbm_pelvis_left;
		Matrix inv_bbm_pelvis_left;
		Matrix bbm_pelvis_right;
		Matrix inv_bbm_pelvis_right;
		Matrix bbm_thigh_left;
		Matrix inv_bbm_thigh_left;
		Matrix bbm_thigh_right;
		Matrix inv_bbm_thigh_right;
		Matrix bbm_calf_left;
		Matrix inv_bbm_calf_left;
		Matrix bbm_calf_right;
		Matrix inv_bbm_calf_right;

		TestShadeX2(const TestShadeX2 &);
		TestShadeX2 &operator = (const TestShadeX2 &);
	public:
		TestShadeX2(XUnit &unit, ListLogger &logger) {
			this->unit = &unit;
			this->list = &logger;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test collada mesh loader - ");

			unit->addAnnotation("load resources - ");
			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/mesh.c");
				shadeX.load();
				unit->assertTrue(true);

				Skeleton &skeleton0 = shadeX.getSkeleton("skeletons.m0");
				unit->addAnnotation("loaded skeleton0 - ");
				unit->assertTrue(skeleton0.isLoaded());

				Skeleton &skeleton1 = shadeX.getSkeleton("skeletons.m1");
				unit->addAnnotation("loaded skeleton1 - ");
				unit->assertTrue(skeleton1.isLoaded());

				unit->addAnnotation("count unmatched bones of skeleton0 - ");
				unit->assertEquals(skeleton0.getUnmatchedBones().size(),(unsigned int)1);

				unit->addAnnotation("count unmatched bones of skeleton1 - ");
				unit->assertEquals(skeleton1.getUnmatchedBones().size(),(unsigned int)0);

				//test state of matrices
				Matrix skeletonMatrix;
				skeletonMatrix.translate(Vector(-0.3246693f, 0.01032972f, -0.8873341f));
				Matrix bone1_m = Matrix().rotate(Vector(1,2,3),2.4521f);;
				Matrix bone2_m = Matrix().scale(Vector(2,3,1));
				Matrix bone3_m = Matrix().translate(Vector(4,5,6));
				Matrix bone4_m = Matrix().translate(Vector(4,8,16));
				Matrix head_m(-1, 0, 0, 0, 0, 1, 0, 1.103895f, 0, 0, -1, 0, 0, 0, 0, 1);
				Matrix shoulder_left_m(-1, 0, 0, 0, 0, -0.5117056f, -0.8591608f, 1.103895f, 0, -0.8591605f, 0.5117058f, 0, 0, 0, 0, 1);
				Matrix shoulder_right_m(-1, 0, 0, 0, 0, -0.5628946f, 0.8265287f, 1.103895f, 0, 0.8265287f, 0.5628946f, 0, 0, 0, 0, 1);
				Matrix upperarm_left_m(1, 0, 0, 0, 0, 0.8738195f, 0.4862503f, 1.174714f, 0, -0.4862504f, 0.8738195f, 0, 0, 0, 0, 1);
				Matrix upperarm_right_m(1, 0, 0, 0, 0, 0.840699f, -0.5415027f, 1.04152f, 0, 0.5415027f, 0.840699f, 0, 0, 0, 0, 1);
				Matrix underarm_left_m(1, 0, 0, 0, 0, 0.9999492f, -0.01007736f, 1.010654f, 0, 0.01007732f, 0.9999492f, 0, 0, 0, 0, 1);
				Matrix underarm_right_m(1, 0, 0, 0, 0, 0.9999677f, 0.008034635f, 1.156943f, 0, -0.008034635f, 0.9999677f, 0, 0, 0, 0, 1);
				Matrix body_m(1, 0 ,0, 0.3205911f, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1);
				Matrix pelvis_left_m(-1, 0, 0, 0.3205911f, 0, 0.8858316f, -0.464007f, 0, 0, -0.464007f, -0.8858316f, 0, 0, 0, 0, 1);
				Matrix pelvis_right_m(-1, 0, 0, 0.3205911f, 0, -0.8762159f, -0.481919f, 0, 0, -0.4819191f, 0.8762158f, 0, 0, 0, 0, 1);
				Matrix thigh_left_m(1, 0, 0, 0, 0, 0.7335238f, -0.6796638f, 0.6080085f, 0, 0.6796637f, 0.7335238f, 0, 0, 0, 0, 1);
				Matrix thigh_right_m(1, 0, 0, 0, 0, 0.7667319f, 0.6419675f, 0.5854102f, 0, -0.6419675f, 0.7667319f, 0, 0, 0, 0, 1);
				Matrix calf_left_m(1, 0, 0, 0, 0, 0.9949591f, 0.1002811f, 1.265454f, 0, -0.1002811f, 0.9949591f, 0, 0, 0, 0, 1);
				Matrix calf_right_m(1, 0, 0, 0, 0, 0.9986193f, 0.05252928f, 1.238328f, 0, -0.05252925f, 0.9986194f, 0, 0, 0, 0, 1);
				Matrix invskeletonMatrix = skeletonMatrix;
				Matrix invbone1_m = bone1_m;
				Matrix invbone2_m = bone2_m;
				Matrix invbone3_m = bone3_m;
				Matrix invbone4_m = bone4_m;
				Matrix invhead_m = head_m;
				Matrix invshoulder_left_m = shoulder_left_m;
				Matrix invshoulder_right_m = shoulder_right_m;
				Matrix invupperarm_left_m = upperarm_left_m;
				Matrix invupperarm_right_m = upperarm_right_m;
				Matrix invunderarm_left_m = underarm_left_m;
				Matrix invunderarm_right_m = underarm_right_m;
				Matrix invbody_m = body_m;
				Matrix invpelvis_left_m = pelvis_left_m;
				Matrix invpelvis_right_m = pelvis_right_m;
				Matrix invthigh_left_m = thigh_left_m;
				Matrix invthigh_right_m = thigh_right_m;
				Matrix invcalf_left_m = calf_left_m;
				Matrix invcalf_right_m = calf_right_m;
				invskeletonMatrix.inverse();
				invbone1_m.inverse();
				invbone2_m.inverse();
				invbone3_m.inverse();
				invbone4_m.inverse();
				invhead_m.inverse();
				invshoulder_left_m.inverse();
				invshoulder_right_m.inverse();
				invupperarm_left_m.inverse();
				invupperarm_right_m.inverse();
				invunderarm_left_m.inverse();
				invunderarm_right_m.inverse();
				invbody_m.inverse();
				invpelvis_left_m.inverse();
				invpelvis_right_m.inverse();
				invthigh_left_m.inverse();
				invthigh_right_m.inverse();
				invcalf_left_m.inverse();
				invcalf_right_m.inverse();

				Matrix ibpmbone1 = invbone1_m * invskeletonMatrix;
				const Bone *bone = &skeleton0.getBone("bone1");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix bone1, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmbone1[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix ibpmbone2 = invbone2_m * invbone1_m * invskeletonMatrix;
				bone = &skeleton0.getBone("bone2");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix bone2, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmbone2[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix ibpmbone3 = invbone3_m * invbone1_m * invskeletonMatrix;
				bone = &skeleton0.getBone("bone3");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix bone3, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmbone3[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix ibpmbone4 = invbone4_m * invbone3_m * invbone1_m * invskeletonMatrix;
				bone = &skeleton0.getBone("bone4");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix bone4, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmbone4[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix ibpmunderarm_left = invunderarm_left_m * invupperarm_left_m * invshoulder_left_m * invbody_m * invskeletonMatrix;
				bone = &skeleton0.getBone("underarm_left");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix underarm_left, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmunderarm_left[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix ibpmthigh_left = invthigh_left_m * invpelvis_left_m * invskeletonMatrix;
				bone = &skeleton0.getBone("thigh_left");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix thigh_left, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmthigh_left[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix ibpmcalf_right = invcalf_right_m * invthigh_right_m * invpelvis_right_m * invskeletonMatrix;
				bone = &skeleton0.getBone("calf_right");
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix calf_right, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmcalf_right[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				Matrix &inputShoulder_right = shadeX.getUniformMatrix("s0.input.shoulder_right");
				Matrix &inputUpperarm_right = shadeX.getUniformMatrix("s0.input.upperarm_right");
				Matrix &inputUnderarm_right = shadeX.getUniformMatrix("s0.input.underarm_right");
				Matrix &outputShoulder_right = shadeX.getUniformMatrix("s0.output.shoulder_right");
				Matrix &outputUpperarm_right = shadeX.getUniformMatrix("s0.output.upperarm_right");
				Matrix &outputUnderarm_right = shadeX.getUniformMatrix("s0.output.underarm_right");
				inputShoulder_right.rotate(Vector(1,0,0),1.2321f);
				inputUpperarm_right.rotate(Vector(0,1,0),1.9923f);
				inputUnderarm_right.rotate(Vector(0.5f,0.5f,0),3.111f);

				Matrix &inputPelvis_left = shadeX.getUniformMatrix("s0.input.pelvis_left");
				Matrix &inputThigh_left = shadeX.getUniformMatrix("s0.input.thigh_left");
				Matrix &inputCalf_left = shadeX.getUniformMatrix("s0.input.calf_left");
				Matrix &outputPelvis_left = shadeX.getUniformMatrix("s0.output.pelvis_left");
				Matrix &outputThigh_left = shadeX.getUniformMatrix("s0.output.thigh_left");
				Matrix &outputCalf_left = shadeX.getUniformMatrix("s0.output.calf_left");
				inputPelvis_left.rotate(Vector(0,0,1),5.2321f);
				inputThigh_left.rotate(Vector(0.5f,0.5f,0.5f),4.0f);
				inputCalf_left.rotate(Vector(1,0,0),0.523f);

				Matrix &inputShoulder_left = shadeX.getUniformMatrix("s01.input.shoulder_left");
				Matrix &inputUpperarm_left = shadeX.getUniformMatrix("s01.input.upperarm_left");
				Matrix &outputUpperarm_left = shadeX.getUniformMatrix("s01.output.upperarm_left");
				inputShoulder_left.rotate(Vector(1,1,1),2.2222f);
				inputUpperarm_left.rotate(Vector(0,1,0),5.5224f);

				Matrix &fantasyinput = shadeX.getUniformMatrix("fantasyinput");
				Matrix &fantasyoutput = shadeX.getUniformMatrix("fantasyoutput");
				fantasyinput.rotate(Vector(1,0,0),1.534f);

				skeleton0.updateBoneTransforms();

				Matrix tUnderarm_right = skeletonMatrix * body_m 
					* shoulder_right_m * Matrix().rotate(Vector(1,0,0),1.2321f) 
					* upperarm_right_m * Matrix().rotate(Vector(0,1,0),1.9923f)
					* underarm_right_m * Matrix().rotate(Vector(0.5f,0.5f,0),3.111f)
					* invunderarm_right_m * invupperarm_right_m * invshoulder_right_m * invbody_m * invskeletonMatrix;
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check underarm_right after transformation, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(outputUnderarm_right[i],tUnderarm_right[i],0.00001f);
				}
				
				Matrix tUpperarm_right = skeletonMatrix * body_m
					* shoulder_right_m * Matrix().rotate(Vector(1,0,0),1.2321f)
					* upperarm_right_m * Matrix().rotate(Vector(0,1,0),1.9923f)
					* invupperarm_right_m * invshoulder_right_m * invbody_m * invskeletonMatrix;
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check upperarm_right after transformation, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(outputUpperarm_right[i],tUpperarm_right[i],0.00001f);
				}

				Matrix tCalf_left = skeletonMatrix
					* pelvis_left_m * Matrix().rotate(Vector(0,0,1),5.2321f)
					* thigh_left_m * Matrix().rotate(Vector(0.5f,0.5f,0.5f),4.0f)
					* calf_left_m * Matrix().rotate(Vector(1,0,0),0.523f)
					* invcalf_left_m * invthigh_left_m * invpelvis_left_m * invskeletonMatrix;
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check calf_left after transformation, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(outputCalf_left[i],tCalf_left[i],0.00001f);
				}

				Matrix tUpperarm_left = skeletonMatrix * body_m
					* shoulder_left_m * Matrix().rotate(Vector(1,1,1),2.2222f)
					* upperarm_left_m * Matrix().rotate(Vector(0,1,0),5.5224f)
					* invupperarm_left_m * invshoulder_left_m * invbody_m * invskeletonMatrix;
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check upperarm_left after transformation, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(outputUpperarm_left[i],tUpperarm_left[i],0.00001f);
				}

				Matrix tFantasyoutput = Matrix().identity();
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check fantasybone after transformation, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(fantasyoutput[i],tFantasyoutput[i],0.00001f);
				}

				Matrix fantasybone_m = Matrix().translate(Vector(1,2,5));
				Matrix invfantasybone_m = fantasybone_m;
				invfantasybone_m.inverse();

				Bone fBone;
				fBone.ID = "fantasybone";
				fBone.parentTransform = Matrix().translate(Vector(1,2,5));
				skeleton0.addRootBone(fBone);

				skeleton0.load();
				unit->addAnnotation("reload skeleton0 with new bone hierachy - ");
				unit->assertTrue(skeleton0.isLoaded());

				unit->addAnnotation("count number of unmatched bones in skeleton0 - ");
				unit->assertEquals(skeleton0.getUnmatchedBones().size(),(unsigned int)15);

				unit->addAnnotation("check number of rootbones - ");
				unit->assertEquals(skeleton0.getRootBones().size(),(unsigned int)1);

				bone = &skeleton0.getBone("fantasybone");
				Matrix ibpmfantasynode = invfantasybone_m * invskeletonMatrix;
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check inverse bind pose matrix fantasybone, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(ibpmfantasynode[i],bone->inverseBindPoseMatrix[i],0.00001f);
				}

				skeleton0.execute(vector<string>());

				Matrix tFantasyOutput2 = skeletonMatrix
					* fantasybone_m * Matrix().rotate(Vector(1,0,0),1.534f)
					* invfantasybone_m * invskeletonMatrix;
				for(unsigned int i=0 ; i<16 ; i++) {
					stringstream amsg;
					amsg << "check fantasybone after repeated transformation, element " << i << " - ";
					unit->addAnnotation(amsg.str());
					unit->assertEquals(fantasyoutput[i],tFantasyOutput2[i],0.00001f);
				}

				//get matrices for the animation with skeletons.m1

				//get input matrices
				in_body = &shadeX.getUniformMatrix("s1.input.body");
				in_head = &shadeX.getUniformMatrix("s1.input.head");
				in_shoulder_left = &shadeX.getUniformMatrix("s1.input.shoulder_left");
				in_shoulder_right = &shadeX.getUniformMatrix("s1.input.shoulder_right");
				in_upperarm_left = &shadeX.getUniformMatrix("s1.input.upperarm_left");
				in_upperarm_right = &shadeX.getUniformMatrix("s1.input.upperarm_right");
				in_underarm_left = &shadeX.getUniformMatrix("s1.input.underarm_left");
				in_underarm_right = &shadeX.getUniformMatrix("s1.input.underarm_right");
				in_pelvis_left = &shadeX.getUniformMatrix("s1.input.pelvis_left");
				in_pelvis_right = &shadeX.getUniformMatrix("s1.input.pelvis_right");
				in_thigh_left = &shadeX.getUniformMatrix("s1.input.thigh_left");
				in_thigh_right = &shadeX.getUniformMatrix("s1.input.thigh_right");
				in_calf_left = &shadeX.getUniformMatrix("s1.input.calf_left");
				in_calf_right = &shadeX.getUniformMatrix("s1.input.calf_right");

				//get 3x3 submatrices of inverse bind pose and bind pose matrices

				bone = &skeleton1.getBone("body");
				bbm_body = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_body = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("head");
				bbm_head = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_head = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("shoulder_left");
				bbm_shoulder_left = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_shoulder_left = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("shoulder_right");
				bbm_shoulder_right = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_shoulder_right = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("upperarm_left");
				bbm_upperarm_left = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_upperarm_left = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("upperarm_right");
				bbm_upperarm_right = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_upperarm_right = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("underarm_left");
				bbm_underarm_left = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_underarm_left = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("underarm_right");
				bbm_underarm_right = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_underarm_right = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("pelvis_left");
				bbm_pelvis_left = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_pelvis_left = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("pelvis_right");
				bbm_pelvis_right = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_pelvis_right = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("thigh_left");
				bbm_thigh_left = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_thigh_left = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("thigh_right");
				bbm_thigh_right = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_thigh_right = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("calf_left");
				bbm_calf_left = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_calf_left = Matrix(bone->inverseBindPoseMatrix).submatrix();

				bone = &skeleton1.getBone("calf_right");
				bbm_calf_right = Matrix(bone->inverseBindPoseMatrix).inverse().submatrix();
				inv_bbm_calf_right = Matrix(bone->inverseBindPoseMatrix).submatrix();

			} catch(Exception &e) {
				unit->addAnnotation("loading shadeX error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
		}

		void reshape(SXRenderArea &area) {
			RenderTarget &target1 = shadeX.getRenderTarget("present1.target");
			target1.setWidth(area.getWidth());
			target1.setHeight(area.getHeight());
			target1.load();

			Matrix &perspective1 = shadeX.getUniformMatrix("present1.projection");
			perspective1.perspectiveMatrix(1.5f,area.getWidth(),area.getHeight(),1,100);
		}

		void render(SXRenderArea &area) {
			if(area.getTime() > 20.0f) {
				area.stopRendering();
			}
			if(area.getTime() < 5.0) {
				Matrix &model = shadeX.getUniformMatrix("objects.m1.transform");
				model.rotate(Vector(0,0,1),(float)Pi * area.getTime());

				Effect &effect = shadeX.getEffect("present1.effect");
				effect.render();
			} else if(area.getTime() < 10.0) {
				Matrix &model = shadeX.getUniformMatrix("objects.m2.transform");
				model = Matrix().translate(Vector(0,0,1)) * Matrix().rotate(Vector(0,1,0),(float)Pi * 0.4f * area.getTime()) * Matrix().rotate(Vector(0,0,1),(float)Pi * area.getTime()) * Matrix().scale(Vector(0.2f,0.2f,0.2f));
				
				Effect &effect = shadeX.getEffect("present2.effect");
				effect.render();
			} else {
				Matrix &model = shadeX.getUniformMatrix("objects.m3.transform");
				model.rotate(Vector(0,0,1),(float)Pi * 0.25f * area.getTime());

				//arms

				float shoulders_move = (float)-sin(Pi * area.getTime());
				(*in_head) = inv_bbm_head * Matrix().rotate(Vector(0,0,1),(float)shoulders_move * Pi * 0.1f) * bbm_head;
				(*in_shoulder_left) = inv_bbm_shoulder_left * Matrix().rotate(Vector(0,0,1),(float)shoulders_move * Pi * 0.1f) * bbm_shoulder_left;
				(*in_shoulder_right) = inv_bbm_shoulder_right * Matrix().rotate(Vector(0,0,1),(float)shoulders_move * Pi * 0.1f) * bbm_shoulder_right;

				//left arm
				float leftarm_move = (float)sin(Pi * area.getTime());
				(*in_upperarm_left) = inv_bbm_upperarm_left * Matrix().rotate(Vector(0,1,0),(float)-leftarm_move * Pi * 0.25f) * Matrix().rotate(Vector(1,0,0),(float)-Pi*0.2f) * bbm_upperarm_left;
				(*in_underarm_left) = inv_bbm_underarm_left * Matrix().rotate(Vector(0,0,1),(leftarm_move*0.2f + 0.2f)*-Pi) * bbm_underarm_left;

				//right arm
				float rightarm_move = (float)-sin(Pi * area.getTime());
				(*in_upperarm_right) = inv_bbm_upperarm_right * Matrix().rotate(Vector(0,1,0),(float)-rightarm_move * Pi * 0.25f) * Matrix().rotate(Vector(1,0,0),(float)Pi*0.2f) * bbm_upperarm_right;
				(*in_underarm_right) = inv_bbm_underarm_right * Matrix().rotate(Vector(0,0,1),(rightarm_move*0.2f + 0.2f)*Pi) * bbm_underarm_right;

				//feet
				float pelvis_move = (float)sin(Pi * area.getTime());

				//left foot
				float leftfoot_move = (float)-(0.5f + sin(Pi * area.getTime())) / 2.0f;
				(*in_pelvis_left) = inv_bbm_pelvis_left * Matrix().rotate(Vector(0,0,1),(float)pelvis_move * Pi * 0.1f) * bbm_pelvis_left;
				(*in_thigh_left) = inv_bbm_thigh_left * Matrix().rotate(Vector(0,1,0),(float)-leftfoot_move * Pi * 0.5f) * bbm_thigh_left;
				(*in_calf_left) = inv_bbm_calf_left * Matrix().rotate(Vector(0,1,0),(float)max(leftfoot_move,0) * Pi * 0.5f) * bbm_calf_left;

				//right foot
				float rightfoot_move = (float)(0.5f + sin(Pi * area.getTime())) / 2.0f;
				(*in_pelvis_right) = inv_bbm_pelvis_right * Matrix().rotate(Vector(0,0,1),(float)pelvis_move * Pi * 0.1f) * bbm_pelvis_right;
				(*in_thigh_right) = inv_bbm_thigh_right * Matrix().rotate(Vector(0,1,0),(float)-rightfoot_move * Pi * 0.5f) * bbm_thigh_right;
				(*in_calf_right) = inv_bbm_calf_right * Matrix().rotate(Vector(0,1,0),(float)max(rightfoot_move,0) * Pi * 0.5f) * bbm_calf_right;

				Effect &effect = shadeX.getEffect("present3.effect");
				effect.render();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
		}

	};

	class TestShadeX3: public SXRenderListener {
	private:
		ShadeX shadeX;
		XUnit *unit;
		ListLogger *list;
		

		TestShadeX3(const TestShadeX3 &);
		TestShadeX3 &operator = (const TestShadeX3 &);
	public:
		TestShadeX3(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test rendering 3D textures");

			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/effects2.c");
			} catch(Exception &e) {
				unit->addAnnotation("loading error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			try {
				shadeX.load();
			} catch(Exception &e) {
				unit->addAnnotation("parsing error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
		}

		void reshape(SXRenderArea &area) {
			RenderTarget &present = shadeX.getRenderTarget("showvolume.target");
			present.setWidth(area.getWidth());
			present.setHeight(area.getHeight());
			present.load();
		}

		void render(SXRenderArea &area) {
			if(area.getTime() > 5.0f) {
				area.stopRendering();
			}

			UniformFloat &textureIndex = shadeX.getUniformFloat("showvolume.index");
			textureIndex = area.getTime() * 3.0f;

			Effect &main = shadeX.getEffect("showvolume.effect");
			main.render();
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
		}

	};

	class TestShadeX4: public SXRenderListener {
		ShadeX shadeX;
		XUnit *unit;
		ListLogger *list;

		TestShadeX4(const TestShadeX4 &);
		TestShadeX4 &operator = (const TestShadeX4 &);
	public:

		TestShadeX4(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test rendering into 3D textures");

			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/effects3.c");
			} catch(Exception &e) {
				unit->addAnnotation("loading error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			try {
				shadeX.load();
			} catch(Exception &e) {
				unit->addAnnotation("parsing error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
		}

		void reshape(SXRenderArea &area) {
			RenderTarget &present = shadeX.getRenderTarget("showvolume.target");
			present.setWidth(area.getWidth());
			present.setHeight(area.getHeight());
			present.load();
		}

		void render(SXRenderArea &area) {
			if(area.getTime() > 4.0f) {
				area.stopRendering();
			}
			Effect &presentEffect = shadeX.getEffect("rendertovolume.effect");
			presentEffect.render();
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
		}

	};

	class TestShadeX5: public SXRenderListener {
		ShadeX shadeX;
		XUnit *unit;
		ListLogger *list;

		TestShadeX5(const TestShadeX5 &);
		TestShadeX5 &operator = (const TestShadeX5 &);
	public:

		TestShadeX5(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test blending, depth function and enabling/disabling depth buffer write");

			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/effects4.c");
			} catch(Exception &e) {
				unit->addAnnotation("loading error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			try {
				shadeX.load();
			} catch(Exception &e) {
				unit->addAnnotation("parsing error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
		}

		void reshape(SXRenderArea &area) {
			RenderTarget &present = shadeX.getRenderTarget("display.target");
			present.setWidth(area.getWidth());
			present.setHeight(area.getHeight());
			present.load();
		}

		void render(SXRenderArea &area) {
			if(area.getTime() > 4.0f) {
				area.stopRendering();
			}
			Effect &presentEffect = shadeX.getEffect("blend.effect");
			presentEffect.render();
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
		}

	};

	class TestShadeX6: public SXRenderListener {
	private:
		ShadeX shadeX;

		XUnit *unit;
		ListLogger *list;

		TestShadeX6(const TestShadeX6 &);
		TestShadeX6 &operator = (const TestShadeX6 &);
	public:
		TestShadeX6(XUnit &unit, ListLogger &list) {
			this-> unit = &unit;
			this->list = &list;
		}

		~TestShadeX6() {
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test transform feedback, instanced rendering");

			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/effects5.c");
			} catch(Exception &e) {
				unit->addAnnotation("loading error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			try {
				shadeX.load();
			} catch(Exception &e) {
				unit->addAnnotation("parsing error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
		}

		void reshape(SXRenderArea &area) {
			unsigned int width = area.getWidth();
			unsigned int height = area.getHeight();
			RenderTarget &display = shadeX.getRenderTarget("display.rendertarget");
			display.setWidth(width);
			display.setHeight(height);
			display.load();
		}

		void render(SXRenderArea &area) {
			if(area.getTime() > 6.0) {
				area.stopRendering();
			}
			if(area.getTime() < 3.0) {
				Effect &quad = shadeX.getEffect("quad.effect");
				quad.render();
			} else if(area.getTime() < 6.0) {
				Effect &transform = shadeX.getEffect("transform.effect");
				transform.render();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
		}

	};

	class TestShadeX7: public SXRenderListener {
	private:
		XUnit *unit;
		ListLogger *logger;

		ShadeX shadeX;

		TestShadeX7(const TestShadeX7 &);
		TestShadeX7 &operator = (const TestShadeX7 &);
	public:
		TestShadeX7(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->logger = &list;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test sound");

			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/sound.c");
			} catch(Exception &e) {
				unit->addAnnotation("loading error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			try {
				shadeX.load();
			} catch(Exception &e) {
				unit->addAnnotation("parsing error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			AudioObject &ambience = shadeX.getAudioObject("object.ambience");
			ambience.start();
			AudioObject &matrix = shadeX.getAudioObject("object.matrix");
			matrix.start(26.04);
			AudioPass &pass2 = shadeX.getAudioPass("audio.pass2");
			pass2.setPause(true);
			Volume &texts = shadeX.getVolume("present.volume");
			UniformFloat &number_texts = shadeX.getUniformFloat("present.number_texts");
			number_texts.value = texts.getDepth();
		}

		void reshape(SXRenderArea &area) {
			RenderTarget &target = shadeX.getRenderTarget("present.target");
			target.setWidth(area.getWidth());
			target.setHeight(area.getHeight());
			target.load();

			Volume &text = shadeX.getVolume("present.volume");
			if(text.getWidth() > 0 && text.getHeight() > 0 && area.getWidth() > 0 && area.getHeight() > 0) {
				//making shure that both text textures and screen are not empty
				Matrix &transform = shadeX.getUniformMatrix("present.transform");
				Matrix quad2screen = Matrix(
					1, 0, 0, 0,
					0, ((float)area.getHeight())/((float)area.getWidth()), 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1
					);
				Matrix screen2text = Matrix(
					0.5f, 0, 0, 0.5f,
					0, ((float)text.getWidth()/(float)text.getHeight())*0.5f, 0, 0.5f,
					0, 0, 1, 0,
					0, 0, 0, 1
					);
				transform = screen2text * quad2screen;
			}

		}

		void render(SXRenderArea &area) {
			UniformFloat &text = shadeX.getUniformFloat("present.volumeindex");
			AudioPass &pass = shadeX.getAudioPass("audio.pass");
			AudioPass &pass2 = shadeX.getAudioPass("audio.pass2");

			//first move ambience source away
			if(area.getTime() < 3) {
				Vector &pos = shadeX.getUniformVector("pos.ambience");
				pos[1] = area.getTime() * 10.0f;
				text.value = 1;
			}
			//then move ambience source back
			if(area.getTime() > 3 && area.getTime() < 6) {
				Vector &pos = shadeX.getUniformVector("pos.ambience");
				pos[1] = (6.0f - area.getTime()) * 10.0f;
				if(area.getTime() > 5.75) {
					pos = Vector(0,0,0);
				}
			}

			//first move listener away
			//rotate view
			if(area.getTime() > 6 && area.getTime() < 9) {
				Vector &pos = shadeX.getUniformVector("pos.listener");
				Vector &view = shadeX.getUniformVector("view.listener");
				pos[1] = (area.getTime() - 6.0f)*10.0f;
				view = Matrix().rotate(Vector(0,0,1),(float)((area.getTime() - 6.0)*10)) * Vector(1,0,0);
				text.value = 2;
			}
			//then move listener back
			//rotate view
			if(area.getTime() > 9 && area.getTime() < 12) {
				Vector &pos = shadeX.getUniformVector("pos.listener");
				Vector &view = shadeX.getUniformVector("view.listener");
				pos[1] = (12.0f - area.getTime())*10.0f;
				view = Matrix().rotate(Vector(0,0,1),(float)((area.getTime() - 6.0)*10)) * Vector(1,0,0);
				if(area.getTime() > 11.75f) {
					pos = Vector(0,0,0);
					view = Vector(1,0,0);
				}
			}

			//change pitch and volume
			if(area.getTime() > 12 && area.getTime() < 17) {
				UniformFloat &pitch = shadeX.getUniformFloat("pitch.ambience");
				UniformFloat &volume = shadeX.getUniformFloat("volume.ambience");

				volume.value = (float)abs(sin(area.getTime()*10));
				pitch.value = (float)abs(sin(Pi*area.getTime()/5.0));

				if(area.getTime() > 16.75) {
					pitch.value = 1;
					volume.value = 1;
				}

				text.value = 3;
			}

			//pause
			if(area.getTime() > 18 && area.getTime() < 18.1) {
				AudioObject &ambience = shadeX.getAudioObject("object.ambience");
				ambience.pause();
				ambience.lock(0.2);
				text.value = 4;
			}
			//continue after pause
			if(area.getTime() > 19 && area.getTime() < 19.1) {
				AudioObject &ambience = shadeX.getAudioObject("object.ambience");
				ambience.start();
				ambience.lock(0.2);
				text.value = 5;
			}
			//restart
			if(area.getTime() > 20 && area.getTime() < 20.1) {
				AudioObject &ambience = shadeX.getAudioObject("object.ambience");
				ambience.start();
				ambience.lock(0.2);
				text.value = 6;
			}
			//stop
			if(area.getTime() > 21 && area.getTime() < 21.1) {
				AudioObject &ambience = shadeX.getAudioObject("object.ambience");
				ambience.stop();
				ambience.lock(0.2);
				text.value = 7;
			}
			//start
			if(area.getTime() > 22 && area.getTime() < 22.1) {
				AudioObject &ambience = shadeX.getAudioObject("object.ambience");
				ambience.start();
				ambience.lock(0.2);
				text.value = 8;
			}

			//use multiple sound sources simultaneously
			if(area.getTime() > 25 && area.getTime() < 25.1) {
				AudioObject &noise = shadeX.getAudioObject("object.noise");
				noise.start();
				noise.lock(0.2);
				text.value = 9;
			}
			if(area.getTime() > 25 && area.getTime() < 28) {
				//turn down volume
				float factor = (float)((28.0 - area.getTime())/(28.0 - 25.0));
				UniformFloat &volume = shadeX.getUniformFloat("volume.ambience");
				volume.value = factor * 1 + (1-factor)*0.1f;
			}
			if(area.getTime() > 30 && area.getTime() < 30.1) {
				AudioObject &boom1 = shadeX.getAudioObject("object.boom1");
				boom1.start();
				boom1.lock(0.2);
			}
			if(area.getTime() > 31 && area.getTime() < 31.1) {
				AudioObject &boom2 = shadeX.getAudioObject("object.boom2");
				boom2.start();
				boom2.lock(0.2);
			}
			if(area.getTime() > 32 && area.getTime() < 32.1) {
				AudioObject &boom3 = shadeX.getAudioObject("object.boom3");
				boom3.start();
				boom3.lock(0.2);
			}

			//pause noise
			if(area.getTime() > 34 && area.getTime() < 34.1) {
				AudioObject &noise = shadeX.getAudioObject("object.noise");
				noise.start();
				noise.lock(0.2);
			}
			if(area.getTime() > 38 && area.getTime() < 38.1) {
				AudioObject &noise = shadeX.getAudioObject("object.noise");
				noise.pause();
				noise.lock(0.2);
				text.value = 10;
			}

			//pause pass, continue pass2
			if(area.getTime() > 39 && area.getTime() < 39.1) {
				AudioObject &boom = shadeX.getAudioObject("object.boom1");
				boom.start();
				boom.lock(0.2);
			}
			if(area.getTime() > 39.83 && area.getTime() < 39.93) {
				pass.setPause(true);
				pass2.setPause(false);
				text.value = 11;
			}

			//continue pass, pause pass2
			if(area.getTime() > 47 && area.getTime() < 47.1) {
				pass.setPause(false);
				pass2.setPause(true);
				text.value = 12;
			}

			//continue noise after pause
			if(area.getTime() > 49 && area.getTime() < 49.1) {
				AudioObject &noise = shadeX.getAudioObject("object.noise");
				noise.start();
				noise.lock(0.2);
				text.value = 13;
			}

			if(area.getTime() > 55.0) {
				area.stopRendering();
			}

			//update scene
			Effect &effect = shadeX.getEffect("present.effect");
			effect.render();
			pass.update();
			pass2.update();
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			AudioPass &pass = shadeX.getAudioPass("audio.pass");
			pass.setPause(true);
			AudioPass &pass2 = shadeX.getAudioPass("audio.pass2");
			pass2.setPause(true);
		}

	};

	class TestShadeX8: public SXRenderListener {
	private:
		int width;
		int height;

		ShadeX shadeX;

		XUnit *unit;
		ListLogger *list;
		timer timeMeasurement;
		double lastMillies;
		TestShadeX8(const TestShadeX8 &);
		TestShadeX8 &operator = (const TestShadeX8 &);
	public:

		TestShadeX8(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			unit->addMarkup("test shadex at work");

			try {
				shadeX.addResources("../data/test/sxengine/files/ShadeX/effects.c");
				shadeX.addResources("../data/test/sxengine/files/ShadeX/resources.c");
			} catch(Exception &e) {
				unit->addAnnotation("loading error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			try {
				shadeX.load();
			} catch(Exception &e) {
				unit->addAnnotation("parsing error - ");
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			//start playing music
			AudioObject &object = shadeX.getAudioObject("audio.object.1");
			object.start();

			//start measuring time
			ptime time(second_clock::local_time());
			timeMeasurement.restart();
			lastMillies = 0;
		}

		void reshape(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			RenderTarget &display = shadeX.getRenderTarget("displaytarget");
			display.setWidth(width);
			display.setHeight(height);
			display.load();
		}

		void render(SXRenderArea &area) {
			double millies = timeMeasurement.elapsed();
			double deltaMillies = millies - lastMillies;

			//setting uniform variables - this is supposed to be done from the entity objects
			Matrix &sceneViewProjection = shadeX.getUniformMatrix("matrix.scene.viewprojection");
			sceneViewProjection = Matrix().perspectiveMatrix(Tau*0.25f, (float)width, (float)height, 0.1f, 1000.0f) * Matrix().viewMatrix(Vector(),Vector(1,1,0),Vector(0,0,1));

			Matrix &trans1 = shadeX.getUniformMatrix("matrices.obj1.translate");
			Matrix &scale1 = shadeX.getUniformMatrix("matrices.obj1.scale");
			trans1.translate(Vector(5,5,0));
			scale1.scale(Vector(1,1,1));

			Matrix &trans2 = shadeX.getUniformMatrix("matrices.obj2.translate");
			Matrix &scale2 = shadeX.getUniformMatrix("matrices.obj2.scale");
			trans2.translate(Vector(5,7,0));
			scale2.scale(Vector(0.25f,0.25f,0.25f));

			Matrix &trans3 = shadeX.getUniformMatrix("matrices.obj3.translate");
			Matrix &scale3 = shadeX.getUniformMatrix("matrices.obj3.scale");
			trans3.translate(Vector(3,3,1));
			scale3.scale(Vector(0.5f,0.5f,0.5f));

			Matrix &trans4 = shadeX.getUniformMatrix("matrices.obj4.translate");
			Matrix &scale4 = shadeX.getUniformMatrix("matrices.obj4.scale");
			trans4.translate(Vector(7,3,4));
			scale4.scale(Vector(0.5f,0.5f,0.5f));

			Matrix &trans5 = shadeX.getUniformMatrix("matrices.obj5.translate");
			Matrix &scale5 = shadeX.getUniformMatrix("matrices.obj5.scale");
			trans5.translate(Vector(2.5f,5,1.5f));
			scale5.scale(Vector(1,1,1));

			Matrix &trans6 = shadeX.getUniformMatrix("matrices.obj6.translate");
			Matrix &scale6 = shadeX.getUniformMatrix("matrices.obj6.scale");
			trans6.translate(Vector(5,2,-0.5f));
			scale6.scale(Vector(0.75f,0.75f,0.75f));

			Vector &movewaves = shadeX.getUniformVector("pass.dowaves.trans");
			Vector w1(movewaves[0],movewaves[1]);
			Vector w2(movewaves[2],movewaves[3]);
			w1 = w1 + Vector(0.1f,0.03f) * (float)deltaMillies;
			w2 = w2 + Vector(0.02f,0.1f) * (float)deltaMillies;
			movewaves = Vector(w1[0],w1[1],w2[0],w2[1]);

			UniformFloat &wavefactor = shadeX.getUniformFloat("pass.dowaves.wavefactor");
			wavefactor.value = max((6.0f - (float)millies)/6.0f , 0.0f);

			UniformFloat &uselogo = shadeX.getUniformFloat("pass.display.uselogo");
			UniformFloat &overlighten = shadeX.getUniformFloat("pass.display.overlighten");
			overlighten = (float)exp(-(millies - 6.5f)*(millies - 6.5f));
			if(millies > 6) {
				uselogo = min((float)millies - 6.0f , 1.0f);
			}

			//render passes

			Effect &testEffect = shadeX.getEffect("test.effect");
			testEffect.render();
			AudioPass &testAudioPass = shadeX.getAudioPass("audio.pass.1");
			testAudioPass.update();

			if(millies > 8) {
				UniformFloat &volume = shadeX.getUniformFloat("audio.object.1.volume");
				volume.value = (float)((9.0-millies)/(9.0-8.0));
			}
			if(millies > 9) {
				//leave window after nine seconds
				area.stopRendering();
			}
			lastMillies = millies;
		}

		void stop(SXRenderArea &area) {
			AudioPass &testAudioPass = shadeX.getAudioPass("audio.pass.1");
			testAudioPass.setPause(true);
			unit->addMarkup("destruction phase");
		}
	};

	void testShadeX(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testshadex",new ListLogger());
			Logger::setDefaultLogger("testshadex");
			ListLogger &list = dynamic_cast<ListLogger &>(Logger::get());

			int argc = 1;
			char **argv = new char *[1];
			char *argvcontent = "testshadex";
			argv[0] = argvcontent;

			QApplication *app1 = new QApplication(argc,argv);

			SXWidget *widget1 = new SXWidget();
			widget1->renderPeriodically(0.01f);
			widget1->setMinimumSize(1,1);
			QObject::connect(widget1,SIGNAL(finishedRendering()),widget1,SLOT(close()));
			widget1->addRenderListener(*new TestShadeX1(unit,list));
			widget1->show();

			unit.addAnnotation("execute app1 - ");
			unit.assertEquals(app1->exec(),0);
			delete widget1;
			delete app1;

			QApplication *app2 = new QApplication(argc,argv);
			
			SXWidget *widget2 = new SXWidget();
			widget2->renderPeriodically(0.01f);
			widget2->setMinimumSize(600,400);
			QObject::connect(widget2,SIGNAL(finishedRendering()),widget2,SLOT(close()));
			widget2->addRenderListener(*new TestShadeX2(unit,list));
			widget2->show();

			unit.addAnnotation("execute app2 - ");
			unit.assertEquals(app2->exec(),0);
			delete widget2;
			delete app2;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");

			QApplication *app3 = new QApplication(argc,argv);
			
			SXWidget *widget3 = new SXWidget();
			widget3->renderPeriodically(0.01f);
			widget3->setMinimumSize(600,400);
			QObject::connect(widget3,SIGNAL(finishedRendering()),widget3,SLOT(close()));
			widget3->addRenderListener(*new TestShadeX3(unit,list));
			widget3->show();

			unit.addAnnotation("execute app3 - ");
			unit.assertEquals(app3->exec(),0);
			delete widget3;
			delete app3;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");

			QApplication *app4 = new QApplication(argc,argv);
			
			SXWidget *widget4 = new SXWidget();
			widget4->renderPeriodically(0.01f);
			widget4->setMinimumSize(600,400);
			QObject::connect(widget4,SIGNAL(finishedRendering()),widget4,SLOT(close()));
			widget4->addRenderListener(*new TestShadeX4(unit,list));
			widget4->show();

			unit.addAnnotation("execute app4 - ");
			unit.assertEquals(app4->exec(),0);
			delete widget4;
			delete app4;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");

			QApplication *app5 = new QApplication(argc,argv);

			SXWidget *widget5 = new SXWidget();
			widget5->renderPeriodically(0.01f);
			widget5->setMinimumSize(1200,800);
			widget5->move(100,100);
			QObject::connect(widget5,SIGNAL(finishedRendering()),widget5,SLOT(close()));
			widget5->addRenderListener(*new TestShadeX5(unit,list));
			widget5->show();

			unit.addAnnotation("execute app5 - ");
			unit.assertEquals(app5->exec(),0);
			delete widget5;
			delete app5;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");

			QApplication *app6 = new QApplication(argc,argv);

			SXWidget *widget6 = new SXWidget();
			widget6->renderPeriodically(0.01f);
			widget6->setMinimumSize(600,400);
			QObject::connect(widget6,SIGNAL(finishedRendering()),widget6,SLOT(close()));
			widget6->addRenderListener(*new TestShadeX6(unit,list));
			widget6->show();

			unit.addAnnotation("execute app6 - ");
			unit.assertEquals(app6->exec(),0);
			delete widget6;
			delete app6;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");

			QApplication *app7 = new QApplication(argc,argv);

			SXWidget *widget7 = new SXWidget();
			widget7->renderPeriodically(0.01f);
			widget7->setMinimumSize(600,400);
			QObject::connect(widget7,SIGNAL(finishedRendering()),widget7,SLOT(close()));
			widget7->addRenderListener(*new TestShadeX7(unit,list));
			widget7->show();

			unit.addAnnotation("execute app7 - ");
			unit.assertEquals(app7->exec(),0);
			delete widget7;
			delete app7;

			unit.addMarkup("sound validation");
			unit.assertInputYes("pink unicorns with wings?");

			QApplication *app8 = new QApplication(argc,argv);

			QWidget *widget8 = new QWidget();
			QGridLayout *layout = new QGridLayout();
			QPushButton *b = new QPushButton("(1,1)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,1,1,1,1);
			b = new QPushButton("(2,1)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,2,1,1,1);
			b = new QPushButton("(3,1)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,3,1,1,1);
			b = new QPushButton("(1,2)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,1,2,1,1);
			b = new QPushButton("(3,2)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,3,2,1,1);
			b = new QPushButton("(1,3)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,1,3,1,1);
			b = new QPushButton("(2,3)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,2,3,1,1);
			b = new QPushButton("(3,3)");
			b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
			layout->addWidget(b,3,3,1,1);
			SXWidget *my = new SXWidget();
			my->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
			my->setMinimumSize(600,400);
			my->renderPeriodically(0.01f);
			my->addRenderListener(*new TestShadeX8(unit,list));
			layout->addWidget(my,2,2,1,1);
			widget8->setLayout(layout);
			QObject::connect(my,SIGNAL(finishedRendering()),widget8,SLOT(close()));
			widget8->show();

			unit.addAnnotation("execute app8 - ");
			unit.assertEquals(app8->exec(),0);
			delete widget8;
			delete app8;

			delete argv;

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif