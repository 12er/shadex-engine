#ifndef _TEST_SX_TESTMATH_CPP_
#define _TEST_SX_TESTMATH_CPP_

/**
 * math test cases
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXEngine.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/Exception.h>
#include <sx/SXMath.h>
using namespace sx;

namespace sxengine {

	void testVector1(XUnit &unit) {
		unit.addMarkup("Vector - test equal methods");
		Vector u(1,2,3,4);
		Vector v(1,2,3,4);
		Vector w(1.1f,2.2f,3.3f,4.399f);
		unit.addAnnotation("completely equal - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("little bit different - ");
		unit.assertTrue(!u.equals(w));
		unit.addAnnotation("epsilon 0.3 - ");
		unit.assertTrue(!u.equals(w,0.3f));
		unit.addAnnotation("epsilon 0.4 - ");
		unit.assertTrue(u.equals(w,0.4f));
		
		unit.addMarkup("Vector - test basic initialization and copying");
		u = Vector();
		v = Vector(0,0,0,1);
		unit.addAnnotation("default constructor - ");
		unit.assertTrue(u.equals(v));
		u = Vector(13);
		v = Vector(13,13,13,1);
		unit.addAnnotation("one argument - ");
		unit.assertTrue(u.equals(v));
		u = Vector(12,13);
		v = Vector(12,13,0,1);
		unit.addAnnotation("two arguments - ");
		unit.assertTrue(u.equals(v));
		u = Vector(13,14,15);
		v = Vector(13,14,15,1);
		unit.addAnnotation("three arguments - ");
		unit.assertTrue(u.equals(v));
		float varray[] = {19,15,12,13};
		u = Vector(varray);
		v = Vector(19,15,12,13);
		unit.addAnnotation("list argument - ");
		unit.assertTrue(u.equals(v));
		u = Vector(12,1,31,2);
		Vector x = u;
		unit.addAnnotation("copy constructor - ");
		unit.assertTrue(u.equals(x));
		unit.addAnnotation("copy - don't use memory from source - ");
		unit.assertTrue(u.elements != x.elements);
		u = Vector(25,100,0,12);
		v = u;
		unit.addAnnotation("assignment operator - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("assignment - don't use memory from source - ");
		unit.assertTrue(u.elements != v.elements);
		u = Vector(1,22,333,4444);
		v << u;
		x = Vector(1,22,333,4444);
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(x));
		unit.addAnnotation("stream - don't use memory from source - ");
		unit.assertTrue(u.elements != v.elements);
		float varray2[] = {5,2,13,12};
		u << varray2;
		v = Vector(5,2,13,12);
		unit.addAnnotation("array assignment - ");
		unit.assertTrue(u.equals(v));
		u = Vector(2,2143,2,4);
		u >> v;
		x = Vector(2,2143,2,4);
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(x));
		unit.addAnnotation("stream - don't use memory from source - ");
		unit.assertTrue(u.elements != v.elements);
		u = Vector(4,22,626,212);
		u >> varray;
		v = Vector(varray);
		x = Vector(4,22,626,212);
		unit.addAnnotation("array assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("array assignment - ");
		unit.assertTrue(u.equals(x));
		u[0] = 23;
		u[1] = 555;
		u[2] = 1337;
		u[3] = 12;
		v = Vector(23,555,1337,12);
		unit.addAnnotation("index - ");
		unit.assertTrue(u.equals(v));
		const Vector constV(525,44,21,2);
		u = Vector(constV[0],constV[1],constV[2],constV[3]);
		unit.addAnnotation("const index - ");
		unit.assertTrue(constV.equals(u));
		try {
			u[4] = 5;
			unit.addAnnotation("index out of bounds - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}
	}

	void testVector2(XUnit &unit) {
		unit.addMarkup("Vector - test basic arithmetic");
		Vector compare;
		Vector x;
		Vector y(42,555,144);
		Vector z(525,66,771);
		Vector a(1,2,3);
		Vector b(44,5,7,5);
		Vector c(13,12,11);
		Vector d(1,2,3);
		Vector e(4,2,5);
		unit.addAnnotation("adding vectors - ");
		x = 5 * y + z * 2 * 3 + a + b * (d * e) + (y + z) * 10 + --c + 100;
		compare = Vector(10130,9586,14749);
		unit.assertTrue(x.equals(compare));
		Vector v = y;
		v.scalarmult(5.0f).add(z*2*3).add(a).add(b * d.innerprod(e)).add((y+z).scalarmult(10.0f)).add(--c).add(100);
		unit.addAnnotation("adding vectors - ");
		unit.assertTrue(v.equals(compare));
		y = Vector(11,4,10);
		z = Vector(20,2,13);
		x = y % z;
		compare = Vector(32,57,-58);
		unit.addAnnotation("crossmult - ");
		unit.assertTrue(x.equals(compare));
		y = Vector(13,19,7);
		z = Vector(2,23,10);
		y.crossmult(z);
		compare = Vector(29,-116,261);
		unit.addAnnotation("crossmult - ");
		unit.assertTrue(y.equals(compare));
		Vector s(3,2,6);
		s.normalize();
		x = Vector(0.428571f,0.2857142f,0.857142f);
		unit.addAnnotation("normalize - ");
		unit.assertTrue(s.equals(x,0.0001f));
		s = Vector(3,2,6);
		unit.addAnnotation("length - ");
		unit.assertEquals(s.length(),7.0f);
		x = Vector(8,15,55);
		y = Vector(11,17,61);
		unit.addAnnotation("distance - ");
		unit.assertEquals(x.distance(y),7.0f);
		x = Vector(23,884,544,973);
		x.homogenize();
		y = Vector(0.0236f,0.9085f,0.5591f);
		unit.addAnnotation("homogenize - ");
		unit.assertTrue(x.equals(y,0.0001f));
		x = Vector(2,3,5,0);
		x.homogenize();
		y = Vector(2,3,5,0);
		unit.addAnnotation("homogenization not possible - ");
		unit.assertTrue(x.equals(y,0.0001f));
	}

	void testVector3(XUnit &unit) {
		unit.addMarkup("Vector - test matrix multiplication");
		const Matrix m(
			3,5,2,100,
			1,13,2,70,
			44,2,33,151,
			775,4,55,6
			);
		const Vector f(14,2,132,321);
		Vector x = f;
		x.leftmult(m);
		Vector compare(32416,22774,53447,20044);
		unit.addAnnotation("leftmult - ");
		unit.assertTrue(compare.equals(x));
		Vector y = m * f;
		unit.addAnnotation("leftmult - ");
		unit.assertTrue(compare.equals(y));
		x = f;
		x.rightmult(m);
		compare = Vector(254627,1644,22043,23398);
		unit.addAnnotation("rightmult - ");
		unit.assertTrue(compare.equals(x));
		y = f * m;
		unit.addAnnotation("rightmult - ");
		unit.assertTrue(compare.equals(y));
	}

	void testMatrix1(XUnit &unit) {
		unit.addMarkup("Matrix - test equal methods");
		Matrix u(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
		Matrix v(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
		Matrix w(1,2,3,4,5,6,7.1f,8,9,10,11,12.2f,13,14.3f,15,16.3999f);
		unit.addAnnotation("completely equal - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("no match - ");
		unit.assertTrue(!u.equals(w));
		unit.addAnnotation("not in epsilon=0.39 - ");
		unit.assertTrue(!u.equals(w,0.39f));
		unit.addAnnotation("in epsilon=0.4 - ");
		unit.assertTrue(u.equals(w,0.4f));

		unit.addMarkup("Matrix - test basic initialization and copying");
		u = Matrix();
		v = Matrix(
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			0,0,0,1);
		unit.addAnnotation("default constructor - ");
		unit.assertTrue(u.equals(v));
		u = Matrix(66);
		v = Matrix(
			66,66,66,66,
			66,66,66,66,
			66,66,66,66,
			66,66,66,66);
		unit.addAnnotation("constructor, one parameter - ");
		unit.assertTrue(u.equals(v));
		u = Matrix(
			1334,23,
			15,44);
		v = Matrix(
			1334,23,0,0,
			15,44,0,0,
			0,0,1,0,
			0,0,0,1);
		unit.addAnnotation("constructor, four parameters - ");
		unit.assertTrue(u.equals(v));
		u = Matrix(
			23,2153,2,
			1337,23,5523,
			7845,2,1234);
		v = Matrix(
			23,2153,2,0,
			1337,23,5523,0,
			7845,2,1234,0,
			0,0,0,1);
		unit.addAnnotation("constructor, nine parameters - ");
		unit.assertTrue(u.equals(v));
		u = Matrix(Vector(389,2,44,458), Vector(1337,7331,132,4252), Vector(75823,243,13,31), Vector(873,88128,4445,5531));
		v = Matrix(
			389,1337,75823,873,
			2,7331,243,88128,
			44,132,13,4445,
			458,4252,31,5531);
		unit.addAnnotation("constructor, column vectors - ");
		unit.assertTrue(u.equals(v));
		float marray1[] = {9823,575,5872,44,234,2,21,233,5821,445,8690,223,34,1,23,123};
		u = Matrix(marray1);
		v = Matrix(
			9823,234,5821,34,
			575,2,445,1,
			5872,21,8690,23,
			44,233,223,123);
		unit.addAnnotation("constructor, array - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("array, don't share memory - ");
		unit.assertTrue(marray1 != u.elements);
		u = Matrix(
			243,21,558,23,
			12,565,23,12,
			59,656,21,85,
			2356,223,12,52);
		Matrix m = u;
		unit.addAnnotation("copy constructor - ");
		unit.assertTrue(u.equals(m));
		unit.addAnnotation("copy constructor, don't share memory - ");
		unit.assertTrue(u.elements != m.elements);
		u = Matrix(
			4858,213,8,5,
			9,238,8,345,
			2020,241,288,877,
			284,58,85,22);
		v = u;
		unit.addAnnotation("assignment operator - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("assignment operator, don't share memory - ");
		unit.assertTrue(u.elements != v.elements);
		u = Matrix(
			2458,23,558,23,
			892,3,585,28,
			13,14,15,16,
			827,28,1,345);
		v << u;
		w = Matrix(
			2458,23,558,23,
			892,3,585,28,
			13,14,15,16,
			827,28,1,345);
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(w));
		unit.addAnnotation("stream assignment, don't share memory");
		unit.assertTrue(u.elements != v.elements);
		float marray2[] = {87347,988,349,394,374,348,5698,68,124,239,398,67873,58,1,2,34};
		u << marray2;
		v = Matrix(
			87347,374,124,58,
			988,348,239,1,
			349,5698,398,2,
			394,68,67873,34);
		unit.addAnnotation("stream array assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("stream array assignment, don't share memory");
		unit.assertTrue(marray2 != u.elements);
		u = Matrix(
			129489,28,548,1,
			95,843843,2347,5,
			2363,384,1228,84,
			9887,34,213,3);
		u >> v;
		w = Matrix(
			129489,28,548,1,
			95,843843,2347,5,
			2363,384,1228,84,
			9887,34,213,3);
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("stream assignment - ");
		unit.assertTrue(u.equals(w));
		unit.addAnnotation("stream assignment, don't share memory - ");
		unit.assertTrue(u.elements != v.elements);
		u = Matrix(
			958,2384,767,34,
			32,65,32,1,
			59856,34,2,1,
			9845,348,23,66);
		u >> marray1;
		v = Matrix(marray1);
		unit.addAnnotation("stream array assignment - ");
		unit.assertTrue(u.equals(v));
		unit.addAnnotation("stream array assignment, don't share memory - ");
		unit.assertTrue(u.elements != marray1);
		u[0] = 34; u[4] = 85; u[8] = 9992; u[12] = 288;
		u[1] = 873; u[5] = 82; u[9] = 29; u[13] = 8877;
		u[2] = 321; u[6] = 1; u[10] = 882; u[14] = 28;
		u[3]= 38; u[7] = 2; u[11] = 55; u[15] = 7715;
		v = Matrix(
			34,85,9992,288,
			873,82,29,8877,
			321,1,882,28,
			38,2,55,7715);
		unit.addAnnotation("index - ");
		unit.assertTrue(u.equals(v));
		const Matrix x(
			4857,324,2384,454388,
			2,5,39,2,
			2,3,4,5,
			9392,43,6,3);
		for(unsigned int i=0 ; i<16 ; i++) {
			u[i] = x[i];
		}
		unit.addAnnotation("const index - ");
		unit.assertTrue(x.equals(u));
		try {
			u[16] = 5;
			unit.addAnnotation("index out of bounds - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}
	}

	void testMatrix2(XUnit &unit) {
		unit.addMarkup("Matrix - test basic arithmetic");
		Matrix m1(
			459,295,293,4,
			852,2,1,10,
			48,58,28,5,
			1,4,2,4);
		Matrix m2(
			8,2,4,5,
			295,2,6,7,
			92,98,4,5,
			2,1,29,3);
		m1.add(m2);
		Matrix m3(
			467, 297, 297, 9,
			1147, 4, 7, 17,
			140, 156, 32, 10,
			3, 5, 31, 7);
		unit.addAnnotation("addition - ");
		unit.assertTrue(m1.equals(m3));
		m1.scalarmult(10);
		m3 = Matrix(
			4670, 2970, 2970, 90,
			11470, 40, 70, 170,
			1400, 1560, 320, 100,
			30, 50, 310, 70);
		unit.addAnnotation("scalarmult - ");
		unit.assertTrue(m1.equals(m3));
		m1.add(10);
		m3 = Matrix(
			4680, 2980, 2980, 100,
			11480, 50, 80, 180,
			1410, 1570, 330, 110,
			40, 60, 320, 80);
		unit.addAnnotation("addition - ");
		unit.assertTrue(m1.equals(m3));
		m1 = Matrix(
			5,32,2,1,
			6,8,92,4,
			1,4,9,3,
			2,1,6,4);
		m2 = Matrix(
			56,2,1,2,
			6,7,1,2,
			2,1,9,10,
			2,9,8,7);
		m3 = m1;
		m3.leftmult(m2);
		Matrix m4(
			297, 1814, 317, 75,
			77, 254, 677, 45,
			45, 118, 237, 73,
			86, 175, 946, 90);
		unit.addAnnotation("leftmult - ");
		unit.assertTrue(m3.equals(m4));
		m3 = m1;
		m3.rightmult(m2);
		m4 = Matrix(
			478, 245, 63, 101,
			576, 196, 874, 976,
			104, 66, 110, 121,
			138, 53, 89, 94);
		unit.addAnnotation("rightmult - ");
		unit.assertTrue(m3.equals(m4));
		m1 = Matrix(
			324, 2, 421, 2,
			95, 1, 3, 5,
			9, 555, 66, 744,
			923, 23, 56, 77
			);
		m1.transpose();
		m2 = Matrix(
			324, 95, 9, 923,
			2, 1, 555, 23,
			421, 3, 66, 56,
			2, 5, 744, 77);
		unit.addAnnotation("transpose - ");
		unit.assertTrue(m1.equals(m2));
		m1 = Matrix(
			15,2,5,7,
			6,28,6,8,
			24,9,2,8,
			4,11,31,2);
		unit.addAnnotation("determinant - ");
		unit.assertEquals(m1.determinant(),54670.0f);
		m1 = Matrix(
			2,9,3,8,
			17,6,1,2,
			5,3,5,4,
			6,3,13,7);
		unit.addAnnotation("determinant - ");
		unit.assertEquals(m1.determinant(),627.0f);
		unit.addAnnotation("cofactor(0,0) - ");
		unit.assertEquals(m1.cofactor(0,0),-63.0f);
		unit.addAnnotation("cofactor(2,0) - ");
		unit.assertEquals(m1.cofactor(2,0),321.0f);
		unit.addAnnotation("cofactor(3,0) - ");
		unit.assertEquals(m1.cofactor(3,0),-108.0f);
		unit.addAnnotation("cofactor(0,1) - ");
		unit.assertEquals(m1.cofactor(0,1),230.0f);
		unit.addAnnotation("cofactor(1,1) - ");
		unit.assertEquals(m1.cofactor(1,1),213.0f);
		m1.inverse();
		m2 = Matrix(
			-0.1005f, -0.0191f, 0.5120f, -0.1722f,
			0.3668f, 0.3397f, -2.1707f, 0.7241f,
			0.1292f, 0.1675f, -1.2297f, 0.5072f,
			-0.3110f, -0.4402f, 2.7751f, -0.9617f);
		unit.addAnnotation("inverse - ");
		unit.assertTrue(m1.equals(m2,0.0001f));
		m1 = Matrix(
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			1,0,0,0);
		m1.inverse();
		m2 = Matrix(
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			1,0,0,0);
		unit.addAnnotation("inverse not existing - ");
		unit.assertTrue(m1.equals(m2));
	}

	void testMatrix3(XUnit &unit) {
		unit.addMarkup("Matrix - test basic arithmetic operators");
		Matrix m1(
			459,295,293,4,
			852,2,1,10,
			48,58,28,5,
			1,4,2,4);
		Matrix m2(
			8,2,4,5,
			295,2,6,7,
			92,98,4,5,
			2,1,29,3);
		Matrix m3 = m1 + m2;
		Matrix m4(
			467, 297, 297, 9,
			1147, 4, 7, 17,
			140, 156, 32, 10,
			3, 5, 31, 7);
		unit.addAnnotation("addition operator - ");
		unit.assertTrue(m3.equals(m4));
		m2 = m3;
		m3 = m2 * 10;
		m4 = Matrix(
			4670, 2970, 2970, 90,
			11470, 40, 70, 170,
			1400, 1560, 320, 100,
			30, 50, 310, 70);
		unit.addAnnotation("scalarmult operator - ");
		unit.assertTrue(m3.equals(m4));
		m3 = 10 * m2;
		unit.addAnnotation("scalarmult operator - ");
		unit.assertTrue(m3.equals(m4));
		m1 = m4;
		m2 = m1 + 10;
		m3 = Matrix(
			4680, 2980, 2980, 100,
			11480, 50, 80, 180,
			1410, 1570, 330, 110,
			40, 60, 320, 80);
		unit.addAnnotation("addition operator - ");
		unit.assertTrue(m2.equals(m3));
		m2 = 10 + m1;
		unit.addAnnotation("addition operator - ");
		unit.assertTrue(m2.equals(m3));
		m1 = Matrix(
			5,32,2,1,
			6,8,92,4,
			1,4,9,3,
			2,1,6,4);
		m2 = Matrix(
			56,2,1,2,
			6,7,1,2,
			2,1,9,10,
			2,9,8,7);
		m3 = m2 * m1;
		m4 = Matrix(
			297, 1814, 317, 75,
			77, 254, 677, 45,
			45, 118, 237, 73,
			86, 175, 946, 90);
		unit.addAnnotation("mult operator - ");
		unit.assertTrue(m3.equals(m4));
		m3 = m1 * m2;
		m4 = Matrix(
			478, 245, 63, 101,
			576, 196, 874, 976,
			104, 66, 110, 121,
			138, 53, 89, 94);
		unit.addAnnotation("mult operator - ");
		unit.assertTrue(m3.equals(m4));
		m3 = -m1;
		m4 = Matrix(
			-5,-32,-2,-1,
			-6,-8,-92,-4,
			-1,-4,-9,-3,
			-2,-1,-6,-4);
		unit.addAnnotation("sign operator - ");
		unit.assertTrue(m3.equals(m4));
		m1 = Matrix(
			324, 2, 421, 2,
			95, 1, 3, 5,
			9, 555, 66, 744,
			923, 23, 56, 77
			);
		m2 = !m1;
		m3 = Matrix(
			324, 95, 9, 923,
			2, 1, 555, 23,
			421, 3, 66, 56,
			2, 5, 744, 77);
		unit.addAnnotation("transpose operator - ");
		unit.assertTrue(m2.equals(m3));
		m1 = Matrix(
			2,9,3,8,
			17,6,1,2,
			5,3,5,4,
			6,3,13,7);
		m2 = m1^0;
		m3 = Matrix();
		unit.addAnnotation("^0 - ");
		unit.assertTrue(m2.equals(m3));
		m2 = m1^-1;
		m3 = Matrix(
			-0.1005f, -0.0191f, 0.5120f, -0.1722f,
			0.3668f, 0.3397f, -2.1707f, 0.7241f,
			0.1292f, 0.1675f, -1.2297f, 0.5072f,
			-0.3110f, -0.4402f, 2.7751f, -0.9617f);
		unit.addAnnotation("^-1 - ");
		unit.assertTrue(m2.equals(m3,0.0001f));
		m2 = m1^-8;
		m3 = Matrix(
			8.3684f, 11.7516f, -82.5238f, 32.0033f,
			-31.1825f, -43.7891f, 307.5016f, -119.2514f,
			-19.6095f, -27.5373f, 193.3762f, -74.9927f,
			39.8311f, 55.9342f, -392.7893f, 152.3266f);
		unit.addAnnotation("^-8 - ");
		unit.assertTrue(m2.equals(m3,0.1f));
		m2 = m1^7;
		m3 = Matrix(
			1187300000.0f, 923600000.0f, 881500000.0f, 907500000.0f,
			1365700000.0f, 1053400000.0f, 1010500000.0f, 1035200000.0f,
			836200000.0f, 647200000.0f, 619800000.0f, 636200000.0f,
			1344100000.0f, 1040600000.0f, 996700000.0f, 1023000000.0f);
		unit.addAnnotation("^7 - ");
		unit.assertTrue(m2.equals(m3,100000.0f));
		m1 = Matrix(
			1,0,0,1,
			0,0,1,0,
			0,1,0,1,
			1,0,0,1
			);
		m1 = m1^-2;
		m2 = Matrix(
			2, 0, 0, 2,
			0, 1, 0, 1,
			1, 0, 1, 1,
			2, 0, 0, 2);
		unit.addAnnotation("^-2, singular - ");
		unit.assertTrue(m1.equals(m2));
	}

	void testMatrix4(XUnit &unit) {
		unit.addMarkup("Matrix - test terms");
		Matrix m1(
			2,3,0,2,
			5,2,3,0,
			0,4,5,6,
			1,2,3,4);
		Matrix m2(
			1,0,1,1,
			0,0,0,1,
			0,1,0,0,
			1,0,0,0);
		Matrix m3(
			1,0,0,0,
			0,1,0,0,
			0,0,0,1,
			0,0,0,0);
		Matrix m4(
			1,0,2,3,
			4,5,0,2,
			1,2,0,3,
			4,4,4,4);
		Matrix m5 = (m1^-1) * m2 * 10 + -m3 + 15 + m4 * !m1 * m2;
		Matrix m6(
			46.0000f, 39.8966f, 23.6897f, 35.5517f,
			59.0000f, 50.1379f, 44.4138f, 74.9310f,
			41.0000f, 43.4138f, 26.2414f, 35.7931f,
			90.5000f, 71.8966f, 43.6897f, 82.0517f
			);
		unit.addAnnotation("operator term - ");
		unit.assertTrue(m5.equals(m6,0.001f));
		Matrix m7 = m1;
		Matrix m8 = m2;
		Matrix m9 = m1;
		m7.inverse().rightmult(m2).scalarmult(10).add(-m3).add(15).add(m8.leftmult(m9.transpose()).leftmult(m4));
		unit.addAnnotation("term - ");
		unit.assertTrue(m7.equals(m6,0.001f));
	}

	void testMatrix5(XUnit &unit) {
		unit.addMarkup("Matrix - test matrix setters");
		Matrix m1(
			34,23,69,3,
			932,394,2,6,
			923923,329,249,294,
			59,394,23,6);
		m1.identity();
		Matrix m2(
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			0,0,0,1);
		unit.addAnnotation("identity - ");
		unit.assertTrue(m1.equals(m2));
		Vector v1(13,55,4);
		m1.rotate(v1,0.65f);
		m2 = Matrix(
			0.806819f , 0.00269419f , 0.590792f , 0 ,
			0.0881471f , 0.988248f , -0.124885f , 0 ,
			-0.584185f , 0.152837f , 0.7971f , 0 ,
			0 , 0 , 0 , 1);
		unit.addAnnotation("rotation - ");
		unit.assertTrue(m1.equals(m2,0.0001f));
		v1 = Vector(33,43,245,234343);
		m1.translate(v1);
		m2 = Matrix(
			1,0,0,33,
			0,1,0,43,
			0,0,1,245,
			0,0,0,1
			);
		unit.addAnnotation("translation - ");
		unit.assertTrue(m1.equals(m2));
		v1 = Vector(45,523,15,643653);
		m1.scale(v1);
		m2 = Matrix(
			45,0,0,0,
			0,523,0,0,
			0,0,15,0,
			0,0,0,1);
		unit.addAnnotation("scale - ");
		unit.assertTrue(m1.equals(m2));
		v1 = Vector(5,9,11);
		m1.shear(v1);
		m2 = Matrix(
			1,0,-5.0f/11.0f,0,
			0,1,-9.0f/11.0f,0,
			0,0,1,0,
			0,0,0,1);
		unit.addAnnotation("shear - ");
		unit.assertTrue(m1.equals(m2,0.00001f));
		m1 = Matrix(
			34,23,69,3,
			932,394,2,6,
			923923,329,249,294,
			59,394,23,6);
		m1.submatrix();
		m2 = Matrix(
			34,23,69,0,
			932,394,2,0,
			923923,329,249,0,
			0,0,0,1);
		unit.addAnnotation("submatrix - ");
		unit.assertTrue(m1.equals(m2));
		m1 = Matrix(
			34,23,69,3,
			932,394,2,6,
			9,329,249,294,
			59,394,23,6);
		m1.normalMatrix();
		m2 = Matrix(
			0.0052f, -0.0123f, 0.0160f, 0,
			0.0009f, 0.0004f, -0.0006f, 0,
			-0.0014f, 0.0034f, -0.0004f, 0,
			0, 0, 0, 1);
		unit.addAnnotation("normalmatrix - ");
		unit.assertTrue(m1.equals(m2,0.0001f));

		Vector position(3,5,4);
		Vector view(10,0,-10);
		Vector up(0,2,0);
		m1.viewMatrix(position,view,up);
		Vector worldPoint((7.0f/sqrt(2.0f)) + 3.0f,7,(-1/sqrt(2.0f)) + 4.0f);
		v1 = m1 * worldPoint;
		Vector v2(3,2,-4);
		unit.addAnnotation("viewmatrix - ");
		unit.assertTrue(v1.equals(v2,0.0001f));
		m1.perspectiveMatrix(Tau/3.0f, 1920.0f, 1200.0f, 1.5f, 1000.0f);
		Vector projectionPoint1(4.1569219f,2.598076f,-1.5f);
		v1 = m1 * projectionPoint1;
		v1.homogenize();
		v2 = Vector(1,1,-1);
		unit.addAnnotation("perspectivematrix - point 1 - ");
		unit.assertTrue(v1.equals(v2,0.0001f));
		Vector projectionPoint2(9.2376043f,8.660254f,-10);
		v1 = m1 * projectionPoint2;
		v1.homogenize();
		unit.addAnnotation("perspectivematrix - point 2 - ");
		unit.assertEquals(v1[0],0.333333f,0.0001f);
		unit.addAnnotation("perspectivematrix - point 2 - ");
		unit.assertEquals(v1[1],0.5f,0.0001f);
		unit.addAnnotation("perspectivematrix - point 2 - ");
		unit.assertTrue(v1[2] >= -1);
		unit.addAnnotation("perspectivematrix - point 2 - ");
		unit.assertTrue(v1[2] <= 1);
		Vector projectionPoint3(-2771.281292f,-1732.050807f,-1000);
		v1 = m1 * projectionPoint3;
		v1.homogenize();
		v2 = Vector(-1,-1,1);
		unit.addAnnotation("perspectivematrix - point 3 - ");
		unit.assertTrue(v1.equals(v2,0.0001f));
		m1.orthographicPerspeciveMatrix(-1200,1000,-600,800,100,400);
		projectionPoint1 = Vector(-1200,-600,-100);
		v1 = m1 * projectionPoint1;
		v2 = Vector(-1,-1,-1);
		unit.addAnnotation("ortho - point 1 - ");
		unit.assertTrue(v1.equals(v2,0.0001f));
		projectionPoint2 = Vector(1000,800,-400);
		v1 = m1 * projectionPoint2;
		v2 = Vector(1,1,1);
		unit.addAnnotation("ortho - point 2 - ");
		unit.assertTrue(v1.equals(v2,0.0001f));
	}

	void testMath(const string logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testmath",new ListLogger());
			Logger::setDefaultLogger("testmath");

			testVector1(unit);
			testVector2(unit);
			testVector3(unit);
			testMatrix1(unit);
			testMatrix2(unit);
			testMatrix3(unit);
			testMatrix4(unit);
			testMatrix5(unit);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif