#ifndef _STARTDEMO_CPP_
#define _STARTDEMO_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Demo.h>
#include <sx/Log4SX.h>
#include <sx/SX.h>
#include <sx/Exception.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QSplashScreen>
#include <QDesktopWidget>
#include <QIcon>
#include <GL/glut.h>
#include <string>
using namespace std;

/**
 * Entry point of the demo
 */
int main(int argc, char **argv) {
	QApplication app(argc,argv);
	QSize desktopSize = QApplication::desktop()->size();
	QSplashScreen splashScreen(QPixmap("../data/texture/Splashscreen.png"));
	splashScreen.show();
	app.processEvents();

	bool useFullscreen = false;
	bool useScreenDimensions = true;
	int imageWidth = 1024;
	int imageHeight = 800;
	string mapconfigfile = "../data/map1.c";

	try {
		sx::Logger::addLogger("demo",new sx::FileLogger("ShadeXEngineDemo.html"));
		sx::Logger::setDefaultLogger("demo");
	} catch(sx::Exception &e) {
		sx::Logger::get() << sx::Level(sx::L_ERROR) << e.getMessage();
	}

	try {
		//parse app configuration
		sx::XTag *sceneTag = sx::parseSXFile("../data/configuration.c");

		//get map config file path
		try {
			mapconfigfile = sceneTag->getStrAttribute("map");
		} catch(sx::Exception &) {
		}

		//fullscreen
		string fullscreen = sceneTag->getStrAttribute("fullscreen");
		if(fullscreen.compare("true") != 0 && fullscreen.compare("false") != 0) {
			sx::Logger::get() << sx::Level(sx::L_ERROR) << "Error: attribute fullscreen in scene.c must have value \"true\" or \"false\"";
			return 1;
		}
		useFullscreen = fullscreen.compare("true") == 0;

		//usescreendimensions
		string usescreendimensions = sceneTag->getStrAttribute("usescreendimensions");
		if(usescreendimensions.compare("true") != 0 && usescreendimensions.compare("false") != 0) {
			sx::Logger::get() << sx::Level(sx::L_ERROR) << "Error: attribute usescreendimensions in scene.c must have value \"true\" or \"false\"";
			return 1;
		}
		useScreenDimensions = usescreendimensions.compare("true") == 0;

		//width and height of internal render targets (and window, if fullscreen isn't used)
		try {
			imageWidth = sceneTag->getRealAttribute("width");
			imageHeight = sceneTag->getRealAttribute("height");
		} catch(sx::Exception &) {
			if(!useScreenDimensions || !useFullscreen) {
				throw sx::Exception("Error: real numbers must be assigned to attributes width and height in scene.c");
			}
		}

	} catch(sx::Exception &e) {
		sx::Logger::get() << sx::Level(sx::L_ERROR) << e.getMessage();
		return 1;
	}

	sx::SXWidget widget;
	widget.setWindowIcon(QIcon("../data/SX.png"));
	widget.setWindowTitle("ShadeXEngine v1.0b demo");
	widget.setMinimumSize(imageWidth,imageHeight);
	widget.move((desktopSize.width()/2)-(imageWidth/2),(desktopSize.height()/2)-(imageHeight/2));
	widget.renderPeriodically(0.01f);
	widget.setShowCursor(false);
	sx::Demo *demo = 0;
	try {
		demo = new sx::Demo(mapconfigfile);
	} catch(sx::Exception &) {
		sx::Logger::get() << sx::Level(sx::L_ERROR) << "could not open map file " << mapconfigfile;
		return 1;
	}
	demo->setUseScreenDimensions(useScreenDimensions);
	demo->setImageWidth(imageWidth);
	demo->setImageHeight(imageHeight);
	sx::Renderer::renderer = demo;
	widget.addRenderListener(*demo);
	QObject::connect(&widget,SIGNAL(finishedRendering()),&widget,SLOT(close()));
	if(useFullscreen) {
		widget.showFullScreen();
	}
	try {
		widget.show();
		splashScreen.finish(&widget);
	} catch(sx::Exception &e) {
		sx::Logger::get() << e.getMessage();
	}

	return app.exec();
}

#endif