#ifndef _DEMO_ENTITIES_GRASS_CPP_
#define _DEMO_ENTITIES_GRASS_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2013 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/ShadeX.h>
#include <sx/SXMath.h>
#include <cmath>
using namespace std;

namespace sx {

	Grass::Grass() {
		Pass &genGrass = SX::shadeX.getPass("gen.grass.pass");
		genGrass.render();
		viewerPosition = &SX::shadeX.getUniformVector("mainscene.cam.position");
		model = &SX::shadeX.getUniformMatrix("gen.grass2.pass.model");
		wave1 = &SX::shadeX.getUniformVector("mainobj.grass.wave1");
		wave2 = &SX::shadeX.getUniformVector("mainobj.grass.wave2");
	}

	Grass::~Grass() {
	}

	void Grass::calculate(SXRenderArea &area) {
		Vector grassArea = grassPosition * (0.1f);
		Vector viewerArea = *viewerPosition * (0.1f);
		grassArea = Vector(floor(grassArea[0]) , floor(grassArea[1])) * 10.0f;
		viewerArea = Vector(floor(viewerArea[0]) , floor(viewerArea[1])) * 10.0f;
		if(!grassArea.equals(viewerArea,0.1f)) {
			//grass's center is not located in the box metric's 5 neighbourhood
			//hence update position of grass
			grassPosition = viewerArea;
			*model = Matrix().translate(grassPosition + Vector(5.0f,5.0f));
			Pass &genGrass = SX::shadeX.getPass("gen.grass2.pass");
			genGrass.render();
		}

		//move windwaves
		*wave1 = Vector(0.1f,0.2f) * (float)area.getTime() * 0.3f;
		*wave2 = Vector(-0.3f,0.05f) * (float)area.getTime() * 0.3f;
		
	}

}

#endif