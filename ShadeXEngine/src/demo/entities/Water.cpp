#ifndef _DEMO_ENTITIES_WATER_CPP_
#define _DEMO_ENTITIES_WATER_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/ShadeX.h>
#include <sx/SXMath.h>
#include <cmath>
using namespace std;

namespace sx {

	Water::Water() {
		viewerPosition = &SX::shadeX.getUniformVector("mainscene.cam.position");
		view = &SX::shadeX.getUniformVector("mainscene.cam.view");
		up = &SX::shadeX.getUniformVector("mainscene.cam.up");
		reflectionprojview = &SX::shadeX.getUniformMatrix("reflectionscene.viewprojection");
		reflectionview = &SX::shadeX.getUniformMatrix("reflectionscene.view");
		reflectionnormalview = &SX::shadeX.getUniformMatrix("reflectionscene.normalv");
		model = &SX::shadeX.getUniformMatrix("mainobj.water.model");
		waterheight = &SX::shadeX.getUniformFloat("mainscene.waterheight");
		abovewater = &SX::shadeX.getUniformFloat("mainscene.abovewater");
		wave1 = &SX::shadeX.getUniformVector("postscene.wave1");
		wave2 = &SX::shadeX.getUniformVector("postscene.wave2");
	}

	Water::~Water() {
	}

	void Water::calculate(SXRenderArea &area) {
		
		//the water is rendered by projecting reflection and refraction onto a grid
		//the grid is always moves with the viewer
		//the terrain grid is assumed to have 20 x 20 equidistant vertices with coordinates in the quad with coordinates in [-300,300]^2 x {0}
		float delta = 300.0f/19.0f;
		float indexX = floor(((*viewerPosition)[0] + (delta/2.0f))/delta);
		float indexY = floor(((*viewerPosition)[1] + (delta/2.0f))/delta);
		*model = Matrix().translate(Vector(delta * indexX , delta * indexY)) * Matrix().translate(Vector(0,0,waterheight->value));
	
		//above water?
		if(waterheight->value < (*viewerPosition)[2]) {
			abovewater->value = 1;
		} else {
			abovewater->value = 0;
		}

		//calculate camera for reflection pass
		//reflect whole camera setting to optain the viewprojectionmatrix
		Vector reflectionPos = *viewerPosition;
		Vector reflectionView = *view;
		Vector reflectionUp = *up;
		reflectionPos[2] = waterheight->value - (reflectionPos[2] - waterheight->value);
		reflectionView[2] = -reflectionView[2];
		reflectionUp[2] = -reflectionUp[2];
		*reflectionview = Matrix().viewMatrix(reflectionPos,reflectionView,reflectionUp);
		*reflectionnormalview = *reflectionview;
		reflectionnormalview->submatrix();
		*reflectionprojview = Matrix().perspectiveMatrix(Tau*0.25f,(float)area.getWidth(),(float)area.getHeight(),0.1f,1000.0f) * *reflectionview;
	
		//move waves
		*wave1 = Vector(0.1f,0.2f) * (float)area.getTime();
		*wave2 = Vector(-0.3f,0.05f) * (float)area.getTime();
	}

}

#endif