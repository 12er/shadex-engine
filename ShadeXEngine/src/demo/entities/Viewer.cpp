#ifndef _DEMO_ENTITIES_VIEWER_CPP_
#define _DEMO_ENTITIES_VIEWER_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/Demo.h>
#include <demo/ShadeX.h>
#include <sx/SX.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <map>
#include <vector>
using namespace std;

namespace sx {

	RenderStageData::RenderStageData() {
		backgroundSound = 1;
		time = 0;
		renderstage = STAGE_COMMON;
		emitsmoke = false;
		lightshaftintensity = 0;
		sunrotationspeed = 0;
	}

	RenderStageData::RenderStageData(const RenderStageData &rsd) {
		backgroundSound = rsd.backgroundSound;
		time = rsd.time;
		renderstage = rsd.renderstage;
		emitsmoke = rsd.emitsmoke;
		smokeposition = rsd.smokeposition;
		fireflyattractor = rsd.fireflyattractor;
		lightshaftintensity = rsd.lightshaftintensity;
		sunrotationspeed = rsd.sunrotationspeed;
	}

	RenderStageData &RenderStageData::operator = (const RenderStageData &rsd) {
		if(&rsd == this) {
			return this [0];
		}
		backgroundSound = rsd.backgroundSound;
		time = rsd.time;
		renderstage = rsd.renderstage;
		emitsmoke = rsd.emitsmoke;
		smokeposition = rsd.smokeposition;
		fireflyattractor = rsd.fireflyattractor;
		lightshaftintensity = rsd.lightshaftintensity;
		sunrotationspeed = rsd.sunrotationspeed;
		return this [0];
	}

	Viewer::Viewer() {
		position = &SX::shadeX.getUniformVector("mainscene.cam.position");
		view = &SX::shadeX.getUniformVector("mainscene.cam.view");
		up = &SX::shadeX.getUniformVector("mainscene.cam.up");
		projectionview = &SX::shadeX.getUniformMatrix("mainscene.viewprojection");
		mainprojectionview = &SX::shadeX.getUniformMatrix("mainscene.main.viewprojection");
		viewM = &SX::shadeX.getUniformMatrix("mainscene.view");
		proj2world = &SX::shadeX.getUniformMatrix("mainscene.proj2world");
		normalview = &SX::shadeX.getUniformMatrix("mainscene.normalv");
		SXengineTime = &SX::shadeX.getUniformFloat("SXengine.time");
		SXengineLogo = &SX::shadeX.getUniformFloat("SXengine.logo");
		glow = &SX::shadeX.getUniformFloat("postscene.glow");
		showlogo = &SX::shadeX.getUniformFloat("postscene.showlogo");
		SXengineCoordTransform = &SX::shadeX.getUniformMatrix("SXengineCoord.transform");
		SXengineTexture = &SX::shadeX.getTexture("SXengine.texture");

		audioPass = &SX::shadeX.getAudioPass("backgroundmusic.pass");
		backgroundMusic = &SX::shadeX.getAudioObject("backgroundmusic.object");
		musicVolume = &SX::shadeX.getUniformFloat("backgroundmusic.volume");

		*position = Vector(0,0,5);
		*view = Vector(1,1,0);
		*up = Vector(0,0,1);

		pressedControlKey = 0.0f;

		switchedMode = 0.0f;
		demoMode = true;

		switchedEmitSmoke = 0.0f;
		emitSmoke = true;

		switchedMoveSun = 0.0f;
		moveSun = false;

		runtime = 0;
		started = false;
		demotime = 0;

		//read scene data
		try {
			XTag *sceneTag = Renderer::renderer->getConfiguration();

			//get logo duration in seconds
			maxLogoTime = (float)sceneTag->getRealAttribute("maxlogotime");

			//get background music
			bool criticalException = false;
			try {
				string bgmusicPath = sceneTag->getStrAttribute("backgroundmusic");
				AudioBuffer &bgmusicBuffer = SX::shadeX.getAudioBuffer("backgroundmusic.buffer");
				bgmusicBuffer.setPath(bgmusicPath);
				bgmusicBuffer.load();
				if(!bgmusicBuffer.isLoaded()) {
					criticalException = true;
					stringstream errmsg;
					errmsg << "Error: could not load backgroundmusic from path " << bgmusicPath;
					throw Exception(errmsg.str());
				}
			} catch(Exception &e) {
				if(criticalException) {
					throw e;
				}
			}

			//read camera trajectory data
			vector<XTag *> keypoints;
			sceneTag->getTags("camerapath.keypoint",keypoints);
			if(keypoints.size() < 2) {
				throw Exception("Error: scene must contain at least two keypoint tags in tag camerapath");
			}
			for(vector<XTag *>::iterator iter = keypoints.begin() ; iter != keypoints.end() ; iter++) {
				XTag *keypointTag = (*iter);
				XTag *positionTag = keypointTag->getFirst("position");
				XTag *viewTag = keypointTag->getFirst("view");
				vector<double> positionComponents = parseNumbers(positionTag->getTexts());
				vector<double> viewComponents = parseNumbers(viewTag->getTexts());
				if(positionComponents.size() < 3 || viewComponents.size() < 3) {
					throw Exception("Error: position and view of each keypoint must contain at least three real components");
				}
				float timeAttr = (float)keypointTag->getRealAttribute("time");
				Vector positionVector((float)positionComponents[0],(float)positionComponents[1],(float)positionComponents[2]);
				Vector viewVector((float)viewComponents[0],(float)viewComponents[1],(float)viewComponents[2]);

				path.push_back(pair<Vector,pair<Vector,float>>(positionVector,pair<Vector,float>(viewVector,timeAttr)));
			}

			//read render stage attributes
			vector<XTag *> renderstagetags;
			sceneTag->getTags("renderstages.*",renderstagetags);
			RenderStageData lastRenderStage;
			for(vector<XTag *>::iterator iter = renderstagetags.begin() ; iter != renderstagetags.end() ; iter++) {
				renderStageData.push_back(RenderStageData());
				RenderStageData &rsd = renderStageData[renderStageData.size()-1];
				rsd = lastRenderStage;

				if((*iter)->name.compare("fluid") == 0) {
					rsd.renderstage = STAGE_USESMOKE;
				} else if((*iter)->name.compare("fluidlightshaft") == 0) {
					rsd.renderstage = STAGE_USESMOKELIGHTSHAFT;
				} else if((*iter)->name.compare("common") == 0) {
					rsd.renderstage = STAGE_COMMON;
				} else if((*iter)->name.compare("lightshaft") == 0) {
					rsd.renderstage = STAGE_USELIGHTSHAFT;
				}

				try {
					double timeAttr = (*iter)->getRealAttribute("time");
					rsd.time = (float)timeAttr;
				} catch(Exception &) {
				}

				try {
					double soundAttr = (*iter)->getRealAttribute("backgroundsound");
					rsd.backgroundSound = (float)soundAttr;
				} catch(Exception &) {
				}

				bool emitsmokeError = false;
				try {
					string emitsmokeAttr = (*iter)->getStrAttribute("emitsmoke");
					if(emitsmokeAttr.compare("true") != 0 && emitsmokeAttr.compare("false") != 0) {
						emitsmokeError = true;
						throw Exception();
					}
					rsd.emitsmoke = emitsmokeAttr.compare("true") == 0;
				} catch(Exception &) {
					if(emitsmokeError) {
						throw Exception("Error: attribute emitsmoke in renderstages can only have values \"true\" or \"false\"");
					}
				}

				bool smokepositionError = false;
				try {
					XTag *smokepositionTag = (*iter)->getFirst("smokeposition");
					try {
						vector<double> spcomponents = parseNumbers(smokepositionTag->getDirectTexts());
						if(spcomponents.size() < 3) {
							throw Exception();
						}
						rsd.smokeposition = Vector((float)spcomponents[0],(float)spcomponents[1],(float)spcomponents[2]);
					} catch(Exception &) {
						smokepositionError = true;
						throw Exception();
					}
				} catch(Exception &) {
					if(smokepositionError) {
						throw Exception("Error: tag smokeposition must contain a string containing three double numbers");
					}
				}

				bool fireflyattractorError = false;
				try {
					XTag *fireflyattractorTag = (*iter)->getFirst("fireflyattractor");
					try {
						vector<double> facomponents = parseNumbers(fireflyattractorTag->getDirectTexts());
						if(facomponents.size() < 3) {
							throw Exception();
						}
						rsd.fireflyattractor = Vector((float)facomponents[0],(float)facomponents[1],(float)facomponents[2]);
					} catch(Exception &) {
						fireflyattractorError = true;
						throw Exception();
					}
				} catch(Exception &) {
					if(fireflyattractorError) {
						throw Exception("Error: tag fireflyattractor must contain a string containing three double numbers");
					}
				}

				try {
					rsd.lightshaftintensity = (float)(*iter)->getRealAttribute("lightshaftintensity");
				} catch(Exception &) {
				}

				try {
					double sunrotationspeedAttr = (*iter)->getRealAttribute("sunrotationspeed");
					rsd.sunrotationspeed = (float)sunrotationspeedAttr;
				} catch(Exception &) {
				}
				
				lastRenderStage = rsd;
			}


		} catch(Exception &e) {
			Logger::get() << Level(L_ERROR) << e.getMessage();
			throw e;
		}

		displaylogo = false;
		SXengineTime->value = 0.0f;
		lasttime = 0;

		if(path.size() > 0) {
			const pair<Vector,pair<Vector,float>> &token = path.front();
			consumeNode = 0;
			*position = token.first;
			*view = token.second.first;
			lastpos = *position;
			lasttime = token.second.second;
			path.erase(path.begin());
			if(path.size() > 0) {
				currenttimedelta = path.front().second.second - lasttime;
			}
		}
		if(renderStageData.size() > 0) {
			currentRenderStageData = renderStageData.front();
			currentLightdir = Vector(-1,-1,-1);
			renderStageData.erase(renderStageData.begin());
		}
	}

	Viewer::~Viewer() {
		saveConfiguration();
	}

	void Viewer::processPath(SXRenderArea &area) {
		if(Renderer::renderer == 0) {
			return;
		}

		//logo settings
		if(displaylogo) {
			SXengineLogo->value = 1.0f;
			Pass &SXlogoPass = SX::shadeX.getPass("SXengine.pass");
			SXlogoPass.render();

			//logo coord transform
			float screenWidth = (float)area.getWidth();
			float screenHeight = (float)area.getHeight();
			float SXTexWidth = (float)SXengineTexture->getWidth();
			float SXTexHeight = (float)SXengineTexture->getHeight();
			*SXengineCoordTransform = 
				Matrix().translate(Vector(0.5f,0.5f,0.0f)) * 
				Matrix().scale(Vector(screenWidth/SXTexWidth,screenHeight/SXTexHeight,1.0f)) * 
				Matrix().translate(Vector(-0.5f,-0.5f,0.0f));

			//logo scalar values
			showlogo->value = min(1.0f,SXengineTime->value);
			float expglow = 2.0f * exp(-(SXengineTime->value - 1.3f)*(SXengineTime->value - 1.3f));
			glow->value = max(1.0f,expglow);

			//logo backgroundmusic
			if(maxLogoTime > 0) {
				musicVolume->value = currentRenderStageData.backgroundSound * (maxLogoTime - SXengineTime->value)/maxLogoTime;
			}

			SXengineTime->value += (float)area.getDeltaTime();
			if(SXengineTime->value > maxLogoTime) {
				area.stopRendering();
			}
			return;
		}

		//start music at beginning
		if(!backgroundMusic->isPlaying()) {
			backgroundMusic->start();
		}
		//process render stage data tokens
		if(renderStageData.size() > 0 && renderStageData.front().time <= consumeNode) {
			currentRenderStageData = renderStageData.front();
			renderStageData.erase(renderStageData.begin());
		}
		//set render stage
		Renderer::renderer->setRenderStage(currentRenderStageData.renderstage);
		//audio settings
		if(renderStageData.size() > 0) {
			RenderStageData nextToken = renderStageData.front();
			float nextSoundIntensity = (float) (consumeNode - currentRenderStageData.time)/(nextToken.time - currentRenderStageData.time);
			musicVolume->value = currentRenderStageData.backgroundSound * (1 - nextSoundIntensity) + nextToken.backgroundSound * nextSoundIntensity;
		} else {
			musicVolume->value = currentRenderStageData.backgroundSound;
		}
		//smoke settings
		Renderer::renderer->setEmitSmoke(currentRenderStageData.emitsmoke);
		if(currentRenderStageData.renderstage == STAGE_USESMOKE || currentRenderStageData.renderstage == STAGE_USESMOKELIGHTSHAFT) {
			Renderer::renderer->setPreferredBottomPosition(currentRenderStageData.smokeposition);
		}
		//lightshaft factor
		if(renderStageData.size() > 0) {
			RenderStageData nextToken = renderStageData.front();
			if(currentRenderStageData.time != nextToken.time) {
				Renderer::renderer->setBlendLightshafts( currentRenderStageData.lightshaftintensity + (nextToken.lightshaftintensity - currentRenderStageData.lightshaftintensity) * (consumeNode - currentRenderStageData.time)/(nextToken.time - currentRenderStageData.time) );
			} else {
				Renderer::renderer->setBlendLightshafts(currentRenderStageData.lightshaftintensity);
			}
		} else {
			Renderer::renderer->setBlendLightshafts(currentRenderStageData.lightshaftintensity);
		}
		//sun rotation
		currentLightdir = (
				Matrix().rotate(Vector(1,0,0),currentRenderStageData.sunrotationspeed * (float)area.getDeltaTime()) *
				currentLightdir
			).normalize();
		Renderer::renderer->setLightdir(currentLightdir);
		//set firefly attraction point
		Renderer::renderer->setFireflyAttractionPoint(currentRenderStageData.fireflyattractor);

		const pair<Vector,pair<Vector,float>> &token = path.front();
		Vector attractor = *position;

		//process tokens of path
		if(consumeNode > token.second.second) {
			lastpos = token.first;
			lasttime = token.second.second;
			if(path.size() <= 1) {
				displaylogo = true;
				return;
			} else {
				path.erase(path.begin());
			}
			if(path.size() > 1) {
				currenttimedelta = path.front().second.second - lasttime;
			}
		} else {
			//update position attractor
			attractor = lastpos + (token.first + lastpos * -1.0f) * ((consumeNode - lasttime) / (token.second.second - lasttime));

			consumeNode = consumeNode + (float)area.getDeltaTime();
		}

		//follow position and view direction attractors
		Vector posAttraction = attractor + -1.0f * (*position);
		*position = *position + posAttraction * ((float)area.getDeltaTime());

		view->normalize();
		Vector viewToken = token.second.first;
		viewToken.normalize();
		Vector viewAttraction = viewToken + (*view) * -1.0f;
		*view = *view + viewAttraction * ((float)area.getDeltaTime() / currenttimedelta);
		
		return;
	}

	void Viewer::printRenderStage() {
		if(Renderer::renderer == 0) {
			return;
		}
		started = true;
		RenderStageData rsd;
		rsd.time = demotime;
		rsd.renderstage = Renderer::renderer->getRenderStage();
		rsd.emitsmoke = emitSmoke;
		rsd.smokeposition = Renderer::renderer->getPreferredBottomPosition();
		rsd.fireflyattractor = Renderer::renderer->getFireflyAttractionPoint();
		float rspeed = (float)Pi / 20.0f;
		Vector lightdir = Renderer::renderer->getLightdir();
		if(Vector(0,0,1).innerprod(-1.0f * lightdir) < 0) {
			//rotate faster during night
			rspeed = (float)Pi / 5.0f;
		}
		if(!moveSun) {
			rspeed = 0.0f;
		}
		rsd.sunrotationspeed = rspeed;
		rsd.lightshaftintensity = 1.0f;
		userdefinedRenderStageData.push_back(rsd);
	}

	void Viewer::printPath() {
		started = true;
		pair<Vector,pair<Vector,float>> posViewTimeTuple;
		posViewTimeTuple.first = (*position);
		posViewTimeTuple.second.first = (*view);
		posViewTimeTuple.second.second = demotime;
		userdefinedPath.push_back(posViewTimeTuple);
	}

	void Viewer::saveConfiguration() {
		try {
			ofstream conf;
			conf.open("configuration.c");
			
			conf <<
				"(scene) {\n" <<
				"	fullscreen = \"false\";\n" <<
				"	usescreendimensions = \"false\";\n" <<
				"	width = 1024;\n" <<
				"	height = 800;\n" <<
				"\n" <<
				"	backgroundmusic = \"../data/audio/HeartOfCourage.wav\";\n" <<
				"	terrainheightmap = \"../data/texture/Terrain.png\";\n" <<
				"	maxlogotime = 15;\n" <<
				"\n" <<
				"	(renderstages) {\n";
			for(vector<RenderStageData>::iterator iter = userdefinedRenderStageData.begin() ; iter != userdefinedRenderStageData.end() ; iter++) {
				RenderStageData &rsd = (*iter);
				conf <<
					"		(";
				if(rsd.renderstage == STAGE_USESMOKE) {
					conf << "fluid) {\n";
				} else if(rsd.renderstage == STAGE_USESMOKELIGHTSHAFT) {
					conf << "fluidlightshaft) {\n";
				} else if(rsd.renderstage == STAGE_COMMON) {
					conf << "common) {\n";
				} else if(rsd.renderstage == STAGE_USELIGHTSHAFT) {
					conf << "lightshaft) {\n";
				}
				conf <<
					"			time = " << rsd.time << ";\n" <<
					"			emitsmoke = \"";
				if(rsd.emitsmoke) {
					conf << "true\";\n";
				} else {
					conf << "false\";\n";
				}
				conf << 
					"			lightshaftintensity = " << rsd.lightshaftintensity << ";\n" <<
					"			sunrotationspeed = " << rsd.sunrotationspeed << ";\n" <<
					"			(smokeposition) {\"";
				for(unsigned int i=0 ; i<3 ; i++) {
					conf << rsd.smokeposition[i];
					if(i < 2) {
						conf << " ";
					}
				}
				conf << "\"}\n" <<
					"			(fireflyattractor) {\"";
				for(unsigned int i=0 ; i<3 ; i++) {
					conf << rsd.fireflyattractor[i];
					if(i < 2) {
						conf << " ";
					}
				}
				conf << "\"}\n" <<
					"		}\n";

			}
			conf << 
				"	}\n" <<
				"\n" <<
				"	(camerapath) {\n";
			for(vector<pair<Vector,pair<Vector,float> > >::iterator iter = userdefinedPath.begin() ; iter != userdefinedPath.end() ; iter++) {
				pair<Vector,pair<Vector,float> > &pvt = (*iter);
				conf <<
					"		(keypoint) {\n" <<
					"			time = " << pvt.second.second << ";\n"
					"			(position) {\"";
				Vector &pos = pvt.first;
				for(unsigned int i=0 ; i<3 ; i++) {
					conf << pos[i];
					if(i < 2) {
						conf << " ";
					}
				}
				conf <<
					"\"}\n" <<
					"			(view) {\"";
				Vector &vw = pvt.second.first;
				for(unsigned int i=0 ; i<3 ; i++) {
					conf << vw[i];
					if(i < 2) {
						conf << " ";
					}
				}
				conf <<
					"\"}\n" << 
					"		}\n";
			}
			conf <<
				"	}\n" <<
				"\n" <<
				"}\n";
		} catch(...) {
			Logger::get() << Level(L_WARNING) << "Warning: could not open file containing new configuration";
		}
	}

	void Viewer::processUserInput(SXRenderArea &area) {

		//prints path tag and/or timepoint on left/right click
		if(runtime <= 0 && (area.hasMouseKey(MOUSE_LEFT) || area.hasMouseKey(MOUSE_RIGHT))) {
			started = true;
			runtime = 1;

			if(area.hasMouseKey(MOUSE_LEFT)) {
				//prints path tag on left click
				printPath();
			} else {
				//prints renderstage configuration
				printRenderStage();
			}
		}
		if(runtime > 0) {
			runtime = runtime - (float)area.getDeltaTime();
		}
		if(started) {
			demotime += (float)area.getDeltaTime();
		}

		//switch render mode
		if(pressedControlKey <= 0 && area.hasKey('1') && Renderer::renderer != 0) {
			pressedControlKey = 1.0f;

			//smoke dynamics rendering mode
			Renderer::renderer->setRenderStage(STAGE_USESMOKE);
			Renderer::renderer->setEmitSmoke(true);

			//prints renderstage configuration
			printRenderStage();
		} else if(pressedControlKey <= 0 && area.hasKey('2') && Renderer::renderer != 0) {
			pressedControlKey = 1.0f;

			//smoke dynamics rendering mode with lightshafts
			Renderer::renderer->setRenderStage(STAGE_USESMOKELIGHTSHAFT);
			Renderer::renderer->setEmitSmoke(true);
			Renderer::renderer->setBlendLightshafts(1);

			//prints renderstage configuration
			printRenderStage();
		} else if(pressedControlKey <= 0 && area.hasKey('3') && Renderer::renderer != 0) {
			pressedControlKey = 1.0f;

			//common rendering mode
			Renderer::renderer->setRenderStage(STAGE_COMMON);

			//prints renderstage configuration
			printRenderStage();
		} else if(pressedControlKey <= 0 && area.hasKey('4') && Renderer::renderer != 0) {
			pressedControlKey = 1.0f;

			//lightshaft rendering mode
			Renderer::renderer->setRenderStage(STAGE_USELIGHTSHAFT);
			Renderer::renderer->setBlendLightshafts(1.0f);
			Renderer::renderer->setBlendLightshafts(1);

			//prints renderstage configuration
			printRenderStage();
		}
		//activate / deactivate smoke emittion
		if((area.getTime()-switchedEmitSmoke)>0.25 && area.hasKey('5')) {
			switchedEmitSmoke = (float)area.getTime();
			emitSmoke = !emitSmoke;

			//prints renderstage configuration
			printRenderStage();
		}
		if(Renderer::renderer != 0) {
			Renderer::renderer->setEmitSmoke(emitSmoke);
		}
		if(pressedControlKey <= 0 &&area.hasKey('6') && Renderer::renderer != 0) {
			pressedControlKey = 1.0f;

			Vector terrainPoint;

			//compute point on terrain under the eyepoint
			Vector &worldpos = SX::shadeX.getUniformVector("terrain.getheight.inputpoint.vector");
			worldpos = *position;
			Pass &getTerrainHeight = SX::shadeX.getPass("terrain.getheight.pass");
			getTerrainHeight.render();
			BufferedMesh &terrpoint = SX::shadeX.getBufferedMesh("terrain.outputpoint.mesh");
			VertexBuffer *buffer = terrpoint.getBuffer("positions");
			const float *posarray = buffer->unlockRead();
			for(unsigned int i=0 ; i<3 ; i++) {
				terrainPoint[i] = posarray[i];
			}
			buffer->lock();

			Vector bottompos = *position;
			bottompos[2] = terrainPoint[2] - 3.0f;
			Renderer::renderer->setPreferredBottomPosition(bottompos);

			//prints renderstage configuration
			printRenderStage();
		}
		//activate / deactivate motion of sun
		if(pressedControlKey <= 0 && (area.getTime()-switchedMoveSun)>0.25 && area.hasKey('7')) {
			pressedControlKey = 1.0f;
			
			switchedMoveSun = (float)area.getTime();
			moveSun = !moveSun;

			//prints renderstage configuration
			printRenderStage();
		}
		if(moveSun && Renderer::renderer != 0) {
			Vector lightdir = Renderer::renderer->getLightdir();
			float angle = ((float)area.getDeltaTime()) * ((float)Pi / 20.0f);
			bool wasNight = false;
			if(Vector(0,0,1).innerprod(-1.0f * lightdir) < 0) {
				//rotate faster during night
				angle = ((float)area.getDeltaTime()) * ((float)Pi / 5.0f);
				wasNight = true;
			}
			lightdir = ( Matrix().rotate(Vector(1,0,0),angle) * lightdir ).normalize();
			Renderer::renderer->setLightdir(lightdir);

			bool isNight = false;
			if(Vector(0,0,1).innerprod(-1.0f * lightdir) < 0) {
				isNight = true;
			}
			if(wasNight != isNight) {
				//day <-> night change
				//prints renderstage configuration
				printRenderStage();
			}
		}
		//change firefly attraction point
		if(pressedControlKey <= 0 && Renderer::renderer != 0 && area.hasKey('8')) {
			pressedControlKey = 1.0f;

			Renderer::renderer->setFireflyAttractionPoint(*position);

			//print renderstage configuration
			printRenderStage();
		}
		//make music start again
		if(pressedControlKey <= 0 && Renderer::renderer != 0 && area.hasKey('9')) {
			pressedControlKey = 1.0f;

			backgroundMusic->start();
		}

		if(pressedControlKey > 0) {
			//wait some time until one of the control keys can be used
			pressedControlKey -= (float)area.getDeltaTime();
			if(pressedControlKey < 0) {
				pressedControlKey = 0;
			}
		}
		
		//w,s,a,d control for the position
		float forward = 0;
		float right = 0;
		float speed = 10.0f;
		if(area.hasKey('W') && !area.hasKey('S')) {
			//move forward
			forward = (float)area.getDeltaTime();
		} else if(!area.hasKey('W') && area.hasKey('S')) {
			//move backward
			forward = (float)-area.getDeltaTime();
		}
		if(area.hasKey('D') && !area.hasKey('A')) {
			//move right
			right = (float)area.getDeltaTime();
			
		} else if(!area.hasKey('D') && area.hasKey('A')) {
			//move left
			right = (float)-area.getDeltaTime();
		}
		Vector vRight = *view % *up;
		*position = *position + view->normalize() * forward * speed + vRight.normalize() * right * speed;
		
		//mousemovement control
		float rotSpeed = 1.0f;
		Vector rotViewerDir = Vector(
			(float)area.getMouseDeltaX(),
			(float)area.getMouseDeltaY()
			);
		rotViewerDir.scalarmult((float)area.getDeltaTime());
		*view = view->normalize() + vRight * rotViewerDir[0] + up->normalize() * rotViewerDir[1]; 
	}

	bool Viewer::isDemoMode() const {
		return demoMode;
	}

	void Viewer::calculate(SXRenderArea &area) {
		if((area.getTime() - switchedMode) > 0.25f && area.hasKey(' ')) {
			switchedMode = (float)area.getTime();
			demoMode = !demoMode;
			if(demoMode) {
				backgroundMusic->start(consumeNode);
			} else {
				musicVolume->value = 1.0f;
			}
		}
		if(demoMode) {
			processPath(area);
		} else {
			processUserInput(area);	
		}

		//calculate projectionview transform
		*viewM = Matrix().viewMatrix(*position,*view,*up);
		*normalview = *viewM;
		normalview->submatrix();
		*projectionview = Matrix().perspectiveMatrix((float)Tau*0.25f,(float)area.getWidth(),(float)area.getHeight(),0.1f,1000.0f) * *viewM;
		*mainprojectionview = Matrix(*projectionview);
		*proj2world = Matrix(*projectionview).inverse();
	}

}

#endif