#ifndef _DEMO_ENTITIES_SMOKE_CPP_
#define _DEMO_ENTITIES_SMOKE_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2013 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/ShadeX.h>
#include <sx/SXMath.h>
#include <cmath>
using namespace std;

namespace sx {

	SmokeInteractionCandidate::SmokeInteractionCandidate() {
		distance = 0;
		position = Vector(-1,-1,-1);
	}

	SmokeInteractionCandidate::SmokeInteractionCandidate(const SmokeInteractionCandidate &candidate) {
		distance = candidate.distance;
		position = candidate.position;
		velocity = candidate.velocity;
	}

	SmokeInteractionCandidate &SmokeInteractionCandidate::operator = (const SmokeInteractionCandidate &candidate) {
		if(this == &candidate) {
			return this [0];
		}
		distance = candidate.distance;
		position = candidate.position;
		velocity = candidate.velocity;
		return this [0];
	}

	bool operator < (const SmokeInteractionCandidate &c1, const SmokeInteractionCandidate &c2) {
		return c1.distance < c2.distance;
	}

	Smoke::Smoke() {
		currentTarget = 0;
		simulateSmokeDynamics = true;

		castboxTransform = &SX::shadeX.getUniformMatrix("castbox.transform");
		castboxInvTransform = &SX::shadeX.getUniformMatrix("castbox.invtransform");
		castboxBox2Tex = &SX::shadeX.getUniformMatrix("castbox.box2tex");
		castboxWorld2Tex = &SX::shadeX.getUniformMatrix("castbox.world2tex");
		castboxTex2World = &SX::shadeX.getUniformMatrix("castbox.tex2world");
		dx = &SX::shadeX.getUniformFloat("dx.float");
		insideCastbox = &SX::shadeX.getUniformFloat("castbox.insidecastbox");
		randacceleration = &SX::shadeX.getUniformVector("fluiddynamic.randacceleration");
		emitSmoke = &SX::shadeX.getUniformFloat("fluiddynamic.emitsmoke");
		decay = &SX::shadeX.getUniformFloat("fluiddynamic.decay");
		smokeboxpos = &SX::shadeX.getUniformVector("smokeboxpos");
		smokeInteractorPositions.push_back(&SX::shadeX.getUniformVector("smoke.p1"));
		smokeInteractorPositions.push_back(&SX::shadeX.getUniformVector("smoke.p2"));
		smokeInteractorPositions.push_back(&SX::shadeX.getUniformVector("smoke.p3"));
		smokeInteractorPositions.push_back(&SX::shadeX.getUniformVector("smoke.p4"));
		smokeInteractorPositions.push_back(&SX::shadeX.getUniformVector("smoke.p5"));
		smokeInteractorVelocities.push_back(&SX::shadeX.getUniformVector("smoke.v1"));
		smokeInteractorVelocities.push_back(&SX::shadeX.getUniformVector("smoke.v2"));
		smokeInteractorVelocities.push_back(&SX::shadeX.getUniformVector("smoke.v3"));
		smokeInteractorVelocities.push_back(&SX::shadeX.getUniformVector("smoke.v4"));
		smokeInteractorVelocities.push_back(&SX::shadeX.getUniformVector("smoke.v5"));
		
		emitSmoke->value = 1.0f;
		decay->value = 0.02f;

		Effect &initSmoke = SX::shadeX.getEffect("fluiddynamic.init.effect");
		initSmoke.render();
	}

	Smoke::~Smoke() {
	}

	void Smoke::setSimulateSmokeDynamics(bool simulateSmokeDynamics) {
		this->simulateSmokeDynamics = simulateSmokeDynamics;
	}

	void Smoke::setEmitSmoke(bool emitSmoke) {
		if(emitSmoke) {
			this->emitSmoke->value = 1.0f;
			decay->value = 0.02f;
		} else {
			this->emitSmoke->value = 0.0f;
			decay->value = 0.1f;
		}
	}

	void Smoke::setPreferredBottomPosition(const Vector &pos) {
		this->preferredBottomPosition = pos;
	}

	Vector Smoke::getPreferredBottomPosition() const {
		return preferredBottomPosition;
	}

	void Smoke::addSmokeInteractor(const Vector &position, const Vector &velocity) {
		smokeInteractionCandidates.push_back(SmokeInteractionCandidate());
		SmokeInteractionCandidate &candidate = smokeInteractionCandidates[smokeInteractionCandidates.size()-1];
		candidate.position = position;
		candidate.velocity = velocity;
	}

	int Smoke::getCurrentTarget() const {
		return currentTarget;
	}

	void Smoke::calculate(SXRenderArea &area) {
		if(!simulateSmokeDynamics) {
			return;
		}
		
		//corner points of fluid box
		Vector boxSize(39,31,30);

		Vector maxBox;
		Vector minBox;
		
		maxBox = preferredBottomPosition + boxSize * 0.5f;
		maxBox[2] = boxSize[2] + preferredBottomPosition[2];
		minBox = preferredBottomPosition + boxSize * (-0.5f);
		minBox[2] = preferredBottomPosition[2];

		//calculate smokeboxpos, and transformation from modelling coordinates (unit cube) to world space (box with above corner points)
		Vector scaleBox = (maxBox + (-1.0f) * minBox) * 0.5f;
		Vector boxCenter = scaleBox + minBox;
		(*castboxTransform) = Matrix().translate(boxCenter) * Matrix().scale(scaleBox);
		(*castboxInvTransform) = Matrix(*castboxTransform).inverse();
		(*smokeboxpos) = boxCenter;

		//determine if viewer is inside or outside the smokebox
		Vector &pos = SX::shadeX.getUniformVector("mainscene.cam.position");
		bool isInside = true;
		for(unsigned int i=0 ; i<3 ; i++) {
			if(pos[i] > maxBox[i] || pos[i] < minBox[i]) {
				isInside = false;
			}
		}
		if(isInside) {
			insideCastbox->value = 1;
		} else {
			insideCastbox->value = 0;
		}

		//calculate transformation from modelling coordinates (unit cube) to texture coordinates (space where raycasting is performed)
		(*castboxBox2Tex) = Matrix().translate(Vector(0.5f,0.5f,0.5f)) * Matrix().scale(Vector(0.5f,0.5f,0.5f));
		(*castboxWorld2Tex) = (*castboxBox2Tex) * (*castboxInvTransform);
		(*castboxTex2World) = Matrix(*castboxWorld2Tex).inverse();

		//choose interactors closest to the center of the smokebox, and transform their positions and velocities to the shader
		for(unsigned int i=0 ; i<smokeInteractorPositions.size() ; i++) {
			*smokeInteractorPositions[i] = Vector(-1,-1,-1);
			*smokeInteractorVelocities[i] = Vector();
		}
		for(unsigned int i=0 ; i<smokeInteractionCandidates.size() ; i++) {
			SmokeInteractionCandidate &candidate = smokeInteractionCandidates[i];
			candidate.distance = (candidate.position + (-1.0f) * (*smokeboxpos)).length();
		}
		std::sort(smokeInteractionCandidates.begin(),smokeInteractionCandidates.end());
		Matrix castboxVecWorld2Tex = Matrix(*castboxWorld2Tex).submatrix();
		for(unsigned int i=0 ; i<smokeInteractorPositions.size() && i<smokeInteractionCandidates.size() ; i++) {
			*smokeInteractorPositions[i] = (*castboxWorld2Tex) * smokeInteractionCandidates[i].position;
			*smokeInteractorVelocities[i] = (1.0f/(2.0f * dx->value)) * (castboxVecWorld2Tex * smokeInteractionCandidates[i].velocity);
		}
		smokeInteractionCandidates.clear();

		//calculate randacceleration
		(*randacceleration) = (Vector().random() + Vector(-0.5f,-0.5f,-0.5f)) * 800.0f;
		(*randacceleration)[2] = max(0.0f,(*randacceleration)[2]);

		//on even frames, medium1 is used as a sampler, and medium2 is used as a rendertarget
		//on odd frames, medium2 is used as a sampler, and medium2 is used as a rendertarget
		if(currentTarget == 0) {
			Effect &calcSmoke = SX::shadeX.getEffect("fluiddynamic1.effect");
			calcSmoke.render();
			currentTarget = 1;
		} else {
			Effect &calcSmoke = SX::shadeX.getEffect("fluiddynamic2.effect");
			calcSmoke.render();
			currentTarget = 0;
		}
	}

}

#endif