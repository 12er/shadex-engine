#ifndef _SX_PARSER_XMESH_CPP_
#define _SX_PARSER_XMESH_CPP_

/**
 * mesh class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Log4SX.h>
#include <boost/foreach.hpp>
using namespace boost;

namespace sx {

	XMesh::XMesh() {
		
	}

	XMesh::~XMesh() {
		SXout(LogMarkup("~XMesh"));
		SXout("~XMesh");
		for(map<string,XBuffer *>::iterator iter = buffers.begin() ; iter != buffers.end() ; iter++) {
			delete (*iter).second;
		}
	}

}

#endif