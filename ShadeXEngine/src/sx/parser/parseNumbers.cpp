#ifndef _SX_PARSER_PARSENUMBERS_CPP_
#define _SX_PARSER_PARSENUMBERS_CPP_

/**
 * number parser
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>

namespace sx {

	vector<double> parseNumbers(const string &numberList) {
		vector<double> numbers;

		parserInternal::parseNumbers(numberList,numbers);

		return numbers;
	}

}

#endif