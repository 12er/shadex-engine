#ifndef _PARSER_READFILE_CPP_
#define _PARSER_READFILE_CPP_

/**
 * file utilities
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sstream>
#include <fstream>
using namespace std;

namespace sx {

	string readFile(string filename) {
		stringstream content;
		ifstream file;
		bool errorbit = false;
		try {
			file.open(filename.c_str());
		} catch(...) {
			errorbit = true;
		}
		if(errorbit || file.fail()) {
			// open caused an exception, or set fail or errorbit to true
			try {
				file.close();
			} catch(...) {
			}
			stringstream exstring;
			exstring << "File " << filename << " can't be opened!";
			throw Exception(exstring.str(),EX_IO);
		}
		if(file.is_open()) {
			//start reading file
			try {
				while(file.good()) {
					unsigned char letter = (unsigned char)file.get();
					if(file.good()) {
						//last attempt at reading letter did not cause trouble
						//so letter can be used
						content << letter;
					}
				}
			} catch(...) {
				//an error occured during reading the file
				try {
					file.close();
				} catch(...) {
				}
				stringstream exstring;
				exstring << "File " << filename << " could not be read!";
				throw Exception(exstring.str(),EX_IO);
			}
		}
		try {
			file.close();
		} catch(...) {
			stringstream exstring;
			exstring << "File " << filename << " could not be closed!";
			throw Exception(exstring.str(),EX_IO);
		}

		return content.str();
	}

}

#endif