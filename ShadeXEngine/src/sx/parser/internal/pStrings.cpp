#ifndef _SX_PARSER_INTERNAL_PSTRINGS_CPP_
#define _SX_PARSER_INTERNAL_PSTRINGS_CPP_

/**
 * internal string parser utility
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <vector>
using namespace std;

namespace sx {

	namespace parserInternal {

		void parseStrings(const string &stringList, vector<string> &strings) {
			bool startNewString = true;
			string *current = 0;
			for(string::const_iterator iter = stringList.begin() ; iter != stringList.end() ; iter++) {
				char val = (*iter);
				if(val != ' ' && val != '\t' && val != '\n' && val != '\r' && val != '\r\n') {
					if(startNewString) {
						strings.push_back(string());
						current = & strings[strings.size()-1];
						startNewString = false;
					}
					(*current).push_back(val);
				} else {
					startNewString = true;
				}
			}
		}

	}

}

#endif