#ifndef _SX_PARSER_INTERNAL_PPLYDATA_CPP_
#define _SX_PARSER_INTERNAL_PPLYDATA_CPP_

/**
 * internal ply parser utility
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_hold.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <vector>
using namespace std;
using namespace boost;
using boost::spirit::qi::phrase_parse;
using boost::spirit::qi::char_;
using boost::spirit::qi::double_;
using boost::spirit::qi::uint_;
using boost::spirit::qi::int_;
using boost::spirit::qi::eps;
using boost::spirit::qi::lit;
using boost::spirit::qi::_1;
using boost::spirit::qi::grammar;
using boost::spirit::qi::lexeme;
using boost::spirit::qi::symbols;
using boost::spirit::qi::rule;
using boost::spirit::qi::hold;
using boost::spirit::qi::omit;
using boost::spirit::qi::no_skip;
using boost::spirit::qi::eol;
using boost::spirit::_val;
using boost::spirit::ascii::space;
using boost::spirit::ascii::space_type;
using boost::phoenix::ref;
using boost::phoenix::push_back;
using boost::phoenix::at_c;

BOOST_FUSION_ADAPT_STRUCT (
	sx::parserInternal::_PROPERTY_ ,
	(std::string		,name)			//0
	(int				,multiplicity)	//1
	(std::vector<int>	,types)			//2
)

BOOST_FUSION_ADAPT_STRUCT (
	sx::parserInternal::_ELEMENT_ ,
	(std::string									,name)			//0
	(unsigned int									,count)			//1
	(std::vector<sx::parserInternal::_PROPERTY_>	,properties)	//2
)

BOOST_FUSION_ADAPT_STRUCT (
	sx::parserInternal::_XMESH_ ,
	(std::vector<sx::parserInternal::_ELEMENT_>		,elements)	//0
	(std::vector<std::vector<double>>				,data)		//1
)

namespace sx {

	namespace parserInternal {

		/**
		 * filters empty lines, and lines starting with comment, ply
		 * and format out of the text
		 */
		struct PLYFilter: public grammar<string::iterator,string()> {

			/**
			 * start rule
			 */
			rule<string::iterator,string()> start;

			/**
			 * recognizes a sequence starting with optional empty space, the
			 * sequence "ply" and anything ending with newline
			 */
			rule<string::iterator,string()> skipPly;

			/**
			 * recognizes a sequence starting with optional empty space, the
			 * sequence "format" and anything ending with newline
			 */
			rule<string::iterator,string()> skipFormat;

			/**
			 * recognizes a sequence starting with optional empty space, the
			 * sequence "comment" and anything ending with newline
			 */
			rule<string::iterator,string()> skipComment;

			/**
			 * recognizes empty space followed by newline
			 */
			rule<string::iterator,string()> skipEmptyLine;

			/**
			 * recognizes empty space
			 */
			rule<string::iterator,string()> skipable;

			/**
			 * recognizes newline
			 */
			rule<string::iterator,string()> separator;

			/**
			 * recognizes any text with at least one letter
			 * in 0-9a-z followed by newline
			 */
			rule<string::iterator,string()> acceptableLine;

			PLYFilter(): PLYFilter::base_type(start) {
				skipable %= lexeme[+char_(" \t")];
				separator %= lexeme[(char_('\n') | char_('\r\n'))];
				skipEmptyLine %= *(skipable) >> separator;
				skipComment %= *skipable >> lit("comment") >> *lexeme[char_ - eol] >> eol;
				skipPly %= *skipable >> lit("ply") >> *lexeme[char_ - eol] >> eol;
				skipFormat %= *skipable >> lit("format") >> *lexeme[char_ - eol] >> eol;
				acceptableLine %= *lexeme[char_ - char_("a-z0-9") - eol] >> char_("a-z0-9") >> *lexeme[char_ - eol] >> separator;
				start %= eps >>
					+(
						omit[skipEmptyLine]
						| omit[skipComment]
						| omit[skipPly]
						| omit[skipFormat]
						| acceptableLine
					)
					;
			}

		};

		/**
		 * grammar for the ply parser
		 */
		struct PLYGrammar: public grammar<string::iterator,_XMESH_()> {
		
			/**
			 * recognizes empty space
			 */
			rule<string::iterator,string()> skipable;

			/**
			 * recognizes identifyers made of any character sequence
			 * without space, tab and newline
			 */
			rule<string::iterator,string()> identifyer;

			/**
			 * holds information of an element row
			 */
			rule<string::iterator,_ELEMENT_()> elementheader;

			/**
			 * data type
			 */
			rule<string::iterator,int()> type;

			/**
			 * a list of data types
			 */
			rule<string::iterator,vector<int>()> proptypes;

			/**
			 * a property
			 */
			rule<string::iterator,_PROPERTY_()> prop;

			/**
			 * a list of properties
			 */
			rule<string::iterator,vector<_PROPERTY_>()> propertylist;

			/**
			 * An element. It's composed of the information provided
			 * by the element row, and a list of porperties.
			 */
			rule<string::iterator,_ELEMENT_()> element;

			/**
			 * a list of elements
			 */
			rule<string::iterator,vector<_ELEMENT_>()> elementlist;

			/**
			 * char sequence end_header ending the header
			 */
			rule<string::iterator,string()> endheader;

			/**
			 * a row of numbers
			 */
			rule<string::iterator,vector<double>()> datarow;

			/**
			 * rows of numbers
			 */
			rule<string::iterator,vector<vector<double>>()> datacolumns;

			/**
			 * the header composed of an element list
			 * and the end of the header
			 */
			rule<string::iterator,_XMESH_()> header;

			/**
			 * section after the header, composed of rows of
			 * numbers
			 */
			rule<string::iterator,_XMESH_()> datasection;

			/**
			 * start rule
			 */
			rule<string::iterator,_XMESH_()> rule;

			PLYGrammar(): PLYGrammar::base_type(rule) {
				
				skipable %= lexeme[char_(" \t")];
				identifyer %= lexeme[+(char_ - space)];
				
				elementheader %= eps >> omit[*(skipable) >> lit("element") >> +(skipable)] >> 
					identifyer[at_c<0>(_val) = _1] >> omit[+(skipable)] >>
					uint_[at_c<1>(_val) = _1] >> omit[*(skipable)] >> eol;
				type %= eps >>
					(
						lit("char")[_val = T_CHAR]
						| lit("uchar")[_val = T_UCHAR]
						| lit("short")[_val = T_SHORT]
						| lit("ushort")[_val = T_USHORT]
						| lit("int")[_val = T_INT]
						| lit("uint")[_val = T_UINT]
						| lit("long")[_val = T_LONG]
						| lit("ulong")[_val = T_ULONG]
						| lit("float")[_val = T_FLOAT]
						| lit("double")[_val = T_DOUBLE]
					);
				proptypes %= type >> *(omit[+(skipable)] >> type);
				prop %= omit[*(skipable) >> lit("property") >> +(skipable)] >>
					(
						(
							lit("list")[at_c<1>(_val) = M_LIST] >> omit[+(skipable)] >>
							proptypes[at_c<2>(_val) = _1]
							)
						| (
							type[push_back(at_c<2>(_val),_1)][at_c<1>(_val) = M_SINGLE]
						)
					) >> omit[+(skipable)] >> identifyer[at_c<0>(_val) = _1] >> omit[*(skipable)] >> eol
					;
				propertylist %= +(prop);
				element %=
					elementheader[at_c<0>(_val) = at_c<0>(_1)][at_c<1>(_val) = at_c<1>(_1)] >> 
					propertylist[at_c<2>(_val) = _1]
				;
				elementlist %= +(element);
				endheader %= *(skipable) >> lit("end_header") >> *(skipable) >> eol;
				header %= elementlist[at_c<0>(_val) = _1] >> omit[endheader];

				datarow %= +(omit[*(skipable)] >> double_ >> omit[*(skipable)]) >> eol;
				datacolumns %= +(datarow);
				datasection %= datacolumns[at_c<1>(_val) = _1];

				rule %= header[at_c<0>(_val) = at_c<0>(_1)] >> datasection[at_c<1>(_val) = at_c<1>(_1)];
			}

		};

		void parsePLYdata1(const string &data, string &output) {
			string iterableData = data;
			string::iterator iter1 = iterableData.begin();
			string::iterator iter2 = iterableData.end();
			PLYFilter filter;
			bool isFiltered = phrase_parse(
				iter1,
				iter2,
				filter,
				space,
				output
				);

			if(!isFiltered || iter1 != iter2) {
				throw Exception("Syntax Error, could not ignore data not needed like comments",EX_SYNTAX);
			}
		}

		void parsePLYdata2(string &filteredIterable, _XMESH_ &output) {
			string::iterator iter1 = filteredIterable.begin();
			string::iterator iter2 = filteredIterable.end();
			PLYGrammar grammar;
			bool parsed = phrase_parse(
				iter1,
				iter2,
				grammar,
				space,
				output
				);

			if(!parsed || iter1 != iter2) {
				throw Exception("Syntax Error, could not parse the file",EX_SYNTAX);
			}
		}

	}

}

#endif