#ifndef _SX_PARSER_INTERNAL_PNUMBERS_CPP_
#define _SX_PARSER_INTERNAL_PNUMBERS_CPP_

/**
 * internal number parser utility
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_hold.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <vector>
using namespace std;
using namespace boost;
using boost::spirit::qi::phrase_parse;
using boost::spirit::qi::char_;
using boost::spirit::qi::double_;
using boost::spirit::qi::eps;
using boost::spirit::qi::lit;
using boost::spirit::qi::_1;
using boost::spirit::qi::grammar;
using boost::spirit::qi::lexeme;
using boost::spirit::qi::symbols;
using boost::spirit::qi::rule;
using boost::spirit::qi::hold;
using boost::spirit::qi::omit;
using boost::spirit::_val;
using boost::spirit::ascii::space;
using boost::spirit::ascii::space_type;
using boost::phoenix::ref;
using boost::phoenix::push_back;
using boost::phoenix::at_c;

namespace sx {

	namespace parserInternal {

		struct NumberGrammar: public grammar<string::iterator,vector<double>(),space_type> {
		
			rule<string::iterator,vector<double>(),space_type > start;

			NumberGrammar(): NumberGrammar::base_type(start) {
				start %= eps >> 
					*(
						double_
					);
			}
		
		};

		void parseNumbers(const string &numberList, vector<double> &numbers) {
			string filterable = numberList;
			string::iterator iter1 = filterable.begin();
			string::iterator iter2 = filterable.end();
			NumberGrammar grammar;
			bool parsed = phrase_parse(
				iter1,
				iter2,
				grammar,
				space,
				numbers
				);
			if(!parsed || iter1 != iter2) {
				throw Exception("Syntax Error",EX_SYNTAX);
			}
		}

	}

}

#endif