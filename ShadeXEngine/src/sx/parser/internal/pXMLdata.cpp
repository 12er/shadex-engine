#ifndef _SX_PARSER_INTERNAL_PXMLDATA_CPP_
#define _SX_PARSER_INTERNAL_PXMLDATA_CPP_

/**
 * internal xml parser utility
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_hold.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/foreach.hpp>
#include <vector>
using namespace std;
using namespace boost;
using boost::spirit::qi::phrase_parse;
using boost::spirit::qi::char_;
using boost::spirit::qi::double_;
using boost::spirit::qi::eps;
using boost::spirit::qi::lit;
using boost::spirit::qi::_1;
using boost::spirit::qi::grammar;
using boost::spirit::qi::lexeme;
using boost::spirit::qi::symbols;
using boost::spirit::qi::rule;
using boost::spirit::qi::hold;
using boost::spirit::qi::omit;
using boost::spirit::_val;
using boost::spirit::ascii::space;
using boost::spirit::ascii::space_type;
using boost::phoenix::ref;
using boost::phoenix::push_back;
using boost::phoenix::at_c;

BOOST_FUSION_ADAPT_STRUCT (
	sx::parserInternal::_XMLNODE_ ,
	(int										,type)		//	0
	(std::string								,text)		//	1 - XText
	(std::string								,name)		//	2 - XTag
	(std::string								,endname)	//	3 - XTag
	(std::vector<sx::parserInternal::_XMLNODE_>	,nodes)		//	4 - XTag
	(std::string								,strID)		//	5 - str. attrib
	(std::string								,strAttrib)	//	6 - str. attrib
	(std::vector<sx::parserInternal::_XMLNODE_>	,ats)		//	7 - attributelist
	(std::string								,eat)		//	8 - bug fix
)

namespace sx {

	namespace parserInternal {

		/**
		 * grammar for the xml parser
		 */
		struct XMLGrammar: public grammar<string::iterator,_XMLNODE_()> {

			/**
			 * start symbol
			 */
			rule<string::iterator,_XMLNODE_()> start;

			/**
			 * a tag
			 */
			rule<string::iterator,_XMLNODE_()> tag;

			/**
			 * child nodes of a tag
			 */
			rule<string::iterator,vector<_XMLNODE_>()> nodelist;

			/**
			 * attributes of a tag
			 */
			rule<string::iterator,vector<_XMLNODE_>()> attrlist;

			/**
			 * identifyer - starts with a letter in a-zA-Z_:. , and can be
			 * continued by a-zA-Z_0-9:. , must have at least one letter
			 */
			rule<string::iterator,string()> identifyer;

			/**
			 * any char sequence without the letters / < > of any
			 * length greater or equal to one
			 */
			rule<string::iterator,string()> textdata;
			
			/**
			 * any char sequence without the letters " / < > of any length
			 * bordered by the letter "
			 */
			rule<string::iterator,string()> stringdata;

			/**
			 * attribute assigned with string value
			 */
			rule<string::iterator,_XMLNODE_()> strAttrib;

			/**
			 * simply textdata returning _XNODE_
			 */
			rule<string::iterator,_XMLNODE_()> textNode;

			/**
			 * The SX framework does not require tags of the
			 * form <? ?> and <! >, it would be just a nice to have feature,
			 * hence those tags are simply ignored. If special tags
			 * appear within special tags not being a comment,
			 * (like for instance <! <!-- --> >) , the parser
			 * will report an error.
			 */
			rule<string::iterator,string()> ignoredData;
			rule<string::iterator,_XMLNODE_()> ignoredNode;
			rule<string::iterator,vector<_XMLNODE_>()> ignorelist;

			/**
			 * constructor, makes tag to the node's root
			 */
			XMLGrammar(): XMLGrammar::base_type(start) {
				identifyer %= omit[*space] >> lexeme[char_("a-zA-Z_:.") >> *( char_("0-9a-zA-Z_:.") )] >> omit[*space];
				textdata %= lexeme[+(char_ - '<' - '>')];
				stringdata %= omit[*space] >> lexeme[omit[char_('"')] >> *(char_ - '<' - '>' - '"') >> omit[char_('"')]] >> omit[*space];
				
				strAttrib %= 
					identifyer[at_c<5>(_val) = _1] >> char_('=') >> 
					stringdata[at_c<6>(_val) = _1][at_c<0>(_val) = _S_ATTRIB_];
				
				textNode %= textdata[at_c<1>(_val) = _1][at_c<0>(_val) = _XTEXT_];
				
				ignoredData %= eps >>
					*space >>
					(
						char_('<') >> char_('?') >> lexeme[*(char_ - '<' - '>' - '?')] >> char_('?') >> char_('>')
						| char_('<') >> char_('!') >> lexeme[*(char_ - '<' - '>')] >> char_('>')
					)
					>> *space
					;

				ignoredNode %= ignoredData[at_c<8>(_val) = _1];
				
				nodelist %= eps >>
					*(
						textNode
						| omit[ignoredNode]
						| tag
					)
					;

				ignorelist %= eps >> *(ignoredNode);
				
				attrlist %= eps >>
					*(
						strAttrib
					)
					;
				
				tag %= eps >>
					char_('<') >> identifyer[at_c<2>(_val) = _1][at_c<3>(_val) = _1] >> attrlist[at_c<7>(_val) = _1] >>
					(
						char_('>')[at_c<8>(_val) = _1] >>
						nodelist[at_c<4>(_val) = _1] >>
						char_('<') >> char_('/') >> identifyer[at_c<3>(_val) = _1] >> char_('>')[at_c<8>(_val) = _1]
					|
						char_('/')[at_c<8>(_val) = _1] >> char_('>')
					)
					[at_c<0>(_val) = _XTAG_]
					;

				start %= omit[ignorelist] >> tag >> omit[ignorelist];
			}

		};

		void parseXMLdata(const string &data, _XMLNODE_ &output) {
			string iterable = data;
			string::iterator iter1 = iterable.begin();
			string::iterator iter2 = iterable.end();
			XMLGrammar grammar;
			bool parsed = phrase_parse(
				iter1,
				iter2,
				grammar,
				space,
				output
				);
			if(!parsed || iter1 != iter2) {
				throw Exception("Syntax Error",EX_SYNTAX);
			}
		}

	}

}

#endif