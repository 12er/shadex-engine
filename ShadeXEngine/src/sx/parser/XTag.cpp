#ifndef _SX_PARSER_XTAG_CPP_
#define _SX_PARSER_XTAG_CPP_

/**
 * tag node class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <sx/Log4SX.h>
#include <list>
#include <sstream>
using namespace std;
using namespace boost;

namespace sx {

	XTag::XTag() {
	}

	XTag::~XTag() {
		SXout(LogMarkup("~XTag"));
		SXout(name);
		BOOST_FOREACH(XNode *node , this->nodes) {
			delete node;
		}
	}

	void XTag::getTags(std::string location, vector<XTag *> &n) {
		list<string> path;
		//the list of nodes, where new nodes are searched
		list<XTag *> searchsource;
		//the list of nodes, which are found in searchoutcome
		list<XTag *> searchoutcome;
		//location is transformed into a list of tag names
		//by removing the char '.'
		char_separator<char> separator(".");
		tokenizer<char_separator<char>> tokens(location,separator);
		BOOST_FOREACH(const string &token,tokens) {
			path.push_back(token);
		}
		//start the search in this
		searchsource.push_back(this);
		//token counter, to check, if searchoutcome needs to
		//be switched with searchsource, or pushed into the
		//output parameter
		unsigned int tokencounter = 1;
		BOOST_FOREACH(const string &token,path) {
			BOOST_FOREACH(XTag *root,searchsource) {
				//search in each node of the searchsource
				BOOST_FOREACH(XNode *node,root->nodes) {
					XTag *tag = dynamic_cast<XTag *>(node);
					if(tag != 0 && (token.compare("*") == 0 || tag->name.compare(token) == 0)) {
						//found a tag matching the given location
						if(tokencounter < path.size()) {
							//push into searchoutcome,
							//where the search is continued
							searchoutcome.push_back(tag);
						} else {
							//consumed whole path, and found something
							n.push_back(tag);
						}
					}
				}
			}
			//the search source is becoming the search outcome
			if(tokencounter < path.size()) {
				searchsource = searchoutcome;
				searchoutcome.clear();
			}

			//count the index of the token
			tokencounter++;
		}
	}

	XTag *XTag::getFirst(std::string location) {
		list<string> path;
		//the list of nodes, where new nodes are searched
		list<XTag *> searchsource;
		//the list of nodes, which are found in searchoutcome
		list<XTag *> searchoutcome;
		//location is transformed into a list of tag names
		//by removing the char '.'
		char_separator<char> separator(".");
		tokenizer<char_separator<char>> tokens(location,separator);
		BOOST_FOREACH(const string &token,tokens) {
			path.push_back(token);
		}
		//start the search in this
		searchsource.push_back(this);
		//token counter, to check, if searchoutcome needs to
		//be switched with searchsource, or pushed into the
		//output parameter
		unsigned int tokencounter = 1;
		BOOST_FOREACH(const string &token,path) {
			if(tokencounter < path.size()) {
				//if it's not the last token,
				//all childnode need to be taken
				//into consideration
				BOOST_FOREACH(XTag *root,searchsource) {
					//search in each node of the searchsource
					BOOST_FOREACH(XNode *node,root->nodes) {
						XTag *tag = dynamic_cast<XTag *>(node);
						if(tag != 0 && (token.compare("*") == 0 || tag->name.compare(token) == 0)) {
							//found a tag matching the given location
							//push into searchoutcome,
							//where the search is continued
							searchoutcome.push_back(tag);
						}
					}
				}
			} else {
				BOOST_FOREACH(XTag *root,searchsource) {
					//search in each node of the searchsource
					BOOST_FOREACH(XNode *node,root->nodes) {
						XTag *tag = dynamic_cast<XTag *>(node);
						if(tag != 0 && (token.compare("*") == 0 || tag->name.compare(token) == 0)) {
							//found a tag matching the given location
							//return the outcome
							return tag;
						}
					}
				}
			}
			//the search source is becoming the search outcome
			if(tokencounter < path.size()) {
				searchsource = searchoutcome;
				searchoutcome.clear();
			}

			//count the index of the token
			tokencounter++;
		}

		stringstream errmsg;
		errmsg << "Error: no first node at \"" << location << "\" found";
		throw Exception(errmsg.str(),EX_NODATA);
	}

	XTag *XTag::getFirst() {
		BOOST_FOREACH(XNode *node, nodes) {
			XTag *tag = dynamic_cast<XTag *>(node);
			if(tag != 0) {
				return tag;
			}
		}

		stringstream errmsg;
		errmsg << "Error: no first node found in " << name;
		throw Exception(errmsg.str(),EX_NODATA);
	}

	string XTag::getStrAttribute(string ID) {
		map<string,string>::iterator iter = stringAttribs.find(ID);
		if(iter == stringAttribs.end()) {
			stringstream errmsg;
			errmsg << "Error: no string attribute " << ID << " found in " << name;
			throw Exception(errmsg.str(),EX_NODATA);
		}
		return (*iter).second;
	}

	double XTag::getRealAttribute(string ID) {
		map<string,double>::iterator iter = realAttribs.find(ID);
		if(iter == realAttribs.end()) {
			stringstream errmsg;
			errmsg << "Error: no real attribute " << ID << " found in " << name;
			throw Exception(errmsg.str(),EX_NODATA);
		}
		return (*iter).second;
	}

	void XTag::getDirectTexts(vector<string> &texts) {
		BOOST_FOREACH(XNode *node , nodes) {
			XText *text = dynamic_cast<XText *>(node);
			if(text != 0) {
				texts.push_back(text->text);
			}
		}
	}

	void XTag::getTexts(vector<string> &texts) {
		XTag flattened;
		map<string,vector<double>> aReals;
		map<string,vector<string>> aStrings;
		flattenTag(flattened,aReals,aStrings);
		BOOST_FOREACH(XNode *node , flattened.nodes) {
			XText *text = dynamic_cast<XText *>(node);
			if(text != 0) {
				texts.push_back(text->text);
			}
		}
	}

	string XTag::getDirectTexts() {
		vector<string> texts;
		stringstream text;
		getDirectTexts(texts);
		BOOST_FOREACH(const string &t , texts) {
			text << t;
		}
		return text.str();
	}

	string XTag::getTexts() {
		vector<string> texts;
		stringstream text;
		getTexts(texts);
		BOOST_FOREACH(const string &t , texts) {
			text << t;
		}
		return text.str();
	}

	void _COLLECT_CONTENT_(const XTag &currentTag, XTag &collectingTag, map<string,vector<double>> &aReals, map<string,vector<string>> &aStrings) {
		//collect attributes
		for(map<string,double>::const_iterator iter = currentTag.realAttribs.begin() ; iter != currentTag.realAttribs.end() ; iter++) {
			map<string,double>::iterator rattrib = collectingTag.realAttribs.find((*iter).first);
			if(rattrib != collectingTag.realAttribs.end()) {
				//ambiguous attribute, overwrite value, add to ambiguous attribute map
				//keep old value, if it' not yet in the ambiguous vector
				double ambiguousFirst = (*rattrib).second;
				(*rattrib).second = (*iter).second;
				map<string,vector<double>>::iterator ambiguousAttrib = aReals.find((*iter).first);
				vector<double> *vals = 0;
				if(ambiguousAttrib == aReals.end()) {
					aReals.insert(pair<string,vector<double>>((*iter).first,vector<double>()));
					vals = &(*aReals.find((*iter).first)).second;
					vals->push_back(ambiguousFirst);
				} else {
					vals = &(*aReals.find((*iter).first)).second;
				}
				vals->push_back((*iter).second);
			} else {
				//attribute appearing the first time, insert
				collectingTag.realAttribs.insert(pair<string,double>((*iter).first,(*iter).second));
			}
		}
		for(map<string,string>::const_iterator iter = currentTag.stringAttribs.begin() ; iter != currentTag.stringAttribs.end() ; iter++) {
			map<string,string>::iterator sattrib = collectingTag.stringAttribs.find((*iter).first);
			if(sattrib != collectingTag.stringAttribs.end()) {
				//ambiguous attribute, overwrite value, add to ambiguous attribute map
				//keep old value, if it' not yet in the ambiguous vector
				string ambiguousFirst = (*sattrib).second;
				(*sattrib).second = (*iter).second;
				map<string,vector<string>>::iterator ambiguousAttrib = aStrings.find((*iter).first);
				vector<string> *vals = 0;
				if(ambiguousAttrib == aStrings.end()) {
					aStrings.insert(pair<string,vector<string>>((*iter).first,vector<string>()));
					vals = &(*aStrings.find((*iter).first)).second;
					vals->push_back(ambiguousFirst);
				} else {
					vals = &(*aStrings.find((*iter).first)).second;
				}
				vals->push_back((*iter).second);
			} else {
				//attribute appearing the first time, insert
				collectingTag.stringAttribs.insert(pair<string,string>((*iter).first,(*iter).second));
			}
		}
		//collect nodes from left to right
		BOOST_FOREACH(XNode *node , currentTag.nodes) {
			XText *nText = dynamic_cast<XText *>(node);
			XTag *nTag = dynamic_cast<XTag *>(node);
			if(nText != 0) {
				XText *newText = new XText();
				newText->text = nText->text;
				collectingTag.nodes.push_back(newText);
			} else if(nTag != 0) {
				//collect the content of nTag
				_COLLECT_CONTENT_(nTag [0],collectingTag,aReals,aStrings);
				if(nTag->nodes.size() == 0) {
					//only empty tags are collected
					XTag *newTag = new XTag();
					newTag->name = nTag->name;
					collectingTag.nodes.push_back(newTag);
				}
			}
		}
	}

	void XTag::flattenTag(sx::XTag &tag, std::map<string,vector<double> > &ambiguousReals, std::map<string,vector<string> > &ambiguousStrings) {
		tag.name = this->name;
		_COLLECT_CONTENT_(this [0], tag, ambiguousReals, ambiguousStrings);
	}

}

#endif