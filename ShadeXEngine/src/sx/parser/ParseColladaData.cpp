#ifndef _SX_PARSER_PARSECOLLADADATA_CPP_
#define _SX_PARSER_PARSECOLLADADATA_CPP_

/**
 * collada mesh parser
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <sx/SXMath.h>
#include <sstream>
#include <cmath>
#include <vector>
using namespace std;

namespace sx {

	void COLLADA_transformToMesh(XTag *collada, bool deleteCollada, XMesh *mesh) {
		XTag *meshTag = 0;
		XTag *controller = 0;
		Matrix transform;
		try {
			Matrix transform1;
			Matrix transform2;
			XTag *geometry = collada->getFirst("library_geometries.geometry");
			meshTag = geometry->getFirst("mesh");
			string geometryName = geometry->getStrAttribute("name");
			//search for geometry in scene
			//there bone weight buffers and transformations
			//can be found
			vector<XTag *> nodes;
			collada->getTags("library_visual_scenes.visual_scene.node",nodes);
			XTag *geoNode = 0;
			for(vector<XTag *>::iterator iter = nodes.begin() ; iter != nodes.end() ; iter++) {
				XTag *node = (*iter);
				if(node->getStrAttribute("id").compare(geometryName) == 0) {
					geoNode = node;
					break;
				}
			}
			if(geoNode != 0) {
				//found geometry in scene
				//lookup transformation
				for(vector<XNode *>::iterator iter = geoNode->nodes.begin() ; iter != geoNode->nodes.end() ; iter++) {
					XTag *node = dynamic_cast<XTag *>((*iter));
					if(node != 0 && node->name.compare("translate") == 0) {
						//translation
						vector<double> n = parseNumbers(node->getTexts());
						Vector translate;
						if(n.size() == 1) {
							translate[0] = (float)n[0];
						} else if(n.size() == 2) {
							translate = Vector((float)n[0],(float)n[1]);
						} else if(n.size() >= 3) {
							translate = Vector((float)n[0],(float)n[1],(float)n[2]);
						}
						transform1 = transform1 * Matrix().translate(translate);
					} else if(node != 0 && node->name.compare("rotate") == 0) {
						//rotate
						vector<double> n = parseNumbers(node->getTexts());
						if(n.size() != 4) {
							throw Exception();
						}
						Vector axis((float)n[0],(float)n[1],(float)n[2]);
						float angle = (float) (sx::Pi * n[3] / 180.0);
						transform1 = transform1 * Matrix().rotate(axis,angle);
					} else if(node != 0 && node->name.compare("scale") == 0) {
						//scale
						vector<double> n = parseNumbers(node->getTexts());
						Vector scale(1,1,1);
						if(n.size() == 1) {
							scale[0] = (float)n[0];
						} else if(n.size() == 2) {
							scale[0] = (float)n[0];
							scale[1] = (float)n[1];
						} else if(n.size() >= 3) {
							scale = Vector((float)n[0],(float)n[1],(float)n[2]);
						}
						transform1 = transform1 * Matrix().scale(scale);
					} else if(node != 0 && node->name.compare("skew") == 0) {
						//skew
						vector<double> skv = parseNumbers(node->getTexts());
						if(skv.size() != 7) {
							throw Exception();
						}
						float angle = (float)(skv[0]*Pi/180.0);
						Vector rot((float)skv[1],(float)skv[2],(float)skv[3]);
						Vector shear((float)skv[4],(float)skv[5],(float)skv[6]);

						rot.normalize();
						shear.normalize();
						Vector a1 = shear * (shear.innerprod(rot));
						Vector a2 = rot + (--a1);
						a2.normalize();

						float an1 = rot.innerprod(a2);
						float an2 = rot.innerprod(shear);
						float rx = an1 * cos(angle) - an2 * sin(angle);
						float ry = an1 * sin(angle) + an2 * cos(angle);
						float alpha = (an1 == 0) ? 0 : (ry/rx - an2/an1);
						Matrix skewM(
							a2[0] * shear[0] * alpha + 1.0f , a2[0] * shear[1] * alpha , a2[0] * shear[2] * alpha ,
							a2[1] * shear[0] * alpha , a2[1] * shear[1] * alpha + 1.0f , a2[1] * shear[2] * alpha ,
							a2[2] * shear[0] * alpha , a2[2] * shear[1] * alpha , a2[2] * shear[2] * alpha + 1.0f
							);
						transform1 = transform1 * skewM;
					} else if(node != 0 && node->name.compare("matrix") == 0) {
						//custom matrix
						vector<double> mv = parseNumbers(node->getTexts());
						if(mv.size() != 16) {
							throw Exception();
						}
						Matrix m;
						for(unsigned int row = 0 ; row < 4 ; row++) {
							for(unsigned int column = 0 ; column < 4 ; column++) {
								unsigned int vIndex = row*4 + column;
								unsigned int mIndex = column*4 + row;
								m[mIndex] = (float)mv[vIndex];
							}
						}
						transform1 = transform1 * m;
					}
				}
				XTag *instance_controller = 0;
				try {
					instance_controller = geoNode->getFirst("instance_controller");
				} catch(Exception &) {
				}
				if(instance_controller != 0) {
					//a bone structure does exist
					//search for it
					string controllerId = instance_controller->getStrAttribute("url");
					controllerId = controllerId.substr(1);
					vector<XTag *> ctrls;
					collada->getTags("library_controllers.controller",ctrls);
					for(vector<XTag *>::iterator iter = ctrls.begin() ; iter != ctrls.end() ; iter++) {
						XTag *ctrl = (*iter);
						if(ctrl->getStrAttribute("id").compare(controllerId) == 0) {
							controller = ctrl;
							break;
						}
					}
					if(controller != 0) {
						//get bind shape matrix
						XTag *matrix = controller->getFirst("skin.bind_shape_matrix");
						vector<double> elem = parseNumbers(matrix->getTexts());
						if(elem.size() != 16) {
							throw Exception();
						}
						for(unsigned int i=0 ; i<4 ; i++) {
							//row
							for(unsigned int j=0 ; j<4 ; j++) {
								//column
								transform2[i + j*4] = (float)elem[i*4 + j];
							}
						}
					}
				}
			}
			transform = transform2 * transform1;
		} catch(Exception &) {
			if(deleteCollada) {
				delete collada;
			}
			delete mesh;
			throw Exception("Not in Collada format",EX_SYNTAX);
		}

		//index extraction: identify facetype of mesh:
		//			one vertex per face, two vertices per face or at least three vertices per face)
		//			if we have at least three vertices per face, polygons are transformed into triangles
		//			if the mesh is of mixed type, an exception is thrown
		//			push indices into combinedIndices
		mesh->faceSize = 0;
		vector<unsigned int> combinedIndices;
		unsigned int inputCount = 0;
		try {
			vector<XTag *> inputs;
			meshTag->getTags("polylist.input",inputs);
			XTag *vcount = meshTag->getFirst("polylist.vcount");
			XTag *p = meshTag->getFirst("polylist.p");
			inputCount = inputs.size();
			vector<double> faceNumbers = parseNumbers(vcount->getTexts());
			vector<double> indexNumbers = parseNumbers(p->getTexts());
			if(faceNumbers.size() == 0) {
				//indexset p must be nonempty and consistent with the size of vcount
				throw Exception();
			}
			unsigned int processedVertexCount = 0;
			for(unsigned int i=0 ; i<faceNumbers.size() ; i++) {
				unsigned int count = (unsigned int)faceNumbers[i];
				if(mesh->faceSize == 0) {
					//faceSize still uninitialized
					//set faceSize
					if(count == 0) {
						//0 is not a valid faceSize
						throw Exception();
					} else if(count == 1) {
						mesh->faceSize = 1;
					} else if(count == 2) {
						mesh->faceSize = 2;
					} else if(count >= 3) {
						mesh->faceSize = 3;
					}
				}
				if((processedVertexCount + count)*inputCount > indexNumbers.size()) {
					//not enough indices
					throw Exception();
				}
				if(mesh->faceSize == 3 && count >= 3) {
					//facetype is triangle
					//transform polygons into triangles
					//such that a polygon with vertices v_1 v_2 ... v_n-1 v_n is transformed to triangles
					//(v_1 v_2 _v3) (v_1 v_3 v_4) (v_1 v_4 v_5) ... (v_1 v_n-1 v_n)
					for(unsigned int j=1 ; j<count-1 ; j++) {
						//push next triangle
						for(unsigned int k=0 ; k<inputCount ; k++) {
							//push first vertex
							combinedIndices.push_back((unsigned int)indexNumbers[processedVertexCount*inputCount + k]);
						}
						for(unsigned int k=0 ; k<inputCount*2 ; k++) {
							//push j. and j+1. vertex
							combinedIndices.push_back((unsigned int)indexNumbers[(processedVertexCount + j)*inputCount + k]);
						}
					}
				} else if(mesh->faceSize == count && (mesh->faceSize == 1 || mesh->faceSize == 2)) {
					//facetype is point or line
					//there is no need to transform those datasets
					for(unsigned int j=0 ; j<inputCount*count ; j++) {
						combinedIndices.push_back((unsigned int)indexNumbers[processedVertexCount*inputCount + j]);
					}
				} else {
						//inconsistent faceSize
						throw Exception();
				}
				processedVertexCount += count;
			}
		} catch(Exception &) {
			if(deleteCollada) {
				delete collada;
			}
			delete mesh;
			throw Exception("Error: indices must be of type points, lines, or polygons exclusively, or the index dataset is inconsistent",EX_SYNTAX);
		}

		Matrix normalTransform = transform;
		normalTransform.normalMatrix();

		//vertex data extraction: extract names, data and dimensions of the databuffers 
		//vertex data
		map<string,vector<double> > dataSets;
		//number of components per dataset
		map<string,unsigned int> attributeSizes;
		map<string,unsigned int> offsets;
		map<string,vector<double> > bones;

		try {
			//collect sources
			vector<XTag *> s;
			meshTag->getTags("source",s);
			map<string,XTag *> sources;
			for(vector<XTag *>::iterator iter = s.begin() ; iter != s.end() ; iter++) {
				XTag *source = (*iter);
				string id = source->getStrAttribute("id");
				map<string,XTag *>::iterator i = sources.find(id);
				if(i != sources.end()) {
					//buffer already exists
					throw Exception();
				}
				sources.insert(pair<string,XTag *>(id,source));
			}

			vector<XTag *> inputs;
			meshTag->getTags("polylist.input",inputs);
			for(vector<XTag *>::iterator iter = inputs.begin() ; iter != inputs.end() ; iter++) {
				XTag *input = (*iter);
				string bufferName = input->getStrAttribute("semantic");
				string bufferID = input->getStrAttribute("source");
				bufferID = bufferID.substr(1);
				vector<double> offsetV = parseNumbers(input->getStrAttribute("offset"));
				if(offsetV.size() != 1) {
					throw Exception();
				}
				unsigned int offset = (unsigned int) offsetV[0];
				
				//treat naming of the buffer
				if(bufferName.compare("VERTEX") == 0) {
					bufferName = "vertices";
					//vertex buffers have another id than source in polylist.input
					//lookup the actual id of the buffer
					XTag *vInput = meshTag->getFirst("vertices");
					if(vInput->getStrAttribute("id").compare(bufferID) != 0) {
						throw Exception();
					}
					bufferID = vInput->getFirst("input")->getStrAttribute("source");
					bufferID = bufferID.substr(1);
				} else if(bufferName.compare("TEXCOORD") == 0) {
					bufferName = "texcoords";
				} else if(bufferName.compare("NORMAL") == 0) {
					bufferName = "normals";
				}

				//extract array of data
				map<string,XTag *>::iterator findSource = sources.find(bufferID);
				if(findSource == sources.end()) {
					//source not found
					throw Exception();
				}
				XTag *fArray = (*findSource).second->getFirst("float_array");
				XTag *accessor = (*findSource).second->getFirst("technique_common.accessor");
				vector<double> attrSizeV = parseNumbers(accessor->getStrAttribute("stride"));
				if(attrSizeV.size() != 1) {
					throw Exception();
				}
				unsigned int attributeSize = (unsigned int) attrSizeV[0];
				//normal in input is true if an attribute
				//is transformed by the normalmatrix
				bool normal = false;
				bool useNormal = false;
				string nAttr;
				try {
					nAttr = input->getStrAttribute("normal");
					useNormal = true;
				} catch(Exception &) {
				}
				if(useNormal) {
					if(nAttr.compare("true") != 0 && nAttr.compare("false") != 0) {
						//attribute normal of input
						//has to be true or false
						throw Exception();
					}
					normal = nAttr.compare("true") == 0;
				}
				//normalize in input is true if the attribute
				//should be normalized
				bool normalize = false;
				bool useNormalize = false;
				try {
					nAttr = input->getStrAttribute("normalize");
					useNormalize = true;
				} catch(Exception &) {
				}
				if(useNormalize) {
					if(nAttr.compare("true") != 0 && nAttr.compare("false") != 0) {
						//attribute normalize of input
						//has to be true or false
						throw Exception();
					}
					normalize = nAttr.compare("true") == 0;
				}
	
				map<string,vector<double> >::iterator dSetFind = dataSets.find(bufferName);
				if(dSetFind != dataSets.end()) {
					//buffer already exists
					throw Exception();
				}
				dataSets.insert(pair<string,vector<double> >(bufferName,vector<double>()));
				dSetFind = dataSets.find(bufferName);
				vector<double> &buffer = (*dSetFind).second;
				buffer = parseNumbers(fArray->getTexts());

				//perform transformation on buffer
				//if it has dimension 4 or three
				unsigned int bSize = buffer.size() / attributeSize;
				Matrix t = transform;
				if(bufferName.compare("normals") == 0 || normal) {
					t = normalTransform;
				}
				if(bufferName.compare("normals") == 0) {
					normalize = true;
				}
				if(attributeSize == 4 || attributeSize == 3) {
					for(unsigned int i=0 ; i<bSize ; i++) {
						double *p = &buffer[i*attributeSize];
						Vector v;
						for(unsigned int j=0 ; j<attributeSize ; j++) {
							v[j] = (float) p[j];
						}
						v = t * v;
						if(normalize) {
							v.normalize();
						}
						for(unsigned int j=0 ; j<attributeSize ; j++) {
							p[j] = v[j];
						}
					}
				}
				//now it's guaranteed that the bufferName
				//is not taken in the other maps
				//hence no lookup in those maps for bufferName
				//is required
				attributeSizes.insert(pair<string,unsigned int>(bufferName,attributeSize));
				offsets.insert(pair<string,unsigned int>(bufferName,offset));
			}
		} catch(Exception &) {
			if(deleteCollada) {
				delete collada;
			}
			delete mesh;
			throw Exception("Error: inconsistency in the buffers",EX_SYNTAX);
		}

		//extract bone weights
		if(controller != 0) {
			try {
				vector<XTag *> sources;
				controller->getTags("skin.source",sources);

				//search for joints and weights
				XTag *vertex_weights = controller->getFirst("skin.vertex_weights");
				XTag *vIndices = vertex_weights->getFirst("v");
				XTag *countIndices = vertex_weights->getFirst("vcount");
				vector<XTag *> inputs;
				vertex_weights->getTags("input",inputs);
				string jointID;
				string weightID;
				unsigned int jointOffset = 0;
				unsigned int weightOffset = 0;
				XTag *jointArray = 0;
				XTag *weightArray = 0;
				vector<double> indices = parseNumbers(vIndices->getTexts());
				vector<double> vcount = parseNumbers(countIndices->getTexts());
				for(vector<XTag *>::iterator iter = inputs.begin() ; iter != inputs.end() ; iter++) {
					XTag *input = (*iter);
					if(input->getStrAttribute("semantic").compare("JOINT") == 0) {
						jointID = input->getStrAttribute("source");
						jointID = jointID.substr(1);
						vector<double> jointOffsetA = parseNumbers(input->getStrAttribute("offset"));
						if(jointOffsetA.size() != 1) {
							throw Exception();
						}
						jointOffset = (unsigned int) jointOffsetA[0];
					} else if(input->getStrAttribute("semantic").compare("WEIGHT") == 0) {
						weightID = input->getStrAttribute("source");
						weightID = weightID.substr(1);
						vector<double> weightOffsetA = parseNumbers(input->getStrAttribute("offset"));
						if(weightOffsetA.size() != 1) {
							throw Exception();
						}
						weightOffset = (unsigned int) weightOffsetA[0];
					}
				}
				for(vector<XTag *>::iterator iter = sources.begin() ; iter != sources.end() ; iter++) {
					XTag *source = (*iter);
					if(source->getStrAttribute("id").compare(jointID) == 0) {
						jointArray = source->getFirst("Name_array");
					} else if(source->getStrAttribute("id").compare(weightID) == 0) {
						weightArray = source->getFirst("float_array");
					}
				}
				if(jointArray == 0 || weightArray == 0) {
					//joint or weight array not found
					throw Exception();
				}
				if( !( (jointOffset == 0 && weightOffset == 1) || (jointOffset == 1 && weightOffset == 0) ) ) {
					//every two indices encode an index of a bone name, and an index
					//of a weight value
					//other cases are not supported by this parser
					throw Exception();
				}


				//extract bone names
				vector<string> boneNames = parseStrings(jointArray->getTexts());
				unsigned int boneCount = boneNames.size();

				//extract weights
				vector<double> weights = parseNumbers(weightArray->getTexts());
				unsigned int weightsSize = weights.size();

				//init bone buffers
				//fastBones allows access of a bone by its index
				vector<vector<double> *> fastBones;
				for(vector<string>::iterator iter = boneNames.begin() ; iter != boneNames.end() ; iter++) {
					string name = (*iter);
					map<string,vector<double> >::iterator fEntry = bones.find(name);
					if(fEntry != bones.end()) {
						//bone declared multiple times
						throw Exception();
					}
					bones.insert(pair<string,vector<double> >(name,vector<double>()));
					fEntry = bones.find(name);
					fastBones.push_back(&(*fEntry).second);
					for(unsigned int i=0 ; i<vcount.size() ; i++) {
						//initialize vertex weights with zero
						(*fEntry).second.push_back(0);
					}
				}

				//insert weights into bone buffers
				unsigned int currVertex = 0;
				vector<double>::iterator currIndex = indices.begin();
				bool switchOffsets = jointOffset == 1 && weightOffset == 0;
				for(vector<double>::iterator iter = vcount.begin() ; iter != vcount.end() ; iter++) {
					unsigned int count = (unsigned int) (*iter);
					for(unsigned int i=0 ; i<count ; i++) {
						//the next count * 2 indices contribute
						//to the boneweights of the currVertex. vertex
						if(currIndex == indices.end()) {
							//reached end too early
							throw Exception();
						}
						unsigned int boneIndex = (unsigned int) (*currIndex);
						currIndex++;
						if(currIndex == indices.end()) {
							//reached end too early
							throw Exception();
						}
						unsigned int weightIndex = (unsigned int) (*currIndex);
						currIndex++;
						if(switchOffsets) {
							//the first index was assumed to be the index of the bone name
							//and the second index was assumed to be the index of the bone weight
							//switchOffsets is true iff each of both indices has the opposite semantic
							//hence switching of the indices is necessary
							unsigned int temp = boneIndex;
							boneIndex = weightIndex;
							weightIndex = temp;
						}
						if(weightIndex >= weightsSize || boneIndex >= boneCount) {
							//weightIndex or boneIndex out of bounds
							throw Exception();
						}
						(*fastBones[boneIndex])[currVertex] = weights[weightIndex];
					}

					currVertex++;
				}
			} catch(Exception &) {
				if(deleteCollada) {
					delete collada;
				}
				delete mesh;
				throw Exception("Error: inconsistency in bone buffers, or an index is out of bounds",EX_SYNTAX);
			}
		}

		//bones require the buffer "vertices" to exist
		//as the boneweights are defined per vertex
		if(bones.size() > 0) {
			//test if buffer with name "vertices" exists
			map<string,vector<double> >::iterator fVertices = dataSets.find("vertices");
			if(fVertices == dataSets.end()) {
				//buffer "vertices" not found
				if(deleteCollada) {
					delete collada;
				}
				delete mesh;
				throw Exception("Error: boneweights require buffer with semantic \"VERTEX\" to exist",EX_SYNTAX);
			}
		}

		//insert vertexattributes according to the indexsets
		//into the mesh
		try {
			unsigned int vertexCount = combinedIndices.size() / inputCount;
			for(map<string,vector<double> >::iterator iter = dataSets.begin() ; iter != dataSets.end() ; iter++) {
				XBuffer *buffer = new XBuffer();
				string buffername = (*iter).first;
				buffer->name = buffername;
				vector<double> &buffercontent = (*iter).second;
				unsigned int buffercontentSize = buffercontent.size();
				unsigned int offset = (*offsets.find(buffername)).second;
				unsigned int attributeSize = (*attributeSizes.find(buffername)).second;
				buffer->attributeSize = attributeSize;

				//insert vertex attribute
				map<string,XBuffer *>::iterator cConsistencyMesh = mesh->buffers.find(buffername);
				if(cConsistencyMesh != mesh->buffers.end()) {
					//another buffer already has the same buffername
					delete buffer;
					throw Exception();
				}
				mesh->buffers.insert(pair<string,XBuffer *>(buffername, buffer));
				vector<double> &bContent = buffer->vertexAttributes;
				
				for(unsigned int i=0 ; i<vertexCount ; i++) {
					unsigned int index = combinedIndices[i*inputCount + offset];
					unsigned int bcIndex = index*attributeSize;
					if(bcIndex + attributeSize > buffercontentSize) {
						//index out of bounds
						throw Exception();
					}
					for(unsigned int j=0 ; j<attributeSize ; j++) {
						bContent.push_back(buffercontent[bcIndex + j]);
					}
				}

				if(buffername.compare("vertices") == 0) {
					//for bones the same indices as for buffer "vertices" are used
					unsigned int combinedIndicesSize = combinedIndices.size();
					for(map<string,vector<double> >::iterator boneIter = bones.begin() ; boneIter != bones.end() ; boneIter++) {
						buffername = (*boneIter).first;
						map<string,XBuffer *>::iterator controlUniquenes = mesh->buffers.find(buffername);
						if(controlUniquenes != mesh->buffers.end()) {
							//bone name not unique
							throw Exception();
						}
						buffer = new XBuffer();
						buffer->name = buffername;
						vector<double> &bonecontent = (*boneIter).second;
						unsigned int bonecontentSize = bonecontent.size();
						attributeSize = 1;
						buffer->attributeSize = attributeSize;

						//insert vertex weights
						mesh->buffers.insert(pair<string,XBuffer *>(buffername,buffer));
						vector<double> &boContent = buffer->vertexAttributes;

						for(unsigned int i=0 ; i<vertexCount ; i++) {
							unsigned int ciIndex = i*inputCount + offset;
							if(ciIndex >= combinedIndicesSize) {
								//ciIndex out of bounds
								throw Exception();
							}
							unsigned int index = combinedIndices[ciIndex];
							if(index >= bonecontentSize) {
								//index out of bounds
								throw Exception();
							}
							boContent.push_back(bonecontent[index]);
						}
					}
				}
			}
		} catch(Exception &) {
			if(deleteCollada) {
				delete collada;
			}
			delete mesh;
			throw Exception("Error: inserting buffers into mesh didn't work, indices might be out of bounds, or buffernames might not be unique",EX_SYNTAX);
		}

	}

	XMesh *parseColladaData(XTag &data) {
		XMesh *mesh = new XMesh();
		COLLADA_transformToMesh(&data, false, mesh);
		return mesh;
	}

	XMesh *parseColladaData(const string &data) {
		XMesh *mesh = new XMesh();
		XTag *collada = parseXMLdata(data);
		COLLADA_transformToMesh(collada, true, mesh);
		delete collada;
		return mesh;
	}

	XMesh *parseColladaFile(const string path) {
		string content = readFile(path);
		return parseColladaData(content);
	}

}

#endif