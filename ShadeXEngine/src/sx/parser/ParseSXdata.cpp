#ifndef _SX_PARSER_PARSESXDATA_CPP_
#define _SX_PARSER_PARSESXDATA_CPP_

/**
 * sx parser
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <boost/foreach.hpp>
#include <vector>
#include <sstream>
using namespace std;
using namespace boost;
using sx::parserInternal::_XNODE_;
using sx::parserInternal::_XTYPE_;
using sx::parserInternal::_XTEXT_;
using sx::parserInternal::_XTAG_;
using sx::parserInternal::_S_ATTRIB_;
using sx::parserInternal::_R_ATTRIB_;

namespace sx {

	/**
	 * converts _XNODE_ to XTag
	 */
	XTag *SXToXNode(_XNODE_ &source,bool &deconstruct,Exception &ex) {
		XTag *tag = new XTag();
		tag->name = source.name;
		BOOST_FOREACH(_XNODE_ &node , source.nodes) {
			if(deconstruct) {
				//if an error occurred, leave node
				return tag;
			}
			if(node.type == _XTAG_) {
				//take child under any circumstances
				XTag *child = SXToXNode(node,deconstruct,ex);
				tag->nodes.push_back(child);
			} else if(node.type == _XTEXT_) {
				XText *text = new XText();
				text->text = node.text;
				tag->nodes.push_back(text);
			} else if(node.type == _S_ATTRIB_) {
				map<string,string>::iterator iter = tag->stringAttribs.find(node.strID);
				if(iter == tag->stringAttribs.end()) {
					tag->stringAttribs.insert(pair<string,string>(node.strID,node.strAttrib));
				} else {
					//attribute IDs must not appear multiple times in the same tag
					stringstream errmsg;
					errmsg << "Error: attribute ID " << node.strID << " appears multiple times";
					deconstruct = true;
					ex.setMessage(errmsg.str());
					ex.setType(EX_SYNTAX);
				}
			} else if(node.type == _R_ATTRIB_) {
				map<string,double>::iterator iter = tag->realAttribs.find(node.rID);
				if(iter == tag->realAttribs.end()) {
					tag->realAttribs.insert(pair<string,double>(node.rID,node.rAttrib));
				} else {
					//attribute IDs must not appear multiple times in the same tag
					stringstream errmsg;
					errmsg << "Error: attribute ID " << node.rID << " appears multiple times";
					deconstruct = true;
					ex.setMessage(errmsg.str());
					ex.setType(EX_SYNTAX);
				}
			}
		}
		return tag;
	}

	XTag *parseSXdata(const string &data) {
		string iterable;
		_XNODE_ output;
		parserInternal::parseSXdata1(data,iterable);
		parserInternal::parseSXdata2(iterable,output);
		
		bool deconstruct = false;
		Exception ex;
		XTag *tag = SXToXNode(output,deconstruct,ex);
		if(deconstruct) {
			delete tag;
			throw ex;
		}
		return tag;
	}

	XTag *parseSXFile(const string path) {
		string content = readFile(path);
		return parseSXdata(content);
	}

}

#endif