#ifndef _SX_PARSER_XBUFFER_CPP_
#define _SX_PARSER_XBUFFER_CPP_

/**
 * buffer class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXParser.h>
#include <sx/Log4SX.h>

namespace sx {

	XBuffer::XBuffer() {
	}

	XBuffer::~XBuffer() {
		SXout(LogMarkup("~XBuffer"));
		SXout(name);
	}

}

#endif