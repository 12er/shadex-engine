#ifndef _SX_PARSER_PARSEXMLDATA_CPP_
#define _SX_PARSER_PARSEXMLDATA_CPP_

/**
 * xml parser
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <sstream>
#include <boost/foreach.hpp>
using namespace std;
using namespace boost;
using sx::parserInternal::_XMLNODE_;
using sx::parserInternal::_XTYPE_;
using sx::parserInternal::_XTEXT_;
using sx::parserInternal::_XTAG_;
using sx::parserInternal::_S_ATTRIB_;
using sx::parserInternal::_R_ATTRIB_;

namespace sx {

	/**
	 * converts _XMLNODE_ to XTag
	 */
	XTag *XMLToXNode(_XMLNODE_ &source,bool &deconstruct,Exception &ex) {
		XTag *tag = new XTag();
		tag->name = source.name;
		if(source.name.compare(source.endname) != 0) {
			//start and endtag must have the same name
			stringstream errmsg;
			errmsg << "Error: opening tag is " << source.name << ", but closing tag is " << source.endname;
			deconstruct = true;
			ex.setMessage(errmsg.str());
			ex.setType(EX_SYNTAX);
		}
		if(deconstruct) {
			//if an error occurred, leave node
			return tag;
		}
		BOOST_FOREACH(_XMLNODE_ &node , source.ats) {
			map<string,string>::iterator iter = tag->stringAttribs.find(node.strID);
			if(iter == tag->stringAttribs.end()) {
				tag->stringAttribs.insert(pair<string,string>(node.strID,node.strAttrib));
			} else {
				//attribute IDs must not appear multiple times in the same tag
				stringstream errmsg;
				errmsg << "Error: attribute ID " << node.strID << " appears multiple times";
				deconstruct = true;
				ex.setMessage(errmsg.str());
				ex.setType(EX_SYNTAX);
			}
		}
		BOOST_FOREACH(_XMLNODE_ &node , source.nodes) {
			if(deconstruct) {
				//if an error occurred, leave node
				return tag;
			}
			if(node.type == _XTAG_) {
				//take child under any circumstances
				XTag *child = XMLToXNode(node,deconstruct,ex);
				tag->nodes.push_back(child);
			} else if(node.type == _XTEXT_) {
				XText *text = new XText();
				text->text = node.text;
				tag->nodes.push_back(text);
			}
		}
		return tag;
	}

	XTag *parseXMLdata(const string &data) {
		_XMLNODE_ output;
		parserInternal::parseXMLdata(data,output);

		bool deconstruct = false;
		Exception ex;
		XTag *tag = XMLToXNode(output,deconstruct,ex);
		if(deconstruct) {
			delete tag;
			throw ex;
		}
		return tag;
	}

	XTag *parseXMLFile(const string path) {
		string content = readFile(path);
		return parseXMLdata(content);
	}

}

#endif