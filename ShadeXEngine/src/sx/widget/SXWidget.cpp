#ifndef _SX_WIDGET_SXWIDGET_CPP_
#define _SX_WIDGET_SXWIDGET_CPP_

/**
 * SX Widget class for the Qt framework
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>
#include <sx/SXWidget.h>
#include <GL/glut.h>
#include <sx/Exception.h>
#include <QGridLayout>
#include <QMouseEvent>
#include <QCursor>
#include <cmath>
#include <sstream>
#include <algorithm>
using namespace std;

namespace sx {

	map<int,int> SXInternalWidget::keys_qt2sx = SXInternalWidget::initKeys_qt2sx();

	map<int,int> SXInternalWidget::initKeys_qt2sx() {
		map<int,int> mapping;
		mapping.insert(pair<int,int>(Qt::Key_F1,SX_F1));
		mapping.insert(pair<int,int>(Qt::Key_F2,SX_F2));
		mapping.insert(pair<int,int>(Qt::Key_F3,SX_F3));
		mapping.insert(pair<int,int>(Qt::Key_F4,SX_F4));
		mapping.insert(pair<int,int>(Qt::Key_F5,SX_F5));
		mapping.insert(pair<int,int>(Qt::Key_F6,SX_F6));
		mapping.insert(pair<int,int>(Qt::Key_F7,SX_F7));
		mapping.insert(pair<int,int>(Qt::Key_F8,SX_F8));
		mapping.insert(pair<int,int>(Qt::Key_F9,SX_F9));
		mapping.insert(pair<int,int>(Qt::Key_F10,SX_F10));
		mapping.insert(pair<int,int>(Qt::Key_F11,SX_F11));
		mapping.insert(pair<int,int>(Qt::Key_F12,SX_F12));
		mapping.insert(pair<int,int>(Qt::Key_Escape,SX_ESC));
		mapping.insert(pair<int,int>(Qt::Key_Insert,SX_INSERT));
		mapping.insert(pair<int,int>(Qt::Key_Pause,SX_PAUSE));
		mapping.insert(pair<int,int>(Qt::Key_Home,SX_HOME));
		mapping.insert(pair<int,int>(Qt::Key_End,SX_END));
		mapping.insert(pair<int,int>(Qt::Key_Delete,SX_DELETE));
		mapping.insert(pair<int,int>(Qt::Key_PageUp,SX_PAGEUP));
		mapping.insert(pair<int,int>(Qt::Key_PageDown,SX_PAGEDOWN));
		mapping.insert(pair<int,int>(Qt::Key_Left,SX_LEFT));
		mapping.insert(pair<int,int>(Qt::Key_Right,SX_RIGHT));
		mapping.insert(pair<int,int>(Qt::Key_Down,SX_DOWN));
		mapping.insert(pair<int,int>(Qt::Key_Up,SX_UP));
		mapping.insert(pair<int,int>(Qt::Key_NumLock,SX_NUMLOCK));
		mapping.insert(pair<int,int>(Qt::Key_Clear,SX_CLEAR));
		mapping.insert(pair<int,int>(Qt::Key_Enter,SX_ENTER));
		mapping.insert(pair<int,int>(Qt::Key_Control,SX_CTRL));
		mapping.insert(pair<int,int>(Qt::Key_Shift,SX_SHIFT));
		mapping.insert(pair<int,int>(Qt::Key_CapsLock,SX_CAPSLOCK));
		mapping.insert(pair<int,int>(Qt::Key_Meta,SX_START));
		mapping.insert(pair<int,int>(Qt::Key_Alt,SX_ALT));
		mapping.insert(pair<int,int>(Qt::Key_Return,SX_RETURN));
		mapping.insert(pair<int,int>(Qt::Key_Backspace,SX_BACKSPACE));
		return mapping;
	}

	void SXInternalWidget::keyPressEvent(QKeyEvent * e) {
		if(!e->isAutoRepeat()) {
			int key = e->key();
			map<int,int>::iterator iter = keys_qt2sx.find(e->key());
			if(iter != keys_qt2sx.end()) {
				//it's a special key
				//translate it to sx keymap
				key = (*iter).second;
			}
			sxWidget->addKey(key);
		}
	}

	void SXInternalWidget::keyReleaseEvent(QKeyEvent * e) {
		if(!e->isAutoRepeat()) {
			int key = e->key();
			map<int,int>::iterator iter = keys_qt2sx.find(e->key());
			if(iter != keys_qt2sx.end()) {
				//it's a special key
				//translate it to sx keymap
				key = (*iter).second;
			}
			sxWidget->removeKey(key);
		}
	}

	void SXInternalWidget::mousePressEvent(QMouseEvent * e) {
		MouseButton button = MOUSE_LEFT;
		switch(e->button()) {
			case Qt::LeftButton:
				button = MOUSE_LEFT;
				break;
			case Qt::MiddleButton:
				button = MOUSE_MIDDLE;
				break;
			case Qt::RightButton:
				button = MOUSE_RIGHT;
				break;
		}
		setFocus();
		sxWidget->addMouseKey(button);
		sxWidget->mouseX = e->x();
		sxWidget->mouseY = this->size().height() - 1 - e->y();
	}

	void SXInternalWidget::mouseReleaseEvent(QMouseEvent * e) {
		MouseButton button = MOUSE_LEFT;
		switch(e->button()) {
			case Qt::LeftButton:
				button = MOUSE_LEFT;
				break;
			case Qt::MiddleButton:
				button = MOUSE_MIDDLE;
				break;
			case Qt::RightButton:
				button = MOUSE_RIGHT;
				break;
		}
		sxWidget->removeMouseKey(button);
		sxWidget->mouseX = e->x();
		sxWidget->mouseY = this->size().height() - 1 - e->y();
	}

	void SXInternalWidget::mouseMoveEvent(QMouseEvent * e) {
		sxWidget->mouseX = e->x();
		sxWidget->mouseY = this->size().height() - 1 - e->y();
	}

	void SXInternalWidget::wheelEvent(QWheelEvent * e) {
		float degreesTimesEight = (float)e->delta();
		float degrees = degreesTimesEight / 8.0f;
		this->sxWidget->wheelRotates(degrees);
	}

	SXInternalWidget::SXInternalWidget(SXWidget *sxWidget): QGLWidget() {
		this->sxWidget = sxWidget;
	}

	SXInternalWidget::~SXInternalWidget() {
	}

	void SXInternalWidget::initializeGL() {
		makeCurrent();
		GLenum err = glewInit();
		if(GLEW_OK != err) {
			throw Exception("no glew support", EX_NODATA);
		}
		int glVersion[2] = {-1,-1};
		glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
		glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
		if(glVersion[0] < 4) {
			stringstream errmsg;
			errmsg << "OpenGL " << glVersion[0] << "." << glVersion[1] << " supported, but OpenGL 4.x support required";
			throw Exception(errmsg.str(),EX_NODATA);
		}
		glEnable(GL_DEPTH_TEST);
		setAutoBufferSwap(true);
		setMouseTracking(true);
		setFocus();
	}

	void SXInternalWidget::resizeGL(int width, int height) {
		if(!sxWidget->constructed) {
			return;
		}
		sxWidget->processingListeners = true;
		if(sxWidget->initListeners.size() > 0) {
			for(vector<SXRenderListener *>::iterator iter = sxWidget->initListeners.begin() ; iter != sxWidget->initListeners.end() ; iter++) {
				(*iter)->create(*sxWidget);
			}
			sxWidget->initListeners.clear();
		}
		for(vector<SXRenderListener *>::iterator iter = sxWidget->listeners.begin() ; iter != sxWidget->listeners.end() ; iter++) {
			(*iter)->reshape(*sxWidget);
		}
		sxWidget->processingListeners = false;
	}

	void SXInternalWidget::paintGL() {
		if(!sxWidget->constructed) {
			return;
		}
		sxWidget->updateTime();
		if(sxWidget->callStopAfterRender) {
			//stopRendering can only process
			//if listeners aren't processed
			sxWidget->callStopAfterRender = false;
			sxWidget->stopRendering();
			return;
		}
		sxWidget->processingListeners = true;
		for(vector<SXRenderListener *>::iterator iter = sxWidget->listeners.begin() ; iter != sxWidget->listeners.end() ; iter++) {
			if(sxWidget->initListeners.size() > 0) {
				vector<SXRenderListener *>::iterator noInit = std::find(sxWidget->initListeners.begin(),sxWidget->initListeners.end(),(*iter));
				if(noInit != sxWidget->initListeners.end()) {
					//listener not initialized yet
					//, so don't render
					continue;
				}
			}
			(*iter)->render(*sxWidget);
		}
		sxWidget->processingListeners = false;
		if(sxWidget->callStopAfterRender) {
			//stopRendering can only process
			//if listeners aren't processed
			sxWidget->callStopAfterRender = false;
			sxWidget->stopRendering();
			return;
		}
	}

	void SXWidget::updateTime() {
		if(lastTimeRendered == -1) {
			timeMeasurement.restart();
		}
		lastTimeRendered = currentTimeRendering;
		currentTimeRendering = timeMeasurement.elapsed();
	}

	void SXWidget::addKey(int key) {
		keys[key] = key;
	}

	void SXWidget::removeKey(int key) {
		unordered_map<int,int>::iterator iter = keys.find(key);
		if(iter != keys.end()) {
			keys.erase(iter);
		}
	}

	void SXWidget::addMouseKey(MouseButton key) {
		int k = (int)key;
		mouseKeys[k] = k;
	}

	void SXWidget::removeMouseKey(MouseButton key) {
		int k = (int)key;
		unordered_map<int,int>::iterator iter = mouseKeys.find(k);
		if(iter != mouseKeys.end()) {
			mouseKeys.erase(iter);
		}
	}

	SXWidget::SXWidget(): QWidget() {
		setMinimumSize(130,100);
		constructed = true;
		processingListeners = false;
		callStopAfterRender = false;
		lastTimeRendered = -1;
		currentTimeRendering = 0;
		mouseDeltaX = mouseDeltaY = 0;
		lastSetMouseTime = 0;
		mouseX = mouseY = 0;
		internalWidget = 0;
		internalWidget = new SXInternalWidget(this);
		connect(&timer,SIGNAL(timeout()),internalWidget,SLOT(repaint()));
		QGridLayout *layout = new QGridLayout();
		layout->setContentsMargins(0,0,0,0);
		layout->addWidget(internalWidget,1,1,1,1);
		setLayout(layout);
	}

	SXWidget::~SXWidget() {
		if(constructed) {
			stopRendering();
		}
		deleteRenderListeners();
	}

	void SXWidget::renderPeriodically(float time) {
		int millies = (int)ceil(time * 1000);
		timer.start(millies);
	}

	void SXWidget::stopRenderPeriodically() {
		timer.stop();
	}

	void SXWidget::addRenderListener(SXRenderListener &l) {
		if(!constructed) {
			return;
		}
		listeners.push_back(&l);
		initListeners.push_back(&l);
	}

	void SXWidget::deleteRenderListener(SXRenderListener &l) {
		processingListeners = true;
		if(constructed) {
			vector<SXRenderListener *>::iterator noInit = std::find(initListeners.begin(),initListeners.end(),&l);
			if(noInit == initListeners.end()) {
				//iter has been initialized
				l.stop(*this);
			}
		}
		vector<vector<SXRenderListener *>::iterator> deletables;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			if((*iter) == &l) {
				deletables.push_back(iter);
			}
		}
		for(vector<vector<SXRenderListener *>::iterator>::iterator iter = deletables.begin() ; iter != deletables.end() ; iter++) {
			listeners.erase((*iter));
		}
		deletables.clear();
		for(vector<SXRenderListener *>::iterator iter = initListeners.begin() ; iter != initListeners.end() ; iter++) {
			if((*iter) == &l) {
				deletables.push_back(iter);
			}
		}
		for(vector<vector<SXRenderListener *>::iterator>::iterator iter = deletables.begin() ; iter != deletables.end() ; iter++) {
			initListeners.erase((*iter));
		}
		delete &l;
		processingListeners = false;
	}

	void SXWidget::deleteRenderListeners() {
		processingListeners = true;
		if(constructed) {
			unordered_map<SXRenderListener *,SXRenderListener *> initializedListeners;
			for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
				vector<SXRenderListener *>::iterator noInit = std::find(initListeners.begin(),initListeners.end(),(*iter));
				if(noInit == initListeners.end()) {
					//iter has been initialized
					initializedListeners[(*iter)] = (*iter);
				}
			}
			for(unordered_map<SXRenderListener *,SXRenderListener *>::iterator iter = initializedListeners.begin() ; iter != initializedListeners.end() ; iter++) {
				(*iter).first->stop(*this);
			}
			constructed = false;
		}
		unordered_map<SXRenderListener *,SXRenderListener *> deletableListeners;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			deletableListeners[(*iter)] = (*iter);
		}
		for(unordered_map<SXRenderListener *,SXRenderListener *>::iterator iter = deletableListeners.begin() ; iter != deletableListeners.end() ; iter++) {
			Logger::get() << LogMarkup("SXWidget::deleteRenderListeners/listener") << (unsigned int)1;
			delete (*iter).first;
		}
		listeners.clear();
		initListeners.clear();
		processingListeners = false;
		Logger::get() << LogMarkup("SXWidget::deleteRenderListeners/listeners") << (unsigned int)listeners.size();
		Logger::get() << LogMarkup("SXWidget::deleteRenderListeners/initListeners") << (unsigned int)initListeners.size();
	}

	double SXWidget::getTime() const {
		return currentTimeRendering;
	}

	double SXWidget::getDeltaTime() const {
		if(lastTimeRendered < 0) {
			return 0;
		}
		return currentTimeRendering - lastTimeRendered;
	}

	int SXWidget::getMouseX() const {
		return mouseX;
	}

	int SXWidget::getMouseY() const {
		return mouseY;
	}

	double SXWidget::getMouseDeltaX() const {
		return mouseDeltaX;
	}

	double SXWidget::getMouseDeltaY() const {
		return mouseDeltaY;
	}

	bool SXWidget::hasKey(int key) const {
		unordered_map<int,int>::const_iterator iter = keys.find(key);
		return iter != keys.end();
	}

	bool SXWidget::hasMouseKey(MouseButton key) const {
		int k = (int)key;
		unordered_map<int,int>::const_iterator iter = mouseKeys.find(k);
		return iter != mouseKeys.end();
	}

	int SXWidget::getWidth() const {
		return size().width();
	}

	int SXWidget::getHeight() const {
		return size().height();
	}

	void SXWidget::stopRendering() {
		if(processingListeners) {
			callStopAfterRender = true;
			return;
		}
		processingListeners = true;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			vector<SXRenderListener *>::iterator noInit = std::find(initListeners.begin(),initListeners.end(),(*iter));
			if(noInit == initListeners.end()) {
				//iter has been initialized
				(*iter)->stop(*this);
			}
		}
		processingListeners = false;
		if(constructed) {
			constructed = false;
			finishedRendering();
		}
	}

	void SXWidget::setMousePointer(int x, int y) {
		double currentTime = timeMeasurement.elapsed();
		if(currentTime - lastSetMouseTime < 0.01) {
			//don't poll too often
			//to optain good results
			return;
		}
		lastSetMouseTime = currentTime;
		float mDX = getMouseX() - x;
		float mDY = getMouseY() - y;
		//simply taking the mouse changes for each frame
		//would result in random behaviour for small mouse
		//changes, because changing the mouseposition for a distance
		//of one pixel leaves only nine possible positions for the mouse
		//to go
		//therefore the changes are smoothed
		setMouseDeltas.push_back(Vector(mDX,mDY));
		if(setMouseDeltas.size() > 5) {
			//keep a finite history of changes in
			//the mouseposition
			setMouseDeltas.erase(setMouseDeltas.begin());
		}
		//calculate mouseDeltaX/Y by averaging a subsequence of
		//the history of mousechanges
		Vector avgDelta;
		float n = 0;
		float sumLength = 0;
		for(int i=setMouseDeltas.size()-1 ; i>=0 ; i--) {
			Vector &d = setMouseDeltas[i];
			avgDelta = avgDelta + d;
			n++;
			sumLength += d.length();
			if(sumLength > 25) {
				//measurements are pretty accurate
				//if the mouse has travelled over
				//larger distances
				//hence don't consider more data
				//if the considered path of the mouse
				//is large enough
				break;
			}
		}
		avgDelta.scalarmult(1/n);
		mouseDeltaX = avgDelta[0];
		mouseDeltaY = avgDelta[1];
		QPoint widgetOrigin = mapToGlobal(QPoint(0,size().height() - 1));
		QCursor::setPos(widgetOrigin.x() + x,widgetOrigin.y() - y);
	}

	void SXWidget::setShowCursor(bool showCursor) {
		if(showCursor) {
			setCursor(QCursor(Qt::ArrowCursor));
		} else {
			setCursor(QCursor(Qt::BlankCursor));
		}
	}

}

#endif