#ifndef _SX_ENGINE_CORE_UNIFORMDOUBLE_CPP_
#define _SX_ENGINE_CORE_UNIFORMDOUBLE_CPP_

/**
 * Uniform double class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <GL/glew.h>

namespace sx {

	UniformDouble::UniformDouble(const string &id): Uniform(id) {
		value = 0;
	}
	
	UniformDouble &UniformDouble::operator = (double v) {
		value = v;
		return this [0];
	}

	UniformDouble &UniformDouble::operator << (double v) {
		value = v;
		return this [0];
	}

	const UniformDouble &UniformDouble::operator >> (double &v) const {
		v = value;
		return this [0];
	}

	void UniformDouble::load() {
	}

	bool UniformDouble::isLoaded() const {
		return true;
	}

	void UniformDouble::use(Shader &shader, const string &identifyer) {
		if(!shader.isLoaded()) {
			//shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(identifyer);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			glUniform1d(loc,(GLdouble)value);
		}
	}
}

#endif