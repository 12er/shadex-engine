#ifndef _SX_ENGINE_CORE_RENDEROBJECT_CPP_
#define _SX_ENGINE_CORE_RENDEROBJECT_CPP_

/**
 * Renderobject class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sstream>
#include <GL/glew.h>
using namespace std;

namespace sx {

	RenderObject::RenderObject(const string &id) {
		this->id = id;
		visible = true;
		cullMode = CULL_NONE;
		mesh = 0;
		shader = 0;
	}

	RenderObject::~RenderObject() {
	}

	void RenderObject::load() {
	}

	bool RenderObject::isLoaded() const {
		return true;
	}

	void RenderObject::setVisible(bool visible) {
		this->visible = visible;
	}

	bool RenderObject::isVisible() const {
		return visible;
	}

	void RenderObject::setCullMode(CullMode cullMode) {
		this->cullMode = cullMode;
	}

	CullMode RenderObject::getCullMode() const {
		return cullMode;
	}

	void RenderObject::setMesh(Mesh &mesh) {
		this->mesh = &mesh;
	}

	Mesh *RenderObject::getMesh() {
		return mesh;
	}


	void RenderObject::setShader(Shader *shader) {
		this->shader = shader;
	}

	Shader *RenderObject::getShader() {
		return shader;
	}

	void RenderObject::addUniform(Uniform &uniform) {
		const string &name = uniform.getUniformName(id);
		uniforms[name] = &uniform;

		SXout(LogMarkup("RenderObject::addUniform"));
		SXout(uniforms.size());
	}

	void RenderObject::removeUniform(const string &name) {
		unordered_map<string, Uniform *>::iterator iter = uniforms.find(name);
		if(iter != uniforms.end()) {
			uniforms.erase(iter);
		}

		SXout(LogMarkup("RenderObject::removeUniform"));
		SXout(uniforms.size());
	}


	void RenderObject::removeUniforms() {
		uniforms.clear();

		SXout(LogMarkup("RenderObject::removeUniforms"));
		SXout(uniforms.size());
	}

	void RenderObject::setInstanceBuffers(const vector<BufferedMesh *> &instanceBuffers) {
		this->instanceBuffers = instanceBuffers;
	}

	void RenderObject::removeInstanceBuffers() {
		instanceBuffers.clear();
	}

	unordered_map<string, Uniform *> &RenderObject::getUniformSet(const int id) {
		return uniformSets[id];
	}

	void RenderObject::removeUniformSet(const int id) {
		unordered_map<int,unordered_map<string,Uniform *>>::iterator iter = uniformSets.find(id);
		if(iter != uniformSets.end()) {
			uniformSets.erase(iter);
		}
	}

	void RenderObject::removeUniformSets() {
		uniformSets.clear();
	}

	void RenderObject::render(Shader *passShader) {
		if(!visible || mesh == 0 || (shader == 0 && passShader == 0)) {
			//invisible objects must not be rendered
			//only objects with a mesh can be rendered
			//objects can only be rendered with shaders
			SXout(LogMarkup("RenderObject::render"));
			SXout("discard");
			return;
		}
		if(cullMode != CULL_NONE) {
			glEnable(GL_CULL_FACE);
			if(cullMode == CULL_CW) {
				glFrontFace(GL_CW);
			} else if(cullMode == CULL_CCW) {
				glFrontFace(GL_CCW);
			}
			glCullFace(GL_FRONT);
		}
		Shader *usedShader = shader;
		if(usedShader == 0) {
			//if no renderobject specific shader is set
			//use the shader from the pass
			usedShader = passShader;
		}
		usedShader->use();
		for(unordered_map<string,Uniform *>::iterator iter = uniforms.begin() ; iter != uniforms.end() ; iter++) {
			//bind uniforms
			(*iter).second->use(*usedShader,id);
		}
		//lock texture slots from the render object
		usedShader->lockTextureSlots();

		unordered_map<int,unordered_map<string,Uniform *>>::iterator iter = uniformSets.begin();
		do {
			//can enter once even without a uniform set
			//as render() can also operate without uniform sets
			if(iter != uniformSets.end()) {
				//process uniformSet
				//free texture slots for each uniform set
				usedShader->freeTextureSlots();
				unordered_map<string,Uniform *> &currentSet = (*iter).second;
				for(unordered_map<string,Uniform *>::iterator uIter = currentSet.begin() ; uIter != currentSet.end() ; uIter++) {
					(*uIter).second->use(*usedShader,id);
				}
			}

			if(instanceBuffers.size() == 0) {
				mesh->render(*usedShader);
			} else {
				BufferedMesh *m = dynamic_cast<BufferedMesh *>(mesh);
				if(m != 0) {
					m->renderInstanced(*usedShader,instanceBuffers);
				} else {
					mesh->render(*usedShader);
				}
			}

			if(iter != uniformSets.end()) {
				iter++;
			}
		} while(iter != uniformSets.end());

		//unlock texture slots from the render object
		usedShader->unlockTextureSlots();

		if(cullMode != CULL_NONE) {
			glDisable(GL_CULL_FACE);
		}
	}

}

#endif