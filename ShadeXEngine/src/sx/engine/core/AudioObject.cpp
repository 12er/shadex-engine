#ifndef _SX_ENGINE_CORE_AUDIOOBJECT_CPP_
#define _SX_ENGINE_CORE_AUDIOOBJECT_CPP_

/**
 * Audio Object class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <sstream>
#include <cstdlib>
using namespace std;

namespace sx {

	ALCdevice *AudioObject::device = 0;
	ALCcontext *AudioObject::context = 0;
	bool AudioObject::initialized = false;

	void AudioObject::initAudio() {
		if(!initialized) {
			device = alcOpenDevice(0);
			if(device == 0) {
				throw Exception("Error: could not init audio device",EX_INIT);
			}
			ALCcontext *context = alcCreateContext(device,0);
			alcMakeContextCurrent(context);
			if(context == 0) {
				alcCloseDevice(device);
				throw Exception("Error: could not create audio context",EX_INIT);
			}
			alDistanceModel(AL_EXPONENT_DISTANCE_CLAMPED);
			//close device and destroy context when leaving the appication
			atexit(AudioObject::closeAudio);
			initialized = true;
		}
	}

	void AudioObject::closeAudio() {
		if(initialized) {
			alcMakeContextCurrent(0);
			alcDestroyContext(context);
			alcCloseDevice(device);
		}
		initialized = false;
	}

	bool AudioObject::isInitialized() {
		return initialized;
	}

	AudioObject::AudioObject(const string &id) {
		this->id = id;
		this->buffer = 0;
		this->position = 0;
		this->referenceRadius = 0;
		this->maxRadius = 0;
		this->distanceExponent = 0;
		this->velocity = 0;
		this->volume = 0;
		this->pitch = 0;
		this->looping = false;
		this->playing = false;
		this->locked = false;
		loaded = false;
	}

	AudioObject::~AudioObject() {
		unload();
	}

	void AudioObject::load() {
		if(!initialized) {
			try {
				initAudio();
			} catch(Exception &) {
				loaded = false;
				return;
			}
			if(!initialized) {
				loaded = false;
				return;
			}
		}
		unload();

		//reading file finished
		//now load resources
		alGenSources(1,(ALuint *)&audio);

		if(buffer != 0 && buffer->isLoaded()) {
			alSourcei(audio,AL_BUFFER,buffer->getBufferID());
		}
		if(position != 0) {
			alSourcefv(audio,AL_POSITION,position->elements);
		} else {
			float pos[] = {0,0,0};
			alSourcefv(audio,AL_POSITION,pos);
		}
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
		if(velocity != 0) {
			alSourcefv(audio,AL_VELOCITY,velocity->elements);
		} else {
			float vel[] = {0,0,0};
			alSourcefv(audio,AL_VELOCITY,vel);
		}
		if(volume != 0) {
			alSourcef(audio,AL_GAIN,volume->value);
		} else {
			alSourcef(audio,AL_GAIN,1);
		}
		if(pitch != 0) {
			alSourcef(audio,AL_PITCH,pitch->value);
		} else {
			alSourcef(audio,AL_PITCH,1);
		}
		if(looping) {
			alSourcei(audio,AL_LOOPING,AL_TRUE);
		} else {
			alSourcei(audio,AL_LOOPING,AL_FALSE);
		}

		if(playing) {
			alSourcePlay(audio);
		}

		loaded = true;
	}

	void AudioObject::unload() {
		if(loaded) {
			alDeleteSources(1,&audio);
			loaded = false;
		}
	}

	bool AudioObject::isLoaded() const {
		return loaded;
	}

	void AudioObject::setAudioBuffer(AudioBuffer &buffer) {
		this->buffer = &buffer;
		if(loaded) {
			//getBufferID returns 0 if buffer is not loaded
			alSourcei(audio,AL_BUFFER,this->buffer->getBufferID());
		}
	}

	void AudioObject::setPosition(const UniformVector *pos) {
		this->position = pos;
		if(this->position != 0 && loaded) {
			alSourcefv(audio,AL_POSITION,this->position->elements);
		} else if(loaded) {
			float pos[] = {0,0,0};
			alSourcefv(audio,AL_POSITION,pos);
		}
	}

	void AudioObject::setReferenceRadius(const UniformFloat *radius) {
		this->referenceRadius = radius;
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
	}

	void AudioObject::setMaxRadius(const UniformFloat *maxRadius) {
		this->maxRadius = maxRadius;
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
	}

	void AudioObject::setDistanceExponent(const UniformFloat *exponent) {
		this->distanceExponent = exponent;
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
	}

	void AudioObject::setVelocity(const UniformVector *vel) {
		this->velocity = vel;
		if(this->velocity != 0 && loaded) {
			alSourcefv(audio,AL_VELOCITY,this->velocity->elements);
		} else if(loaded) {
			float vel[] = {0,0,0};
			alSourcefv(audio,AL_VELOCITY,vel);
		}
	}

	void AudioObject::setVolume(const UniformFloat *v) {
		this->volume = v;
		if(this->volume != 0 && loaded) {
			alSourcef(audio,AL_GAIN,this->volume->value);
		} else if(loaded) {
			alSourcef(audio,AL_GAIN,1);
		}
	}

	void AudioObject::setPitch(const UniformFloat *pitch) {
		this->pitch = pitch;
		if(this->pitch != 0 && loaded) {
			alSourcef(audio,AL_PITCH,pitch->value);
		} else if(loaded) {
			alSourcef(audio,AL_PITCH,1);
		}
	}

	void AudioObject::setLooping(bool looping) {
		this->looping = looping;
		if(loaded) {
			if(this->looping) {
				alSourcei(audio,AL_LOOPING,AL_TRUE);
			} else {
				alSourcei(audio,AL_LOOPING,AL_FALSE);
			}
		}
	}

	void AudioObject::start() {
		if(!loaded) {
			return;
		}
		if(locked) {
			double t = timedLock.elapsed();
			if(t > lockTime) {
				locked = false;
			} else {
				return;
			}
		}
		playing = true;
		ALint state;
		alGetSourceiv(audio, AL_SOURCE_STATE, &state);
		if(position != 0) {
			alSourcefv(audio,AL_POSITION,position->elements);
		} else {
			float pos[] = {0,0,0};
			alSourcefv(audio,AL_POSITION,pos);
		}
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
		if(velocity != 0) {
			alSourcefv(audio,AL_VELOCITY,velocity->elements);
		} else {
			float vel[] = {0,0,0};
			alSourcefv(audio,AL_VELOCITY,vel);
		}
		if(volume != 0) {
			alSourcef(audio,AL_GAIN,volume->value);
		} else {
			alSourcef(audio,AL_GAIN,1);
		}
		if(pitch != 0) {
			alSourcef(audio,AL_PITCH,pitch->value);
		} else {
			alSourcef(audio,AL_PITCH,1);
		}
		if(buffer != 0) {
			//buffer returns 0 if it's not loaded
			alSourcei(audio,AL_BUFFER,buffer->getBufferID());
		}
		alSourcePlay(audio);
	}

	void AudioObject::start(double t) {
		if(!loaded) {
			return;
		}
		if(locked) {
			double t = timedLock.elapsed();
			if(t > lockTime) {
				locked = false;
			} else {
				return;
			}
		}
		playing = true;
		ALint state;
		alGetSourceiv(audio, AL_SOURCE_STATE, &state);
		if(position != 0) {
			alSourcefv(audio,AL_POSITION,position->elements);
		} else {
			float pos[] = {0,0,0};
			alSourcefv(audio,AL_POSITION,pos);
		}
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
		if(velocity != 0) {
			alSourcefv(audio,AL_VELOCITY,velocity->elements);
		} else {
			float vel[] = {0,0,0};
			alSourcefv(audio,AL_VELOCITY,vel);
		}
		if(volume != 0) {
			alSourcef(audio,AL_GAIN,volume->value);
		} else {
			alSourcef(audio,AL_GAIN,1);
		}
		if(pitch != 0) {
			alSourcef(audio,AL_PITCH,pitch->value);
		} else {
			alSourcef(audio,AL_PITCH,1);
		}
		if(buffer != 0) {
			//buffer returns 0 if it's not loaded
			alSourcei(audio,AL_BUFFER,buffer->getBufferID());
			
			//calculate length of video in seconds
			int bSize = 0;
			int freq = 0;
			int bitsPerSample = 0;
			int channels = 0;
			int bps = 0;
			alGetBufferi(buffer->getBufferID(), AL_SIZE, (ALint *)&bSize);
			alGetBufferi(buffer->getBufferID(), AL_FREQUENCY, (ALint *)&freq);
			alGetBufferi(buffer->getBufferID(), AL_CHANNELS, (ALint *)&channels);
			alGetBufferi(buffer->getBufferID(), AL_BITS, (ALint *)&bps);

			double length = ((double)bSize)/((double)(freq*channels*(bps/8)));

			//clamp time t to [0,length], and calculate position in buffer
			//of the timepoint which should be set
			double clampedTime = min(max(t,0.0),length);
			double percentage = 0;
			if(length > 0) {
				percentage = clampedTime / length;
			}
			ALint position = (int)(percentage * ((double)bSize));

			//set position and start from there
			alSourceRewind(audio);
			alSourcei(audio, AL_BYTE_OFFSET, position);
		}
		alSourcePlay(audio);
	}

	void AudioObject::stop() {
		if(!loaded) {
			return;
		}
		if(locked) {
			double t = timedLock.elapsed();
			if(t > lockTime) {
				locked = false;
			} else {
				return;
			}
		}
		playing = false;
		alSourceStop(audio);
	}

	void AudioObject::pause() {
		if(!loaded) {
			return;
		}
		if(locked) {
			double t = timedLock.elapsed();
			if(t > lockTime) {
				locked = false;
			} else {
				return;
			}
		}
		playing = false;
		alSourcePause(audio);
	}

	bool AudioObject::isPlaying() {
		if(!loaded) {
			return false;
		}
		ALint state;
		alGetSourceiv(audio, AL_SOURCE_STATE, &state);
		if(state == AL_PLAYING) {
			playing = true;
			return true;
		}
		playing = false;
		return false;
	}

	double AudioObject::getLength() const {
		if(!loaded || buffer == 0 || !buffer->isLoaded()) {
			return 0;
		}
		int bSize = 0;
		int freq = 0;
		int bitsPerSample = 0;
		int channels = 0;
		int bps = 0;
		alGetBufferi(buffer->getBufferID(), AL_SIZE, (ALint *)&bSize);
		alGetBufferi(buffer->getBufferID(), AL_FREQUENCY, (ALint *)&freq);
		alGetBufferi(buffer->getBufferID(), AL_CHANNELS, (ALint *)&channels);
		alGetBufferi(buffer->getBufferID(), AL_BITS, (ALint *)&bps);

		return ((double)bSize)/((double)(freq*channels*(bps/8)));
	}

	double AudioObject::getPlaybackPosition() const {
		if(!loaded || buffer == 0 || !buffer->isLoaded()) {
			return 0;
		}
		double length = getLength();
		if(length == 0) {
			return 0;
		}
		int position = 0;
		int bSize = 0;
		alGetSourceiv(audio, AL_BYTE_OFFSET, (ALint *)&position);
		alGetBufferi(buffer->getBufferID(), AL_SIZE, (ALint *)&bSize);
		
		return length * ((double)position)/((double)bSize);
	}

	void AudioObject::lock(double t) {
		if(locked) {
			return;
		}
		this->lockTime = t;
		timedLock.restart();
		locked = true;
	}

	void AudioObject::unlock() {
		locked = false;
	}

	void AudioObject::update() {
		if(!loaded || !isPlaying()) {
			return;
		}
		if(position != 0) {
			alSourcefv(audio,AL_POSITION,position->elements);
		} else {
			float pos[] = {0,0,0};
			alSourcefv(audio,AL_POSITION,pos);
		}
		if(referenceRadius != 0 && maxRadius != 0 && distanceExponent != 0) {
			alSourcef(audio, AL_REFERENCE_DISTANCE, referenceRadius->value);
			alSourcef(audio,AL_ROLLOFF_FACTOR , distanceExponent->value);
			alSourcef(audio, AL_MAX_DISTANCE, maxRadius->value);
		} else {
			alSourcef(audio, AL_REFERENCE_DISTANCE, 1);
			alSourcef(audio,AL_ROLLOFF_FACTOR , 0);
			alSourcef(audio, AL_MAX_DISTANCE, 1);
		}
		if(velocity != 0) {
			alSourcefv(audio,AL_VELOCITY,velocity->elements);
		} else {
			float vel[] = {0,0,0};
			alSourcefv(audio,AL_VELOCITY,vel);
		}
		if(volume != 0) {
			alSourcef(audio,AL_GAIN,volume->value);
		} else {
			alSourcef(audio,AL_GAIN,1);
		}
		if(pitch != 0) {
			alSourcef(audio,AL_PITCH,pitch->value);
		} else {
			alSourcef(audio,AL_PITCH,1);
		}
	}

}

#endif