#ifndef _SX_ENGINE_CORE_RENDERTARGET_CPP_
#define _SX_ENGINE_CORE_RENDERTARGET_CPP_

/**
 * framebuffer class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <GL/glew.h>
#include <cmath>
#include <algorithm>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
using namespace boost;
using namespace std;

namespace sx {

	unordered_map<int,int> RenderTarget::depthTest2GL = RenderTarget::initDepthTest2GL();

	unordered_map<int,int> RenderTarget::blendFactor2GL = RenderTarget::initBlendFactor2GL();

	unordered_map<int,int> RenderTarget::initDepthTest2GL() {
		unordered_map<int,int> m;

		m[ACCEPT_NEVER] = GL_NEVER;
		m[ACCEPT_ALWAYS] = GL_ALWAYS;
		m[ACCEPT_LESS] = GL_LESS;
		m[ACCEPT_LESS_EQUAL] = GL_LEQUAL;
		m[ACCEPT_GREATER] = GL_GREATER;
		m[ACCEPT_GREATER_EQUAL] = GL_GEQUAL;
		m[ACCEPT_EQUAL] = GL_EQUAL;
		m[ACCEPT_NOT_EQUAL] = GL_NOTEQUAL;

		return m;
	}

	unordered_map<int,int> RenderTarget::initBlendFactor2GL() {
		unordered_map<int,int> m;

		m[ZERO] = GL_ZERO;
		m[ONE] = GL_ONE;
		m[FRAGMENTCOLOR] = GL_SRC_COLOR;
		m[FRAMEBUFFERCOLOR] = GL_DST_COLOR;
		m[FRAGMENTALPHA] = GL_SRC_ALPHA;
		m[FRAMEBUFFERALPHA] = GL_DST_ALPHA;
		m[ONE_MINUS_FRAGMENTCOLOR] = GL_ONE_MINUS_SRC_COLOR;
		m[ONE_MINUS_FRAMEBUFFERCOLOR] = GL_ONE_MINUS_DST_COLOR;
		m[ONE_MINUS_FRAGMENTALPHA] = GL_ONE_MINUS_SRC_ALPHA;
		m[ONE_MINUS_FRAMEBUFFERALPHA] = GL_ONE_MINUS_DST_ALPHA;

		return m;
	}

	RenderTarget::RenderTarget(const string &id) {
		this->id = id;
		char_separator<char> separator(".");
		tokenizer<char_separator<char>> tokens(id,separator);
		BOOST_FOREACH(const string &token,tokens) {
			//use last token as idToken
			idToken = token;
		}

		loaded = false;
		newRenderToDisplay = renderToDisplay = false;
		newWidth = -1;
		newHeight = -1;
		width = -1;
		height = -1;
	}
		
	RenderTarget::~RenderTarget() {
		unload();
	}

	void RenderTarget::clearTarget() {
		glClearColor(0,0,0,255);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void RenderTarget::clearDepthBuffer() {
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	void RenderTarget::clearColorBuffer() {
		glClearColor(0,0,0,255);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void RenderTarget::applyDepthTest(DepthTest test) {
		glEnable(GL_DEPTH_TEST);
		unordered_map<int,int>::iterator iter = depthTest2GL.find(test);
		if(iter != depthTest2GL.end()) {
			glDepthFunc((*iter).second);
		} else {
			//invalid parameter, hence reset to default test
			glDepthFunc(GL_LESS);
		}
	}

	/**
	* Determines, if rendering into the framebuffer
	* has an effect on the depthbuffer or not.
	* @param write true iff rendering into the framebuffer alters the depthbuffer
	*/
	void RenderTarget::applyWriteDepthBuffer(bool write) {
		glEnable(GL_DEPTH_TEST);
		if(write) {
			glDepthMask(GL_TRUE);
		} else {
			glDepthMask(GL_FALSE);
		}
	}

	/**
	* Assigns value factor to fragmentblendfactor.
	* For a detailed description of the blendfactors see
	* @see sx::BlendFactor
	*/
	void RenderTarget::applyBlendFactors(BlendFactor fragmentBlendFactor, BlendFactor targetBlendFactor) {
		if(fragmentBlendFactor == ONE && targetBlendFactor == ZERO) {
			//default values of blendfactors
			glBlendFunc(GL_ONE,GL_ZERO);
			glDisable(GL_BLEND);
		} else {
			
			unordered_map<int,int>::iterator fragmentIter = blendFactor2GL.find(fragmentBlendFactor);
			unordered_map<int,int>::iterator targetIter = blendFactor2GL.find(targetBlendFactor);
			if(fragmentIter != blendFactor2GL.end() && targetIter != blendFactor2GL.end()) {
				glEnable(GL_BLEND);
				glBlendFunc((*fragmentIter).second,(*targetIter).second);
			} else {
				//invalid parameters, hence reset blendfactors to default
				glBlendFunc(GL_ONE,GL_ZERO);
				glDisable(GL_BLEND);
			}
		}
	}

	void RenderTarget::setViewport(int x, int y, int width, int height) {
		glViewport(x,y,width,height);
	}

	void RenderTarget::setViewport() {
		if(loaded) {
			glViewport(0,0,width,height);
		}
	}

	void RenderTarget::setViewport(float xFactor, float yFactor, float widthFactor, float heightFactor) {
		if(loaded) {
			glViewport((int)floor(width * xFactor), (int)floor(height * yFactor), (int)floor(width * widthFactor), (int)floor(height * heightFactor));
		}
	}

	void RenderTarget::unload() {
		if(loaded && !renderToDisplay) {
			glDeleteRenderbuffers(1,&renderbuffer);
			glDeleteFramebuffers(1,&target);
			loaded = false;
		}
	}

	void RenderTarget::load() {
		if(newWidth <= 0 || newHeight <= 0) {
			//no changes specified, hence don't
			//do anything
			return;
		}
		unload();
		width = newWidth;
		height = newHeight;
		if(newRenderToDisplay) {
			//rendering to display doesn't require a framebuffer
			renderToDisplay = true;
			loaded = true;
			return;
		}
		//generate framebuffer in vram
		glGenFramebuffers(1,(GLuint *)&target);
		glBindFramebuffer(GL_FRAMEBUFFER,target);
		glGenRenderbuffers(1,(GLuint *)&renderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER,renderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH_COMPONENT24,width,height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,renderbuffer);
		glBindFramebuffer(GL_FRAMEBUFFER,0);
		loaded = true;
		//clear resource requests
		newWidth = newHeight = -1;
	}

	bool RenderTarget::isLoaded() const {
		return loaded;
	}

	void RenderTarget::beginRender(Shader &shader, vector<Texture *> &textures, const string &resourceID) {
		if(renderToDisplay) {
			return;
		}
		glPushAttrib(GL_VIEWPORT_BIT | GL_COLOR_BUFFER_BIT);
		if(!loaded || !shader.isLoaded()) {
			//texture or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		glBindFramebuffer(GL_FRAMEBUFFER,target);
		vector<unsigned int> texLocations;
		for(vector<Texture *>::iterator iter = textures.begin() ; iter != textures.end() ; iter++) {
			if((*iter)->isLoaded()) {
				unsigned int tex = (*iter)->getTextureID();
				const string &texName = (*iter)->getUniformName(resourceID);
				int loc = shader.getTargetLocation(texName);
				if(loc >= 0) {
					//this target does exist
					//in the shader code, use it
					glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0 + (unsigned int)loc,GL_TEXTURE_2D,tex,0);
					texLocations.push_back((unsigned int)loc);
				}
			}
		}
		//drawbuffers have to be in the same order as
		//the appearances of the frag locations in the shader
		sort(texLocations.begin(),texLocations.end());

		GLenum *drawBuffers = new GLenum[texLocations.size()];
		unsigned int index = 0;
		for(vector<unsigned int>::iterator iter = texLocations.begin() ; iter != texLocations.end() ; iter++) {
			drawBuffers[index] = GL_COLOR_ATTACHMENT0 + (*iter);
			index++;
		}
		glDrawBuffers(texLocations.size(),drawBuffers);
		delete drawBuffers;
	}

	void RenderTarget::beginRender(Shader &shader, vector<Volume *> &textures, unsigned int layer, const string &resourceID) {
		if(renderToDisplay) {
			return;
		}
		glPushAttrib(GL_VIEWPORT_BIT | GL_COLOR_BUFFER_BIT);
		if(!loaded || !shader.isLoaded()) {
			//texture or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		glBindFramebuffer(GL_FRAMEBUFFER,target);
		vector<unsigned int> texLocations;
		for(vector<Volume *>::iterator iter = textures.begin() ; iter != textures.end() ; iter++) {
			if((*iter)->isLoaded()) {
				unsigned int tex = (*iter)->getTextureID();
				const string &texName = (*iter)->getUniformName(resourceID);
				int loc = shader.getTargetLocation(texName);
				if(loc >= 0) {
					//this target does exist
					//in the shader code, use it
					glFramebufferTexture3D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0 + (unsigned int)loc,GL_TEXTURE_3D,tex,0,layer);
					texLocations.push_back((unsigned int)loc);
				}
			}
		}
		//drawbuffers have to be in the same order as
		//the appearances of the frag locations in the shader
		sort(texLocations.begin(),texLocations.end());

		GLenum *drawBuffers = new GLenum[texLocations.size()];
		unsigned int index = 0;
		for(vector<unsigned int>::iterator iter = texLocations.begin() ; iter != texLocations.end() ; iter++) {
			drawBuffers[index] = GL_COLOR_ATTACHMENT0 + (*iter);
			index++;
		}
		glDrawBuffers(texLocations.size(),drawBuffers);
		delete drawBuffers;
	}

	void RenderTarget::endRender() {
		if(renderToDisplay) {
			return;
		}
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glPopAttrib();
		glBindFramebuffer(GL_FRAMEBUFFER,0);
	}

	void RenderTarget::setRenderToDisplay(bool renderToDisplay) {
		newRenderToDisplay = renderToDisplay;
	}

	bool RenderTarget::isRenderingToDisplay() const {
		return renderToDisplay;
	}

	void RenderTarget::setWidth(int width) {
		if(renderToDisplay) {
			this->width = width;
		}
		newWidth = width;
	}

	int RenderTarget::getWidth() const {
		return width;
	}

	void RenderTarget::setHeight(int height) {
		if(renderToDisplay) {
			this->height = height;
		}
		newHeight = height;
	}

	int RenderTarget::getHeight() const {
		return height;
	}

}

#endif