#ifndef _SX_ENGINE_CORE_EFFECT_CPP_
#define _SX_ENGINE_CORE_EFFECT_CPP_

/**
 * Effect class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <boost/foreach.hpp>
using namespace boost;

namespace sx {

	Effect::Effect(const string &id) {
		this->id = id;
		visible = true;
	}

	Effect::~Effect() {
	}

	void Effect::load() {
	}

	bool Effect::isLoaded() const {
		return true;
	}

	void Effect::setVisible(bool visible) {
		this->visible = visible;
	}

	bool Effect::isVisible() const {
		return visible;
	}

	vector<EffectCommand> &Effect::getObjects() {
		return objects;
	}

	void Effect::render() {
		if(!visible) {
			//only visible effects are rendered
			return;
		}
		BOOST_FOREACH(EffectCommand &obj , objects) {
			obj.object->execute(obj.input);
		}
	}

}

#endif