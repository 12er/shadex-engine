#ifndef _SX_ENGINE_CORE_AUDIOBUFFER_CPP_
#define _SX_ENGINE_CORE_AUDIOBUFFER_CPP_

/**
 * Audio Buffer class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <fstream>
#include <sstream>
using namespace std;

namespace sx {

	AudioBuffer::AudioBuffer(const string &id) {
		this->id = id;
		this->loaded = false;
	}

	AudioBuffer::~AudioBuffer() {
		unload();
	}

	void AudioBuffer::load() {
		if(newPath.size() == 0) {
			return;
		}
		if(!AudioObject::isInitialized()) {
			try {
				AudioObject::initAudio();
			} catch(Exception &) {
				loaded = false;
				return;
			}
			if(!AudioObject::isInitialized()) {
				loaded = false;
				return;
			}
		}
		unload();

		//load resources from file
		//specified by newPath
		if(newPath.size() < 4) {
			//only .wav (wave) files supported currently
			newPath = "";
			return;
		}
		string extension = newPath.substr(newPath.size()-4);
		if(extension.compare(".wav") == 0) {
			//read data from file
			char type[5];
			type[4] = 0;
			unsigned int size;
			unsigned int chunkSize;
			short formatType;
			short channels;
			unsigned int sampleRate;
			unsigned int avgBytesPerSec;
			short bytesPerSample;
			short bitsPerSample;
			unsigned int dataSize;

			ifstream file(newPath.c_str(),ios::in|ios::binary);
			if(!file.is_open()) {
				//cannot finish loading
				//when file isn't opened
				file.close();
				newPath = "";
				return;
			}
			file.read(type,sizeof(char)*4);
			if(string(type).compare("RIFF") != 0) {
				//or wrong type
				file.close();
				newPath = "";
				return;
			}
			file.read((char *)&size,sizeof(unsigned int));
			file.read(type,sizeof(char)*4);
			if(string(type).compare("WAVE") != 0) {
				//wrong type
				file.close();
				newPath = "";
				return;
			}
			file.read(type,sizeof(char)*4);
			if(string(type).compare("fmt ") != 0) {
				//wrong type
				file.close();
				newPath = "";
				return;
			}
			file.read((char *)&chunkSize,sizeof(unsigned int));
			file.read((char *)&formatType,sizeof(short));
			file.read((char *)&channels,sizeof(short));
			file.read((char *)&sampleRate,sizeof(unsigned int));
			file.read((char *)&avgBytesPerSec,sizeof(unsigned int));
			file.read((char *)&bytesPerSample,sizeof(short));
			file.read((char *)&bitsPerSample,sizeof(short));
			file.read(type,sizeof(char)*4);
			if(string(type).compare("data") != 0) {
				//search for word data
				string typeString;
				bool match = false;
				do {
					char nextChar = 0;
					file.read(&nextChar,sizeof(char));
					typeString.push_back(nextChar);
					if(typeString.size()>4) {
						typeString = typeString.substr(1,4);
					}
					match = typeString.compare("data") == 0;
				} while(!file.eof() && !file.bad() && !file.fail() && !match);
			} 
			file.read((char *)&dataSize,sizeof(unsigned int));
			if(file.eof() || file.bad() || file.fail()) {
				//errors occured during reading the file
				file.close();
				newPath = "";
				return;
			}
			unsigned char *content = new unsigned char[dataSize];
			file.read((char *)content,sizeof(unsigned char)*dataSize);
			if(file.eof() || file.bad() || file.fail()) {
				//errors occured during reading the file
				file.close();
				delete [] content;
				newPath = "";
				return;
			}
			file.close();

			alGenBuffers(1,(ALuint *)&buffer);
			ALuint format = 0;
			ALuint frequency = sampleRate;

			if(bitsPerSample == 8) {
				if(channels == 1) {
					format = AL_FORMAT_MONO8;
				} else if(channels == 2) {
					format = AL_FORMAT_STEREO8;
				}
			} else if(bitsPerSample == 16) {
				if(channels == 1) {
					format = AL_FORMAT_MONO16;
				} else if(channels == 2) {
					format = AL_FORMAT_STEREO16;
				}
			}
			alBufferData((ALuint)buffer,format,content,dataSize,frequency);
			delete [] content;

		
		} else {
			//only .wav (wave) files supported currently
			newPath = "";
			return;
		}

		newPath = "";
		loaded = true;
	}

	void AudioBuffer::unload() {
		if(loaded) {
			alDeleteBuffers(1,&buffer);
			loaded = false;
		}
	}

	bool AudioBuffer::isLoaded() const {
		return loaded;
	}

	void AudioBuffer::setPath(const string &path) {
		this->newPath = path;
	}

	unsigned int AudioBuffer::getBufferID() const {
		if(!loaded) {
			return 0;
		}
		return (unsigned int)buffer;
	}

}

#endif