#ifndef _SX_ENGINE_CORE_BUFFEREDMESH_CPP_
#define _SX_ENGINE_CORE_BUFFEREDMESH_CPP_

/**
 * GPU mesh class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <list>
#include <GL/glew.h>
#include <boost/date_time/posix_time/posix_time.hpp>
using boost::posix_time::to_simple_string;
using boost::posix_time::ptime;
using namespace std;

namespace sx {

	BufferedMesh::BufferedMesh(const string &id) {
		this->id = id;
		loaded = false;
		vertexCount = 0;
		newMaxVertexCount = maxVertexCount = 0;
		faceSize = 0;
	}

	BufferedMesh::~BufferedMesh() {
		unload();
	}

	void BufferedMesh::unload() {
		if(loaded) {
			SXout(LogMarkup("BufferedMesh::unload"));
			SXout((unsigned int)vbos.size());
			for(map<string,VertexBuffer *>::iterator iter = vbos.begin() ; iter != vbos.end() ; iter++) {
				VertexBuffer *buffer = (*iter).second;
				glDeleteBuffers((GLuint)1 , (const GLuint *)&buffer->bufferID);
				delete buffer;
			}
			vbos.clear();
			outputVBOs.clear();
			glDeleteVertexArrays((GLuint)1, (const GLuint *)&vao);
			resetUniforms();
			glDeleteQueries((GLuint)1, (const GLuint *)&query_primitives_written);
			glDeleteQueries((GLuint)1, (const GLuint *)&query_primitives_generated);
			maxVertexCount = 0;
			bones.clear();
			skeletonMatrix.identity();
			loaded = false;
		}
	}

	void BufferedMesh::load() {
		if(newBuffers.size() == 0 && newMeshes.size() == 0) {
			//no changes specified, hence don't
			//do anything
			return;
		}
		//remove already loaded resources
		//before recreation
		unload();
		vertexCount = 0;
		maxVertexCount = 0;
		//location of the next buffer created
		unsigned int location = 0;
		//create vertex array object
		//from here on unload must be
		//called, if the mesh can't be
		//initialized
		glGenVertexArrays(1,(GLuint *)&vao);
		glBindVertexArray(vao);
		//attrib buffers
		//loaded from files
		vector<string>::iterator meshIter = newMeshes.begin();
		vector<bool>::iterator discardIter = newDiscardStandard.begin();
		while(meshIter != newMeshes.end() && discardIter != newDiscardStandard.end()) {
			XMesh *data = 0;
			try {
				string fileName = (*meshIter);
				if(fileName.size() < 4) {
					throw Exception("file must have extension .dae or .ply");
				}
				string extension = fileName.substr(fileName.size()-4);
				if(extension.compare(".dae") == 0) {
					//the file has Collada file format
					XTag *collada = 0;
					try {
						collada = parseXMLFile(fileName);
						try {
							pair<vector<Bone>,Matrix> skeleton = parseColladaSkeleton(*collada);
							bones = skeleton.first;
							skeletonMatrix = skeleton.second;
						} catch(Exception &) {
						}
						data = parseColladaData(*collada);
						delete collada;
					} catch(Exception &e) {
						bones.clear();
						skeletonMatrix.identity();
						if(collada != 0) {
							delete collada;
						}
						throw e;
					}
				} else if(extension.compare(".ply") == 0) {
					//the file has polygon file format
					data = parsePLYFile(fileName);
				} else {
					throw Exception("file must have extension .dae or .ply");
				}
				if(vertexCount == 0) {
					//first buffer to be read
					//, hence it determines the faceSize
					newFaceSize = faceSize = data->faceSize;
				} else if(faceSize != data->faceSize) {
					//face sizes of buffers inconsistent
					throw Exception("inconsistent face size");
				}
				for(map<string,XBuffer *>::iterator iter = data->buffers.begin() ; iter != data->buffers.end() ; iter++) {
					//add buffer
					XBuffer *buffer = (*iter).second;
					unsigned int attrSize = buffer->attributeSize;
					string name = buffer->name;
					if((*discardIter) && 
						(name.compare("vertices") == 0 || name.compare("normals") == 0
						|| name.compare("texcoords") == 0 || name.compare("colors") == 0)
						) {
						//discard standard buffer
						continue;
					}
					map<string,VertexBuffer *>::iterator otherVBO = vbos.find(name);
					if(otherVBO != vbos.end()
						|| (vertexCount != 0 && buffer->vertexAttributes.size() / attrSize != vertexCount)
						) {
						//number of vertex attributes must
						//be the same for each buffer
						throw Exception("inconsisten or non-unique vbo");
					}
					//vbo consistent, create it
					if(vertexCount == 0) {
						vertexCount = buffer->vertexAttributes.size() / attrSize;
					}
					VertexBuffer *vbo = new VertexBuffer();
					vbo->attributeSize = attrSize;
					vbo->location = location;

					unsigned int capacity = vertexCount;
					if(vertexCount < newMaxVertexCount) {
						//the size of the buffer is larger
						//than the amount of vertices in the dataset
						//hence capacity is not equal to vertexCount
						capacity = newMaxVertexCount;
					}
					float *content = new float[capacity * attrSize];
					unsigned int index = 0;
					vector<double>::iterator bIter = buffer->vertexAttributes.begin();
					while(bIter != buffer->vertexAttributes.end()) {
						content[index] = (float)(*bIter);
						bIter++;
						index++;
					}
					while(index < capacity*attrSize) {
						//fill the part of the buffer
						//not occupied by the dataset with
						//zeros
						content[index] = 0.0f;
						index++;
					}

					glGenBuffers(1,(GLuint *)&vbo->bufferID);
					glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
					glBufferData(GL_ARRAY_BUFFER,capacity * attrSize * sizeof(GLfloat),(GLfloat *)content,GL_STATIC_DRAW);
					glVertexAttribPointer((GLuint)location,attrSize,GL_FLOAT,GL_FALSE,0,0);

					delete content;
					//store vbo
					loaded = true;
					vbos.insert(pair<string,VertexBuffer *>(name,vbo));
					string outputName = name;
					map<string,string>::iterator outputIter = newOutputIDs.find(name);
					if(outputIter != newOutputIDs.end()) {
						outputName = (*outputIter).second;
					}
					map<string,VertexBuffer *>::iterator outputVIter = outputVBOs.find(outputName);
					if(outputVIter != outputVBOs.end()) {
						//the same outputname can't be used twice
						throw Exception("output names of vbo must be unique");
					}
					vbo->ID = name;
					vbo->outputID = outputName;
					outputVBOs.insert(pair<string,VertexBuffer *>(outputName,vbo));
				}
				delete data;
			} catch(Exception &) {
				//error, unload loaded resources
				//clear previous resource requests
				delete data;
				unload();
				glBindVertexArray(0);
				glBindBuffer(GL_ARRAY_BUFFER,0);
				newBuffers.clear();
				newAttributeIDs.clear();
				newAttributeSizes.clear();
				newMeshes.clear();
				newDiscardStandard.clear();
				newOutputIDs.clear();
				newMaxVertexCount = 0;
				return;
			}
			meshIter++;
			discardIter++;
			location++;
		}
		vector<vector<float>>::iterator bufferIter = newBuffers.begin();
		vector<string>::iterator idIter = newAttributeIDs.begin();
		vector<unsigned int>::iterator attrIter = newAttributeSizes.begin();
		while(bufferIter != newBuffers.end() && idIter != newAttributeIDs.end() && attrIter != newAttributeSizes.end()) {
			vector<float> &currBuffer = (*bufferIter);
			try {
				if(vertexCount == 0) {
					//first buffer to be loaded
					//, hence newFaceSize is the face size
					if(newFaceSize == 0 || newFaceSize > 3) {
						//faceSize must have a value in {1,2,3}
						throw Exception("impossible face size");
					}
					faceSize = newFaceSize;
				}
				unsigned int attrSize = (*attrIter);
				string name = (*idIter);
				map<string,VertexBuffer *>::iterator otherVBO = vbos.find(name);
				if(vertexCount != 0 && currBuffer.size() / attrSize < vertexCount) {
					//buffer is made of less vertex attributes than the other buffers
					//append zeros to the buffer such that it has the same amount
					//of vertex attributes as the already loaded buffers
					int additionalVertexCount = vertexCount * attrSize - currBuffer.size();
					vector<float> additionalData(additionalVertexCount);
					currBuffer.insert(currBuffer.end(),additionalData.begin(),additionalData.end());
				}
				if(otherVBO != vbos.end()
					|| attrSize == 0
					|| currBuffer.size() % attrSize != 0
					|| (currBuffer.size() / attrSize) % faceSize != 0
					|| (vertexCount != 0 && currBuffer.size() / attrSize != vertexCount)
					) {
					//number of vertex attributes must
					//be the same for each buffer
					//, and it must be divisible by attrSize and attrSize * faceSize
					//, and attrSize must be greater than zero
					throw Exception("inconsisten or non-unique vbo");
				}
				//vbo consistent, create it
				if(vertexCount == 0) {
					vertexCount = currBuffer.size() / attrSize;
				}
				VertexBuffer *vbo = new VertexBuffer();
				vbo->attributeSize = attrSize;
				vbo->location = location;

				unsigned int capacity = vertexCount;
				if(vertexCount < newMaxVertexCount) {
					//the size of the buffer is larger
					//than the amount of vertices in the dataset
					//hence capacity is not equal to vertexCount
					capacity = newMaxVertexCount;
				}
				float *content = new float[capacity * attrSize];
				unsigned int index = 0;
				vector<float>::iterator bIter = currBuffer.begin();
				while(bIter != currBuffer.end()) {
					content[index] = (*bIter);
					bIter++;
					index++;
				}
				while(index < capacity*attrSize) {
					//fill the part of the buffer
					//not occupied by the dataset with
					//zeros
					content[index] = 0.0f;
					index++;
				}

				glGenBuffers(1,(GLuint *)&vbo->bufferID);
				glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
				glBufferData(GL_ARRAY_BUFFER,capacity * attrSize * sizeof(GLfloat),(GLfloat *)content,GL_STATIC_DRAW);
				glVertexAttribPointer((GLuint)location,attrSize,GL_FLOAT,GL_FALSE,0,0);

				delete content;
				//store vbo
				loaded = true;
				vbos.insert(pair<string,VertexBuffer *>(name,vbo));
				string outputName = name;
				map<string,string>::iterator outputIter = newOutputIDs.find(name);
				if(outputIter != newOutputIDs.end()) {
					outputName = (*outputIter).second;
				}
				map<string,VertexBuffer *>::iterator outputVIter = outputVBOs.find(outputName);
				if(outputVIter != outputVBOs.end()) {
					//the same outputname can't be used twice
					throw Exception("output names of vbo must be unique");
				}
				vbo->ID = name;
				vbo->outputID = outputName;
				outputVBOs.insert(pair<string,VertexBuffer *>(outputName,vbo));
			} catch(Exception &) {
				//error, unload loaded resources
				//clear previous resource requests
				unload();
				glBindVertexArray(0);
				glBindBuffer(GL_ARRAY_BUFFER,0);
				newBuffers.clear();
				newAttributeIDs.clear();
				newAttributeSizes.clear();
				newMeshes.clear();
				newDiscardStandard.clear();
				newOutputIDs.clear();
				newMaxVertexCount = 0;
				return;
			}

			bufferIter++;
			idIter++;
			attrIter++;
			location++;
		}
		//successfully loaded buffers
		//now unbind vbos and vaos
		//and clear previous resource requests
		SXout(LogMarkup("BufferedMesh::load"));
		SXout((unsigned int)vbos.size());
		maxVertexCount = max(newMaxVertexCount,vertexCount);
		glGenQueries(1,&query_primitives_written);
		glGenQueries(1,&query_primitives_generated);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER,0);
		newBuffers.clear();
		newAttributeIDs.clear();
		newAttributeSizes.clear();
		newMeshes.clear();
		newDiscardStandard.clear();
	}

	bool BufferedMesh::isLoaded() const {
		return loaded;
	}

	void BufferedMesh::save(const string &filename) {
		if(!loaded) {
			//mesh has to be loaded
			stringstream errmsg;
			errmsg << "Error: BufferedMesh " << id << " can't be saved in file " << filename << ", because the mesh is not loaded" << endl;
			throw Exception(errmsg.str(),EX_NODATA);
		}
		if(vertexCount <= 0) {
			//mesh has to be nonempty
			stringstream errmsg;
			errmsg << "Error: BufferedMesh " << id << " can't be saved in file " << filename << ", because vertexCount is zero" << endl;
			throw Exception(errmsg.str(),EX_NODATA);
		}
		if(filename.size() < 2) {
			stringstream errmsg;
			errmsg << "Error: BufferedMesh " << id << " can't be saved in file " << filename << ", only .dae (Collada) and .c (SX resource) files are supported" << endl;
			throw Exception(errmsg.str(),EX_INIT);
		}
		string extension;
		if(filename.size() >= 4) {
			extension = filename.substr(filename.size()-4);
		}
		string smallextension = filename.substr(filename.size()-2);
		if(extension.compare(".dae") == 0) {
			//store file in Collada format
			Skeleton tempSkeleton(id);
			if(bones.size() != 0) {
				//control bones for uniqueness
				//and calculate inverse bind pose matrices
				tempSkeleton.addBones(*this);
				tempSkeleton.load();
				if(!tempSkeleton.isLoaded()) {
					//bones not consistent
					stringstream errmsg;
					errmsg << "Error: BufferedMesh " << id << " contains inconsistent bones. Bonenames might not be unique";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}

			//collect ordinary buffers and bonebuffers
			vector<VertexBuffer *> meshBuffers;
			vector<string> boneNames;
			vector<VertexBuffer *> boneBuffers;
			vector<double> invMatrices;
			map<string,VertexBuffer *> boneMap; //temporary map, used to examine if a bone has not been considered yet in the traversal below
			vector<Bone *> inorderTraversal; //each element != 0 is a push, each element = 0 is a pop in the inorder traversal
			bool hasPoints = false;

			for(map<string,VertexBuffer *>::iterator vIter = vbos.begin() ; vIter != vbos.end() ; vIter++) {
				const string &vID = (*vIter).first;
				VertexBuffer *vbo = (*vIter).second;
				bool doRethrow = false;
				//is it a bonebuffer?
				try {
					//it's a bonebuffer
					//check attributeSize, must be 1 for each bonebuffer
					if(vbo->attributeSize != 1) {
						try {
							//test if a bone with the same ID exists
							//if this is the case, an exception needs to be thrown
							tempSkeleton.getBone(vID);
							doRethrow = true;
						} catch(Exception &) {
						}
						stringstream errmsg;
						errmsg << "Error: found bonebuffer " << vbo->ID << " in BufferedMesh " << id << " having attributeSize = " << vbo->attributeSize << ", but the bonebuffers' attributeSizes are required to be 1";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
					const Bone &bone = tempSkeleton.getBone(vID);
					boneNames.push_back(vID);
					boneBuffers.push_back(vbo);
					boneMap[vID] = vbo;
					//push inverse bind pose matrix
					for(unsigned int row = 0 ; row < 4 ; row++) {
						for(unsigned int column = 0 ; column < 4 ; column++) {
							invMatrices.push_back((double)bone.inverseBindPoseMatrix[column*4 + row]);
						}
					}
				} catch(Exception &e) {
					if(doRethrow) {
						throw e;
					}
					//it's not a bonebuffer
					meshBuffers.push_back(vbo);
					if(vbo->ID.compare("vertices") == 0) {
						//this is the buffer containing the point data of the mesh
						//that buffer is required for a mesh in Collada format
						hasPoints = true;
						if(vbo->attributeSize != 3 && vbo->attributeSize != 4) {
							//wrong number of components for point data
							stringstream errmsg;
							errmsg << "Error: buffer \"vertices\" in BufferedMesh " << id << " is required to have three or four components per point";
							throw Exception(errmsg.str(),EX_AMBIGUOUS);
						}
					} else if(vbo->ID.compare("normals") == 0 && vbo->attributeSize != 3) {
						//wrong number of components for normal data
						stringstream errmsg;
						errmsg << "Error: buffer \"normals\" in BufferedMesh " << id << " is required to have three components per normal";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					} else if(vbo->ID.compare("texcoords") == 0 && vbo->attributeSize != 2) {
						//wrong number of components for texcoord data
						stringstream errmsg;
						errmsg << "Error: buffer \"texcoords\" in BuffereMesh " << id << " is required to have two components per texture coordinate";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
				}
			}
			if(!hasPoints) {
				//the point dataset required for Collada is missing
				stringstream errmsg;
				errmsg << "Error: BufferedMesh " << id << " does not contain a buffer called \"vertices\", but that buffer is required for meshes in Collada format";
				throw Exception(errmsg.str(),EX_INIT);
			}
			//calculate inorder traversal
			//complete boneMap
			//complete boneNames
			//complete invMatrices
			vector<vector<Bone>::iterator> iterStack;
			iterStack.push_back(bones.begin());
			vector<vector<Bone> *> arrayStack;
			arrayStack.push_back(&bones);
			while(iterStack.size() > 0) {
				vector<Bone>::iterator cIter = iterStack[iterStack.size()-1];
				vector<Bone> *cArray = arrayStack[arrayStack.size()-1];
				if(cIter != cArray->end()) {
					//inspect current element
					Bone &currBone = (*cIter);
					inorderTraversal.push_back(&currBone);
					map<string,VertexBuffer *>::iterator findIter = boneMap.find(currBone.ID);
					if(findIter == boneMap.end()) {
						//found a bone without a VertexBuffer
						boneMap[currBone.ID] = 0;
						boneNames.push_back(currBone.ID);
						//push matrix
						const Matrix &bMatrix = tempSkeleton.getBone(currBone.ID).inverseBindPoseMatrix; //tempSkeleton contains correct inverse bind bone matrices
						for(unsigned int row = 0 ; row < 4 ; row++) {
							for(unsigned int column = 0 ; column < 4 ; column++) {
								invMatrices.push_back((float)bMatrix[column*4 + row]);
							}
						}
					}
					//traverse children
					iterStack.push_back(currBone.bones.begin());
					arrayStack.push_back(&currBone.bones);

					//advance iterator
					iterStack[iterStack.size()-2]++;
				} else {
					//reached end of iterator
					iterStack.pop_back();
					arrayStack.pop_back();
					if(arrayStack.size() > 0) {
						//pop pushed tag
						//the last pop has no closing tag to it
						//hence ignore pop, if arrayStack.size() == 0
						inorderTraversal.push_back(0);
					}
				}
			}

			//acquiring data completed, now write into the file
			ptime time(second_clock::local_time());
			ofstream file;
			try {
				file.open(filename.c_str());

				file << 
					"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
					"<COLLADA xmlns=\"http://www.collada.org/2005/11/COLLADASchema\" version=\"1.4.1\">\n"
					"	<asset>\n"
					"		<contributor>\n"
					"			<author>ShadeX Engine User</author>\n"
					"			<authoring_tool>ShadeX Engine</authoring_tool>\n"
					"		</contributor>\n"
					"		<created>" << to_simple_string(time) << "</created>\n"
					"		<modified>" << to_simple_string(time) << "</modified>\n"
					"		<unit name=\"meter\" meter=\"1\"/>\n"
					"		<up_axis>Z_UP</up_axis>\n"
					"	</asset>\n"
					"	<library_geometries>\n"
					"		<geometry id=\"SX-mesh\" name=\"SX\">\n"
					"			<mesh>\n"
					;
				for(vector<VertexBuffer *>::iterator bIter = meshBuffers.begin() ; bIter != meshBuffers.end() ; bIter++) {
					//write buffers
					VertexBuffer *vbo = (*bIter);
					string bufferName = vbo->ID;
					bool isVertices = false;
					bool isNormals = false;
					bool isTexcoords = false;
					if(bufferName.compare("vertices") == 0) {
						bufferName = "positions";
						isVertices = true;
					} else if(bufferName.compare("normals") == 0) {
						isNormals = true;
					} else if(bufferName.compare("texcoords") == 0) {
						bufferName = "map-0";
						isTexcoords = true;
					}
					stringstream sbID;
					stringstream sArrayID;
					sbID << "SX-mesh-" << bufferName;
					sArrayID << "SX-mesh-" << bufferName << "-array";
					string bufferID = sbID.str();
					string arrayID = sArrayID.str();
					
					unsigned int vCount = vertexCount * vbo->attributeSize;
					if(isVertices && vbo->attributeSize == 4) {
						vCount = vertexCount * 3;
					}
					file <<
					"				<source id=\"" << bufferID << "\">\n"
					"					<float_array id=\"" << arrayID << "\" count = \"" << vCount << "\">"
					;
					const float *bufContent = vbo->unlockRead();
					if(isVertices && vbo->attributeSize == 4) {
						//vertexbuffer: only post first three components
						for(unsigned int i=0 ; i<vertexCount ; i++) {
							unsigned int addr = i*vbo->attributeSize;
							file << ((double)bufContent[addr]) << " "
								<< ((double)bufContent[addr+1]) << " "
								<< ((double)bufContent[addr+2]) << " "
								;
						}
					} else {
						for(unsigned int i=0 ; i<vertexCount ; i++) {
							unsigned int addr = i*vbo->attributeSize;
							for(unsigned int j=0 ; j<vbo->attributeSize ; j++) {
								file << ((double)bufContent[addr+j]) << " ";
							}
						}
					}
					vbo->lock();

					unsigned int stride = vbo->attributeSize;
					if(isVertices) {
						stride = 3;
					}
					file << "</float_array>\n";
					file <<
					"					<technique_common>\n"
					"						<accessor source=\"#" << arrayID << "\" count=\"" << vertexCount << "\" stride=\"" << stride << "\">\n"
					;
					if(isVertices) {
						file <<
						"							<param name=\"X\" type=\"float\"/>\n"
						"							<param name=\"Y\" type=\"float\"/>\n"
						"							<param name=\"Z\" type=\"float\"/>\n"
						;
					} else if(isNormals) {
						file <<
						"							<param name=\"X\" type=\"float\"/>\n"
						"							<param name=\"Y\" type=\"float\"/>\n"
						"							<param name=\"Z\" type=\"float\"/>\n"
						;
					} else if(isTexcoords) {
						file <<
						"							<param name=\"S\" type=\"float\"/>\n"
						"							<param name=\"T\" type=\"float\"/>\n"
						;
					} else if(vbo->attributeSize <= 4) {
						string nameArray[] = {"X","Y","Z","W"};
						for(unsigned int i=0 ; i<vbo->attributeSize ; i++) {
							file <<
							"							<param name=\"" << nameArray[i] << "\" type=\"float\"/>\n"
							;
						}
					} else {
						for(unsigned int i=0 ; i<vbo->attributeSize ; i++) {
							file <<
							"							<param name=\"X" << i << "\" type=\"float\"/>\n"
							;
						}
					}
					file <<
					"						</accessor>\n"
					"					</technique_common>\n"
					"				</source>\n"
					;
				}
				file <<
					"				<vertices id=\"SX-mesh-vertices\">\n"
					"					<input semantic=\"POSITION\" source=\"#SX-mesh-positions\"/>\n"
					"				</vertices>\n"
					"				<polylist material=\"Material-material\" count=\"" << (vertexCount / faceSize) << "\">\n"
				;
				unsigned int offset = 0;
				for(vector<VertexBuffer *>::iterator bIter = meshBuffers.begin() ; bIter != meshBuffers.end() ; bIter++) {
					//add input for each buffer
					VertexBuffer *vbo = (*bIter);
					string semantic;
					string buffername;
					if(vbo->ID.compare("vertices") == 0) {
						semantic = "VERTEX";
						buffername = "vertices";
					} else if(vbo->ID.compare("normals") == 0) {
						semantic = "NORMAL";
						buffername = "normals";
					} else if(vbo->ID.compare("texcoords") == 0) {
						semantic = "TEXCOORD";
						buffername = "map-0";
					} else {
						semantic = vbo->ID;
						buffername = semantic;
					}
					stringstream ssID;
					ssID << "SX-mesh-" << buffername;
					string sourceID = ssID.str();
					file <<
						"					<input semantic=\"" << semantic << "\" source=\"#" << sourceID << "\" offset=\"" << offset << "\"/>\n"
					;
					offset++;
				}
				file << 
				"					<vcount>";
				unsigned int faceCount = vertexCount / faceSize;
				for(unsigned int i=0 ; i<faceCount ; i++) {
					//print face sizes
					file << faceSize << " ";
				}
				file << 
					"</vcount>\n"
				"					<p>"
				;
				unsigned int bufferCount = offset;
				for(unsigned int i=0 ; i<vertexCount ; i++) {
					//print indices for each vertex to each buffer
					for(unsigned int j=0 ; j<bufferCount ; j++) {
						file << i << " ";
					}
				}
				file << 
					"</p>\n"
				"				</polylist>\n"
				"			</mesh>\n"
				"			<extra><technique profile=\"MAYA\"><double_sided>1</double_sided></technique></extra>\n"
				"		</geometry>\n"
				"	</library_geometries>\n"
				;

				if(bones.size() > 0 && tempSkeleton.isLoaded()) {
					//add skins
					file <<
						"	<library_controllers>\n"
						"		<controller id=\"Armature_SX-skin\" name=\"Armature\">\n"
						"			<skin source=\"#SX-mesh\">\n"
						"				<bind_shape_matrix>1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1</bind_shape_matrix>\n"
						"				<source id=\"Armature_SX-skin-joints\">\n"
						"					<Name_array id=\"Armature_SX-skin-joints-array\" count=\"" << boneNames.size() << "\">"
						;
					for(vector<string>::iterator nIter = boneNames.begin() ; nIter != boneNames.end() ; nIter++) {
						const string &bName = (*nIter);
						file << bName << " ";
					}
					file << "</Name_array>\n"
						"					<technique_common>\n"
						"						<accessor source=\"#Armature_SX-skin-joints-array\" count=\"" << boneNames.size() << "\" stride=\"1\">\n"
						"							<param name=\"JOINT\" type=\"name\"/>\n"
						"						</accessor>\n"
						"					</technique_common>\n"
						"				</source>\n"
						"				<source id=\"Armature_SX-skin-bind_poses\">\n"
						"					<float_array id=\"Armature_SX-skin-bind_poses-array\" count=\"" << invMatrices.size() << "\">"
						;
					for(vector<double>::iterator imIter = invMatrices.begin() ; imIter != invMatrices.end() ; imIter++) {
						double num = (*imIter);
						file << num << " ";
					}
					unsigned int weightsCount = boneBuffers.size() * vertexCount;
					file << "</float_array>\n"
						"					<technique_common>\n"
						"						<accessor source=\"#Armature_SX-skin-bind_poses-array\" count=\"" << boneNames.size() << "\" stride=\"16\">\n"
						"							<param name=\"TRANSFORM\" type=\"float4x4\"/>\n"
						"						</accessor>\n"
						"					</technique_common>\n"
						"				</source>\n"
						"				<source id=\"Armature_SX-skin-weights\">\n"
						"					<float_array id=\"Armature_SX-skin-weights-array\" count=\""
						;
					if(boneBuffers.size() > 0) {
						file << weightsCount;
					} else {
						//using one dummy weight
						file << "1";
					}
					file << "\">";
					if(boneBuffers.size() > 0) {
						for(vector<VertexBuffer *>::iterator vboIter = boneBuffers.begin() ; vboIter != boneBuffers.end() ; vboIter++) {
							//list all bone vertex buffers sequentially
							VertexBuffer *vbo = (*vboIter);
							const float *bufContent = vbo->unlockRead();
							for(unsigned int i=0 ; i<vertexCount ; i++) {
								file << bufContent[i] << " ";
							}
							vbo->lock();
						}
					} else {
						file << "0";
					}
					file << "</float_array>\n"
						"					<technique_common>\n"
						"						<accessor source=\"#Armature_SX-skin-weights-array\" count=\""
						;
					if(boneBuffers.size() > 0) {
						file << weightsCount;
					} else {
						//using one dummy weight
						file << "1";
					}
					file << "\" stride=\"1\">\n"
						"							<param name=\"WEIGHT\" type=\"float\"/>\n"
						"						</accessor>\n"
						"					</technique_common>\n"
						"				</source>\n"
						"				<joints>\n"
						"					<input semantic=\"JOINT\" source=\"#Armature_SX-skin-joints\"/>\n"
						"					<input semantic=\"INV_BIND_MATRIX\" source=\"#Armature_SX-skin-bind_poses\"/>\n"
						"				</joints>\n"
						"				<vertex_weights count=\"" << vertexCount << "\">\n"
						"					<input semantic=\"JOINT\" source=\"#Armature_SX-skin-joints\" offset=\"0\"/>\n"
						"					<input semantic=\"WEIGHT\" source=\"#Armature_SX-skin-weights\" offset=\"1\"/>\n"
						"					<vcount>"
						;
					if(boneBuffers.size() > 0) {
						unsigned int bufCount = boneBuffers.size();
						for(unsigned int i=0 ; i<vertexCount ; i++) {
							//number of indices per vertex
							file << bufCount << " ";
						}
					} else {
						for(unsigned int i=0 ; i<vertexCount ; i++) {
							//one dummy vertex weight
							file << 1 << " ";
						}
					}
					file << "</vcount>\n"
						"					<v>"
						;
					unsigned int bufCount = boneBuffers.size();
					for(unsigned int i=0 ; i<vertexCount ; i++) {
						//fill in indices
						unsigned int currIndex = i;
						if(bufCount > 0) {
							for(unsigned int boneIndex=0 ; boneIndex < bufCount ; boneIndex++) {
								//post boneindex, and then a weight index
								file << boneIndex << " " << currIndex << " ";
								//the next weight index is just vertexCount
								//elements away, as each buffer contains
								//vertexCount weights
								currIndex += vertexCount;
							}
						} else {
							//post dummy weight indices
							file << 0 << " " << 0 << " ";
						}
					}
					file << "</v>\n"
						"				</vertex_weights>\n"
						"			</skin>\n"
						"		</controller>\n"
						"	</library_controllers>\n"
						;
				}
				file <<
					"	<library_visual_scenes>\n"
					"		<visual_scene id=\"Scene\" name=\"Scene\">\n"
					;

				if(bones.size() > 0 && tempSkeleton.isLoaded()) {
					//add bones
					file <<
					"			<node id=\"Armature\" name=\"Armature\" type=\"NODE\">\n"
					"				<matrix sid=\"transform\">"
					;
					//post skeleton matrix
					for(unsigned int row=0 ; row<4 ; row++) {
						for(unsigned int column=0 ; column<4 ; column++) {
							file << (double)skeletonMatrix[column*4 + row] << " ";
						}
					}
					file << "</matrix>\n";
					//follow inorderTraversal to post bone hierachy
					unsigned int tabCount = 4;
					for(vector<Bone *>::iterator tIter = inorderTraversal.begin() ; tIter != inorderTraversal.end() ; tIter++) {
						Bone *bone = (*tIter);
						if(bone != 0) {
							//start node
							for(unsigned int i=0 ; i<tabCount ; i++) {
								//insert tabs
								file << "\t";
							}
							//names in bone nodes have . instead of the _ characters in the bone IDs
							string boneName = bone->ID;
							std::replace(boneName.begin(),boneName.end(),'_','.');
							file << "<node id=\"" << bone->ID << "\" name=\"" << boneName << "\" sid=\"" << bone->ID << "\" type=\"JOINT\">\n";
							tabCount++;
							for(unsigned int i=0 ; i<tabCount ; i++) {
								//insert tabs
								file << "\t";
							}
							//push bone transform
							file << "<matrix sid=\"transform\">";
							for(unsigned int row=0 ; row<4 ; row++) {
								for(unsigned int column=0 ; column<4 ; column++) {
									file << (double)bone->parentTransform[column*4 + row] << " "; 
								}
							}
							file << "</matrix>\n";
						} else {
							//finish node
							tabCount--;
							for(unsigned int i=0 ; i<tabCount ; i++) {
								//insert tabs
								file << "\t";
							}
							file << "</node>\n";
						}
					}
					//close bone hierachy
					file << "			</node>\n";
				}

				//post node containing geometry and skin url
				file << 
					"			<node id=\"SX\" name=\"SX\" type=\"NODE\">\n"
					"				<matrix sid=\"transform\">1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1</matrix>\n"
					;
				if(bones.size() > 0 && tempSkeleton.isLoaded()) {
					//mesh has bones
					file <<
					"				<instance_controller url=\"#Armature_SX-skin\">\n"
					;
					//post root bones
					for(vector<Bone>::iterator bIter = bones.begin() ; bIter != bones.end() ; bIter++) {
						Bone &bone = (*bIter);
						file << "					<skeleton>#" << bone.ID << "</skeleton>\n";
					}
					file <<
					"				</instance_controller>\n"
					;
				} else {
					//mesh has no bones
					file <<
					"				<instance_geometry url=\"#SX-mesh\"/>\n"
					;
				}
				file <<
					"			</node>\n"
					"		</visual_scene>\n"
					"	</library_visual_scenes>\n"
					"	<scene>\n"
					"		<instance_visual_scene url=\"#Scene\"/>\n"
					"	</scene>\n"
					"</COLLADA>"
					;

				//finish writing into file
				file.close();
			} catch(...) {
				try {
					file.close();
				} catch(...) {
				}
				stringstream errmsg;
				errmsg << "Error: writing BufferedMesh " << id << " into file " << filename << " didn't work";
				throw Exception(errmsg.str(),EX_IO);
			}
		} else if(smallextension.compare(".c") == 0) {
			//store mesh in sx resource file
			Skeleton tempSkeleton(id);
			if(bones.size() != 0) {
				//control bones for uniqueness
				//and calculate inverse bind pose matrices
				tempSkeleton.addBones(*this);
				tempSkeleton.load();
				if(!tempSkeleton.isLoaded()) {
					//bones not consistent
					stringstream errmsg;
					errmsg << "Error: BufferedMesh " << id << " contains inconsistent bones. Bonenames might not be unique";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}

			//collect ordinary buffers and bonebuffers
			vector<Bone *> inorderTraversal; //each element != 0 is a push, each element = 0 is a pop in the inorder traversal

			//calculate inorder traversal
			vector<vector<Bone>::iterator> iterStack;
			iterStack.push_back(bones.begin());
			vector<vector<Bone> *> arrayStack;
			arrayStack.push_back(&bones);
			while(iterStack.size() > 0) {
				vector<Bone>::iterator cIter = iterStack[iterStack.size()-1];
				vector<Bone> *cArray = arrayStack[arrayStack.size()-1];
				if(cIter != cArray->end()) {
					//inspect current element
					Bone &currBone = (*cIter);
					inorderTraversal.push_back(&currBone);
					
					//traverse children
					iterStack.push_back(currBone.bones.begin());
					arrayStack.push_back(&currBone.bones);

					//advance iterator
					iterStack[iterStack.size()-2]++;
				} else {
					//reached end of iterator
					iterStack.pop_back();
					arrayStack.pop_back();
					if(arrayStack.size() > 0) {
						//pop pushed tag
						//the last pop has no closing tag to it
						//hence ignore pop, if arrayStack.size() == 0
						inorderTraversal.push_back(0);
					}
				}
			}

			//acquiring data completed, now write into the file
			ptime time(second_clock::local_time());
			ofstream file;
			try {
				file.open(filename.c_str());

				file <<
					"(sx) {\n"
					"\n"
					"	(mesh) {\n"
					"		id = \"" << id << "\";\n"
					"		faceSize = " << faceSize << ";\n"
					"		maxVertexCount = " << maxVertexCount << ";\n"
					;

				for(map<string,VertexBuffer *>::iterator vboIter = vbos.begin() ; vboIter != vbos.end() ; vboIter++) {
					VertexBuffer *vbo = (*vboIter).second;
					file <<
					"		(attribute) {\n"
					"			name = \"" << vbo->ID << "\";\n"
					;
					if(vbo->ID.compare(vbo->outputID) != 0) {
						//outputID != ID
						file << "			outputName = \"" << vbo->outputID << "\";\n";
					}
					file <<
					"			attributeSize = " << vbo->attributeSize << ";\n"
					"			(data) {\n"
					"				\""
					;
					const float *dSet = vbo->unlockRead();
					unsigned int dSize = vertexCount * vbo->attributeSize;
					for(unsigned int i=0 ; i<dSize ; i++) {
						file << dSet[i] << " ";
					}
					vbo->lock();
					file <<
					"\"\n"
					"			}\n"
					"		}\n"
					;
				}
				file <<
					"	}\n"
					;

				if(tempSkeleton.isLoaded()) {
					//mesh has a bone hierachy attached to it
					//save bonehierachy as skeleton
					file <<
						"\n"
						"	(skeleton) {\n"
						"		id = \"" << id << ".skeleton\";\n"
						"		(skeletonMatrix) {\n"
						"			\""
						;
					for(unsigned int row=0 ; row<4 ; row++) {
						for(unsigned int column=0 ; column<4 ; column++) {
							file << tempSkeleton.getSkeletonMatrix()[column*4 + row] << " ";
						}
					}
					file <<
						"\"\n"
						"		}\n"
						;
					unsigned int tabCount = 2;
					for(vector<Bone *>::iterator travIter = inorderTraversal.begin() ; travIter != inorderTraversal.end() ; travIter++) {
						Bone *bone = (*travIter);
						if(bone != 0) {
							//open new bone
							for(unsigned int i=0 ; i<tabCount ; i++) {
								file << "\t";
							}
							file << "(bone) {\n";
							tabCount++;
							for(unsigned int i=0 ; i<tabCount ; i++) {
								file << "\t";
							}
							file << "id = \"" << bone->ID << "\";\n";
							for(unsigned int i=0 ; i<tabCount ; i++) {
								file << "\t";
							}
							file << "(parentTransform) {\n";
							for(unsigned int i=0 ; i<tabCount+1 ; i++) {
								file << "\t";
							}
							file << "\"";
							for(unsigned int row=0 ; row<4 ; row++) {
								for(unsigned int column=0 ; column<4 ; column++) {
									file << bone->parentTransform[column*4 + row] << " ";
								}
							}
							file << "\"\n";
							for(unsigned int i=0 ; i<tabCount; i++) {
								file << "\t";
							}
							file << "}\n";
						} else {
							//close bone
							tabCount--;
							for(unsigned int i=0 ; i<tabCount ; i++) {
								file << "\t";
							}
							file << "}\n";
						}
					}
					file << "	}\n";
				}
				file <<
					"\n"
					"}";

				file.close();
			} catch(Exception &) {
				try {
					file.close();
				} catch(...) {
				}
				stringstream errmsg;
				errmsg << "Error: writing BufferedMesh " << id << " into file " << filename << " didn't work";
				throw Exception(errmsg.str(),EX_IO);
			}
		} else {
			//not a supported fileformat
			stringstream errmsg;
			errmsg << "Error: BufferedMesh " << id << " can't be saved in " << filename << ", currently only .dae files (Collada) and .c files (SX resource) are supported for saving";
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
	}

	void BufferedMesh::beginCapture(Shader &shader) {
		if(!loaded || !shader.isLoaded()) {
			//mesh or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		glBeginQuery(GL_PRIMITIVES_GENERATED,query_primitives_generated);
		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN,query_primitives_written);
		const vector<string> &outputBuffers = shader.getTransformFeedbackBuffers();
		unsigned int index = 0;
		for(vector<string>::const_iterator iter = outputBuffers.begin() ; iter != outputBuffers.end() ; iter++) {
			const string &oName = (*iter);
			map<string,VertexBuffer *>::iterator vboIter = outputVBOs.find(oName);
			if(vboIter == outputVBOs.end()) {
				//buffer does not contain such an output mesh
				//the renderer might be left in an inconsistent state
				//generate error message
				Logger::get() << LogMarkup("BufferedMesh::beginCapture") << Annotation("output buffer ") << Annotation(oName) << Annotation(" is missing in mesh ") << Annotation(id);
				index++;
				continue;
			}
			VertexBuffer *vbo = (*vboIter).second;
			glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, index, vbo->bufferID);
			index++;
		}
		if(!shader.isFragmentShading()) {
			glEnable(GL_RASTERIZER_DISCARD);
		}
		if(faceSize == 3) {
			glBeginTransformFeedback(GL_TRIANGLES);
		} else if(faceSize == 2) {
			glBeginTransformFeedback(GL_LINES);
		} else if(faceSize == 1) {
			glBeginTransformFeedback(GL_POINTS);
		}
	}

	void BufferedMesh::endCapture(Shader &shader) {
		if(!loaded || !shader.isLoaded()) {
			//mesh or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		glEndTransformFeedback();
		if(!shader.isFragmentShading()) {
			glDisable(GL_RASTERIZER_DISCARD);
		}
		glEndQuery(GL_PRIMITIVES_GENERATED);
		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
		vertexCount = min(countCreatedVertices() , maxVertexCount);
	}

	unsigned int BufferedMesh::countCreatedVertices() const {
		unsigned int numWritten;
		glGetQueryObjectuiv(query_primitives_written,GL_QUERY_RESULT,(GLuint *)&numWritten);
		return numWritten * faceSize;
	}

	void BufferedMesh::setMaxVertexCount(unsigned int count) {
		newMaxVertexCount = count;
	}

	unsigned int BufferedMesh::getMaxVertexCount() const {
		return maxVertexCount;
	}

	void BufferedMesh::setVertexCount(unsigned int count) {
		if(!loaded) {
			//mesh has to be loaded for a change
			//of vertexCount
			return;
		}
		//vertexCount must not be bigger than maxVertexCount
		unsigned int newVertexCount = min(count,maxVertexCount);
		//find biggest number smaller than newVertexCount divisable by faceSize
		unsigned int divided = newVertexCount / faceSize;
		this->vertexCount = divided * faceSize;
	}

	void BufferedMesh::render(Shader &shader) {
		if(!loaded || !shader.isLoaded()) {
			//mesh or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		//glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		//bind vertex array object
		glBindVertexArray(vao);
		vector<int> usedPointer;
		for(map<string,VertexBuffer *>::iterator iter = vbos.begin() ; iter != vbos.end() ; iter++) {
			//set attrib locations of attributes in the shader
			//and collect used attrib locations
			VertexBuffer *vbo = (*iter).second;
			if(vbo->attributeSize != 16 && (vbo->attributeSize < 1 || vbo->attributeSize > 4) ) {
				//only floats, vectors or 4x4 matrices can be used
				//as vertex attributes
				continue;
			}
			int newLocation = shader.getAttribLocation((*iter).first);
			if(newLocation >= 0) {
				//this attribute does exist
				// in the shader code, use it
				if(vbo->attributeSize != 16) {
					usedPointer.push_back(newLocation);
					glEnableVertexAttribArray((GLuint)newLocation);
					if(newLocation != vbo->location) {
						//different location -> update location
						vbo->location = newLocation;
						glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
						glVertexAttribPointer((GLuint)newLocation,vbo->attributeSize,GL_FLOAT,GL_FALSE,0,0);
					}
				} else {
					//a matrix occupies four slots
					for(unsigned int i=0 ; i<4 ; i++) {
						usedPointer.push_back(newLocation+i);
						glEnableVertexAttribArray((GLuint)newLocation+i);
						if(newLocation != vbo->location) {
							//different location -> update location
							glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
							glVertexAttribPointer((GLuint)newLocation+i,4,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*16,(const GLvoid *)(sizeof(GLfloat)*i*4));
						}
					}
					if(newLocation != vbo->location) {
						//after all four slots are enabled, update
						//vbo location on cpu
						vbo->location = newLocation;
					}
				}
			}
		}
		if(shader.isTransformFeedback()) {
			//enable slots of transfom feedback buffers
			unsigned int bufferNum = shader.getTransformFeedbackBuffers().size();
			for(unsigned int i=0 ; i<bufferNum ; i++) {
				usedPointer.push_back(i);
				glEnableVertexAttribArray((GLuint)i);
			}
		}
		bool tessellate = shader.isTessellating();
		if(faceSize == 3 && !tessellate) {
			glDrawArrays(GL_TRIANGLES,0,vertexCount);
		} else if(tessellate) {
			glPatchParameteri(GL_PATCH_VERTICES,faceSize);
			glDrawArrays(GL_PATCHES,0,vertexCount);
		} else if(faceSize == 2) {
			glDrawArrays(GL_LINES,0,vertexCount);
		} else if(faceSize == 1) {
			glDrawArrays(GL_POINTS,0,vertexCount);
		}
		for(vector<int>::iterator iter = usedPointer.begin() ; iter != usedPointer.end() ; iter++) {
			//disable those vbos which have been used
			glDisableVertexAttribArray((GLuint)(*iter));
		}
		//unbind vao
		glBindVertexArray(0);
	}

	void BufferedMesh::renderInstanced(Shader &shader, vector<BufferedMesh *> &instanceBuffers) {
		if(!loaded || !shader.isLoaded()) {
			//mesh or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		//glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		//bind vertex array object
		glBindVertexArray(vao);
		vector<int> usedPointer;
		for(map<string,VertexBuffer *>::iterator iter = vbos.begin() ; iter != vbos.end() ; iter++) {
			//set attrib locations of attributes in the shader
			//and collect used attrib locations
			VertexBuffer *vbo = (*iter).second;
			if(vbo->attributeSize != 16 && (vbo->attributeSize < 1 || vbo->attributeSize > 4) ) {
				//only floats, vectors or 4x4 matrices can be used
				//as vertex attributes
				continue;
			}
			int newLocation = shader.getAttribLocation((*iter).first);
			if(newLocation >= 0) {
				//this attribute does exist
				// in the shader code, use it
				if(vbo->attributeSize != 16) {
					usedPointer.push_back(newLocation);
					glEnableVertexAttribArray((GLuint)newLocation);
					if(newLocation != vbo->location) {
						//different location -> update location
						vbo->location = newLocation;
						glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
						glVertexAttribPointer((GLuint)newLocation,vbo->attributeSize,GL_FLOAT,GL_FALSE,0,0);
					}
				} else {
					//a matrix occupies four slots
					for(unsigned int i=0 ; i<4 ; i++) {
						usedPointer.push_back(newLocation+i);
						glEnableVertexAttribArray((GLuint)newLocation+i);
						if(newLocation != vbo->location) {
							//different location -> update location
							glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
							glVertexAttribPointer((GLuint)newLocation+i,4,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*16,(const GLvoid *)(sizeof(GLfloat)*i*4));
						}
					}
					if(newLocation != vbo->location) {
						//after all four slots are enabled, update
						//vbo location on cpu
						vbo->location = newLocation;
					}
				}
			}
		}
		//number of instances drawn
		int numInstances = -1;
		//slots containing the instance specific datasets
		vector<int> instanceSlots;
		for(vector<BufferedMesh *>::iterator iter = instanceBuffers.begin() ; iter != instanceBuffers.end() ; iter++) {
			BufferedMesh *m = (*iter);
			if(m != 0 && m->isLoaded()) {
				if(numInstances < 0) {
					//init numInstances with the number of vertices
					//of the first mesh
					numInstances = m->getVertexCount();
				} else {
					//the final number of instances is the
					//minimum number of vertices in any mesh in instanceBuffers
					numInstances = min(numInstances,(int)m->getVertexCount());
				}
				map<string,VertexBuffer *> &v = m->getBuffers();
				for(map<string,VertexBuffer *>::iterator iter2 = v.begin() ; iter2 != v.end() ; iter2++) {
					//set attrib locations of attributes in the shader
					//and collect used attrib locations
					VertexBuffer *vbo = (*iter2).second;
					if(vbo->attributeSize != 16 && (vbo->attributeSize < 1 || vbo->attributeSize > 4) ) {
						//only floats, vectors or 4x4 matrices can be used
						//as vertex attributes
						continue;
					}
					int newLocation = shader.getAttribLocation((*iter2).first);
					if(newLocation >= 0) {
						//this attribute does exist
						//in the shader code, use it
						if(vbo->attributeSize != 16) {
							usedPointer.push_back(newLocation);
							instanceSlots.push_back(newLocation);
							glEnableVertexAttribArray((GLuint)newLocation);
							glVertexAttribDivisor((GLuint)newLocation,1);
							glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
							glVertexAttribPointer((GLuint)newLocation,vbo->attributeSize,GL_FLOAT,GL_FALSE,0,0);
						} else {
							//a matrix occupies four slots
							for(unsigned int i=0 ; i<4 ; i++) {
								usedPointer.push_back(newLocation+i);
								instanceSlots.push_back(newLocation+i);
								glEnableVertexAttribArray((GLuint)(newLocation + i));
								glVertexAttribDivisor((GLuint)(newLocation + i),1);
								glBindBuffer(GL_ARRAY_BUFFER,vbo->bufferID);
								glVertexAttribPointer((GLuint)newLocation + i,4,GL_FLOAT,GL_FALSE,sizeof(GLfloat)*16,(const GLvoid *)(sizeof(GLfloat)*i*4));
							}
						}
					}
				}
			}
		}
		if(shader.isTransformFeedback()) {
			//enable slots of transfom feedback buffers
			unsigned int bufferNum = shader.getTransformFeedbackBuffers().size();
			for(unsigned int i=0 ; i<bufferNum ; i++) {
				usedPointer.push_back(i);
				glEnableVertexAttribArray((GLuint)i);
			}
		}
		bool tessellate = shader.isTessellating();
		if(numInstances > 0) {
			//if numInstances is zero or below
			//nothing needs to be rendered
			if(faceSize == 3 && !tessellate) {
				glDrawArraysInstanced(GL_TRIANGLES,0,vertexCount,numInstances);
			} else if(tessellate) {
				glPatchParameteri(GL_PATCH_VERTICES,faceSize);
				glDrawArraysInstanced(GL_PATCHES,0,vertexCount,numInstances);
			} else if(faceSize == 2) {
				glDrawArraysInstanced(GL_LINES,0,vertexCount,numInstances);
			} else if(faceSize == 1) {
				glDrawArraysInstanced(GL_POINTS,0,vertexCount,numInstances);
			}
		}
		for(vector<int>::iterator iter = instanceSlots.begin() ; iter != instanceSlots.end() ; iter++) {
			//prepare the slots used for the instances
			//for common usage
			glVertexAttribDivisor((GLuint)(*iter),0);
		}
		for(vector<int>::iterator iter = usedPointer.begin() ; iter != usedPointer.end() ; iter++) {
			//disable those vbos which have been used
			glDisableVertexAttribArray((GLuint)(*iter));
		}
		//unbind vao
		glBindVertexArray(0);
	}

	void BufferedMesh::addBuffer(const string &identifier, const string &outputIdentifier, vector<float> &buffer, unsigned int attributeSize) {
		setOutputIdentifier(identifier,outputIdentifier);
		addBuffer(identifier,buffer,attributeSize);
	}

	void BufferedMesh::addBuffer(const string &identifier, vector<float> &buffer, unsigned int attributeSize) {
		newAttributeIDs.push_back(identifier);
		newBuffers.push_back(vector<float>());
		vector<float> &last = newBuffers[newBuffers.size() - 1];
		for(vector<float>::iterator iter = buffer.begin() ; iter != buffer.end() ; iter++) {
			last.push_back((*iter));
		}
		newAttributeSizes.push_back(attributeSize);
	}

	void BufferedMesh::addBuffers(const string &path) {
		newMeshes.push_back(path);
		newDiscardStandard.push_back(false);
	}

	void BufferedMesh::addBuffers(const string &path, bool discardStandardBuffers) {
		newMeshes.push_back(path);
		newDiscardStandard.push_back(discardStandardBuffers);
	}

	VertexBuffer *BufferedMesh::getBuffer(const string &identifier) {
		map<string,VertexBuffer *>::iterator iter = vbos.find(identifier);
		if(iter == vbos.end()) {
			//not found, hence return nullpointer
			return 0;
		}
		return (*iter).second;
	}

	map<string,VertexBuffer *> &BufferedMesh::getBuffers() {
		return vbos;
	}

	void BufferedMesh::setOutputIdentifier(const string &identifier, const string &outputIdentifier) {
		map<string,string>::iterator iter = newOutputIDs.find(identifier);
		if(iter != newOutputIDs.end()) {
			newOutputIDs.erase(iter);
		}
		newOutputIDs.insert(pair<string,string>(identifier,outputIdentifier));
	}

	bool BufferedMesh::setUniforms(const vector<Uniform *> &uniforms, const vector<string> &bufferIDs, unsigned int location) {
		if(!loaded || uniforms.size() != bufferIDs.size() || location >= maxVertexCount) {
			//the mesh has to be loaded, location must be the
			//index of an existing vertex in the VertexBuffers,
			//and uniforms must have the same ize as bufferIDs,
			//as each uniform is associated with a buffer
			return false;
		}
		vector<pair<string,UniformFloat *> > uFloats;
		vector<pair<string,UniformVector *> > uVectors;
		vector<pair<string,UniformMatrix *> > uMatrices;
		vector<Uniform *>::const_iterator uIter = uniforms.begin();
		vector<string>::const_iterator bIter = bufferIDs.begin();
		for( ; uIter != uniforms.end() ; uIter++, bIter++) {
			map<string, VertexBuffer *>::iterator iter2 = vbos.find((*bIter));
			if(iter2 == vbos.end()) {
				//each bufferID must be the id of a VertexBuffer
				return false;
			}
			UniformFloat *uFloat = dynamic_cast<UniformFloat *>((*uIter));
			UniformVector *uVector = dynamic_cast<UniformVector *>((*uIter));
			UniformMatrix *uMatrix = dynamic_cast<UniformMatrix *>((*uIter));
			if(uFloat != 0) {
				if((*iter2).second->attributeSize != 1) {
					//UniformFloats can only exchange data with VertexBuffers
					//with attributeSize 1
					return false;
				}
				uFloats.push_back(pair<string,UniformFloat *>((*bIter),uFloat));
			} else if(uVector != 0) {
				if((*iter2).second->attributeSize < 2 && (*iter2).second->attributeSize > 4) {
					//UniformVectors can only exchange data with VertexBuffers
					//with attributeSize in between 2 and 4
					return false;
				}
				uVectors.push_back(pair<string,UniformVector *>((*bIter),uVector));
			} else if(uMatrix != 0) {
				if((*iter2).second->attributeSize != 16) {
					//UniformMatrices can only exchange data with VertexBuffers
					//with attributeSize 16
					return false;
				}
				uMatrices.push_back(pair<string,UniformMatrix *>((*bIter),uMatrix));
			} else {
				//the subtype of the uniform in uIter
				//makes data exchange with a VertexBuffer
				//impossible
				return false;
			}
		}
		for(vector<pair<string,UniformFloat *> >::iterator ufIter = uFloats.begin() ; ufIter != uFloats.end() ; ufIter++) {
			//associate each float with a buffer
			string &name = (*ufIter).first;
			UniformFloat *uFloat = (*ufIter).second;
			UniformFloat **uFloatArray = 0;
			map<string,UniformFloat **>::iterator findIter = uniformFloatSets.find(name);
			if(findIter == uniformFloatSets.end()) {
				//first uniform associated with VertexBuffer name
				//hence create array
				uFloatArray = new UniformFloat *[maxVertexCount];
				uniformFloatSets.insert(pair<string,UniformFloat **>(name,uFloatArray));
				for(unsigned int i=0 ; i<maxVertexCount ; i++) {
					uFloatArray[i] = 0;
				}
			} else {
				uFloatArray = (*findIter).second;
			}
			uFloatArray[location] = uFloat;
		}
		for(vector<pair<string,UniformVector *> >::iterator uvIter = uVectors.begin() ; uvIter != uVectors.end() ; uvIter++) {
			//associate each vector with a buffer
			string &name = (*uvIter).first;
			UniformVector *uVector = (*uvIter).second;
			UniformVector **uVectorArray = 0;
			map<string,UniformVector **>::iterator findIter = uniformVectorSets.find(name);
			if(findIter == uniformVectorSets.end()) {
				//first uniform associated with VertexBuffer name
				//hence create array
				uVectorArray = new UniformVector *[maxVertexCount];
				uniformVectorSets.insert(pair<string,UniformVector **>(name,uVectorArray));
				for(unsigned int i=0 ; i<maxVertexCount ; i++) {
					uVectorArray[i] = 0;
				}
			} else {
				uVectorArray = (*findIter).second;
			}
			uVectorArray[location] = uVector;
		}
		for(vector<pair<string,UniformMatrix *> >::iterator umIter = uMatrices.begin() ; umIter != uMatrices.end() ; umIter++) {
			//associate each matrix with a buffer
			string &name = (*umIter).first;
			UniformMatrix *uMatrix = (*umIter).second;
			UniformMatrix **uMatrixArray = 0;
			map<string,UniformMatrix **>::iterator findIter = uniformMatrixSets.find(name);
			if(findIter == uniformMatrixSets.end()) {
				//first uniform associated with VertexBuffer name
				//hence create array
				uMatrixArray = new UniformMatrix *[maxVertexCount];
				uniformMatrixSets.insert(pair<string,UniformMatrix **>(name,uMatrixArray));
				for(unsigned int i=0 ; i<maxVertexCount ; i++) {
					uMatrixArray[i] = 0;
				}
			} else {
				uMatrixArray = (*findIter).second;
			}
			uMatrixArray[location] = uMatrix;
		}
		
		return true;
	}

	bool BufferedMesh::removeUniforms(unsigned int location) {
		if(!loaded || location >= maxVertexCount) {
			//not loaded, or location out of bounds
			return false;
		}
		for(map<string,UniformFloat **>::iterator iter = uniformFloatSets.begin() ; iter != uniformFloatSets.end() ; iter++) {
			(*iter).second[location] = 0;
		}
		for(map<string,UniformVector **>::iterator iter = uniformVectorSets.begin() ; iter != uniformVectorSets.end() ; iter++) {
			(*iter).second[location] = 0;
		}
		for(map<string,UniformMatrix **>::iterator iter = uniformMatrixSets.begin() ; iter != uniformMatrixSets.end() ; iter++) {
			(*iter).second[location] = 0;
		}
		
		return true;
	}

	void BufferedMesh::removeUniforms() {
		if(!loaded) {
			return;
		}
		for(map<string,UniformFloat **>::iterator iter = uniformFloatSets.begin() ; iter != uniformFloatSets.end() ; iter++) {
			for(unsigned int i = 0 ; i<maxVertexCount ; i++) {
				(*iter).second[i] = 0;
			}
		}
		for(map<string,UniformVector **>::iterator iter = uniformVectorSets.begin() ; iter != uniformVectorSets.end() ; iter++) {
			for(unsigned int i = 0 ; i<maxVertexCount ; i++) {
				(*iter).second[i] = 0;
			}
		}
		for(map<string,UniformMatrix **>::iterator iter = uniformMatrixSets.begin() ; iter != uniformMatrixSets.end() ; iter++) {
			for(unsigned int i = 0 ; i<maxVertexCount ; i++) {
				(*iter).second[i] = 0;
			}
		}
	}

	void BufferedMesh::resetUniforms() {
		if(!loaded) {
			return;
		}
		for(map<string,UniformFloat **>::iterator iter = uniformFloatSets.begin() ; iter != uniformFloatSets.end() ; iter++) {
			delete [] (*iter).second;
		}
		for(map<string,UniformVector **>::iterator iter = uniformVectorSets.begin() ; iter != uniformVectorSets.end() ; iter++) {
			delete [] (*iter).second;
		}
		for(map<string,UniformMatrix **>::iterator iter = uniformMatrixSets.begin() ; iter != uniformMatrixSets.end() ; iter++) {
			delete [] (*iter).second;
		}
		uniformFloatSets.clear();
		uniformVectorSets.clear();
		uniformMatrixSets.clear();
	}

	void BufferedMesh::loadBuffersToUniforms() {
		loadBuffersToUniforms(0,maxVertexCount-1);
	}

	void BufferedMesh::loadBuffersToUniforms(unsigned int firstLocation, unsigned int lastLocation) {
		if(!loaded || maxVertexCount == 0) {
			return;
		}
		//clamp indices to buffers
		firstLocation = min(firstLocation,maxVertexCount-1);
		lastLocation = min(lastLocation,maxVertexCount-1);
		for(map<string,UniformFloat **>::iterator ufIter = uniformFloatSets.begin() ; ufIter != uniformFloatSets.end() ; ufIter++) {
			UniformFloat **uFloats = (*ufIter).second;
			VertexBuffer *vbo = (*vbos.find((*ufIter).first)).second;
			unsigned int attributeSize = vbo->attributeSize;
			const float *content = vbo->unlockRead();
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uFloats[i] == 0) {
					continue;
				}
				uFloats[i]->value = content[i];
			}
			vbo->lock();
		}
		for(map<string,UniformVector **>::iterator uvIter = uniformVectorSets.begin() ; uvIter != uniformVectorSets.end() ; uvIter++) {
			UniformVector **uVectors = (*uvIter).second;
			VertexBuffer *vbo = (*vbos.find((*uvIter).first)).second;
			unsigned int attributeSize = vbo->attributeSize;
			const float *content = vbo->unlockRead();
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uVectors[i] == 0) {
					continue;
				}
				unsigned int j=0;
				for( ; j<attributeSize ; j++) {
					uVectors[i]->elements[j] = content[i*attributeSize + j];
				}
				//fill the rest of the vector such that
				//the w component equals 1, and the other
				//components equal 0
				for( ; j<3 ; j++) {
					uVectors[i]->elements[j] = 0.0f;
				}
				if(j<4) {
					uVectors[i]->elements[3] = 1.0f;
				}
			}
			vbo->lock();
		}
		for(map<string,UniformMatrix **>::iterator umIter = uniformMatrixSets.begin() ; umIter != uniformMatrixSets.end() ; umIter++) {
			UniformMatrix **uMatrices = (*umIter).second;
			VertexBuffer *vbo = (*vbos.find((*umIter).first)).second;
			unsigned int attributeSize = vbo->attributeSize;
			const float *content = vbo->unlockRead();
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uMatrices[i] == 0) {
					continue;
				}
				for(unsigned int j=0 ; j<16 ; j++) {
					uMatrices[i]->elements[j] = content[i*16 + j];
				}
			}
			vbo->lock();
		}
	}

	void BufferedMesh::loadBufferToUniforms(const string &bufferID) {
		loadBufferToUniforms(bufferID,0,maxVertexCount-1);
	}

	void BufferedMesh::loadBufferToUniforms(const string &bufferID, unsigned int firstLocation, unsigned int lastLocation) {
		if(!loaded || maxVertexCount == 0) {
			return;
		}
		map<string,UniformFloat **>::iterator ufIter = uniformFloatSets.find(bufferID);
		map<string,UniformVector **>::iterator uvIter = uniformVectorSets.find(bufferID);
		map<string,UniformMatrix **>::iterator umIter = uniformMatrixSets.find(bufferID);
		if(ufIter == uniformFloatSets.end() && uvIter == uniformVectorSets.end() && umIter == uniformMatrixSets.end()) {
			//no uniforms associated to VertexBuffer bufferID exist
			//hence nothing needs to be done
			return;
		}
		//clamp first and lastLocation to the buffers
		firstLocation = min(firstLocation,maxVertexCount-1);
		lastLocation = min(lastLocation,maxVertexCount-1);
		VertexBuffer *vbo = (*vbos.find(bufferID)).second;
		unsigned int attributeSize = vbo->attributeSize;
		const float *content = vbo->unlockRead();
		if(attributeSize == 1) {
			//the currently read buffer is copied
			//UniformFloats
			UniformFloat **uFloats = (*ufIter).second;
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uFloats[i] == 0) {
					continue;
				}
				uFloats[i]->value = content[i];
			}
		} else if(attributeSize <= 4) {
			//the currently read buffer is copied
			//UniformVectors
			UniformVector **uVectors = (*uvIter).second;
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uVectors[i] == 0) {
					continue;
				}
				unsigned int j=0;
				for( ; j<attributeSize ; j++) {
					uVectors[i]->elements[j] = content[i*attributeSize + j];
				}
				//fill the rest of the vector such that
				//the w component equals 1, and the other
				//components equal 0
				for( ; j<3 ; j++) {
					uVectors[i]->elements[j] = 0.0f;
				}
				if(j<4) {
					uVectors[i]->elements[3] = 1.0f;
				}
			}
		} else if(attributeSize == 16) {
			//the currently read buffer is copied
			//UniformMatrices
			UniformMatrix **uMatrices = (*umIter).second;
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uMatrices[i] == 0) {
					continue;
				}
				for(unsigned int j=0 ; j<16 ; j++) {
					uMatrices[i]->elements[j] = content[i*16 + j];
				}
			}
		}
		vbo->lock();
	}

	void BufferedMesh::loadUniformsToBuffers() {
		loadUniformsToBuffers(0,maxVertexCount-1);
	}

	void BufferedMesh::loadUniformsToBuffers(unsigned int firstLocation, unsigned int lastLocation) {
		if(!loaded || maxVertexCount == 0) {
			return;
		}
		//clamp indices to buffers
		firstLocation = min(firstLocation,maxVertexCount-1);
		lastLocation = min(lastLocation,maxVertexCount-1);
		for(map<string,UniformFloat **>::iterator ufIter = uniformFloatSets.begin() ; ufIter != uniformFloatSets.end() ; ufIter++) {
			UniformFloat **uFloats = (*ufIter).second;
			VertexBuffer *vbo = (*vbos.find((*ufIter).first)).second;
			unsigned int attributeSize = vbo->attributeSize;
			float *content = vbo->unlockWrite();
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uFloats[i] == 0) {
					continue;
				}
				content[i] = uFloats[i]->value;
			}
			vbo->lock();
		}
		for(map<string,UniformVector **>::iterator uvIter = uniformVectorSets.begin() ; uvIter != uniformVectorSets.end() ; uvIter++) {
			UniformVector **uVectors = (*uvIter).second;
			VertexBuffer *vbo = (*vbos.find((*uvIter).first)).second;
			unsigned int attributeSize = vbo->attributeSize;
			float *content = vbo->unlockWrite();
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uVectors[i] == 0) {
					continue;
				}
				for(unsigned int j=0 ; j<attributeSize ; j++) {
					content[i*attributeSize + j] = uVectors[i]->elements[j];
				}
			}
			vbo->lock();
		}
		for(map<string,UniformMatrix **>::iterator umIter = uniformMatrixSets.begin() ; umIter != uniformMatrixSets.end() ; umIter++) {
			UniformMatrix **uMatrices = (*umIter).second;
			VertexBuffer *vbo = (*vbos.find((*umIter).first)).second;
			unsigned int attributeSize = vbo->attributeSize;
			float *content = vbo->unlockWrite();
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uMatrices[i] == 0) {
					continue;
				}
				for(unsigned int j=0 ; j<16 ; j++) {
					content[i*16 + j] = uMatrices[i]->elements[j];
				}
			}
			vbo->lock();
		}
	}

	void BufferedMesh::loadUniformsToBuffer(const string &bufferID) {
		loadUniformsToBuffer(bufferID,0,maxVertexCount-1);
	}

	void BufferedMesh::loadUniformsToBuffer(const string &bufferID, unsigned int firstLocation, unsigned int lastLocation) {
		if(!loaded || maxVertexCount == 0) {
			return;
		}
		map<string,UniformFloat **>::iterator ufIter = uniformFloatSets.find(bufferID);
		map<string,UniformVector **>::iterator uvIter = uniformVectorSets.find(bufferID);
		map<string,UniformMatrix **>::iterator umIter = uniformMatrixSets.find(bufferID);
		if(ufIter == uniformFloatSets.end() && uvIter == uniformVectorSets.end() && umIter == uniformMatrixSets.end()) {
			//no uniforms associated to VertexBuffer bufferID exist
			//hence nothing needs to be done
			return;
		}
		//clamp indices to buffers
		firstLocation = min(firstLocation,maxVertexCount-1);
		lastLocation = min(lastLocation,maxVertexCount-1);
		VertexBuffer *vbo = (*vbos.find(bufferID)).second;
		unsigned int attributeSize = vbo->attributeSize;
		float *content = vbo->unlockWrite();
		if(attributeSize == 1) {
			//the currently read buffer is updated by
			//UniformFloats
			UniformFloat **uFloats = (*ufIter).second;
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uFloats[i] == 0) {
					continue;
				}
				content[i] = uFloats[i]->value;
			}
		} else if(attributeSize <=4) {
			//the currently read buffer is updated by
			//UniformVectors
			UniformVector **uVectors = (*uvIter).second;
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uVectors[i] == 0) {
					continue;
				}
				for(unsigned int j=0 ; j<attributeSize ; j++) {
					content[i*attributeSize + j] = uVectors[i]->elements[j];
				}
			}
		} else if(attributeSize == 16) {
			//the currently read buffer is updated by
			//UniformMatrices
			UniformMatrix **uMatrices = (*umIter).second;
			for(unsigned int i=firstLocation ; i<=lastLocation ; i++) {
				if(uMatrices[i] == 0) {
					continue;
				}
				for(unsigned j=0 ; j<16 ; j++) {
					content[i*16 + j] = uMatrices[i]->elements[j];
				}
			}
		}
		vbo->lock();
	}

	vector<Bone> BufferedMesh::getSkeleton() const {
		return bones;
	}

	Matrix BufferedMesh::getSkeletonMatrix() const {
		return skeletonMatrix;
	}

	void BufferedMesh::setSkeleton(const vector<Bone> &bones) {
		map<string,const Bone *> uniqueness;

		//traversal stack
		vector<vector<Bone>::const_iterator> iterStack;
		iterStack.push_back(bones.begin());
		vector<const vector<Bone> *> arrayStack;
		arrayStack.push_back(&bones);
		while(iterStack.size() > 0) {
			vector<Bone>::const_iterator iter = iterStack[iterStack.size()-1];
			const vector<Bone> *a  = arrayStack[arrayStack.size()-1];
			if(iter != a->end()) {
				const Bone &bone = (*iter);

				//control uniqueness
				map<string,const Bone *>::iterator searchIter = uniqueness.find(bone.ID);
				if(searchIter != uniqueness.end()) {
					//uniqueness constraint violated
					stringstream errmsg;
					errmsg << "Error: added bone id " << bone.ID << " multiple time to skeleton of BufferedMesh " << id;
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
				uniqueness[bone.ID] = &bone;

				//push children
				iterStack.push_back(bone.bones.begin());
				arrayStack.push_back(&bone.bones);

				//advance to next element
				iterStack[iterStack.size()-2]++;
			} else {
				//reached end of stack
				//pop it
				iterStack.pop_back();
				arrayStack.pop_back();
			}
		}

		this->bones = bones;
	}

	void BufferedMesh::setSkeletonMatrix(const Matrix &skeletonMatrix) {
		this->skeletonMatrix = skeletonMatrix;
	}

	void BufferedMesh::execute(vector<string> &input) {
		for(vector<string>::iterator iter = input.begin() ; iter != input.end() ; iter++) {
			const string &command = (*iter);
			if(command.compare("readUniforms") == 0) {
				loadUniformsToBuffers();
			} else if(command.compare("writeUniforms") == 0) {
				loadBuffersToUniforms();
			}
		}
	}

}

#endif