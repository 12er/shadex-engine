#ifndef _SX_ENGINE_CORE_VERTEXBUFFER_CPP_
#define _SX_ENGINE_CORE_VERTEXBUFFER_CPP_

/**
 * Vertex buffer
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <GL/glew.h>
#include <sstream>
using namespace std;

namespace sx {

	float *VertexBuffer::unlock() {
		glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		return (float *)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);
	}

	const float *VertexBuffer::unlockRead() {
		glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		return (const float *)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
	}
		
	float *VertexBuffer::unlockWrite() {
		glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		return (float *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	}

	void VertexBuffer::lock() {
		glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		GLboolean worked = glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		if(worked == GL_FALSE) {
			//an error occured turing the lock operation
			//generate error message
			Logger::get() << LogMarkup("VertexBuffer::lock") << Annotation("vertex buffer ") << Annotation(ID) << Annotation(" could not be locked");
		}
	}

}

#endif