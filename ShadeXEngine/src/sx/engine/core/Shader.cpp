#ifndef _SX_CORE_SHADER_CPP_
#define _SX_CORE_SHADER_CPP_

/**
 * Shader class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <GL/glew.h>
#include <sstream>
#include <boost/foreach.hpp>
using namespace boost;
using namespace std;

namespace sx {

	Shader::Shader(const string &id) {
		this->id = id;
		loaded = false;
		newTessellate = false;
		tessellate = false;
		newFragmentShade = false;
		fragmentShade = false;
		newTextureSlot = 0;
		freeTextureSlot = 0;
	}

	Shader::~Shader() {
		if(loaded) {
			//deconstruct original shader
			BOOST_FOREACH(unsigned int sh , shaders) {
				glDetachShader(program,sh);
				glDeleteShader(sh);
			}
			glDeleteProgram(program);
			loaded = false;
		}
	}

	void Shader::load() {
		Logger::get() << LogMarkup(id) << newShaders.size();
		if(newShaders.size() == 0) {
			//no new shaders specified
			//hence the shader should stay as it is
			return;
		}
		if(loaded) {
			//deconstruct original shader
			BOOST_FOREACH(unsigned int sh , shaders) {
				Logger::get() << LogMarkup("Shader::load/unload shader") << "unload";
				glDetachShader(program,sh);
				glDeleteShader(sh);
			}
			Logger::get() << LogMarkup("Shader::load/unload program") << "unload";
			glDeleteProgram(program);
			shaders.clear();
			errorLog.clear();
			transformFeedbackBuffers.clear();
			uniformLocations.clear();
			targetLocations.clear();
			attribLocations.clear();
			loaded = false;
			tessellate = false;
			fragmentShade = false;
		}
		program = glCreateProgram();
		if(program) {
			vector<string>::iterator codes = newShaders.begin();
			vector<ShaderType>::iterator types = newShaderTypes.begin();
			vector<bool>::iterator isFiles = newShaderIsFile.begin();
			bool detachShaders = false;
			while(codes != newShaders.end() && types != newShaderTypes.end() && isFiles != newShaderIsFile.end()) {
				//compile and attach shaders
				unsigned int type = 0;
				string typelabel = "";
				switch((*types)) {
					case VERTEX:
						type = GL_VERTEX_SHADER;
						typelabel = "vertexshader";
						break;
					case TESSELLATION_CONTROL:
						type = GL_TESS_CONTROL_SHADER;
						typelabel = "tessellationcontrollshader";
						break;
					case TESSELLATION_EVALUATION:
						type = GL_TESS_EVALUATION_SHADER;
						typelabel = "tessellationevaluationshader";
						break;
					case GEOMETRY:
						type = GL_GEOMETRY_SHADER;
						typelabel = "geometryshader";
						break;
					case FRAGMENT:
						type = GL_FRAGMENT_SHADER;
						typelabel = "fragmentshader";
						break;
				}
				unsigned int shader = glCreateShader(type);
				if(shader) {
					//compile shader
					int status = 0;
					char logbuffer[1024];
					int loglength = 0;
					if((*isFiles)) {
						//current string is path
						//load code from path
						try {
							string codestr = readFile((*codes));
							const char *code = codestr.c_str();
							glShaderSource(shader,1,&code,0);
						} catch(Exception &e) {
							//could not read file
							stringstream errs;
							errs << typelabel << " " << id << " cannot be loaded, " << e.getMessage();
							errorLog.push_back(errs.str());
							Logger::get() << LogMarkup("Shader::load") << Annotation("file ") << Annotation(id) << e.getMessage();
							detachShaders = true;
							break;
						}
					} else {
						//current string is already code
						const char *code = (*codes).c_str();
						glShaderSource(shader,1,&code,0);
					}
					glCompileShader(shader);
					glGetShaderInfoLog(shader,1024,&loglength,logbuffer);
					glGetShaderiv(shader,GL_COMPILE_STATUS,&status);
					if(loglength > 0) {
						stringstream errs;
						errs << "compilation error in " << typelabel << " " << id << ": " << string(logbuffer);
						errorLog.push_back(errs.str());
						Logger::get() << LogMarkup("Shader::load") << Annotation("compile ") << Annotation(id) << Annotation(" ") << Annotation(typelabel) << string(logbuffer);
					}
					if(status == GL_FALSE) {
						//shaders will be deleted
						glDeleteShader(shader);
						detachShaders = true;
						break;
					}
					glAttachShader(program,shader);
					shaders.push_back(shader);
				} else {
					//error
					detachShaders = true;
					break;
				}
				codes++;
				types++;
				isFiles++;
			}
			if(detachShaders) {
				//an error occurred, detach and delete shader
				BOOST_FOREACH(unsigned int sh , shaders) {
					glDetachShader(program,sh);
					glDeleteShader(sh);
				}
				shaders.clear();
				transformFeedbackBuffers.clear();
				glDeleteProgram(program);
				newShaders.clear();
				newShaderTypes.clear();
				newShaderIsFile.clear();
				newTransformFeedbackBuffers.clear();
				newTessellate = tessellate = false;
				newFragmentShade = fragmentShade = false;
				return;
			}
			//everything went well up to here
			//setup transform feedback buffers
			transformFeedbackBuffers = newTransformFeedbackBuffers;
			char **ids = 0;
			if(transformFeedbackBuffers.size() > 0) {
				unsigned int index = 0;
				ids = new char*[transformFeedbackBuffers.size()];
				for(vector<string>::iterator iter = transformFeedbackBuffers.begin() ; iter != transformFeedbackBuffers.end() ; iter++) {
					const char *sid = (*iter).c_str();
					char *id = new char [(*iter).size() + 1];
					for(unsigned int i=0 ; i<(*iter).size() ; i++) {
						id[i] = sid[i];
					}
					id[(*iter).size()] = '\0';
					ids[index] = id;
					index++;
				}
				glTransformFeedbackVaryings(program, transformFeedbackBuffers.size(), (const char **)ids, GL_SEPARATE_ATTRIBS);
			}
			//everything went well up to here
			//link shaders
			int status = 0;
			char logbuffer[1024];
			int loglength = 0;
			glLinkProgram(program);
			if(ids == 0) {
				//buffernames have been included
				//into the program object turing linking
				//hence the array of buffernames can now be
				//deleted
				for(unsigned int i=0 ; i<transformFeedbackBuffers.size() ; i++) {
					delete [] ids[i];
				}
				delete [] ids;
			}
			glGetProgramInfoLog(program,1024,&loglength,logbuffer);
			if(loglength > 0) {
				stringstream errs;
				errs << "linker error in " << id << ": " << string(logbuffer);
				errorLog.push_back(errs.str());
				Logger::get() << LogMarkup("Shader::load") << Annotation("link ") << Annotation(id) << string(logbuffer);
			}
			glValidateProgram(program);
			glGetProgramiv(program,GL_VALIDATE_STATUS,&status);
			if(status == GL_FALSE) {
				//an error occurred, detach and delete shader
				BOOST_FOREACH(unsigned int sh , shaders) {
					glDetachShader(program,sh);
					glDeleteShader(sh);
				}
				shaders.clear();
				transformFeedbackBuffers.clear();
				newTransformFeedbackBuffers.clear();
				glDeleteProgram(program);
				newTessellate = tessellate = false;
				newFragmentShade = fragmentShade = false;
			} else {
				//everything worked fine
				//it's loaded
				loaded = true;
				tessellate = newTessellate;
				fragmentShade = newFragmentShade;
			}
			newShaders.clear();
			newShaderTypes.clear();
			newShaderIsFile.clear();
			newTransformFeedbackBuffers.clear();
		}
	}

	bool Shader::isLoaded() const {
		return loaded;
	}

	vector<string> Shader::getErrorLog() const {
		return errorLog;
	}

	void Shader::addShader(const std::string &code, sx::ShaderType type) {
		newShaders.push_back(code);
		newShaderTypes.push_back(type);
		newShaderIsFile.push_back(false);
		if(type == TESSELLATION_CONTROL || type == TESSELLATION_EVALUATION) {
			newTessellate = true;
		}
		if(type == FRAGMENT) {
			newFragmentShade = true;
		}
	}

	void Shader::addShaderFile(const std::string &path, sx::ShaderType type) {
		newShaders.push_back(path);
		newShaderTypes.push_back(type);
		newShaderIsFile.push_back(true);
		if(type == TESSELLATION_CONTROL || type == TESSELLATION_EVALUATION) {
			newTessellate = true;
		}
		if(type == FRAGMENT) {
			newFragmentShade = true;
		}
	}

	void Shader::addTransformFeedbackBuffer(const string &identifyer) {
		newTransformFeedbackBuffers.push_back(identifyer);
	}

	void Shader::use() {
		freeTextureSlots();
		if(!loaded) {
			return;
		}
		glUseProgram(program);
	}

	bool Shader::isTessellating() const {
		return tessellate;
	}

	bool Shader::isTransformFeedback() const {
		return transformFeedbackBuffers.size() > 0;
	}

	bool Shader::isFragmentShading() const {
		return fragmentShade;
	}

	int Shader::getProgramID() const {
		return program;
	}

	const vector<string> &Shader::getTransformFeedbackBuffers() const {
		return transformFeedbackBuffers;
	}

	int Shader::getTransformFeedbackLocation(const string &identifyer) {
		unsigned int index = 0;
		for(vector<string>::iterator iter = transformFeedbackBuffers.begin() ; iter != transformFeedbackBuffers.end() ; iter++) {
			if((*iter).compare(identifyer) == 0) {
				return index;
			}
			index++;
		}
		return -1;
	}

	int Shader::getUniformLocation(const string &identifyer) {
		unordered_map<string,int>::iterator iter = uniformLocations.find(identifyer);
		if(iter == uniformLocations.end()) {
			//uniform location does not exist yet
			//create it
			int loc = (int)glGetUniformLocation(program,identifyer.c_str());
			uniformLocations[identifyer] = loc;
			return loc;
		}
		return (*iter).second;
	}

	int Shader::getTargetLocation(const std::string &identifyer) {
		unordered_map<string,int>::iterator iter = targetLocations.find(identifyer);
		if(iter == targetLocations.end()) {
			//target location does not exist yet
			//create it
			int loc = glGetFragDataLocation(program,identifyer.c_str());
			targetLocations[identifyer] = loc;
			return loc;
		}
		return (*iter).second;
	}

	int Shader::getAttribLocation(const std::string &identifyer) {
		unordered_map<string,int>::iterator iter = attribLocations.find(identifyer);
		if(iter == attribLocations.end()) {
			//attrib location does not exist yet
			//create it
			int location = glGetAttribLocation(program,identifyer.c_str());
			attribLocations[identifyer] = location;
			return location;
		}
		return (*iter).second;
	}

	void Shader::freeTextureSlots() {
		newTextureSlot = freeTextureSlot;
	}

	unsigned int Shader::createTextureSlot() {
		return newTextureSlot++;
	}

	void Shader::lockTextureSlots() {
		freeTextureSlot = newTextureSlot;
		lockedSlotsStack.push_back(freeTextureSlot);
	}

	void Shader::unlockTextureSlots() {
		freeTextureSlot = 0;
		if(lockedSlotsStack.size() > 0) {
			//top of stack always points
			//to the first unlocked slot
			//hence the current top of the stack
			//has to be removed
			lockedSlotsStack.pop_back();
		}
		if(lockedSlotsStack.size() > 0) {
			//stack is nonempty
			//, hence freeTextureSlot might not
			//be zero
			freeTextureSlot = lockedSlotsStack[lockedSlotsStack.size() - 1];
		}
	}

}

#endif