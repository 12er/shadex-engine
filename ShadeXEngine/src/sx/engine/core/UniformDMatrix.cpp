#ifndef _SX_ENGINE_CORE_UNIFORMDMATRIX_CPP_
#define _SX_ENGINE_CORE_UNIFORMDMATRIX_CPP_

/**
 * Uniform matrix class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <GL/glew.h>

namespace sx {

	UniformDMatrix::UniformDMatrix(const string &id): Uniform(id), DMatrix() {
		
	}

	DMatrix &UniformDMatrix::operator = (const DMatrix &m) {
		return DMatrix::operator = (m);
	}

	void UniformDMatrix::load() {
	}

	bool UniformDMatrix::isLoaded() const {
		return true;
	}

	void UniformDMatrix::use(Shader &shader, const string &identifyer) {
		if(!shader.isLoaded()) {
			//shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(identifyer);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			glUniformMatrix4dv(loc,1,GL_FALSE,(GLdouble *)elements);
		}
	}

}

#endif