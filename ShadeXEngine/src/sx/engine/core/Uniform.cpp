#ifndef _SX_ENGINE_CORE_UNIFORM_CPP_
#define _SX_ENGINE_CORE_UNIFORM_CPP_

/**
 * Uniform variable class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <boost/tokenizer.hpp>
#include <boost/foreach.hpp>
using namespace boost;

namespace sx {

	Uniform::Uniform(const string &id) {
		this->id = id;
		char_separator<char> separator(".");
		tokenizer<char_separator<char>> tokens(id,separator);
		BOOST_FOREACH(const string &token,tokens) {
			//use last token as idToken
			idToken = token;
		}
	}

	Uniform::~Uniform() {
	}

	void Uniform::setUniformName(const string &name, const string &id) {
		uniformNames[id] = name;
	}

	const string &Uniform::getUniformName(const string &id) const {
		unordered_map<string,string>::const_iterator iter = uniformNames.find(id);
		if(iter != uniformNames.end()) {
			return (*iter).second;
		}
		//no uniform name set,
		//hence the name is the last token
		//of the id
		return idToken;
	}

}

#endif