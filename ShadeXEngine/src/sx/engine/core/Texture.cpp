#ifndef _SX_ENGINE_CORE_TEXTURE_CPP_
#define _SX_ENGINE_CORE_TEXTURE_CPP_

/**
 * 2d texture class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <GL/glew.h>

namespace sx {

	Texture::Texture(const string &id): Uniform(id) {
		loaded = false;
		newWidth = -1;
		newHeight = -1;
		newPixelFormat = FLOAT_RGBA;
		width = -1;
		height = -1;
		pixelFormat = FLOAT_RGBA;
	}

	Texture::~Texture() {
		unload();
	}

	void Texture::unload() {
		if(loaded) {
			glDeleteTextures(1,&texture);
			loaded = false;
		}
	}

	void Texture::load() {
		if(newPath.size() == 0 && (newWidth <= 0 || newHeight <= 0)) {
			//no changes specified, hence don't
			//do anything
			return;
		}
		unload();
		if(newPath.size() > 0) {
			//a path has been specified
			//, load image from path
			try {
				Bitmap image(newPath);
				width = (int)image.getWidth();
				height = (int)image.getHeight();
				if(image.getPixelSize() == 4) {
					pixelFormat = BYTE_RGBA;
				} else if(image.getPixelSize() == 16) {
					pixelFormat = FLOAT16_RGBA;
				} else {
					throw Exception();
				}
				//upload data to vram
				glEnable(GL_TEXTURE_2D);
				glGenTextures(1,(GLuint *)&texture);
				glBindTexture(GL_TEXTURE_2D,texture);
				if(image.getPixelSize() == 4) {
					unsigned char *data = image.getData();
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
				} else if(image.getPixelSize() == 16) {
					float *data = (float *)image.getData();
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA16F,width,height,0,GL_RGBA,GL_FLOAT,data);
				}
				glGenerateMipmap(GL_TEXTURE_2D);
				glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
				GLfloat aLargest;
				glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&aLargest);
				glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,aLargest);
				glBindTexture(GL_TEXTURE_2D,0);
				loaded = true;
				//clear resource requests
				newPath = "";
				newWidth = newHeight = -1;
				vector<unsigned char> shrinkByte;
				vector<float> shrinkFloat;
				newByteBuffer.clear();
				newByteBuffer.swap(shrinkByte);
				newFloatBuffer.clear();
				newFloatBuffer.swap(shrinkFloat);
				return;
			} catch(Exception &) {
				//image not loaded
				//clear resource requests
				newPath = "";
				width = height = newWidth = newHeight = -1;
				newByteBuffer.clear();
				newFloatBuffer.clear();
				return;
			}
		} else {
			//create image with specified width and height
			width = newWidth;
			height = newHeight;
			pixelFormat = newPixelFormat;
			//upload data to vram
			glEnable(GL_TEXTURE_2D);
			glGenTextures(1,(GLuint *)&texture);
			glBindTexture(GL_TEXTURE_2D,texture);
			if(pixelFormat == BYTE_RGBA) {
				//texture with color channels made of unsigned bytes
				if(newByteBuffer.size() == width * height * 4) {
					//byte buffer's size exactly fits texture format
					//, hence use byte buffer as data source
					unsigned char *data = new unsigned char[width * height * 4];
					unsigned int index = 0;
					for(vector<unsigned char>::iterator iter = newByteBuffer.begin() ; iter != newByteBuffer.end() ; iter++) {
						data[index] = (*iter);
						index++;
					}
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
					delete data;
				} else {
					//byte buffer's size doesn't fit texture format
					//, hence create a texture without uploading data
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,0);
				}
			} else if(pixelFormat == FLOAT16_RGBA) {
				//texture with color channels made of 16 bit floating point numbers
				if(newFloatBuffer.size() == width * height * 4) {
					//float buffer's size exactly fits texture format
					//, hence use byte buffer as data source
					float *data = new float[width * height * 4];
					unsigned int index = 0;
					for(vector<float>::iterator iter = newFloatBuffer.begin() ; iter != newFloatBuffer.end() ; iter++) {
						data[index] = (*iter);
						index++;
					}
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA16F,width,height,0,GL_RGBA,GL_FLOAT,data);
					delete data;
				} else {
					//float buffer's size doesn't fit texture format
					//, hence create a texture without uploading data
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA16F,width,height,0,GL_RGBA,GL_FLOAT,0);
				}
			} else if(pixelFormat == FLOAT_RGBA) {
				//texture with color channels made of floating point numbers
				if(newFloatBuffer.size() == width * height * 4) {
					//float buffer's size exactly fits texture format
					//, hence use byte buffer as data source
					float *data = new float[width * height * 4];
					unsigned int index = 0;
					for(vector<float>::iterator iter = newFloatBuffer.begin() ; iter != newFloatBuffer.end() ; iter++) {
						data[index] = (*iter);
						index++;
					}
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA32F,width,height,0,GL_RGBA,GL_FLOAT,data);
					delete data;
				} else {
					//float buffer's size doesn't fit texture format
					//, hence create a texture without uploading data
					glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA32F,width,height,0,GL_RGBA,GL_FLOAT,0);
				}
			}
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			GLfloat aLargest;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&aLargest);
			glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,aLargest);
			glBindTexture(GL_TEXTURE_2D,0);
			loaded = true;
			//clear resource requests
			newPath = "";
			newWidth = newHeight = -1;
			vector<unsigned char> shrinkByte;
			vector<float> shrinkFloat;
			newByteBuffer.clear();
			newByteBuffer.swap(shrinkByte);
			newFloatBuffer.clear();
			newFloatBuffer.swap(shrinkFloat);
			return;
		}
	}

	bool Texture::isLoaded() const {
		return loaded;
	}

	void Texture::save(const string &filename) {
		if(!loaded) {
			//texture has to be loaded
			stringstream errmsg;
			errmsg << "Error: Texture " << id << " can't be saved in file " << filename << ", because the texture is not loaded" << endl;
			throw Exception(errmsg.str(),EX_NODATA);
		}

		glBindTexture(GL_TEXTURE_2D,texture);
		if(pixelFormat == BYTE_RGBA) {
			unsigned char *data = new unsigned char[width * height * 4];
			glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
			Bitmap image(width,height);
			unsigned char *img = image.getData();
			for(unsigned int i=0 ; i<(unsigned int)width*height*4 ; i++) {
				img[i] = data[i];
			}
			try {
				image.save(filename);
			} catch(Exception &e) {
				delete data;
				throw e;
			}
			delete data;
		} else if(pixelFormat == FLOAT16_RGBA || pixelFormat == FLOAT_RGBA) {
			float *data = new float[width * height * 4];
			glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA,GL_FLOAT,data);
			Bitmap image(width,height,16);
			float *img = (float *)image.getData();
			for(unsigned int i=0 ; i<(unsigned int)width*height*4 ; i++) {
				img[i] = data[i];
			}
			try {
				image.save(filename);
			} catch(Exception &e) {
				delete data;
				throw e;
			}
			delete data;
		}
	}

	void Texture::use(Shader &shader, const string &id) {
		if(!loaded || !shader.isLoaded()) {
			//texture or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(id);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			//use next free slot for the texture (multitexturing requires
			//the textures used simultaneously being associated with unique slots
			//... sadly :(, simply binding them as uniform variables
			//without the redundant slot mechanism would be more elegant)
			unsigned int slot = shader.createTextureSlot();
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindTexture(GL_TEXTURE_2D,texture);
			glUniform1i(loc,slot);
		}
	}

	unsigned int Texture::getTextureID() const {
		return texture;
	}

	void Texture::setPath(const string &path) {
		newPath = path;
	}

	void Texture::setWidth(int width) {
		newWidth = width;
	}

	int Texture::getWidth() const {
		return width;
	}

	void Texture::setHeight(int height) {
		newHeight = height;
	}

	int Texture::getHeight() const {
		return height;
	}

	void Texture::setPixelFormat(PixelFormat pixelFormat) {
		newPixelFormat = pixelFormat;
	}

	PixelFormat Texture::getPixelFormat() const {
		return pixelFormat;
	}

	void Texture::setByteBuffer(const vector<unsigned char> &buffer) {
		newByteBuffer.clear();
		for(vector<unsigned char>::const_iterator iter = buffer.begin() ; iter != buffer.end() ; iter++) {
			newByteBuffer.push_back((*iter));
		}
		newPixelFormat = BYTE_RGBA;
	}

	void Texture::setFloatBuffer(const vector<float> &buffer) {
		newFloatBuffer.clear();
		newFloatBuffer.reserve(buffer.size());
		for(vector<float>::const_iterator iter = buffer.begin() ; iter != buffer.end() ; iter++) {
			newFloatBuffer.push_back((*iter));
		}
		newPixelFormat = FLOAT_RGBA;
	}

}

#endif