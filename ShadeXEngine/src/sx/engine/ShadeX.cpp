#ifndef _ENGINE_SHADEX_CPP_
#define _ENGINE_SHADEX_CPP_

/**
 * ShadeX class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <sx/SXParser.h>
#include <sstream>
#include <cmath>
#include <boost/foreach.hpp>
using namespace std;
using namespace boost;

namespace sx {

	ShadeX::ShadeX() {
	}

	ShadeX::~ShadeX() {
		clear();
	}

	void ShadeX::clear(const std::string &id) {
		unordered_map<string,SXResource *>::iterator iter = resources.find(id);
		if(iter == resources.end()) {
			//error, resource not found
			stringstream errmsg;
			errmsg << "Error: resource " << id << " can't be deleted, because it wasn't found";
			throw Exception(errmsg.str(),EX_NODATA);
		}
		delete (*iter).second;
		resources.erase(iter);
		unordered_map<string, Shader *>::iterator iterShader = shaders.find(id);
		if(iterShader != shaders.end()) {
			shaders.erase(iterShader);
		}
		unordered_map<string, BufferedMesh *>::iterator iterBMesh = bufferedMeshes.find(id);
		if(iterBMesh != bufferedMeshes.end()) {
			bufferedMeshes.erase(iterBMesh);
			//remove uniform variables attached to the mesh
			unordered_map<string,BufferUniforms>::iterator iterUBMesh = bufferUniforms.find(id);
			if(iterUBMesh != bufferUniforms.end()) {
				bufferUniforms.erase(iterUBMesh);
			}
		}
		unordered_map<string, Skeleton *>::iterator iterSkeleton = skeletons.find(id);
		if(iterSkeleton != skeletons.end()) {
			skeletons.erase(iterSkeleton);
		}
		unordered_map<string, UniformFloat *>::iterator iterUFloat = uniformFloats.find(id);
		if(iterUFloat != uniformFloats.end()) {
			uniformFloats.erase(iterUFloat);
		}
		unordered_map<string, UniformDouble *>::iterator iterUDouble = uniformDoubles.find(id);
		if(iterUDouble != uniformDoubles.end()) {
			uniformDoubles.erase(iterUDouble);
		}
		unordered_map<string, UniformVector *>::iterator iterUVector = uniformVectors.find(id);
		if(iterUVector != uniformVectors.end()) {
			uniformVectors.erase(iterUVector);
		}
		unordered_map<string, UniformDVector *>::iterator iterUDVector = uniformDVectors.find(id);
		if(iterUDVector != uniformDVectors.end()) {
			uniformDVectors.erase(iterUDVector);
		}
		unordered_map<string, UniformMatrix *>::iterator iterUMatrix = uniformMatrices.find(id);
		if(iterUMatrix != uniformMatrices.end()) {
			uniformMatrices.erase(iterUMatrix);
		}
		unordered_map<string, UniformDMatrix *>::iterator iterUDMatrix = uniformDMatrices.find(id);
		if(iterUDMatrix != uniformDMatrices.end()) {
			uniformDMatrices.erase(iterUDMatrix);
		}
		unordered_map<string, Texture *>::iterator iterTexture = textures.find(id);
		if(iterTexture != textures.end()) {
			textures.erase(iterTexture);
		}
		unordered_map<string, Volume *>::iterator iterVolume = volumes.find(id);
		if(iterVolume != volumes.end()) {
			volumes.erase(iterVolume);
		}
		unordered_map<string, RenderTarget *>::iterator iterRTarget = renderTargets.find(id);
		if(iterRTarget != renderTargets.end()) {
			renderTargets.erase(iterRTarget);
		}
		unordered_map<string, RenderObject *>::iterator iterRObject = renderObjects.find(id);
		if(iterRObject != renderObjects.end()) {
			renderObjects.erase(iterRObject);
		}
		unordered_map<string, Pass *>::iterator iterPass = passes.find(id);
		if(iterPass != passes.end()) {
			passes.erase(iterPass);
		}
		unordered_map<string, Effect *>::iterator iterEffect = effects.find(id);
		if(iterEffect != effects.end()) {
			effects.erase(iterEffect);
		}
		unordered_map<string,AudioBuffer *>::iterator iterABuffer = audioBuffers.find(id);
		if(iterABuffer != audioBuffers.end()) {
			audioBuffers.erase(iterABuffer);
		}
		unordered_map<string,AudioObject *>::iterator iterAObject = audioObjects.find(id);
		if(iterAObject != audioObjects.end()) {
			audioObjects.erase(iterAObject);
		}
		unordered_map<string,AudioListener *>::iterator iterAListener = audioListeners.find(id);
		if(iterAListener != audioListeners.end()) {
			audioListeners.erase(iterAListener);
		}
		unordered_map<string,AudioPass *>::iterator iterAPass = audioPasses.find(id);
		if(iterAPass != audioPasses.end()) {
			audioPasses.erase(iterAPass);
		}

		SXout(LogMarkup("ShadeX::clear()/resources"));
		SXout(resources.size());
		SXout(LogMarkup("ShadeX::clear()/shaders"));
		SXout(shaders.size());
		SXout(LogMarkup("ShadeX::clear()/bufferedMeshes"));
		SXout(bufferedMeshes.size());
		SXout(LogMarkup("ShadeX::clear()/skeletons"));
		SXout(skeletons.size());
		SXout(LogMarkup("ShadeX::clear()/uniformFloats"));
		SXout(uniformFloats.size());
		SXout(LogMarkup("ShadeX::clear()/uniformDoubles"));
		SXout(uniformDoubles.size());
		SXout(LogMarkup("ShadeX::clear()/uniformVectors"));
		SXout(uniformVectors.size());
		SXout(LogMarkup("ShadeX::clear()/uniformDVectors"));
		SXout(uniformDVectors.size());
		SXout(LogMarkup("ShadeX::clear()/uniformMatrices"));
		SXout(uniformMatrices.size());
		SXout(LogMarkup("ShadeX::clear()/uniformDMatrices"));
		SXout(uniformDMatrices.size());
		SXout(LogMarkup("ShadeX::clear()/textures"));
		SXout(textures.size());
		SXout(LogMarkup("ShadeX::clear()/volumes"));
		SXout(volumes.size());
		SXout(LogMarkup("ShadeX::clear()/renderTargets"));
		SXout(renderTargets.size());
		SXout(LogMarkup("ShadeX::clear()/renderObjects"));
		SXout(renderObjects.size());
		SXout(LogMarkup("ShadeX::clear()/passes"));
		SXout(passes.size());
		SXout(LogMarkup("ShadeX::clear()/effects"));
		SXout(effects.size());
		SXout(LogMarkup("ShadeX::clear()/bufferUniforms"));
		SXout(bufferUniforms.size());
		SXout(LogMarkup("ShadeX::clear()/audioBuffers"));
		SXout(audioBuffers.size());
		SXout(LogMarkup("ShadeX::clear()/audioObjects"));
		SXout(audioObjects.size());
		SXout(LogMarkup("ShadeX::clear()/audioListeners"));
		SXout(audioListeners.size());
		SXout(LogMarkup("ShadeX::clear()/audioPasses"));
		SXout(audioPasses.size());
	}

	void ShadeX::clear() {
		for(unordered_map<string,SXResource *>::iterator iter = resources.begin() ; iter != resources.end() ; iter++) {
			delete (*iter).second;
		}
		resources.clear();
		shaders.clear();
		bufferedMeshes.clear();
		skeletons.clear();
		uniformFloats.clear();
		uniformDoubles.clear();
		uniformVectors.clear();
		uniformDVectors.clear();
		uniformMatrices.clear();
		uniformDMatrices.clear();
		textures.clear();
		volumes.clear();
		renderTargets.clear();
		renderObjects.clear();
		passes.clear();
		effects.clear();
		audioBuffers.clear();
		audioObjects.clear();
		audioListeners.clear();
		audioPasses.clear();

		bufferUniforms.clear();

		SXout(LogMarkup("ShadeX::clear()/resources"));
		SXout(resources.size());
		SXout(LogMarkup("ShadeX::clear()/shaders"));
		SXout(shaders.size());
		SXout(LogMarkup("ShadeX::clear()/bufferedMeshes"));
		SXout(bufferedMeshes.size());
		SXout(LogMarkup("ShadeX::clear()/skeletons"));
		SXout(skeletons.size());
		SXout(LogMarkup("ShadeX::clear()/uniformFloats"));
		SXout(uniformFloats.size());
		SXout(LogMarkup("ShadeX::clear()/uniformDoubles"));
		SXout(uniformDoubles.size());
		SXout(LogMarkup("ShadeX::clear()/uniformVectors"));
		SXout(uniformVectors.size());
		SXout(LogMarkup("ShadeX::clear()/uniformDVectors"));
		SXout(uniformDVectors.size());
		SXout(LogMarkup("ShadeX::clear()/uniformMatrices"));
		SXout(uniformMatrices.size());
		SXout(LogMarkup("ShadeX::clear()/uniformDMatrices"));
		SXout(uniformDMatrices.size());
		SXout(LogMarkup("ShadeX::clear()/textures"));
		SXout(textures.size());
		SXout(LogMarkup("ShadeX::clear()/volumes"));
		SXout(volumes.size());
		SXout(LogMarkup("ShadeX::clear()/renderTargets"));
		SXout(renderTargets.size());
		SXout(LogMarkup("ShadeX::clear()/renderObjects"));
		SXout(renderObjects.size());
		SXout(LogMarkup("ShadeX::clear()/passes"));
		SXout(passes.size());
		SXout(LogMarkup("ShadeX::clear()/effects"));
		SXout(effects.size());
		SXout(LogMarkup("ShadeX::clear()/audioBuffers"));
		SXout(audioBuffers.size());
		SXout(LogMarkup("ShadeX::clear()/audioObjects"));
		SXout(audioObjects.size());
		SXout(LogMarkup("ShadeX::clear()/audioListeners"));
		SXout(audioListeners.size());
		SXout(LogMarkup("ShadeX::clear()/audioPasses"));
		SXout(audioPasses.size());
		SXout(LogMarkup("ShadeX::clear()/bufferUniforms"));
		SXout(bufferUniforms.size());
	}

	void ShadeX::INTERNAL_LOAD_SHADER(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every shadernode loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a shader without an id",EX_COMPILE);
		}
		Shader *shader = 0;
		unordered_map<string,Shader *>::iterator iter = shaders.find(id);
		if(iter == shaders.end()) {
			//make shure no other resources have the same identifyer
			//then create a shader
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a shader has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			shader = new Shader(id);
			shaders[id] = shader;
			resources[id] = shader;
		} else {
			shader = (*iter).second;
		}
		//load shader parts
		try {
			string vertex = rNode->getStrAttribute("vertex");
			shader->addShaderFile(vertex,VERTEX);
		} catch(Exception &) {
		}
		try {
			string tessellation_control = rNode->getStrAttribute("tessellation_control");
			shader->addShaderFile(tessellation_control,TESSELLATION_CONTROL);
		} catch(Exception &) {
		}
		try {
			string tessellation_evaluation = rNode->getStrAttribute("tessellation_evaluation");
			shader->addShaderFile(tessellation_evaluation,TESSELLATION_EVALUATION);
		} catch(Exception &) {
		}
		try {
			string geometry = rNode->getStrAttribute("geometry");
			shader->addShaderFile(geometry,GEOMETRY);
		} catch(Exception &) {
		}
		try {
			string fragment = rNode->getStrAttribute("fragment");
			shader->addShaderFile(fragment,FRAGMENT);
		} catch(Exception &) {
		}

		XTag *vertex = 0;
		try {
			vertex = rNode->getFirst("vertex");
		} catch(Exception &) {
		}
		if(vertex != 0) {
			string code = vertex->getTexts();
			shader->addShader(code,VERTEX);
		}
		XTag *tessellation_control = 0;
		try {
			tessellation_control = rNode->getFirst("tessellation_control");
		} catch(Exception &) {
		}
		if(tessellation_control != 0) {
			string code = tessellation_control->getTexts();
			shader->addShader(code,TESSELLATION_CONTROL);
		}
		XTag *tessellation_evaluation = 0;
		try {
			tessellation_evaluation = rNode->getFirst("tessellation_evaluation");
		} catch(Exception &) {
		}
		if(tessellation_evaluation != 0) {
			string code = tessellation_evaluation->getTexts();
			shader->addShader(code,TESSELLATION_EVALUATION);
		}
		XTag *geometry = 0;
		try {
			geometry = rNode->getFirst("geometry");
		} catch(Exception &) {
		}
		if(geometry != 0) {
			string code = geometry->getTexts();
			shader->addShader(code,GEOMETRY);
		}
		XTag *fragment = 0;
		try {
			fragment = rNode->getFirst("fragment");
		} catch(Exception &) {
		}
		if(fragment != 0) {
			string code = fragment->getTexts();
			shader->addShader(code,FRAGMENT);
		}
		XTag *transformfeedback = 0;
		try {
			transformfeedback = rNode->getFirst("transformfeedback");
		} catch(Exception &) {
		}
		if(transformfeedback != 0) {
			//collect all transform feedback varying buffer names
			vector<string> buffers;
			transformfeedback->getTexts(buffers);
			BOOST_FOREACH(const string &buffer , buffers) {
				shader->addTransformFeedbackBuffer(buffer);
			}
		}
		//errorcheck
		//search for attributes and tags, which have wrong names
		map<string,int> strControl;
		strControl["id"] = 0;
		strControl["vertex"] = 0;
		strControl["tessellation_control"] = 0;
		strControl["tessellation_evaluation"] = 0;
		strControl["geometry"] = 0;
		strControl["fragment"] = 0;
		for(map<string,string>::iterator strIter = rNode->stringAttribs.begin() ; strIter != rNode->stringAttribs.end() ; strIter++) {
			const string &tName = (*strIter).first;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute " << tName << " found in Shader " << id << ", but attributes must be called \"vertex\", \"tessellation_control\", \"tessellation_evaluation\", \"geometry\" or \"fragment\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		//performing errorcheck for tags
		map<string,int>::iterator delControlIter = strControl.find("id");
		strControl.erase(delControlIter);
		strControl["transformfeedback"] = 0;
		for(vector<XNode *>::iterator nIter = rNode->nodes.begin() ; nIter != rNode->nodes.end() ; nIter++) {
			XTag *cTag = dynamic_cast<XTag *>((*nIter));
			if(cTag == 0) {
				//not a tag, skip this element
				continue;
			}
			const string &tName = cTag->name;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//tag with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: tag " << tName << " found in Shader " << id << ", but tags must be called \"vertex\", \"tessellation_control\", \"tessellation_evaluation\", \"geometry\", \"fragment\" or \"transformfeedback\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_MATRIXCONTENT(Matrix &matrix, const string &id, XTag *rNode, XTag *data) {
		vector<XNode *> &nodes = rNode->nodes;
		//iterate through nodes until
		//components are detected,
		//such that the explicitly specified
		//matrix can be multiplied with the other
		//matrices in the correct place
		bool awaitingComponents = true;
		for(vector<XNode *>::iterator nodeIter = nodes.begin() ; nodeIter != nodes.end() ; nodeIter++) {
			XNode *node = (*nodeIter);
			XTag *tag = dynamic_cast<XTag *>(node);
			XText *text = dynamic_cast<XText *>(node);

			if(text != 0 && awaitingComponents) {
				//found matrix components
				//collect all components
				awaitingComponents = false;
				Matrix m;

				vector<double> values;
				vector<string> texts;
				rNode->getDirectTexts(texts);
				stringstream concTexts;
				for(vector<string>::iterator iter = texts.begin() ; iter != texts.end() ; iter++) {
					//separate numbers by empty space
					concTexts << " " << (*iter);
				}
				try {
					values = parseNumbers(concTexts.str());
				} catch(Exception &) {
					//list of matrix componentes
					// has incorrect syntax
					delete data;
					stringstream str;
					str << "Error: matrix value of object " << id << " has syntax error";
					throw Exception(str.str(),EX_SYNTAX);
				}
				if(values.size() == 0) {
					//no components specified
					//assume identity matrix
					m.identity();
				} else if(values.size() == 1) {
					m = Matrix((float)values[0]);
				} else if(values.size() == 4) {
					m = Matrix(
						(float)values[0],	(float)values[1],
						(float)values[2],	(float)values[3]
					);
				} else if(values.size() == 9) {
					m = Matrix(
						(float)values[0],	(float)values[1],	(float)values[2],
						(float)values[3],	(float)values[4],	(float)values[5],
						(float)values[6],	(float)values[7],	(float)values[8]
					);
				} else if(values.size() == 16) {
					m = Matrix(
						(float)values[0],	(float)values[1],	(float)values[2],	(float)values[3],
						(float)values[4],	(float)values[5],	(float)values[6],	(float)values[7],
						(float)values[8],	(float)values[9],	(float)values[10],	(float)values[11],
						(float)values[12],	(float)values[13],	(float)values[14],	(float)values[15]
					);
				} else {
					//wrong number of components
					delete data;
					stringstream errmsg;
					errmsg << "Error: invalid number of components of matrix in object " << id;
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				//multiply matrix with m
				matrix = matrix * m;
			} else if(tag != 0 && tag->name.compare("matrix") == 0) {
				//explicitly specify components of matrix
				Matrix m;
				vector<double> values;
				vector<string> texts;
				tag->getDirectTexts(texts);
				stringstream concTexts;
				for(vector<string>::iterator iter = texts.begin() ; iter != texts.end() ; iter++) {
					//separate numbers by empty space
					concTexts << " " << (*iter);
				}
				try {
					values = parseNumbers(concTexts.str());
				} catch(Exception &) {
					//list of matrix componentes
					// has incorrect syntax
					delete data;
					stringstream str;
					str << "Error: matrix value of object " << id << " has syntax error";
					throw Exception(str.str(),EX_SYNTAX);
				}
				if(values.size() == 0) {
					//no components specified
					//assume identity matrix
					m.identity();
				} else if(values.size() == 1) {
					m = Matrix((float)values[0]);
				} else if(values.size() == 4) {
					m = Matrix(
						(float)values[0],	(float)values[1],
						(float)values[2],	(float)values[3]
					);
				} else if(values.size() == 9) {
					m = Matrix(
						(float)values[0],	(float)values[1],	(float)values[2],
						(float)values[3],	(float)values[4],	(float)values[5],
						(float)values[6],	(float)values[7],	(float)values[8]
					);
				} else if(values.size() == 16) {
					m = Matrix(
						(float)values[0],	(float)values[1],	(float)values[2],	(float)values[3],
						(float)values[4],	(float)values[5],	(float)values[6],	(float)values[7],
						(float)values[8],	(float)values[9],	(float)values[10],	(float)values[11],
						(float)values[12],	(float)values[13],	(float)values[14],	(float)values[15]
					);
				} else {
					//wrong number of components
					delete data;
					stringstream errmsg;
					errmsg << "Error: invalid number of components of matrix in object " << id;
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				//multiply matrix with m
				matrix = matrix * m;
			} else if(tag != 0 && tag->name.compare("rotate") == 0) {
				//rotation matrix around a vector
				//for a certain angle in rad
				Vector v;
				float angle = 0.0f;
				INTERNAL_LOAD_VECTORCONTENT(v,id,tag,data);
				if(v.length() == 0) {
					//rotation vector must be non-zero
					delete data;
					stringstream errmsg;
					errmsg << "Error: rotationvector in matrix of object " << id << " must not be the null vector";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				try {
					angle = (float)tag->getRealAttribute("angle");
				} catch(Exception &) {
					//attribute angle not found
					delete data;
					stringstream errmsg;
					errmsg << "Error: rotation matrix of object " << id << " must have an angle attribute";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				matrix = matrix * Matrix().rotate(v,angle);
			} else if(tag != 0 && tag->name.compare("translate") == 0) {
				//translation matrix along translation vector
				Vector v;
				INTERNAL_LOAD_VECTORCONTENT(v,id,tag,data);
				matrix = matrix * Matrix().translate(v);
			} else if(tag != 0 && tag->name.compare("scale") == 0) {
				//scale matrix with scale vector in its diagonal
				Vector v;
				INTERNAL_LOAD_VECTORCONTENT(v,id,tag,data);
				matrix = matrix * Matrix().scale(v);
			} else if(tag != 0 && tag->name.compare("shear") == 0) {
				//shear matrix along a shear vector
				Vector v;
				INTERNAL_LOAD_VECTORCONTENT(v,id,tag,data);
				matrix = matrix * Matrix().shear(v);
			} else if(tag != 0 && tag->name.compare("view") == 0) {
				//view matrix
				XTag *positionTag = 0;
				XTag *viewTag = 0;
				XTag *upTag = 0;
				Vector position;
				Vector view;
				Vector up;
				try {
					positionTag = tag->getFirst("position");
					viewTag = tag->getFirst("view");
					upTag = tag->getFirst("up");
				} catch(Exception &) {
					//view-projection specification must contain
					//tags position, view and up
					delete data;
					stringstream errmsg;
					errmsg << "Error: view-projection matrix of object " << id << " must contain tags position, view and up";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				INTERNAL_LOAD_VECTORCONTENT(position,id,positionTag,data);
				INTERNAL_LOAD_VECTORCONTENT(view,id,viewTag,data);
				INTERNAL_LOAD_VECTORCONTENT(up,id,upTag,data);
				matrix = matrix * Matrix().viewMatrix(position,view,up);
			} else if(tag != 0 && tag->name.compare("orthographic") == 0) {
				//orthographic projection matrix
				float left = 0.0f;
				float right = 0.0f;
				float bottom = 0.0f;
				float top = 0.0f;
				float znear = 0.0f;
				float zfar = 0.0f;
				try {
					left = (float)tag->getRealAttribute("left");
					right = (float)tag->getRealAttribute("right");
					bottom = (float)tag->getRealAttribute("bottom");
					top = (float)tag->getRealAttribute("top");
					znear = (float)tag->getRealAttribute("znear");
					zfar = (float)tag->getRealAttribute("zfar");
				} catch(Exception &) {
					//orthographic projection matrix must contain attributes
					//left, right, bottom, top, znear, zfar
					delete data;
					stringstream errmsg;
					errmsg << "Error: orthographic projection matrix in object " << id << " must contain attributes left, right, bottom, top, znear, zfar";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				matrix = matrix * Matrix().orthographicPerspeciveMatrix(left,right,bottom,top,znear,zfar);
			} else if(tag != 0 && tag->name.compare("perspective") == 0) {
				//perspective projection matrix
				float angle = 0.0f;
				float width = 0.0f;
				float height = 0.0f;
				float znear = 0.0f;
				float zfar = 0.0f;
				try {
					angle = (float)tag->getRealAttribute("angle");
					width = (float)tag->getRealAttribute("width");
					height = (float)tag->getRealAttribute("height");
					znear = (float)tag->getRealAttribute("znear");
					zfar = (float)tag->getRealAttribute("zfar");
				} catch(Exception &) {
					//perspective projection matrix must contain attributes
					//angle, width, height, znear, zfar
					delete data;
					stringstream errmsg;
					errmsg << "Error: orthographic projection matrix in object " << id << " must contain attributes angle, width, height, znear, zfar";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				matrix = matrix * Matrix().perspectiveMatrix(angle,width,height,znear,zfar);
			} else if(tag != 0) {
				//incorrect tag detected
				delete data;
				stringstream errmsg;
				errmsg << "Error: incorrect tag in matrix of object " << id << " detected, must have name \"matrix\", \"rotate\", \"translate\", \"scale\", \"shear\", \"orthographic\", \"view\" or \"perspective\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_DMATRIXCONTENT(DMatrix &matrix, const string &id, XTag *rNode, XTag *data) {
		vector<XNode *> &nodes = rNode->nodes;
		//iterate through nodes until
		//components are detected,
		//such that the explicitly specified
		//matrix can be multiplied with the other
		//matrices in the correct place
		bool awaitingComponents = true;
		for(vector<XNode *>::iterator nodeIter = nodes.begin() ; nodeIter != nodes.end() ; nodeIter++) {
			XNode *node = (*nodeIter);
			XTag *tag = dynamic_cast<XTag *>(node);
			XText *text = dynamic_cast<XText *>(node);

			if(text != 0 && awaitingComponents) {
				//found matrix components
				//collect all components
				awaitingComponents = false;
				DMatrix m;

				vector<double> values;
				vector<string> texts;
				rNode->getDirectTexts(texts);
				stringstream concTexts;
				for(vector<string>::iterator iter = texts.begin() ; iter != texts.end() ; iter++) {
					//separate numbers by empty space
					concTexts << " " << (*iter);
				}
				try {
					values = parseNumbers(concTexts.str());
				} catch(Exception &) {
					//list of matrix componentes
					// has incorrect syntax
					delete data;
					stringstream str;
					str << "Error: dmatrix value of object " << id << " has syntax error";
					throw Exception(str.str(),EX_SYNTAX);
				}
				if(values.size() == 0) {
					//no components specified
					//assume identity matrix
					m.identity();
				} else if(values.size() == 1) {
					m = DMatrix(values[0]);
				} else if(values.size() == 4) {
					m = DMatrix(
						values[0],	values[1],
						values[2],	values[3]
					);
				} else if(values.size() == 9) {
					m = DMatrix(
						values[0],	values[1],	values[2],
						values[3],	values[4],	values[5],
						values[6],	values[7],	values[8]
					);
				} else if(values.size() == 16) {
					m = DMatrix(
						values[0],	values[1],	values[2],	values[3],
						values[4],	values[5],	values[6],	values[7],
						values[8],	values[9],	values[10],	values[11],
						values[12],	values[13],	values[14],	values[15]
					);
				} else {
					//wrong number of components
					delete data;
					stringstream errmsg;
					errmsg << "Error: invalid number of components of dmatrix in object " << id;
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				//multiply matrix with m
				matrix = matrix * m;
			} else if(tag != 0 && tag->name.compare("matrix") == 0) {
				//explicitly specify components of matrix
				DMatrix m;
				vector<double> values;
				vector<string> texts;
				tag->getDirectTexts(texts);
				stringstream concTexts;
				for(vector<string>::iterator iter = texts.begin() ; iter != texts.end() ; iter++) {
					//separate numbers by empty space
					concTexts << " " << (*iter);
				}
				try {
					values = parseNumbers(concTexts.str());
				} catch(Exception &) {
					//list of matrix componentes
					// has incorrect syntax
					delete data;
					stringstream str;
					str << "Error: dmatrix value of object " << id << " has syntax error";
					throw Exception(str.str(),EX_SYNTAX);
				}
				if(values.size() == 0) {
					//no components specified
					//assume identity matrix
					m.identity();
				} else if(values.size() == 1) {
					m = DMatrix(values[0]);
				} else if(values.size() == 4) {
					m = DMatrix(
						values[0],	values[1],
						values[2],	values[3]
					);
				} else if(values.size() == 9) {
					m = DMatrix(
						values[0],	values[1],	values[2],
						values[3],	values[4],	values[5],
						values[6],	values[7],	values[8]
					);
				} else if(values.size() == 16) {
					m = DMatrix(
						values[0],	values[1],	values[2],	values[3],
						values[4],	values[5],	values[6],	values[7],
						values[8],	values[9],	values[10],	values[11],
						values[12],	values[13],	values[14],	values[15]
					);
				} else {
					//wrong number of components
					delete data;
					stringstream errmsg;
					errmsg << "Error: invalid number of components of dmatrix in object " << id;
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				//multiply matrix with m
				matrix = matrix * m;
			} else if(tag != 0 && tag->name.compare("rotate") == 0) {
				//rotation matrix around a vector
				//for a certain angle in rad
				DVector v;
				double angle = 0.0;
				INTERNAL_LOAD_DVECTORCONTENT(v,id,tag,data);
				if(v.length() == 0) {
					//rotation vector must be non-zero
					delete data;
					stringstream errmsg;
					errmsg << "Error: rotationvector in dmatrix of object " << id << " must not be the null vector";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				try {
					angle = tag->getRealAttribute("angle");
				} catch(Exception &) {
					//attribute angle not found
					delete data;
					stringstream errmsg;
					errmsg << "Error: rotation dmatrix of object " << id << " must have an angle attribute";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				matrix = matrix * DMatrix().rotate(v,angle);
			} else if(tag != 0 && tag->name.compare("translate") == 0) {
				//translation matrix along translation vector
				DVector v;
				INTERNAL_LOAD_DVECTORCONTENT(v,id,tag,data);
				matrix = matrix * DMatrix().translate(v);
			} else if(tag != 0 && tag->name.compare("scale") == 0) {
				//scale matrix with scale vector in its diagonal
				DVector v;
				INTERNAL_LOAD_DVECTORCONTENT(v,id,tag,data);
				matrix = matrix * DMatrix().scale(v);
			} else if(tag != 0 && tag->name.compare("shear") == 0) {
				//shear matrix along a shear vector
				DVector v;
				INTERNAL_LOAD_DVECTORCONTENT(v,id,tag,data);
				matrix = matrix * DMatrix().shear(v);
			} else if(tag != 0 && tag->name.compare("view") == 0) {
				//view matrix
				XTag *positionTag = 0;
				XTag *viewTag = 0;
				XTag *upTag = 0;
				DVector position;
				DVector view;
				DVector up;
				try {
					positionTag = tag->getFirst("position");
					viewTag = tag->getFirst("view");
					upTag = tag->getFirst("up");
				} catch(Exception &) {
					//view-projection specification must contain
					//tags position, view and up
					delete data;
					stringstream errmsg;
					errmsg << "Error: view-projection dmatrix of object " << id << " must contain tags position, view and up";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				INTERNAL_LOAD_DVECTORCONTENT(position,id,positionTag,data);
				INTERNAL_LOAD_DVECTORCONTENT(view,id,viewTag,data);
				INTERNAL_LOAD_DVECTORCONTENT(up,id,upTag,data);
				matrix = matrix * DMatrix().viewMatrix(position,view,up);
			} else if(tag != 0 && tag->name.compare("orthographic") == 0) {
				//orthographic projection matrix
				double left = 0.0;
				double right = 0.0;
				double bottom = 0.0;
				double top = 0.0;
				double znear = 0.0;
				double zfar = 0.0;
				try {
					left = tag->getRealAttribute("left");
					right = tag->getRealAttribute("right");
					bottom = tag->getRealAttribute("bottom");
					top = tag->getRealAttribute("top");
					znear = tag->getRealAttribute("znear");
					zfar = tag->getRealAttribute("zfar");
				} catch(Exception &) {
					//orthographic projection matrix must contain attributes
					//left, right, bottom, top, znear, zfar
					delete data;
					stringstream errmsg;
					errmsg << "Error: orthographic projection dmatrix in object " << id << " must contain attributes left, right, bottom, top, znear, zfar";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				matrix = matrix * DMatrix().orthographicPerspeciveMatrix(left,right,bottom,top,znear,zfar);
			} else if(tag != 0 && tag->name.compare("perspective") == 0) {
				//perspective projection matrix
				double angle = 0.0;
				double width = 0.0;
				double height = 0.0;
				double znear = 0.0;
				double zfar = 0.0;
				try {
					angle = tag->getRealAttribute("angle");
					width = tag->getRealAttribute("width");
					height = tag->getRealAttribute("height");
					znear = tag->getRealAttribute("znear");
					zfar = tag->getRealAttribute("zfar");
				} catch(Exception &) {
					//perspective projection matrix must contain attributes
					//angle, width, height, znear, zfar
					delete data;
					stringstream errmsg;
					errmsg << "Error: orthographic projection dmatrix in object " << id << " must contain attributes angle, width, height, znear, zfar";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				matrix = matrix * DMatrix().perspectiveMatrix(angle,width,height,znear,zfar);
			} else if(tag != 0) {
				//incorrect tag detected
				delete data;
				stringstream errmsg;
				errmsg << "Error: incorrect tag in dmatrix of object " << id << " detected, must have name \"matrix\", \"rotate\", \"translate\", \"scale\", \"shear\", \"orthographic\", \"view\" or \"perspective\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_VECTORCONTENT(Vector &vec, const string &id, XTag *rNode, XTag *data) {
		vector<double> values;
		vector<string> texts;
		rNode->getDirectTexts(texts);
		stringstream concTexts;
		for(vector<string>::iterator iter = texts.begin() ; iter != texts.end() ; iter++) {
			//separate numbers by empty space
			concTexts << " " << (*iter);
		}
		try {
			values = parseNumbers(concTexts.str());
		} catch(Exception &) {
			//list of matrix componentes
			// has incorrect syntax
			delete data;
			stringstream str;
			str << "Error: vector value of object " << id << " has syntax error";
			throw Exception(str.str(),EX_SYNTAX);
		}
		if(values.size() == 0) {
			//no components specified
			//assume identity matrix
			vec = Vector();
		} else if(values.size() == 1) {
			vec = Vector((float)values[0]);
		} else if(values.size() == 2) {
			vec = Vector((float)values[0], (float)values[1]);
		} else if(values.size() == 3) {
			vec = Vector((float)values[0], (float)values[1], (float)values[2]);
		} else if(values.size() == 4) {
			vec = Vector((float)values[0], (float)values[1], (float)values[2], (float)values[3]);
		} else {
			//wrong number of components
			delete data;
			stringstream errmsg;
			errmsg << "Error: invalid number of components of vector in object " << id;
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_DVECTORCONTENT(DVector &vec, const string &id, XTag *rNode, XTag *data) {
		vector<double> values;
		vector<string> texts;
		rNode->getDirectTexts(texts);
		stringstream concTexts;
		for(vector<string>::iterator iter = texts.begin() ; iter != texts.end() ; iter++) {
			//separate numbers by empty space
			concTexts << " " << (*iter);
		}
		try {
			values = parseNumbers(concTexts.str());
		} catch(Exception &) {
			//list of matrix componentes
			// has incorrect syntax
			delete data;
			stringstream str;
			str << "Error: dvector value of object " << id << " has syntax error";
			throw Exception(str.str(),EX_SYNTAX);
		}
		if(values.size() == 0) {
			//no components specified
			//assume identity matrix
			vec = DVector();
		} else if(values.size() == 1) {
			vec = DVector(values[0]);
		} else if(values.size() == 2) {
			vec = DVector(values[0], values[1]);
		} else if(values.size() == 3) {
			vec = DVector(values[0], values[1], values[2]);
		} else if(values.size() == 4) {
			vec = DVector(values[0], values[1], values[2], values[3]);
		} else {
			//wrong number of components
			delete data;
			stringstream errmsg;
			errmsg << "Error: invalid number of components of dvector in object " << id;
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_BUFFEREDMESH(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every shadernode loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a mesh without an id",EX_COMPILE);
		}
		BufferedMesh *mesh = 0;
		unordered_map<string,BufferedMesh *>::iterator iter = bufferedMeshes.find(id);
		if(iter == bufferedMeshes.end()) {
			//make shure no other resources have the same identifyer
			//then create a mesh
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a mesh has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			mesh = new BufferedMesh(id);
			bufferedMeshes[id] = mesh;
			resources[id] = mesh;
		} else {
			mesh = (*iter).second;
		}
		//load mesh parts
		mesh->setFaceSize(3);
		bool useFaceSize = false;
		try {
			double fs = rNode->getRealAttribute("faceSize");
			unsigned int faceSize = (unsigned int)max(0.0,floor(fs));
			mesh->setFaceSize(faceSize);
			useFaceSize = true;
		} catch(Exception &) {
		}
		bool useMaxVertexCount = false;
		try {
			double mvc = rNode->getRealAttribute("maxVertexCount");
			unsigned int maxVertexCount = (unsigned int)max(0.0,floor(mvc));
			mesh->setMaxVertexCount(maxVertexCount);
			useMaxVertexCount = true;
		} catch(Exception &) {
		}
		bool useDiscardStandardBuffers0 = false;
		string discardStandardBuffers0;
		try {
			discardStandardBuffers0 = rNode->getStrAttribute("discardStandardBuffers");
			useDiscardStandardBuffers0 = true;
		} catch(Exception &) {
		}
		if(useDiscardStandardBuffers0) {
			if(discardStandardBuffers0.compare("true") != 0 && discardStandardBuffers0.compare("false") != 0) {
				//attribute discardStandardBuffers must be "true" or "false"
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute discardStandardBuffers in BufferedMesh " << id << " must have value \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool usePath = false;
		try {
			string path = rNode->getStrAttribute("path");
			if(useDiscardStandardBuffers0) {
				bool disc = discardStandardBuffers0.compare("true") == 0;
				mesh->addBuffers(path,disc);
			} else {
				mesh->addBuffers(path);
			}
			usePath = true;
		} catch(Exception &) {
		}
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			//collect buffers
			XTag *b = dynamic_cast<XTag *>(node);
			if(b != 0 && b->name.compare("path") == 0) {
				//it's a path node, load path
				string discard = "false";
				bool useDiscardStandardBuffers1 = false;
				try {
					discard = b->getStrAttribute("discardStandardBuffers");
					useDiscardStandardBuffers1 = true;
				} catch(Exception &) {
				}
				if(discard.compare("true") != 0 && discard.compare("false")) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: attribute discardStandardBuffers in mesh " << id << " must be either \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				bool discardVal = discard.compare("true") == 0;
				string p = b->getTexts();
				mesh->addBuffers(p,discardVal);
				//check for wrong named attributes
				unsigned int attrCount = 0;
				if(useDiscardStandardBuffers1) {
					attrCount++;
				}
				if(b->stringAttribs.size() != attrCount || b->realAttribs.size() != 0) {
					//wrong named attributes detected
					delete data;
					stringstream errmsg;
					errmsg << "Error: tag path in BufferedMesh " << id << " must not have attributes with names different from \"discardStandardBuffers\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			} else if(b != 0 && b->name.compare("attribute") == 0) {
				//it's a buffer, load buffer
				string name;
				string outputName;
				unsigned int attributeSize;
				vector<float> attributeData;
				try {
					name = b->getStrAttribute("name");
					outputName = name;
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: mesh " << id << " has a buffer without a name";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				try {
					double as = b->getRealAttribute("attributeSize");
					attributeSize = (unsigned int)max(0.0,floor(as));
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: buffer " << name << " in mesh " << id << " does not specify an attributeSize";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				bool useOutputName = false;
				try {
					outputName = b->getStrAttribute("outputName");
					useOutputName = true;
				} catch(Exception &) {
				}
				XTag *meshData = 0;
				try {
					meshData = b->getFirst("data");
				} catch(Exception &) {
				}
				if(meshData != 0) {
					//this buffer contains some data
					vector<string> md;
					b->getTexts(md);
					stringstream connectedData;
					BOOST_FOREACH(const string &cstr , md) {
						//separate numbers from each other
						connectedData << cstr << " ";
					}
					vector<double> nums;
					try {
						nums = parseNumbers(connectedData.str());
					} catch(Exception &) {
						//not a list of numbers
						delete data;
						stringstream errmsg;
						errmsg << "Error: data in buffer " << name << " of mesh " << id << " must be floating point numbers";
						throw Exception(errmsg.str(),EX_COMPILE);
					}
					BOOST_FOREACH(double num , nums) {
						attributeData.push_back((float)num);
					}
				}
				mesh->addBuffer(name, outputName, attributeData, attributeSize);
				//check for wrong attribute and tag names
				unsigned int strAttrCount = 1;
				unsigned int realAttrCount = 1;
				if(useOutputName) {
					strAttrCount++;
				}
				if(b->stringAttribs.size() != strAttrCount || b->realAttribs.size() != realAttrCount) {
					//wrong attribute names detected
					delete data;
					stringstream errmsg;
					errmsg << "Error: tag attribute in BufferedMesh " << id << " must not have attributes with names different from \"name\", \"outputName\" or \"attributeSize\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				unsigned int tagCount = 0;
				if(meshData != 0) {
					tagCount++;
				}
				if(b->nodes.size() != tagCount) {
					//wrong tags detected
					delete data;
					stringstream errmsg;
					errmsg << "Error: tag attribute in BufferedMesh " << id << " must only contain one or zero tags called \"data\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			} else if(b != 0 && b->name.compare("uniformset") == 0) {
				unsigned int setID = 0;
				try {
					setID = (unsigned int)b->getRealAttribute("id");
				} catch(Exception &) {
					//attribute id is required
					delete data;
					stringstream errmsg;
					errmsg << "Error: uniformset without id found in BufferedMesh " << id;
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				for(vector<XNode *>::iterator uIter = b->nodes.begin() ; uIter != b->nodes.end() ; uIter++) {
					XNode *uNode = (*uIter);
					XTag *uniformSet = dynamic_cast<XTag *>(uNode);
					if(uniformSet == 0) {
						//not an XTag, skip this element
						continue;
					}
					bool isFloats = false;
					bool isVectors = false;
					bool isMatrices = false;
					if(uniformSet->name.compare("floats") == 0) {
						isFloats = true;
					} else if(uniformSet->name.compare("vectors") == 0) {
						isVectors = true;
					} else if(uniformSet->name.compare("matrices") == 0) {
						isMatrices = true;
					} else {
						//subtags must be floats, vectors or matrices
						delete data;
						stringstream errmsg;
						errmsg << "Error: subtags of uniformset in BufferedMesh " << id << " must have name \"floats\", \"vectors\" or \"matrices\"";
						throw Exception(errmsg.str(),EX_COMPILE);
					}
					for(vector<XNode *>::iterator uNodeIter = uniformSet->nodes.begin() ; uNodeIter != uniformSet->nodes.end() ; uNodeIter++) {
						XText *uText = dynamic_cast<XText *>((*uNodeIter));
						if(uText == 0) {
							//not a text node, skip this element
							continue;
						}
						//assemble id of uniform
						stringstream uIDStream;
						uIDStream << id << "." << uText->text;
						string uID = uIDStream.str();
						Uniform *uniform = 0;
						try {
							if(isFloats) {
								uniform = &getUniformFloat(uID);
							} else if(isVectors) {
								uniform = &getUniformVector(uID);
							} else {
								uniform = &getUniformMatrix(uID);
							}
						} catch(Exception &) {
							//id already taken
							delete data;
							stringstream errmsg;
							errmsg << "Error: attribute " << uText->text << " in uniformset " << setID << " in BufferedMesh " << id << " is associated with an uniform with id " << uID << ", but another resource has the same id";
							throw Exception(errmsg.str(),EX_COMPILE);
						}
						BufferUniforms &uniformSets = bufferUniforms[id];
						uniformSets.uniforms[setID].push_back(uniform);
						uniformSets.attributeNames[setID].push_back(uText->text);
					}
					for(map<string,string>::iterator uNodeIter = uniformSet->stringAttribs.begin() ; uNodeIter != uniformSet->stringAttribs.end() ; uNodeIter++) {
						const string &bufferID = (*uNodeIter).first;
						const string &uID = (*uNodeIter).second;
						Uniform *uniform = 0;
						try {
							if(isFloats) {
								uniform = &getUniformFloat(uID);
							} else if(isVectors) {
								uniform = &getUniformVector(uID);
							} else {
								uniform = &getUniformMatrix(uID);
							}
						} catch(Exception &) {
							//id already taken
							delete data;
							stringstream errmsg;
							errmsg << "Error: attribute " << bufferID << " in uniformset " << setID << " in BufferedMesh " << id << " is associated with an uniform with id " << uID << ", but another resource has the same id";
							throw Exception(errmsg.str(),EX_COMPILE);
						}
						BufferUniforms &uniformSets = bufferUniforms[id];
						uniformSets.uniforms[setID].push_back(uniform);
						uniformSets.attributeNames[setID].push_back(bufferID);
					}
				}
			} else if(b != 0) {
				//subtags of BufferedMesh must be attribute, path or uniformset
				delete data;
				stringstream errmsg;
				errmsg << "Error: subtags of BufferedMesh " << id << " must have name \"attribute\", \"path\" or \"uniformset\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		//check if attributes with wrong names were used
		unsigned int strAttrCount = 1;
		unsigned int realAttrCount = 0;
		if(usePath) {
			strAttrCount++;
		}
		if(useFaceSize) {
			realAttrCount++;
		}
		if(useMaxVertexCount) {
			realAttrCount++;
		}
		if(useDiscardStandardBuffers0) {
			strAttrCount++;
		}
		if(rNode->stringAttribs.size() != strAttrCount || rNode->realAttribs.size() != realAttrCount) {
			//wrong attribute names detected
			delete data;
			stringstream errmsg;
			errmsg << "Error: BufferedMesh " << id << " must not have attributes with names different from \"id\", \"path\", \"faceSize\", \"maxVertexCount\" or \"discardStandardBuffers\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_SKELETON(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every skeleton loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a skeleton without an id",EX_COMPILE);
		}
		Skeleton *skeleton = 0;
		unordered_map<string,Skeleton *>::iterator iter = skeletons.find(id);
		if(iter == skeletons.end()) {
			//make shure no other resources have the same identifyer
			//then create a skeleton
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a skeleton has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			skeleton = new Skeleton(id);
			skeletons[id] = skeleton;
			resources[id] = skeleton;
		} else {
			skeleton = (*iter).second;
		}

		//load skeleton parts
		try {
			//check for path
			const string &path = rNode->getStrAttribute("path");
			skeleton->addBones(path);
		} catch(Exception &) {
		}
		string meshname;
		bool useMesh = false;
		try {
			//check for mesh
			meshname = rNode->getStrAttribute("mesh");
			useMesh = true;
		} catch(Exception &) {
		}
		if(useMesh) {
			//loading bones from a mesh
			//load mesh
			try {
				BufferedMesh &mesh = getBufferedMesh(meshname);
				skeleton->addBones(mesh);
			} catch(Exception &) {
				//error retrieving mesh
				delete data;
				stringstream errmsg;
				errmsg << "Error: failed to load mesh " << meshname << " for skeleton " << id << ", as another resource than a mesh uses the same id";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
		}
		XTag *matrixTag = 0;
		try {
			//check for skeletonMatrix
			matrixTag = rNode->getFirst("skeletonMatrix");
		} catch(Exception &) {
		}
		if(matrixTag != 0) {
			//skeletonMatrix specification exists
			//load matrix
			Matrix matrix;
			INTERNAL_LOAD_MATRIXCONTENT(matrix,id,matrixTag,data);
			skeleton->setSkeletonMatrix(matrix);
		}

		// collect rootbones
		vector<XTag *> boneTags;
		rNode->getTags("bone",boneTags);
		for(vector<XTag *>::iterator boneTagIter = boneTags.begin() ; boneTagIter != boneTags.end() ; boneTagIter++) {
			XTag *boneTag = (*boneTagIter);
			Bone bone;

			//load root bone data
			XTag *rbParentTransformTag = 0;
			try {
				bone.ID = boneTag->getStrAttribute("id");
				rbParentTransformTag = boneTag->getFirst("parentTransform");
			} catch(Exception &) {
				//necessary data not present
				delete data;
				stringstream errmsg;
				errmsg << "Error: each bone of skeleton " << id << " must have an id and a parentTransform";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			if(boneTag->stringAttribs.size() != 1 || boneTag->realAttribs.size() != 0) {
				//attributes with wrong names detected
				delete data;
				stringstream errmsg;
				errmsg << "Error: bones in Skeleton " << id << " must have only one attribute called \"id\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			INTERNAL_LOAD_MATRIXCONTENT(bone.parentTransform,id,rbParentTransformTag,data);

			//stacks for skeletal traversal
			vector<Bone *> boneStack;
			boneStack.push_back(&bone);
			vector<vector<XNode *>::iterator> iterStack;
			iterStack.push_back(boneTag->nodes.begin());
			vector<vector<XNode *> *> arrayStack;
			arrayStack.push_back(&boneTag->nodes);
			while(iterStack.size() > 0) {
				//traverse bone tags
				Bone *fatherBone = boneStack[boneStack.size()-1];
				vector<XNode *>::iterator currIter = iterStack[iterStack.size()-1];
				vector<XNode *> *currArray = arrayStack[arrayStack.size()-1];
				if(currIter != currArray->end()) {
					XNode *currNode = (*currIter);
					XTag *currTag = dynamic_cast<XTag *>(currNode);
					
					bool pushedChild = false;

					//collect data of bone
					if(currTag != 0 && currTag->name.compare("bone") == 0) {
						//get id and parentTransform
						//otherwise quit
						Bone *currentBone = 0;
						XTag *parentTransformTag = 0;
						try {
							string boneID = currTag->getStrAttribute("id");
							parentTransformTag = currTag->getFirst("parentTransform");
							//generate bone
							fatherBone->bones.push_back(Bone());
							currentBone = &fatherBone->bones[fatherBone->bones.size()-1];
							currentBone->ID = boneID;
						} catch(Exception &) {
							//necessary data not present
							delete data;
							stringstream errmsg;
							errmsg << "Error: each bone in skeleton " << id << " must have an id and a parentTransform";
							throw Exception(errmsg.str(),EX_SYNTAX);
						}
						if(currTag->stringAttribs.size() != 1 || currTag->realAttribs.size() != 0) {
							//attributes with wrong names detected
							delete data;
							stringstream errmsg;
							errmsg << "Error: bones in Skeleton " << id << " must have only one attribute called \"id\"";
							throw Exception(errmsg.str(),EX_COMPILE);
						}
						INTERNAL_LOAD_MATRIXCONTENT(currentBone->parentTransform,id,parentTransformTag,data);

						//push itself and children
						boneStack.push_back(currentBone);
						iterStack.push_back(currTag->nodes.begin());
						arrayStack.push_back(&currTag->nodes);
						pushedChild = true;
					} else if(currTag != 0 && currTag->name.compare("bone") != 0 && currTag->name.compare("parentTransform") != 0) {
						//found childtag of a bone with wrong name
						delete data;
						stringstream errmsg;
						errmsg << "Error: found childtag " << currTag->name << " of a bone in Skeleton " << id << ", but childtags of bone must be called \"bone\" or \"parentTransform\"";
						throw Exception(errmsg.str(),EX_SYNTAX);
					}
					
					//advance to next element
					if(pushedChild) {
						 iterStack[iterStack.size()-2]++;
					} else {
						 iterStack[iterStack.size()-1]++;
					}
				} else {
					//finished all children of this branch
					//pop branch
					boneStack.pop_back();
					iterStack.pop_back();
					arrayStack.pop_back();
				}
			}

			//insert root bone
			skeleton->addRootBone(bone);
		}

		//collect transformationsets
		vector<XTag *> setTags;
		rNode->getTags("transformationset",setTags);
		for(vector<XTag *>::iterator setIter = setTags.begin() ; setIter != setTags.end() ; setIter++) {
			XTag *setTag = (*setIter);
			int setID = 0;
			try {
				setID = (int)setTag->getRealAttribute("id");
			} catch(Exception &) {
				//id of set is required
				delete data;
				stringstream errmsg;
				errmsg << "Error: transformationset without an id in skeleton " << id << " detected";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			if(setTag->realAttribs.size() != 1 || setTag->stringAttribs.size() != 0) {
				//attributes with wrong names exist
				delete data;
				stringstream errmsg;
				errmsg << "Error: transformationset in skeleton " << id << " has attributes with names different from \"id\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			//collect BoneTransforms
			vector<XTag *> transformTags;
			setTag->getTags("transform",transformTags);
			if(transformTags.size() != setTag->nodes.size()) {
				//transformationset has nodes different from transform
				delete data;
				stringstream errmsg;
				errmsg << "Error: transformationset in skeleton " << id << " has tags with names different from \"transform\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			for(vector<XTag *>::iterator tIter = transformTags.begin() ; tIter != transformTags.end() ; tIter++) {
				XTag *transformTag = (*tIter);
				string tID;
				try {
					tID = transformTag->getStrAttribute("id");
				} catch(Exception &) {
					//transform id is required
					delete data;
					stringstream errmsg;
					errmsg << "Error: each transformation in skeleton " << id << " requires an id of a bone";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				bool useInput = false;
				bool useOutput = false;
				string inputID;
				string outputID;
				try {
					inputID = transformTag->getStrAttribute("input");
					useInput = true;
				} catch(Exception &) {
				}
				try {
					outputID = transformTag->getStrAttribute("output");
					useOutput = true;
				} catch(Exception &) {
				}
				UniformMatrix *inputMatrix = 0;
				UniformMatrix *outputMatrix = 0;
				if(useInput) {
					try {
						inputMatrix = &getUniformMatrix(inputID);
					} catch(Exception &) {
						//id of input matrix taken by another resource
						delete data;
						stringstream errmsg;
						errmsg << "Error: input matrix in skeleton " << id << " has an id taken by another resource";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
				}
				if(useOutput) {
					try {
						outputMatrix = &getUniformMatrix(outputID);
					} catch(Exception &) {
						//id of output matrix taken by another resource
						delete data;
						stringstream errmsg;
						errmsg << "Error: output matrix in skeleton " << id << " has an id taken by another resource";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
				}
				BoneTransform trans;
				trans.boneID = tID;
				trans.input = inputMatrix;
				trans.output = outputMatrix;
				skeleton->addBoneTransform(setID,trans);
				//check attributecount
				//if it's not right, attributes with wrong names exist
				//count number of stringattributes
				unsigned int checkNum = 1;
				if(inputMatrix != 0) {
					checkNum++;
				}
				if(outputMatrix != 0) {
					checkNum++;
				}
				if(transformTag->stringAttribs.size() != checkNum || transformTag->realAttribs.size() != 0) {
					//attribute with a wrong name
					delete data;
					stringstream errmsg;
					errmsg << "Error: attributes of transforms in transformationsets in Skeleton " << id << " must be called \"id\", \"input\" or \"output\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}
		//errorcheck
		//search for attributes, which have wrong names
		map<string,int> strControl;
		strControl["id"] = 0;
		strControl["path"] = 0;
		strControl["mesh"] = 0;
		for(map<string,string>::iterator strIter = rNode->stringAttribs.begin() ; strIter != rNode->stringAttribs.end() ; strIter++) {
			const string &tName = (*strIter).first;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute " << tName << " found in Skeleton " << id << ", but attributes must be called \"id\", \"path\" or \"mesh\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		map<string,int> tControl;
		tControl["skeletonMatrix"] = 0;
		tControl["bone"] = 0;
		tControl["transformationset"] = 0;
		for(vector<XNode *>::iterator nIter = rNode->nodes.begin() ; nIter != rNode->nodes.end() ; nIter++) {
			XTag *sTag = dynamic_cast<XTag *>((*nIter));
			if(sTag == 0) {
				//node not a tag, ignore it
				continue;
			}
			const string &tName = sTag->name;
			map<string,int>::iterator control = tControl.find(tName);
			if(control == tControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: tag " << tName << " found in Skeleton " << id << ", but tags must be called \"skeletonMatrix\", \"bone\" or \"transformationset\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_UNIFORMFLOAT(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every uniform float loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a uniform float without an id",EX_COMPILE);
		}
		UniformFloat *uFloat = 0;
		unordered_map<string,UniformFloat *>::iterator iter = uniformFloats.find(id);
		if(iter == uniformFloats.end()) {
			//make shure no other resources have the same identifyer
			//then create a uniform float
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a uniform float has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			uFloat = new UniformFloat(id);
			uniformFloats[id] = uFloat;
			resources[id] = uFloat;
		} else {
			uFloat = (*iter).second;
		}
		//load uniform float parts
		bool useValue = false;
		try {
			float value = (float)rNode->getRealAttribute("value");
			*uFloat = value;
			useValue = true;
		} catch(Exception &) {
		}
		if( (rNode->realAttribs.size() != 0 && !useValue) ||
			(rNode->realAttribs.size() != 1 && useValue) ||
			rNode->stringAttribs.size() != 1) {
			//errorcheck
			//some attributes have wrong names
			delete data;
			stringstream errmsg;
			errmsg << "Error: UniformFloat " << id << " must only contain attributes called \"id\" or \"value\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_UNIFORMDOUBLE(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every uniform float loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a uniform double without an id",EX_COMPILE);
		}
		UniformDouble *uDouble = 0;
		unordered_map<string,UniformDouble *>::iterator iter = uniformDoubles.find(id);
		if(iter == uniformDoubles.end()) {
			//make shure no other resources have the same identifyer
			//then create a uniform float
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a uniform double has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			uDouble = new UniformDouble(id);
			uniformDoubles[id] = uDouble;
			resources[id] = uDouble;
		} else {
			uDouble = (*iter).second;
		}
		//load uniform double parts
		bool useValue = false;
		try {
			double value = rNode->getRealAttribute("value");
			*uDouble = value;
			useValue = true;
		} catch(Exception &) {
		}
		if( (rNode->realAttribs.size() != 0 && !useValue) ||
			(rNode->realAttribs.size() != 1 && useValue) ||
			rNode->stringAttribs.size() != 1) {
			//errorcheck
			//some attributes have wrong names
			delete data;
			stringstream errmsg;
			errmsg << "Error: UniformDouble " << id << " must only contain attributes called \"id\" or \"value\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_UNIFORMVECTOR(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every uniform vector loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a uniform vector without an id",EX_COMPILE);
		}
		UniformVector *uVector = 0;
		unordered_map<string,UniformVector *>::iterator iter = uniformVectors.find(id);
		if(iter == uniformVectors.end()) {
			//make shure no other resources have the same identifyer
			//then create a uniform vector
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a uniform vector has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			uVector = new UniformVector(id);
			uniformVectors[id] = uVector;
			resources[id] = uVector;
		} else {
			uVector = (*iter).second;
		}
		//load uniform vector parts
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			//set value
			XTag *value = dynamic_cast<XTag *>(node);
			if(value != 0 && value->name.compare("value") == 0) {
				INTERNAL_LOAD_VECTORCONTENT(*uVector,id,value,data);
			} else if(value != 0) {
				//subtags of vector must be called value
				delete data;
				stringstream errmsg;
				errmsg << "Error: each subtag of UniformVector " << id << " must be called \"value\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_UNIFORMDVECTOR(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every uniform vector loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a uniform dvector without an id",EX_COMPILE);
		}
		UniformDVector *uDVector = 0;
		unordered_map<string,UniformDVector *>::iterator iter = uniformDVectors.find(id);
		if(iter == uniformDVectors.end()) {
			//make shure no other resources have the same identifyer
			//then create a uniform vector
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a uniform dvector has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			uDVector = new UniformDVector(id);
			uniformDVectors[id] = uDVector;
			resources[id] = uDVector;
		} else {
			uDVector = (*iter).second;
		}
		//load uniform vector parts
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			//set value
			XTag *value = dynamic_cast<XTag *>(node);
			if(value != 0 && value->name.compare("value") == 0) {
				INTERNAL_LOAD_DVECTORCONTENT(*uDVector,id,value,data);
			} else if(value != 0) {
				//subtags of vector must be called value
				delete data;
				stringstream errmsg;
				errmsg << "Error: each subtag of UniformDVector " << id << " must be called \"value\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_UNIFORMMATRIX(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every uniform matrix loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a uniform matrix without an id",EX_COMPILE);
		}
		UniformMatrix *uMatrix = 0;
		unordered_map<string,UniformMatrix *>::iterator iter = uniformMatrices.find(id);
		if(iter == uniformMatrices.end()) {
			//make shure no other resources have the same identifyer
			//then create a uniform matrix
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a uniform matrix has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			uMatrix = new UniformMatrix(id);
			uniformMatrices[id] = uMatrix;
			resources[id] = uMatrix;
		} else {
			uMatrix = (*iter).second;
		}
		//load uniform matrix parts
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			//set value
			XTag *value = dynamic_cast<XTag *>(node);
			if(value != 0 && value->name.compare("value") == 0) {
				INTERNAL_LOAD_MATRIXCONTENT(*uMatrix,id,value,data);
			} else if(value != 0) {
				//subtags of matrix must be called value
				delete data;
				stringstream errmsg;
				errmsg << "Error: each subtag of UniformMatrix " << id << " must be called \"value\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_UNIFORMDMATRIX(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every uniform matrix loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a uniform dmatrix without an id",EX_COMPILE);
		}
		UniformDMatrix *uDMatrix = 0;
		unordered_map<string,UniformDMatrix *>::iterator iter = uniformDMatrices.find(id);
		if(iter == uniformDMatrices.end()) {
			//make shure no other resources have the same identifyer
			//then create a uniform matrix
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a uniform dmatrix has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			uDMatrix = new UniformDMatrix(id);
			uniformDMatrices[id] = uDMatrix;
			resources[id] = uDMatrix;
		} else {
			uDMatrix = (*iter).second;
		}
		//load uniform matrix parts
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			//set value
			XTag *value = dynamic_cast<XTag *>(node);
			if(value != 0 && value->name.compare("value") == 0) {
				INTERNAL_LOAD_DMATRIXCONTENT(*uDMatrix,id,value,data);
			} else if(value != 0) {
				//subtags of matrix must be called value
				delete data;
				stringstream errmsg;
				errmsg << "Error: each subtag of UniformDMatrix " << id << " must be called \"value\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_TEXTURE(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every shadernode loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a texture without an id",EX_COMPILE);
		}
		Texture *texture = 0;
		unordered_map<string,Texture *>::iterator iter = textures.find(id);
		if(iter == textures.end()) {
			//make shure no other resources have the same identifyer
			//then create a texture
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a texture has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			texture = new Texture(id);
			textures[id] = texture;
			resources[id] = texture;
		} else {
			texture = (*iter).second;
		}
		//load mesh parts
		try {
			string path = rNode->getStrAttribute("path");
			texture->setPath(path);
		} catch(Exception &) {
		}
		try {
			int width = (int)ceil(rNode->getRealAttribute("width"));
			texture->setWidth(width);
		} catch(Exception &) {
		}
		try {
			int height = (int)ceil(rNode->getRealAttribute("height"));
			texture->setHeight(height);
		} catch(Exception &) {
		}
		bool wrongFormat = false;
		try {
			string format = rNode->getStrAttribute("format");
			if(format.compare("float") != 0 && format.compare("float16") != 0 && format.compare("byte") != 0) {
				wrongFormat = true;
				throw Exception();
			}
			if(format.compare("float") == 0) {
				texture->setPixelFormat(FLOAT_RGBA);
			} else if(format.compare("float16") == 0) {
				texture->setPixelFormat(FLOAT16_RGBA);
			} else if(format.compare("byte") == 0) {
				texture->setPixelFormat(BYTE_RGBA);
			}
		} catch(Exception &) {
			if(wrongFormat) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute format in texture " << id << " must be either \"float\" or \"byte\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		//errorcheck
		//search for attributes, which have wrong names
		map<string,int> strControl;
		strControl["id"] = 0;
		strControl["path"] = 0;
		strControl["format"] = 0;
		map<string,int> rControl;
		rControl["width"] = 0;
		rControl["height"] = 0;
		for(map<string,string>::iterator strIter = rNode->stringAttribs.begin() ; strIter != rNode->stringAttribs.end() ; strIter++) {
			const string &tName = (*strIter).first;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: string attribute " << tName << " found in Texture " << id << ", but string attributes must be called \"id\", \"path\" or \"format\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		for(map<string,double>::iterator rlIter = rNode->realAttribs.begin() ; rlIter != rNode->realAttribs.end() ; rlIter++) {
			const string &tName = (*rlIter).first;
			map<string,int>::iterator control = rControl.find(tName);
			if(control == rControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: real attribute " << tName << " found in Texture " << id << ", but real attributes must be called \"width\" or \"height\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_VOLUME(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every volumenode loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a volume without an id",EX_COMPILE);
		}
		Volume *volume = 0;
		unordered_map<string, Volume *>::iterator iter = volumes.find(id);
		if(iter == volumes.end()) {
			//make shure no other resources have the same identifyer
			//then create a volume
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a volume has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			volume = new Volume(id);
			volumes[id] = volume;
			resources[id] = volume;
		} else {
			volume = (*iter).second;
		}
		//load volume parts
		try {
			int width = (int)ceil(rNode->getRealAttribute("width"));
			volume->setWidth(width);
		} catch(Exception &) {
		}
		try {
			int height = (int)ceil(rNode->getRealAttribute("height"));
			volume->setHeight(height);
		} catch(Exception &) {
		}
		try {
			int depth = (int)ceil(rNode->getRealAttribute("depth"));
			volume->setDepth(depth);
		} catch(Exception &) {
		}
		bool wrongFormat = false;
		try {
			string format = rNode->getStrAttribute("format");
			if(format.compare("float") != 0 && format.compare("float16") != 0 && format.compare("byte") != 0) {
				wrongFormat = true;
				throw Exception();
			}
			if(format.compare("float") == 0) {
				volume->setPixelFormat(FLOAT_RGBA);
			} else if(format.compare("float16") == 0) {
				volume->setPixelFormat(FLOAT16_RGBA);
			} else if(format.compare("byte") == 0) {
				volume->setPixelFormat(BYTE_RGBA);
			}
		} catch(Exception &) {
			if(wrongFormat) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute format in volume " << id << " must be either \"float\" or \"byte\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		XTag *paths = 0;
		try {
			//search for node paths
			//to collect the paths of
			//the layers
			paths = rNode->getFirst("paths");
		} catch(Exception &) {
		}
		if(paths != 0) {
			//collect paths
			vector<string> vpaths;
			paths->getTexts(vpaths);
			volume->setPaths(vpaths);
		}

		//errorcheck
		//search for attributes, which have wrong names
		map<string,int> strControl;
		strControl["id"] = 0;
		strControl["format"] = 0;
		map<string,int> rControl;
		rControl["width"] = 0;
		rControl["height"] = 0;
		rControl["depth"] = 0;
		for(map<string,string>::iterator strIter = rNode->stringAttribs.begin() ; strIter != rNode->stringAttribs.end() ; strIter++) {
			const string &tName = (*strIter).first;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: string attribute " << tName << " found in Texture " << id << ", but string attributes must be called \"id\" or \"format\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		for(map<string,double>::iterator rlIter = rNode->realAttribs.begin() ; rlIter != rNode->realAttribs.end() ; rlIter++) {
			const string &tName = (*rlIter).first;
			map<string,int>::iterator control = rControl.find(tName);
			if(control == rControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: real attribute " << tName << " found in Texture " << id << ", but real attributes must be called \"width\", \"height\" or \"depth\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_RENDERTARGET(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every target node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a texture without an id",EX_COMPILE);
		}
		RenderTarget *target = 0;
		unordered_map<string,RenderTarget *>::iterator iter = renderTargets.find(id);
		if(iter == renderTargets.end()) {
			//make shure no other resources have the same identifyer
			//then create a target
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a rendertarget has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			target = new RenderTarget(id);
			renderTargets[id] = target;
			resources[id] = target;
		} else {
			target = (*iter).second;
		}
		//load target parts
		try {
			int width = (int)ceil(rNode->getRealAttribute("width"));
			target->setWidth(width);
		} catch(Exception &) {
		}
		try {
			int height = (int)ceil(rNode->getRealAttribute("height"));
			target->setHeight(height);
		} catch(Exception &) {
		}
		string renderToDisplay;
		bool useRenderToDisplay = false;
		try {
			renderToDisplay = rNode->getStrAttribute("renderToDisplay");
			useRenderToDisplay = true;
		} catch(Exception &) {
		}
		if(useRenderToDisplay) {
			//renderToDisplay can only have values true or false
			if(renderToDisplay.compare("true") != 0 && renderToDisplay.compare("false") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: renderToDisplay in rendertarget " << id << " can only have values \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			target->setRenderToDisplay(renderToDisplay.compare("true") == 0);
		}
		//errorcheck
		//search for attributes, which have wrong names
		map<string,int> strControl;
		strControl["id"] = 0;
		strControl["renderToDisplay"] = 0;
		map<string,int> rControl;
		rControl["width"] = 0;
		rControl["height"] = 0;
		for(map<string,string>::iterator strIter = rNode->stringAttribs.begin() ; strIter != rNode->stringAttribs.end() ; strIter++) {
			const string &tName = (*strIter).first;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: string attribute " << tName << " found in Texture " << id << ", but string attributes must be called \"id\" or \"renderToDisplay\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		for(map<string,double>::iterator rlIter = rNode->realAttribs.begin() ; rlIter != rNode->realAttribs.end() ; rlIter++) {
			const string &tName = (*rlIter).first;
			map<string,int>::iterator control = rControl.find(tName);
			if(control == rControl.end()) {
				//attribute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: real attribute " << tName << " found in RenderTarget " << id << ", but real attributes must be called \"width\" or \"height\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_RENDEROBJECT(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every renderobject node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: an object without an id",EX_COMPILE);
		}
		RenderObject *object = 0;
		unordered_map<string,RenderObject *>::iterator iter = renderObjects.find(id);
		if(iter == renderObjects.end()) {
			//make shure no other resources have the same identifyer
			//then create a renderobject
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than an object has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			object = new RenderObject(id);
			renderObjects[id] = object;
			resources[id] = object;
		} else {
			object = (*iter).second;
		}
		//load renderobject parts
		object->setVisible(true);
		string visible;
		bool useVisible = false;
		try {
			visible = rNode->getStrAttribute("visible");
			useVisible = true;
		} catch(Exception &) {
		}
		if(useVisible) {
			//read visible value, can only be true or false
			if(visible.compare("true") != 0 && visible.compare("false") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute visible of renderobject " << id << " can only have values \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			object->setVisible(visible.compare("true") == 0);
		}
		object->setCullMode(CULL_NONE);
		string cullMode;
		bool useCullMode = false;
		try {
			cullMode = rNode->getStrAttribute("cullmode");
			useCullMode = true;
		} catch(Exception &) {
		}
		if(useCullMode) {
			//read cullmode value, can only be none, cw or ccw
			if(cullMode.compare("none") != 0 && cullMode.compare("cw") != 0 && cullMode.compare("ccw") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute cullmode of renderobject " << id << " can only have values \"none\", \"cw\" or \"ccw\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			if(cullMode.compare("none") == 0) {
				object->setCullMode(CULL_NONE);
			} else if(cullMode.compare("cw") == 0) {
				object->setCullMode(CULL_CW);
			} else if(cullMode.compare("ccw") == 0) {
				object->setCullMode(CULL_CCW);
			}
		}

		string shaderID;
		bool useShader = false;
		try {
			shaderID = rNode->getStrAttribute("shader");
			useShader = true;
		} catch(Exception &) {
		}
		if(useShader) {
			//use a shader
			//if another resource than a shader
			//has identifyer shaderID, an exception
			//is thrown
			try {
				Shader &shader = getShader(shaderID);
				object->setShader(&shader);
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute shader in renderobject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
		}
		string meshID;
		bool useMesh = false;
		try {
			meshID = rNode->getStrAttribute("mesh");
			useMesh = true;
		} catch(Exception &) {
		}
		if(useMesh) {
			try {
				BufferedMesh &mesh = getBufferedMesh(meshID);
				object->setMesh(mesh);
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute mesh in renderobject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
		}
		XTag *instancebuffers = 0;
		try {
			instancebuffers = rNode->getFirst("instancebuffers");
		} catch(Exception &) {
		}
		if(instancebuffers != 0) {
			//collect all instancebuffers
			vector<BufferedMesh *> buffers;
			vector<string> ibids;
			instancebuffers->getTexts(ibids);
			BOOST_FOREACH(const string &ibid , ibids) {
				try {
					BufferedMesh &mesh = getBufferedMesh(ibid);
					buffers.push_back(&mesh);
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: instancebuffer " << ibid << " in renderobject " << id << " has an id used by another resource";
				}
			}
			object->setInstanceBuffers(buffers);
		}
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			XTag *tag = dynamic_cast<XTag *>(node);
			if(tag != 0) {
				if(tag->name.compare("textures") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"textures",false,0,tag,data);
				} else if(tag->name.compare("volumes") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"volumes",false,0,tag,data);
				} else if(tag->name.compare("floats") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"floats",false,0,tag,data);
				} else if(tag->name.compare("doubles") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"doubles",false,0,tag,data);
				} else if(tag->name.compare("vectors") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"vectors",false,0,tag,data);
				} else if(tag->name.compare("dvectors") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"dvectors",false,0,tag,data);
				} else if(tag->name.compare("matrices") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"matrices",false,0,tag,data);
				} else if(tag->name.compare("dmatrices") == 0) {
					INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"dmatrices",false,0,tag,data);
				} else if(tag->name.compare("uniformset") == 0) {
					int uSetID;
					try {
						uSetID = (int)tag->getRealAttribute("id");
					} catch(Exception &) {
						delete data;
						stringstream errmsg;
						errmsg << "Error: each uniformset in renderobject " << id << " must have an id";
						throw Exception(errmsg.str(),EX_NODATA);
					}
					if(tag->realAttribs.size() != 1 || tag->stringAttribs.size() != 0) {
						//attribute with wrong names detected
						delete data;
						stringstream errmsg;
						errmsg << "Error: attributes in uniformset in RenderObject " << id << " must be called \"id\"";
						throw Exception(errmsg.str(),EX_COMPILE);
					}
					BOOST_FOREACH(XNode *uNode , tag->nodes) {
						XTag *uTag = dynamic_cast<XTag *>(uNode);
						if(uTag != 0) {
							if(uTag->name.compare("textures") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"textures",true,uSetID,uTag,data);
							} else if(uTag->name.compare("volumes") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"volumes",true,uSetID,uTag,data);
							} else if(uTag->name.compare("floats") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"floats",true,uSetID,uTag,data);
							} else if(uTag->name.compare("doubles") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"doubles",true,uSetID,uTag,data);
							} else if(uTag->name.compare("vectors") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"vectors",true,uSetID,uTag,data);
							} else if(uTag->name.compare("dvectors") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"dvectors",true,uSetID,uTag,data);
							} else if(uTag->name.compare("matrices") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"matrices",true,uSetID,uTag,data);
							} else if(uTag->name.compare("dmatrices") == 0) {
								INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(*object,id,"dmatrices",true,uSetID,uTag,data);
							} else {
								//tag with a wrong name
								delete data;
								stringstream errmsg;
								errmsg << "Error: tag " << uTag->name << " found in uniformset in RenderObject " << id << ", but tags must be called \"textures\", \"volumes\", \"floats\", \"doubles\", \"vectors\", \"dvectors\", \"matrices\" or \"dmatrices\"";
								throw Exception(errmsg.str(),EX_COMPILE);
							}
						}
					}
				} else if(tag->name.compare("passes") == 0) {
					vector<string> passIDS;
					tag->getTexts(passIDS);
					BOOST_FOREACH(const string &passID , passIDS) {
						try {
							//add renderobject to the listed pass
							Pass &pass = getPass(passID);
							pass.addRenderObject(*object);
						} catch(Exception &) {
							delete data;
							stringstream errmsg;
							errmsg << "Error: pass " << passID << " in renderobject " << id << " has an id used by another resource";
							throw Exception(errmsg.str(),EX_AMBIGUOUS);
						}
					}
				} else if(tag->name.compare("instancebuffers") == 0) {
					//tag instancebuffers has already been dealt with
					//do nothing
				} else {
					//tag with a wrong name
					delete data;
					stringstream errmsg;
					errmsg << "Error: tag " << tag->name << " found in RenderObject " << id << ", but tags must be called \"textures\", \"volumes\", \"floats\", \"doubles\", \"vectors\", \"dvectors\", \"matrices\", \"dmatrices\", \"uniformset\", \"passes\" or \"instancebuffers\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}

		//errorcheck
		//search for attributes, which have wrong names
		unsigned int attribCount = 1;
		if(useVisible) {
			attribCount++;
		}
		if(useCullMode) {
			attribCount++;
		}
		if(useShader) {
			attribCount++;
		}
		if(useMesh) {
			attribCount++;
		}
		if(rNode->stringAttribs.size() != attribCount || rNode->realAttribs.size() != 0) {
			//attributes with wrong names detected
			delete data;
			stringstream errmsg;
			errmsg << "Error: attributes of RenderObject " << id << " must be called \"id\", \"visible\", \"cullmode\", \"shader\" or \"mesh\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(RenderObject &obj, const string &id, const string &uType, bool isUniformSet, int uniformSetID, XTag *rNode, XTag *data) {
		BOOST_FOREACH(XNode *node,rNode->nodes) {
			//add uniform without changed names
			XText *uID = dynamic_cast<XText *>(node);
			if(uID != 0) {
				stringstream fullID;
				fullID << id << "." << uID->text;
				Uniform *u = 0;
				try {
					//get uniform
					if(uType.compare("textures") == 0) {
						u = &getTexture(fullID.str());
					} else if(uType.compare("volumes") == 0) {
						u = &getVolume(fullID.str());
					} else if(uType.compare("floats") == 0) {
						u = &getUniformFloat(fullID.str());
					} else if(uType.compare("doubles") == 0) {
						u = &getUniformDouble(fullID.str());
					} else if(uType.compare("vectors") == 0) {
						u = &getUniformVector(fullID.str());
					} else if(uType.compare("dvectors") == 0) {
						u = &getUniformDVector(fullID.str());
					} else if(uType.compare("matrices") == 0) {
						u = &getUniformMatrix(fullID.str());
					} else if(uType.compare("dmatrices") == 0) {
						u = &getUniformDMatrix(fullID.str());
					}
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: uniform " << fullID.str() << " in renderobject " << id << " has an id used by another resource";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
				const string &name = u->getUniformName(id);
				if(isUniformSet) {
					//parsing uniform of uniform set
					//get uniform set
					unordered_map<string, Uniform *> &uSet = obj.getUniformSet(uniformSetID);
					unordered_map<string,Uniform *>::iterator iter = uSet.find(name);
					if(iter != uSet.end()) {
						//another uniform has the same name
						delete data;
						stringstream errmsg;
						errmsg << "Error: uniform " << fullID.str() << " has name " << name << ", but another uniform has the same name";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
					//name not used yet, insert uniform
					uSet[name] = u;
				} else {
					try {
						obj.addUniform(*u);
					} catch(Exception &) {
						//another uniform has the same name
						delete data;
						stringstream errmsg;
						errmsg << "Error: uniform " << fullID.str() << " in renderobject " << id << " has name " << name << ", but another uniform has the same name";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
				}
			}
		}
		for(map<string,string>::iterator iter = rNode->stringAttribs.begin() ; iter != rNode->stringAttribs.end() ; iter++) {
			//use id of attribute as name of uniform
			const string &varName = (*iter).first;
			//use value of attribute as uniform identifyer
			const string &varID = (*iter).second;
			Uniform *u = 0;
			try {
				//get uniform
				if(uType.compare("textures") == 0) {
					u = &getTexture(varID);
				} else if(uType.compare("volumes") == 0) {
					u = &getVolume(varID);
				} else if(uType.compare("floats") == 0) {
					u = &getUniformFloat(varID);
				} else if(uType.compare("doubles") == 0) {
					u = &getUniformDouble(varID);
				} else if(uType.compare("vectors") == 0) {
					u = &getUniformVector(varID);
				} else if(uType.compare("dvectors") == 0) {
					u = &getUniformDVector(varID);
				} else if(uType.compare("matrices") == 0) {
					u = &getUniformMatrix(varID);
				} else if(uType.compare("dmatrices") == 0) {
					u = &getUniformDMatrix(varID);
				}
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: uniform " << varID << " in renderobject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//set name used with the current RenderObject
			u->setUniformName(varName,id);
			if(isUniformSet) {
				//parsing uniform of uniform set
				//get uniform set
				unordered_map<string, Uniform *> &uSet = obj.getUniformSet(uniformSetID);
				unordered_map<string,Uniform *>::iterator iter = uSet.find(varName);
				if(iter != uSet.end()) {
					//another uniform has the same name
					delete data;
					stringstream errmsg;
					errmsg << "Error: uniform " << varID << " in renderobject " << id << " has name " << varName << ", but another uniform has the same name";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
				//name not used yet, insert uniform
				uSet[varName] = u;
			} else {
				try {
					obj.addUniform(*u);
				} catch(Exception &) {
					//another uniform has the same name
					delete data;
					stringstream errmsg;
					errmsg << "Error: uniform " << varID << " in renderobject " << id << " has name " << varName << ", but another uniform has the same name";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_PASS(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every pass node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: a pass without an id",EX_COMPILE);
		}
		Pass *pass = 0;
		unordered_map<string,Pass *>::iterator iter = passes.find(id);
		if(iter == passes.end()) {
			//make shure no other resources have the same identifyer
			//then create a pass
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than a pass has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			pass = new Pass(id);
			passes[id] = pass;
			resources[id] = pass;
		} else {
			pass = (*iter).second;
		}
		//load pass parts
		pass->setVisible(true);
		string visible;
		bool useVisible = false;
		try {
			visible = rNode->getStrAttribute("visible");
			useVisible = true;
		} catch(Exception &) {
		}
		if(useVisible) {
			//visible has one of the values true or false
			if(visible.compare("true") != 0 && visible.compare("false") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute visible of pass " << id << " can only have values \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			pass->setVisible(visible.compare("true") == 0);
		}
		string wireframe;
		bool useWireframe = false;
		try {
			wireframe = rNode->getStrAttribute("wireframe");
			useWireframe = true;
		} catch(Exception &) {
		}
		if(useWireframe) {
			//wireframe has one of the values true or false
			if(wireframe.compare("true") != 0 && wireframe.compare("false") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute wireframe of pass " << id << " can only have values \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			pass->setWireframe(wireframe.compare("true") == 0);
		}
		string backgroundQuad;
		bool useBackgroundQuad = false;
		try {
			backgroundQuad = rNode->getStrAttribute("backgroundQuad");
			useBackgroundQuad = true;
		} catch(Exception &) {
		}
		if(useBackgroundQuad) {
			//backgroundQuad has one of the values true or false
			if(backgroundQuad.compare("true") != 0 && backgroundQuad.compare("false") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute backgroundQuad of pass " << id << " can only have values \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			pass->useBackgroundQuad(backgroundQuad.compare("true") == 0);
		}
		string geometryoutput;
		bool useGeometryoutput = false;
		try {
			geometryoutput = rNode->getStrAttribute("geometryoutput");
			useGeometryoutput = true;
		} catch(Exception &) {
		}
		if(useGeometryoutput) {
			//if another resource than a BufferedMesh
			//has identifyer geometryoutput, an exception
			//is thrown
			try {
				BufferedMesh &mesh = getBufferedMesh(geometryoutput);
				pass->setOutput(&mesh);
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute geometryoutput " << geometryoutput << " in pass " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
		}
		XTag *output = 0;
		try {
			//search for node output
			//to collect data about the
			//output of the pass
			output = rNode->getFirst("output");
		} catch(Exception &) {
		}
		if(output != 0) {
			//analyse output
			string rendertargetID;
			try {
				rendertargetID = output->getStrAttribute("rendertarget");
			} catch(Exception &) {
				//rendertarget is an option of output, which must
				//be specified, otherwise output can't be set
				// -> stop
				delete data;
				stringstream errmsg;
				errmsg << "Error: output of pass " << id << " doesn't specify a rendertarget";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			RenderTarget *target = 0;
			try {
				target = &getRenderTarget(rendertargetID);
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: rendertarget in pass " << id << "uses id " << rendertargetID << ", although another resource uses the same id";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			vector<string> textureIDs;
			vector<Texture *> textures;
			output->getTexts(textureIDs);
			BOOST_FOREACH(const string &textureToken , textureIDs) {
				//collect output textures without changed names
				stringstream textureID;
				try {
					textureID << id << "." << textureToken;
					Texture *texture = &getTexture(textureID.str());
					textures.push_back(texture);
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: a texture in pass " << id << " has id " << textureID.str() << ", but another resource has the same id";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}
			for(map<string,string>::iterator iter = output->stringAttribs.begin() ; iter != output->stringAttribs.end() ; iter++) {
				//collect named output textures
				const string &tName = (*iter).first;
				const string &tID = (*iter).second;
				if(tName.compare("clearDepthBuffer") == 0 || tName.compare("clearColorBuffer") == 0 || tName.compare("rendertarget") == 0
					|| tName.compare("fragmentBlendFactor") == 0 || tName.compare("targetBlendFactor") == 0 || tName.compare("depthTest") == 0 || tName.compare("writeDepthBuffer") == 0) {
					//not a target name, but a property, hence don't
					//evaluate it as a target
					continue;
				}
				try {
					Texture *texture = &getTexture(tID);
					texture->setUniformName(tName,id);
					textures.push_back(texture);
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: texture in pass " << id << " uses identifyer " << tID << ", but another resource has the same id";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}
			pass->setOutput(*target,textures);
			pass->setClearDepthBuffer(true);
			pass->setClearColorBuffer(true);
			string clearDepthbuffer;
			bool useClearDepthbuffer = false;
			try {
				clearDepthbuffer = output->getStrAttribute("clearDepthBuffer");
				useClearDepthbuffer = true;
			} catch(Exception &) {
			}
			if(useClearDepthbuffer) {
				//option clearDepthbuffer specified
				if(clearDepthbuffer.compare("true") != 0 && clearDepthbuffer.compare("false") != 0) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option clearDepthBuffer in pass " << id << " must have value \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				pass->setClearDepthBuffer(clearDepthbuffer.compare("true") == 0);
			}
			string clearColorbuffer;
			bool useClearColorbuffer = false;
			try {
				clearColorbuffer = output->getStrAttribute("clearColorBuffer");
				useClearColorbuffer = true;
			} catch(Exception &) {
			}
			if(useClearColorbuffer) {
				//option clearColorbuffer specified
				if(clearColorbuffer.compare("true") != 0 && clearColorbuffer.compare("false") != 0) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option clearColorBuffer in pass " << id << " must have value \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				pass->setClearColorBuffer(clearColorbuffer.compare("true") == 0);
			}

			string fragmentBlendFactor;
			bool useFragmentBlendFactor = false;
			try {
				fragmentBlendFactor = output->getStrAttribute("fragmentBlendFactor");
				useFragmentBlendFactor = true;
			} catch(Exception &) {
			}
			if(useFragmentBlendFactor) {
				//option clearColorbuffer specified
				if(fragmentBlendFactor.compare("zero") != 0 && fragmentBlendFactor.compare("one") != 0
					 && fragmentBlendFactor.compare("fragmentcolor") != 0 && fragmentBlendFactor.compare("framebuffercolor") != 0
					 && fragmentBlendFactor.compare("fragmentalpha") != 0 && fragmentBlendFactor.compare("framebufferalpha") != 0
					 && fragmentBlendFactor.compare("one_minus_fragmentcolor") != 0 && fragmentBlendFactor.compare("one_minus_framebuffercolor") != 0
					 && fragmentBlendFactor.compare("one_minus_fragmentalpha") != 0 && fragmentBlendFactor.compare("one_minus_framebufferalpha") != 0
					) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option fragmentBlendFactor in pass " << id << " must have value \"zero\" or \"one\" or \"fragmentcolor\" or \"framebuffercolor\" or \"fragmentalpha\" or \"framebufferalpha\" or \"one_minus_fragmentcolor\" or \"one_minus_framebuffercolor\" or \"one_minus_fragmentalpha\" or \"one_minus_framebufferalpha\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				BlendFactor factor = ZERO;
				if(fragmentBlendFactor.compare("zero") == 0) {
					factor = ZERO;
				} else if(fragmentBlendFactor.compare("one") == 0) {
					factor = ONE;
				} else if(fragmentBlendFactor.compare("fragmentcolor") == 0) {
					factor = FRAGMENTCOLOR;
				} else if(fragmentBlendFactor.compare("framebuffercolor") == 0) {
					factor = FRAMEBUFFERCOLOR;
				} else if(fragmentBlendFactor.compare("fragmentalpha") == 0) {
					factor = FRAGMENTALPHA;
				} else if(fragmentBlendFactor.compare("framebufferalpha") == 0) {
					factor = FRAMEBUFFERALPHA;
				} else if(fragmentBlendFactor.compare("one_minus_fragmentcolor") == 0) {
					factor = ONE_MINUS_FRAGMENTCOLOR;
				} else if(fragmentBlendFactor.compare("one_minus_framebuffercolor") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERCOLOR;
				} else if(fragmentBlendFactor.compare("one_minus_fragmentalpha") == 0) {
					factor = ONE_MINUS_FRAGMENTALPHA;
				} else if(fragmentBlendFactor.compare("one_minus_framebufferalpha") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERALPHA;
				}
				pass->setFragmentBlendFactor(factor);
			}

			string targetBlendFactor;
			bool useTargetBlendFactor = false;
			try {
				targetBlendFactor = output->getStrAttribute("targetBlendFactor");
				useTargetBlendFactor = true;
			} catch(Exception &) {
			}
			if(useTargetBlendFactor) {
				//option clearColorbuffer specified
				if(targetBlendFactor.compare("zero") != 0 && targetBlendFactor.compare("one") != 0
					 && targetBlendFactor.compare("fragmentcolor") != 0 && targetBlendFactor.compare("framebuffercolor") != 0
					 && targetBlendFactor.compare("fragmentalpha") != 0 && targetBlendFactor.compare("framebufferalpha") != 0
					 && targetBlendFactor.compare("one_minus_fragmentcolor") != 0 && targetBlendFactor.compare("one_minus_framebuffercolor") != 0
					 && targetBlendFactor.compare("one_minus_fragmentalpha") != 0 && targetBlendFactor.compare("one_minus_framebufferalpha") != 0
					) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option targetBlendFactor in pass " << id << " must have value \"zero\" or \"one\" or \"fragmentcolor\" or \"framebuffercolor\" or \"fragmentalpha\" or \"framebufferalpha\" or \"one_minus_fragmentcolor\" or \"one_minus_framebuffercolor\" or \"one_minus_fragmentalpha\" or \"one_minus_framebufferalpha\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				BlendFactor factor = ZERO;
				if(targetBlendFactor.compare("zero") == 0) {
					factor = ZERO;
				} else if(targetBlendFactor.compare("one") == 0) {
					factor = ONE;
				} else if(targetBlendFactor.compare("fragmentcolor") == 0) {
					factor = FRAGMENTCOLOR;
				} else if(targetBlendFactor.compare("framebuffercolor") == 0) {
					factor = FRAMEBUFFERCOLOR;
				} else if(targetBlendFactor.compare("fragmentalpha") == 0) {
					factor = FRAGMENTALPHA;
				} else if(targetBlendFactor.compare("framebufferalpha") == 0) {
					factor = FRAMEBUFFERALPHA;
				} else if(targetBlendFactor.compare("one_minus_fragmentcolor") == 0) {
					factor = ONE_MINUS_FRAGMENTCOLOR;
				} else if(targetBlendFactor.compare("one_minus_framebuffercolor") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERCOLOR;
				} else if(targetBlendFactor.compare("one_minus_fragmentalpha") == 0) {
					factor = ONE_MINUS_FRAGMENTALPHA;
				} else if(targetBlendFactor.compare("one_minus_framebufferalpha") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERALPHA;
				}
				pass->setTargetBlendFactor(factor);
			}

			string depthTest;
			bool useDepthTest = false;
			try {
				depthTest = output->getStrAttribute("depthTest");
				useDepthTest = true;
			} catch(Exception &) {
			}
			if(useDepthTest != 0) {
				//option clearColorbuffer specified
				if(depthTest.compare("accept_never") != 0 && depthTest.compare("accept_always") != 0
					 && depthTest.compare("accept_less") != 0 && depthTest.compare("accept_less_equal") != 0
					 && depthTest.compare("accept_greater") != 0 && depthTest.compare("accept_greater_equal") != 0
					 && depthTest.compare("accept_equal") != 0 && depthTest.compare("accept_not_equal") != 0
					) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option depthTest in pass " << id << " must have value \"accept_never\" or \"accept_always\" or \"accept_less\" or \"accept_less_equal\" or \"accept_greater\" or \"accept_greater_equal\" or \"accept_equal\" or \"accept_not_equal\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				DepthTest test = ACCEPT_LESS;
				if(depthTest.compare("accept_never") == 0) {
					test = ACCEPT_NEVER;
				} else if(depthTest.compare("accept_always") == 0) {
					test = ACCEPT_ALWAYS;
				} else if(depthTest.compare("accept_less") == 0) {
					test = ACCEPT_LESS;
				} else if(depthTest.compare("accept_less_equal") == 0) {
					test = ACCEPT_LESS_EQUAL;
				} else if(depthTest.compare("accept_greater") == 0) {
					test = ACCEPT_GREATER;
				} else if(depthTest.compare("accept_greater_equal") == 0) {
					test = ACCEPT_GREATER_EQUAL;
				} else if(depthTest.compare("accept_equal") == 0) {
					test = ACCEPT_EQUAL;
				} else if(depthTest.compare("accept_not_equal") == 0) {
					test = ACCEPT_NOT_EQUAL;
				}
				pass->setDepthTest(test);
			}
			string writeDepthBuffer;
			bool useWriteDepthBuffer = false;
			try {
				writeDepthBuffer = output->getStrAttribute("writeDepthBuffer");
				useWriteDepthBuffer = true;
			} catch(Exception &) {
			}
			if(useWriteDepthBuffer != 0) {
				//option clearColorbuffer specified
				if(writeDepthBuffer.compare("true") != 0 && writeDepthBuffer.compare("false") != 0) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option writeDepthBuffer in pass " << id << " must have value \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				pass->setWriteDepthBuffer(writeDepthBuffer.compare("true") == 0);
			}
		}
		XTag *volumeoutput = 0;
		try {
			//search for node volumeoutput
			//to collect data about the
			//volume output of the pass
			volumeoutput = rNode->getFirst("volumeoutput");
		} catch(Exception &) {
		}
		if(volumeoutput != 0) {
			//analyze volumeoutput
			string rendertargetID;
			try {
				rendertargetID = volumeoutput->getStrAttribute("rendertarget");
			} catch(Exception &) {
				//rendertarget is an option of output, which must
				//be specified, otherwise output can't be set
				// -> stop
				delete data;
				stringstream errmsg;
				errmsg << "Error: output of pass " << id << " doesn't specify a rendertarget";
				throw Exception(errmsg.str(),EX_SYNTAX);
			}
			RenderTarget *target = 0;
			try {
				target = &getRenderTarget(rendertargetID);
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: rendertarget in pass " << id << "uses id " << rendertargetID << ", although another resource uses the same id";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			vector<string> volumeIDs;
			vector<Volume *> volumes;
			volumeoutput->getDirectTexts(volumeIDs);
			BOOST_FOREACH(const string &volumeToken , volumeIDs) {
				//collect output volumes without changed names
				stringstream volumeID;
				try {
					volumeID << id << "." << volumeToken;
					Volume *volume = &getVolume(volumeID.str());
					const string &name = volume->getUniformName(id);
					volumes.push_back(volume);
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: a volume in pass " << id << " has id " << volumeID.str() << ", but another resource has the same id";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}
			for(map<string,string>::iterator iter = volumeoutput->stringAttribs.begin() ; iter != volumeoutput->stringAttribs.end() ; iter++) {
				//collect named output textures
				const string &vName = (*iter).first;
				const string &vID = (*iter).second;
				if(vName.compare("clearDepthBuffer") == 0 || vName.compare("clearColorBuffer") == 0 || vName.compare("rendertarget") == 0
					|| vName.compare("fragmentBlendFactor") == 0 || vName.compare("targetBlendFactor") == 0 || vName.compare("depthTest") == 0 || vName.compare("writeDepthBuffer") == 0) {
					//not a target name, but a property, hence don't
					//evaluate it as a target
					continue;
				}
				try {
					Volume *volume = &getVolume(vID);
					volume->setUniformName(vName,id);
					volumes.push_back(volume);
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: volume in pass " << id << " uses identifyer " << vID << ", but another resource has the same id";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}
			pass->setOutput(*target,volumes);
			pass->setClearDepthBuffer(true);
			pass->setClearColorBuffer(true);
			string clearDepthbuffer;
			bool useClearDepthbuffer = false;
			try {
				clearDepthbuffer = volumeoutput->getStrAttribute("clearDepthBuffer");
				useClearDepthbuffer = true;
			} catch(Exception &) {
			}
			if(useClearDepthbuffer) {
				//option clearDepthbuffer specified
				if(clearDepthbuffer.compare("true") != 0 && clearDepthbuffer.compare("false") != 0) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option clearDepthBuffer in pass " << id << " must have value \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				pass->setClearDepthBuffer(clearDepthbuffer.compare("true") == 0);
			}
			string clearColorbuffer;
			bool useClearColorbuffer = false;
			try {
				clearColorbuffer = volumeoutput->getStrAttribute("clearColorBuffer");
				useClearColorbuffer = true;
			} catch(Exception &) {
			}
			if(useClearColorbuffer) {
				//option clearColorbuffer specified
				if(clearColorbuffer.compare("true") != 0 && clearColorbuffer.compare("false") != 0) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option clearColorBuffer in pass " << id << " must have value \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				pass->setClearColorBuffer(clearColorbuffer.compare("true") == 0);
			}

			string fragmentBlendFactor;
			bool useFragmentBlendFactor = false;
			try {
				fragmentBlendFactor = volumeoutput->getStrAttribute("fragmentBlendFactor");
				useFragmentBlendFactor = true;
			} catch(Exception &) {
			}
			if(useFragmentBlendFactor) {
				//option clearColorbuffer specified
				if(fragmentBlendFactor.compare("zero") != 0 && fragmentBlendFactor.compare("one") != 0
					 && fragmentBlendFactor.compare("fragmentcolor") != 0 && fragmentBlendFactor.compare("framebuffercolor") != 0
					 && fragmentBlendFactor.compare("fragmentalpha") != 0 && fragmentBlendFactor.compare("framebufferalpha") != 0
					 && fragmentBlendFactor.compare("one_minus_fragmentcolor") != 0 && fragmentBlendFactor.compare("one_minus_framebuffercolor") != 0
					 && fragmentBlendFactor.compare("one_minus_fragmentalpha") != 0 && fragmentBlendFactor.compare("one_minus_framebufferalpha") != 0
					) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option fragmentBlendFactor in pass " << id << " must have value \"zero\" or \"one\" or \"fragmentcolor\" or \"framebuffercolor\" or \"fragmentalpha\" or \"framebufferalpha\" or \"one_minus_fragmentcolor\" or \"one_minus_framebuffercolor\" or \"one_minus_fragmentalpha\" or \"one_minus_framebufferalpha\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				BlendFactor factor = ZERO;
				if(fragmentBlendFactor.compare("zero") == 0) {
					factor = ZERO;
				} else if(fragmentBlendFactor.compare("one") == 0) {
					factor = ONE;
				} else if(fragmentBlendFactor.compare("fragmentcolor") == 0) {
					factor = FRAGMENTCOLOR;
				} else if(fragmentBlendFactor.compare("framebuffercolor") == 0) {
					factor = FRAMEBUFFERCOLOR;
				} else if(fragmentBlendFactor.compare("fragmentalpha") == 0) {
					factor = FRAGMENTALPHA;
				} else if(fragmentBlendFactor.compare("framebufferalpha") == 0) {
					factor = FRAMEBUFFERALPHA;
				} else if(fragmentBlendFactor.compare("one_minus_fragmentcolor") == 0) {
					factor = ONE_MINUS_FRAGMENTCOLOR;
				} else if(fragmentBlendFactor.compare("one_minus_framebuffercolor") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERCOLOR;
				} else if(fragmentBlendFactor.compare("one_minus_fragmentalpha") == 0) {
					factor = ONE_MINUS_FRAGMENTALPHA;
				} else if(fragmentBlendFactor.compare("one_minus_framebufferalpha") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERALPHA;
				}
				pass->setFragmentBlendFactor(factor);
			}

			string targetBlendFactor;
			bool useTargetBlendFactor = false;
			try {
				targetBlendFactor = volumeoutput->getStrAttribute("targetBlendFactor");
				useTargetBlendFactor = true;
			} catch(Exception &) {
			}
			if(useTargetBlendFactor) {
				//option clearColorbuffer specified
				if(targetBlendFactor.compare("zero") != 0 && targetBlendFactor.compare("one") != 0
					 && targetBlendFactor.compare("fragmentcolor") != 0 && targetBlendFactor.compare("framebuffercolor") != 0
					 && targetBlendFactor.compare("fragmentalpha") != 0 && targetBlendFactor.compare("framebufferalpha") != 0
					 && targetBlendFactor.compare("one_minus_fragmentcolor") != 0 && targetBlendFactor.compare("one_minus_framebuffercolor") != 0
					 && targetBlendFactor.compare("one_minus_fragmentalpha") != 0 && targetBlendFactor.compare("one_minus_framebufferalpha") != 0
					) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option targetBlendFactor in pass " << id << " must have value \"zero\" or \"one\" or \"fragmentcolor\" or \"framebuffercolor\" or \"fragmentalpha\" or \"framebufferalpha\" or \"one_minus_fragmentcolor\" or \"one_minus_framebuffercolor\" or \"one_minus_fragmentalpha\" or \"one_minus_framebufferalpha\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				BlendFactor factor = ZERO;
				if(targetBlendFactor.compare("zero") == 0) {
					factor = ZERO;
				} else if(targetBlendFactor.compare("one") == 0) {
					factor = ONE;
				} else if(targetBlendFactor.compare("fragmentcolor") == 0) {
					factor = FRAGMENTCOLOR;
				} else if(targetBlendFactor.compare("framebuffercolor") == 0) {
					factor = FRAMEBUFFERCOLOR;
				} else if(targetBlendFactor.compare("fragmentalpha") == 0) {
					factor = FRAGMENTALPHA;
				} else if(targetBlendFactor.compare("framebufferalpha") == 0) {
					factor = FRAMEBUFFERALPHA;
				} else if(targetBlendFactor.compare("one_minus_fragmentcolor") == 0) {
					factor = ONE_MINUS_FRAGMENTCOLOR;
				} else if(targetBlendFactor.compare("one_minus_framebuffercolor") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERCOLOR;
				} else if(targetBlendFactor.compare("one_minus_fragmentalpha") == 0) {
					factor = ONE_MINUS_FRAGMENTALPHA;
				} else if(targetBlendFactor.compare("one_minus_framebufferalpha") == 0) {
					factor = ONE_MINUS_FRAMEBUFFERALPHA;
				}
				pass->setTargetBlendFactor(factor);
			}

			string depthTest;
			bool useDepthTest = false;
			try {
				depthTest = volumeoutput->getStrAttribute("depthTest");
				useDepthTest = true;
			} catch(Exception &) {
			}
			if(useDepthTest) {
				//option clearColorbuffer specified
				if(depthTest.compare("accept_never") != 0 && depthTest.compare("accept_always") != 0
					 && depthTest.compare("accept_less") != 0 && depthTest.compare("accept_less_equal") != 0
					 && depthTest.compare("accept_greater") != 0 && depthTest.compare("accept_greater_equal") != 0
					 && depthTest.compare("accept_equal") != 0 && depthTest.compare("accept_not_equal") != 0
					) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option depthTest in pass " << id << " must have value \"accept_never\" or \"accept_always\" or \"accept_less\" or \"accept_less_equal\" or \"accept_greater\" or \"accept_greater_equal\" or \"accept_equal\" or \"accept_not_equal\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				DepthTest test = ACCEPT_LESS;
				if(depthTest.compare("accept_never") == 0) {
					test = ACCEPT_NEVER;
				} else if(depthTest.compare("accept_always") == 0) {
					test = ACCEPT_ALWAYS;
				} else if(depthTest.compare("accept_less") == 0) {
					test = ACCEPT_LESS;
				} else if(depthTest.compare("accept_less_equal") == 0) {
					test = ACCEPT_LESS_EQUAL;
				} else if(depthTest.compare("accept_greater") == 0) {
					test = ACCEPT_GREATER;
				} else if(depthTest.compare("accept_greater_equal") == 0) {
					test = ACCEPT_GREATER_EQUAL;
				} else if(depthTest.compare("accept_equal") == 0) {
					test = ACCEPT_EQUAL;
				} else if(depthTest.compare("accept_not_equal") == 0) {
					test = ACCEPT_NOT_EQUAL;
				}
				pass->setDepthTest(test);
			}
			string writeDepthBuffer;
			bool useWriteDepthBuffer = false;
			try {
				writeDepthBuffer = volumeoutput->getStrAttribute("writeDepthBuffer");
				useWriteDepthBuffer = true;
			} catch(Exception &) {
			}
			if(useWriteDepthBuffer) {
				//option clearColorbuffer specified
				if(writeDepthBuffer.compare("true") != 0 && writeDepthBuffer.compare("false") != 0) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: option writeDepthBuffer in pass " << id << " must have value \"true\" or \"false\"";
					throw Exception(errmsg.str(),EX_SYNTAX);
				}
				pass->setWriteDepthBuffer(writeDepthBuffer.compare("true") == 0);
			}

			XTag *layercoordinate = 0;
			try {
				//search for node layercoordinate
				//to use a uniform float as a layercoordinate
				layercoordinate = volumeoutput->getFirst("layercoordinate");
			} catch(Exception &) {
			}
			if(layercoordinate != 0) {
				//analyze layercoordinate
				UniformFloat *lcoordinate = 0;
				vector<string> layercoordinates;
				layercoordinate->getTexts(layercoordinates);
				if(layercoordinates.size() > 0) {
					stringstream lcID;
					lcID << id << "." << layercoordinates[layercoordinates.size()-1];
					try {
						lcoordinate = &getUniformFloat(lcID.str());
						const string &name = lcoordinate->getUniformName(id);
						pass->setLayerCoordinate(lcoordinate);
					} catch(Exception &) {
						delete data;
						stringstream errmsg;
						errmsg << "Error: id " << lcID.str() << " can't be used as a layer coordinate of pass " << id << ", as another resource uses the same id";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
				}
				if(layercoordinate->stringAttribs.size() > 0) {
					map<string,string>::iterator coordIter = layercoordinate->stringAttribs.begin(); 
					const string &coordName = (*coordIter).first;
					const string &coordID = (*coordIter).second;
					try {
						lcoordinate = &getUniformFloat(coordID);
						lcoordinate->setUniformName(coordName,id);
						pass->setLayerCoordinate(lcoordinate);
					} catch(Exception &) {
						delete data;
						stringstream errmsg;
						errmsg << "Error: id " << coordID << " can't be used as a layer coordinate of pass " << id << ", as another resource uses the same id";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
				}
			}

			XTag *startlayer = 0;
			try {
				//search for node startlayer
				//to use an index for the first layer
				startlayer = volumeoutput->getFirst("startlayer");
			} catch(Exception &) {
			}
			if(startlayer != 0) {
				//analyze startlayer
				UniformFloat *sLayer = 0;
				vector<string> slIDs;
				startlayer->getTexts(slIDs);
				if(slIDs.size() > 0) {
					//ID of startlayer specified
					//hence investigate into ID and value
					try {
						sLayer = &getUniformFloat(slIDs[slIDs.size() - 1]);
						pass->setStartLayer(sLayer);
					} catch(Exception &) {
						delete data;
						stringstream errmsg;
						errmsg << "Error: id " << slIDs[slIDs.size() - 1] << " can't be used as a start layer index of pass " << id << ", as another resource uses the same id";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
					try {
						//assign value to startlayer
						//if a value is specified
						float value = (float)startlayer->getRealAttribute("value");
						(*sLayer) << value;
					} catch(Exception &) {
					}
				}
			}
			XTag *endlayer = 0;
			try {
				//search for node endlayer
				//to use an index for the last layer
				endlayer = volumeoutput->getFirst("endlayer");
			} catch(Exception &) {
			}
			if(endlayer != 0) {
				//analyze endlayer
				UniformFloat *eLayer = 0;
				vector<string> elIDs;
				endlayer->getTexts(elIDs);
				if(elIDs.size() > 0) {
					//ID of endlayer specified
					//hence investigate into ID and value
					try {
						eLayer = &getUniformFloat(elIDs[elIDs.size() - 1]);
						pass->setEndLayer(eLayer);
					} catch(Exception &) {
						delete data;
						stringstream errmsg;
						errmsg << "Error: id " << elIDs[elIDs.size() - 1] << " can't be used as a end layer index of pass " << id << ", as another resource uses the same id";
						throw Exception(errmsg.str(),EX_AMBIGUOUS);
					}
					try {
						//assign value to endlayer
						//if a value is specified
						float value = (float)endlayer->getRealAttribute("value");
						(*eLayer) << value;
					} catch(Exception &) {
					}
				}
			}

		}

		string shaderID;
		bool useShader = false;
		try {
			shaderID = rNode->getStrAttribute("shader");
			useShader = true;
		} catch(Exception &) {
		}
		if(useShader) {
			//shader id is nonempty
			//use shader with identifyer shaderID
			try {
				Shader *shader = &getShader(shaderID);
				pass->setShader(shader);
			} catch(Exception &) {
				//another resource uses the id shaderID
				// -> stop
				delete data;
				stringstream errmsg;
				errmsg << "Error: pass " << id << " tried to load shader " << shaderID << ", but another resource uses identifyer " << shaderID;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
		}
		BOOST_FOREACH(XNode *node , rNode->nodes) {
			XTag *tag = dynamic_cast<XTag *>(node);
			if(tag != 0) {
				if(tag->name.compare("textures") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"textures",tag,data);
				} else if(tag->name.compare("volumes") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"volumes",tag,data);
				} else if(tag->name.compare("floats") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"floats",tag,data);
				} else if(tag->name.compare("doubles") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"doubles",tag,data);
				} else if(tag->name.compare("vectors") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"vectors",tag,data);
				} else if(tag->name.compare("dvectors") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"dvectors",tag,data);
				} else if(tag->name.compare("matrices") == 0) {
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"matrices",tag,data);
				} else if(tag->name.compare("dmatrices") == 0) {		
					INTERNAL_PASS_COLLECT_UNIFORMS(*pass,id,"dmatrices",tag,data);
				} else if(tag->name.compare("output") == 0) {
					//tag output has already been dealt with
				} else if(tag->name.compare("volumeoutput") == 0) {
					//tag volumeoutput has already been dealt with
				} else if(tag->name.compare("objects") == 0) {
					//collect RenderObjects
					//add them to the pass
					for(vector<XNode *>::iterator strIter = tag->nodes.begin() ; strIter != tag->nodes.end() ; strIter++) {
						XText *str = dynamic_cast<XText *>((*strIter));
						if(str == 0) {
							//not a textnode, skip
							continue;
						}
						RenderObject *rObj = 0;						
						try {
							rObj = &getRenderObject(str->text);
							pass->addRenderObject(*rObj);
						} catch(Exception &) {
							delete data;
							stringstream errmsg;
							errmsg << "Error: RenderObject " << str->text << " listed in tag objects in Pass " << id << " has an id used by another resource";
							throw Exception(errmsg.str(),EX_COMPILE);
						}
					}
				} else {
					//tag with a wrong name
					delete data;
					stringstream errmsg;
					errmsg << "Error: tag " << tag->name << " found in Pass " << id << ", but tags must be called \"textures\", \"volumes\", \"floats\", \"doubles\", \"vectors\", \"dvectors\", \"matrices\", \"dmatrices\", \"output\" or \"volumeoutput\"";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}
		
		//check, if incorrect attribute names are used
		unsigned int attributeCount = 1;
		if(useBackgroundQuad) {
			attributeCount++;
		}
		if(useVisible) {
			attributeCount++;
		}
		if(useShader) {
			attributeCount++;
		}
		if(useGeometryoutput) {
			attributeCount++;
		}
		if(useWireframe) {
			attributeCount++;
		}
		if(rNode->stringAttribs.size() != attributeCount || rNode->realAttribs.size() != 0) {
			//tag with a wrong name
			delete data;
			stringstream errmsg;
			errmsg << "Error: tag " << rNode->name << " found in Pass " << id << ", but tags must be called \"id\", \"backgroundQuad\", \"wireframe\", \"visible\", \"shader\" or \"geometryoutput\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_PASS_COLLECT_UNIFORMS(Pass &pass, const string &id, const string &uType, XTag *rNode, XTag *data) {
		BOOST_FOREACH(XNode *node,rNode->nodes) {
			//add uniform without changed names
			XText *uID = dynamic_cast<XText *>(node);
			if(uID != 0) {
				stringstream fullID;
				fullID << id << "." << uID->text;
				Uniform *u = 0;
				try {
					//get uniform
					if(uType.compare("textures") == 0) {
						u = &getTexture(fullID.str());
					} else if(uType.compare("volumes") == 0) {
						u = &getVolume(fullID.str());
					} else if(uType.compare("floats") == 0) {
						u = &getUniformFloat(fullID.str());
					} else if(uType.compare("doubles") == 0) {
						u = &getUniformDouble(fullID.str());
					} else if(uType.compare("vectors") == 0) {
						u = &getUniformVector(fullID.str());
					} else if(uType.compare("dvectors") == 0) {
						u = &getUniformDVector(fullID.str());
					} else if(uType.compare("matrices") == 0) {
						u = &getUniformMatrix(fullID.str());
					} else if(uType.compare("dmatrices") == 0) {
						u = &getUniformDMatrix(fullID.str());
					}
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: uniform " << fullID.str() << " in pass " << id << " has an id used by another resource";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
				const string &name = u->getUniformName(id);
				try {
					pass.addUniform(*u);
				} catch(Exception &) {
					//another uniform has the same name
					delete data;
					stringstream errmsg;
					errmsg << "Error: uniform " << fullID.str() << " in pass " << id << " has name " << name << ", but another uniform has the same name";
					throw Exception(errmsg.str(),EX_AMBIGUOUS);
				}
			}
		}
		for(map<string,string>::iterator iter = rNode->stringAttribs.begin() ; iter != rNode->stringAttribs.end() ; iter++) {
			//use id of attribute as name of uniform
			const string &varName = (*iter).first;
			//use value of attribute as uniform identifyer
			const string &varID = (*iter).second;
			Uniform *u = 0;
			try {
				//get uniform
				if(uType.compare("textures") == 0) {
					u = &getTexture(varID);
				} else if(uType.compare("volumes") == 0) {
					u = &getVolume(varID);
				} else if(uType.compare("floats") == 0) {
					u = &getUniformFloat(varID);
				} else if(uType.compare("doubles") == 0) {
					u = &getUniformDouble(varID);
				} else if(uType.compare("vectors") == 0) {
					u = &getUniformVector(varID);
				} else if(uType.compare("dvectors") == 0) {
					u = &getUniformDVector(varID);
				} else if(uType.compare("matrices") == 0) {
					u = &getUniformMatrix(varID);
				} else if(uType.compare("dmatrices") == 0) {
					u = &getUniformDMatrix(varID);
				}
			} catch(Exception &) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: uniform " << varID << " in pass " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//set name used with the current RenderObject
			u->setUniformName(varName,id);
			try {
				pass.addUniform(*u);
			} catch(Exception &) {
				//another uniform has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: uniform " << varID << " in pass " << id << " has name " << varName << ", but another uniform has the same name";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
		}
	}

	void ShadeX::INTERNAL_LOAD_EFFECT(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every effect node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: an effect without an id",EX_COMPILE);
		}
		Effect *effect = 0;
		unordered_map<string,Effect *>::iterator iter = effects.find(id);
		if(iter == effects.end()) {
			//make shure no other resources have the same identifyer
			//then create an effect
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than an effect has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			effect = new Effect(id);
			effects[id] = effect;
			resources[id] = effect;
		} else {
			effect = (*iter).second;
		}
		//load effect parts
		effect->setVisible(true);
		string visible;
		bool useVisible = false;
		try {
			visible = rNode->getStrAttribute("visible");
			useVisible = true;
		} catch(Exception &) {
		}
		if(useVisible) {
			//visible can have values true or false
			if(visible.compare("true") != 0 && visible.compare("false") != 0) {
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute visible of effect " << id << " can only have values \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			effect->setVisible(visible.compare("true") == 0);
		}
		vector<EffectCommand> &eObjects = effect->getObjects();
		for(vector<XNode *>::iterator containerIter = rNode->nodes.begin() ; containerIter != rNode->nodes.end() ; containerIter++) {
			//collect passes and meshes
			XTag *containerTag = dynamic_cast<XTag *>((*containerIter));
			if(containerTag == 0) {
				//not a tag, skip element
				continue;
			}
			bool isPass = false;
			bool isMesh = false;
			bool isSkeleton = false;
			if(containerTag->name.compare("passes") == 0) {
				isPass = true;
			} else if(containerTag->name.compare("meshes") == 0) {
				isMesh = true;
			} else if(containerTag->name.compare("skeletons") == 0) {
				isSkeleton = true;
			} else {
				//wrong tagname
				delete data;
				stringstream errmsg;
				errmsg << "Error: tag " << containerTag->name << " in Effect " << id << " found, but childtags must be called \"passes\", \"meshes\" or \"skeletons\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			for(vector<XNode *>::iterator objIter = containerTag->nodes.begin() ; objIter != containerTag->nodes.end() ; objIter++) {
				//collect EffectObjects
				XText *objText = dynamic_cast<XText *>((*objIter));
				XTag *objTag = dynamic_cast<XTag *>((*objIter));
				string objID;
				vector<string> input;
				if(objText != 0) {
					objID = objText->text;
				} else if(objTag != 0) {
					objID = objTag->name;
					objTag->getTexts(input);
				} else {
					//an empty element, skip it
					continue;
				}
				EffectObject *obj = 0;
				try {
					if(isPass) {
						obj = &getPass(objID);
					} else if(isMesh) {
						//check if input strings are correct
						map<string,int> checkInput;
						checkInput["readUniforms"] = 0;
						checkInput["writeUniforms"] = 0;
						for(vector<string>::iterator sIter = input.begin() ; sIter != input.end() ; sIter++) {
							const string &sElem = (*sIter);
							map<string,int>::iterator fIter = checkInput.find(sElem);
							if(fIter == checkInput.end()) {
								//wrong input
								delete data;
								stringstream errmsg;
								errmsg << "Error: input of meshes in Effect " << id << " must be \"readUniforms\" or \"writeUniforms\"";
								throw Exception(errmsg.str(), EX_COMPILE);
							}
						}
						//find mesh
						obj = &getBufferedMesh(objID);
					} else if(isSkeleton) {
						obj = &getSkeleton(objID);
					} else {
						throw Exception();
					}
				} catch(Exception &) {
					delete data;
					stringstream errmsg;
					errmsg << "Error: type of EffectObject " << objID << " in Effect " << id << " has another type than a resource with the same id";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
				eObjects.push_back(EffectCommand());
				EffectCommand &command = eObjects[eObjects.size()-1];
				command.object = obj;
				command.input = input;
			}
		}
		//performing errorcheck for attributes
		map<string,int> strControl;
		strControl["id"] = 0;
		strControl["visible"] = 0;
		for(map<string,string>::iterator nIter = rNode->stringAttribs.begin() ; nIter != rNode->stringAttribs.end() ; nIter++) {
			const string &tName = (*nIter).first;
			map<string,int>::iterator control = strControl.find(tName);
			if(control == strControl.end()) {
				//attibute with a wrong name
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute " << tName << " found in Effect " << id << ", but tags must be called \"id\" or \"visible\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		if(rNode->realAttribs.size() != 0) {
			//attribute with wrong name or type
			delete data;
			stringstream errmsg;
			errmsg << "Error: attributes in Effect " << id << " must be called \"id\" or \"visible\", and have a string value";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	
	void ShadeX::INTERNAL_LOAD_AUDIOBUFFER(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every buffer node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: an AudioBuffer without an id",EX_COMPILE);
		}
		AudioBuffer *buffer = 0;
		unordered_map<string,AudioBuffer *>::iterator iter = audioBuffers.find(id);
		if(iter == audioBuffers.end()) {
			//make sure no other resources have the same identifyer
			//then create a pass
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than an AudioBuffer has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			buffer = new AudioBuffer(id);
			audioBuffers[id] = buffer;
			resources[id] = buffer;
		} else {
			buffer = (*iter).second;
		}
		//load AudioBuffer parts
		for(map<string,string>::iterator aIter = rNode->stringAttribs.begin() ; aIter != rNode->stringAttribs.end() ; aIter++) {
			const string &name = (*aIter).first;
			const string &val = (*aIter).second;
			if(name.compare("path") == 0) {
				buffer->setPath(val);
			} else if(name.compare("id") == 0) {
				//attribute id has already been dealt with
			} else {
				//wrong attribute name
				delete data;
				stringstream errmsg;
				errmsg << "Error: AudioBuffer " << id << " must only contain attributes \"id\" and \"path\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		//check for wrong attributes
		if(rNode->realAttribs.size() > 0) {
			//wrong attribute name
			delete data;
			stringstream errmsg;
			errmsg << "Error: AudioBuffer " << id << " must only contain attributes \"id\" and \"path\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_AUDIOOBJECT(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every object node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: an AudioObject without an id",EX_COMPILE);
		}
		AudioObject *object = 0;
		unordered_map<string,AudioObject *>::iterator iter = audioObjects.find(id);
		if(iter == audioObjects.end()) {
			//make sure no other resources have the same identifyer
			//then create a pass
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than an AudioObject has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			object = new AudioObject(id);
			audioObjects[id] = object;
			resources[id] = object;
		} else {
			object = (*iter).second;
		}
		//counters for string attributes
		//for checking if wrong attribute names were used
		unsigned int rAttrCount = 0;
		unsigned int sAttrCount = 1;
		unsigned int tagCount = 0;
		
		//load AudioObject parts
		bool useBuffer = false;
		string buffer;
		try {
			buffer = rNode->getStrAttribute("buffer");
			useBuffer = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useBuffer) {
			try {
				AudioBuffer &b = getAudioBuffer(buffer);
				object->setAudioBuffer(b);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: AudioBuffer " << buffer << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool usePosition = false;
		string position;
		try {
			position = rNode->getStrAttribute("position");
			usePosition = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(usePosition) {
			try {
				UniformVector &up = getUniformVector(position);
				object->setPosition(&up);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformVector " << position << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool useReferenceRadius = false;
		string referenceRadius;
		try {
			referenceRadius = rNode->getStrAttribute("referenceRadius");
			useReferenceRadius = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useReferenceRadius) {
			try {
				UniformFloat &urr = getUniformFloat(referenceRadius);
				object->setReferenceRadius(&urr);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << referenceRadius << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool useMaxRadius = false;
		string maxRadius;
		try {
			maxRadius = rNode->getStrAttribute("maxRadius");
			useMaxRadius = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useMaxRadius) {
			try {
				UniformFloat &umr = getUniformFloat(maxRadius);
				object->setMaxRadius(&umr);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << maxRadius << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool useDistanceExponent = false;
		string distanceExponent;
		try {
			distanceExponent = rNode->getStrAttribute("distanceExponent");
			useDistanceExponent = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useDistanceExponent) {
			try {
				UniformFloat &ude = getUniformFloat(distanceExponent);
				object->setDistanceExponent(&ude);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << distanceExponent << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool useVelocity = false;
		string velocity;
		try {
			velocity = rNode->getStrAttribute("velocity");
			useVelocity = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useVelocity) {
			try {
				UniformVector &uv = getUniformVector(velocity);
				object->setVelocity(&uv);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << velocity << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool useVolume = false;
		string volume;
		try {
			volume = rNode->getStrAttribute("volume");
			useVolume = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useVolume) {
			try {
				UniformFloat &uv = getUniformFloat(volume);
				object->setVolume(&uv);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << volume << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool usePitch = false;
		string pitch;
		try {
			pitch = rNode->getStrAttribute("pitch");
			usePitch = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(usePitch) {
			try {
				UniformFloat &up = getUniformFloat(pitch);
				object->setPitch(&up);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << pitch << " in AudioObject " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}
		bool useLooping = false;
		string looping;
		object->setLooping(false);
		try {
			looping = rNode->getStrAttribute("looping");
			useLooping = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useLooping) {
			if(looping.compare("true") != 0 && looping.compare("false") != 0) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: attribute looping in AudioObject " << id << " must have value \"true\" or \"false\"";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			object->setLooping(looping.compare("true") == 0);
		}
		XTag *passes = 0;
		try {
			passes = rNode->getFirst("passes");
			tagCount++;
		} catch(Exception &) {
		}
		if(passes != 0) {
			for(vector<XNode *>::iterator nIter = passes->nodes.begin() ; nIter != passes->nodes.end() ; nIter++) {
				XText *txt = dynamic_cast<XText *>((*nIter));
				if(txt == 0) {
					//not a text node, skip
					continue;
				}
				try {
					AudioPass &pass = getAudioPass(txt->text);
					pass.addAudioObject(*object);
				} catch(Exception &) {
					//error, another resource has the same name
					delete data;
					stringstream errmsg;
					errmsg << "Error: pass " << txt->text << " in AudioObject " << id << " has an id used by another resource";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}
		//check if tag and attribute names were correct
		if(sAttrCount != rNode->stringAttribs.size() || rAttrCount != rNode->realAttribs.size() || tagCount != rNode->nodes.size()) {
			//error, another resource has the same name
			delete data;
			stringstream errmsg;
			errmsg << "Error: AudioObject " << id << " must only contain zero or one tag called \"passes\", and attributes \"id\", \"buffer\", \"position\", \"referenceRadius\", \"maxRadius\", \"distanceExponent\", \"velocity\", \"volume\", \"pitch\" or \"looping\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_AUDIOLISTENER(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every AudioListener node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: an AudioListener without an id",EX_COMPILE);
		}
		AudioListener *listener = 0;
		unordered_map<string,AudioListener *>::iterator iter = audioListeners.find(id);
		if(iter == audioListeners.end()) {
			//make sure no other resources have the same identifyer
			//then create a pass
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than an AudioListener has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			listener = new AudioListener(id);
			audioListeners[id] = listener;
			resources[id] = listener;
		} else {
			listener = (*iter).second;
		}
		//counters for string attributes
		//for checking if wrong attribute names were used
		unsigned int rAttrCount = 0;
		unsigned int sAttrCount = 1;
		unsigned int tagCount = 0;

		//load AudioListener parts
		bool usePosition = false;
		string position;
		try {
			position = rNode->getStrAttribute("position");
			usePosition = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(usePosition) {
			try {
				UniformVector &upos = getUniformVector(position);
				listener->setPosition(&upos);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformVector " << position << " in AudioListener " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}

		bool useView = false;
		string view;
		try {
			view = rNode->getStrAttribute("view");
			useView = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useView) {
			try {
				UniformVector &uv = getUniformVector(view);
				listener->setView(&uv);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformVector " << view << " in AudioListener " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}

		bool useUp = false;
		string up;
		try {
			up = rNode->getStrAttribute("up");
			useUp = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useUp) {
			try {
				UniformVector &uu = getUniformVector(up);
				listener->setUp(&uu);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformVector " << up << " in AudioListener " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}

		bool useVelocity = false;
		string velocity;
		try {
			velocity = rNode->getStrAttribute("velocity");
			useVelocity = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useVelocity) {
			try {
				UniformVector &uv = getUniformVector(velocity);
				listener->setVelocity(&uv);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformVector " << velocity << " in AudioListener " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}

		bool useVolume = false;
		string volume;
		try {
			volume = rNode->getStrAttribute("volume");
			useVolume = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useVolume) {
			try {
				UniformFloat &uv = getUniformFloat(volume);
				listener->setVolume(&uv);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: UniformFloat " << volume << " in AudioListener " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}

		XTag *passes = 0;
		try {
			passes = rNode->getFirst("passes");
			tagCount++;
		} catch(Exception &) {
		}
		if(passes != 0) {
			for(vector<XNode *>::iterator nIter = passes->nodes.begin() ; nIter != passes->nodes.end() ; nIter++) {
				XText *txt = dynamic_cast<XText *>((*nIter));
				if(txt == 0) {
					//not a textnode, skip it
					continue;
				}
				try {
					AudioPass &p = getAudioPass(txt->text);
					p.setAudioListener(*listener);
				} catch(Exception &) {
					//error, another resource has the same name
					delete data;
					stringstream errmsg;
					errmsg << "Error: AudioPass " << txt->text << " in AudioListener " << id << " has an id used by another resource";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}

		//check if tag and attribute names were correct
		if(sAttrCount != rNode->stringAttribs.size() || rAttrCount != rNode->realAttribs.size() || tagCount != rNode->nodes.size()) {
			//error, another resource has the same name
			delete data;
			stringstream errmsg;
			errmsg << "Error: AudioListener " << id << " must only contain zero or one tag called \"passes\", and attributes \"id\", \"position\", \"view\", \"up\", \"velocity\" or \"volume\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::INTERNAL_LOAD_AUDIOPASS(XTag *rNode, XTag *data) {
		//... getStrAttribute, getRealAttribute
		string id;
		try {
			id = rNode->getStrAttribute("id");
		} catch(Exception &) {
			//every AudioPass node loaded by this method
			//must have an id, hence the loading procedure
			//can't be continued
			delete data;
			throw Exception("Error: an AudioPass without an id",EX_COMPILE);
		}
		AudioPass *pass = 0;
		unordered_map<string,AudioPass *>::iterator iter = audioPasses.find(id);
		if(iter == audioPasses.end()) {
			//make sure no other resources have the same identifyer
			//then create a pass
			unordered_map<string,SXResource *>::iterator rIter = resources.find(id);
			if(rIter != resources.end()) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: another resource than an AudioPass has id " << id;
				throw Exception(errmsg.str(),EX_COMPILE);
			}
			pass = new AudioPass(id);
			audioPasses[id] = pass;
			resources[id] = pass;
		} else {
			pass = (*iter).second;
		}
		//counters for string attributes
		//for checking if wrong attribute names were used
		unsigned int rAttrCount = 0;
		unsigned int sAttrCount = 1;
		unsigned int tagCount = 0;

		//load AudioPass parts
		bool useListener = false;
		string listener;
		try {
			listener = rNode->getStrAttribute("listener");
			useListener = true;
			sAttrCount++;
		} catch(Exception &) {
		}
		if(useListener) {
			try {
				AudioListener &alis = getAudioListener(listener);
				pass->setAudioListener(alis);
			} catch(Exception &) {
				//error, another resource has the same name
				delete data;
				stringstream errmsg;
				errmsg << "Error: AudioListener " << listener << " in AudioPass " << id << " has an id used by another resource";
				throw Exception(errmsg.str(),EX_COMPILE);
			}
		}

		XTag *objects = 0;
		try {
			objects = rNode->getFirst("objects");
			tagCount++;
		} catch(Exception &) {
		}
		if(objects != 0) {
			for(vector<XNode *>::iterator nIter = objects->nodes.begin() ; nIter != objects->nodes.end() ; nIter++) {
				XText *txt = dynamic_cast<XText *>((*nIter));
				if(txt == 0) {
					//not a text node, skip it
					continue;
				}
				try {
					AudioObject &obj = getAudioObject(txt->text);
					pass->addAudioObject(obj);
				} catch(Exception &) {
					//error, another resource has the same name
					delete data;
					stringstream errmsg;
					errmsg << "Error: AudioObject " << txt->text << " in AudioPass " << id << " has an id used by another resource";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}

		//check if tag and attribute names were correct
		if(sAttrCount != rNode->stringAttribs.size() || rAttrCount != rNode->realAttribs.size() || tagCount != rNode->nodes.size()) {
			//error, another resource has the same name
			delete data;
			stringstream errmsg;
			errmsg << "Error: AudioPass " << id << " must only contain zero or one tag called \"objects\", and attributes \"id\" and \"listener\"";
			throw Exception(errmsg.str(),EX_COMPILE);
		}
	}

	void ShadeX::addResourcesFromString(const std::string &r) {
		XTag *data = parseSXdata(r);

		//collect shaders
		BOOST_FOREACH(XNode *node , data->nodes) {
			XTag *rNode = dynamic_cast<XTag *>(node);
			if(rNode != 0) {
				if(rNode->name.compare("shader") == 0) {
					INTERNAL_LOAD_SHADER(rNode,data);
				} else if(rNode->name.compare("mesh") == 0) {
					INTERNAL_LOAD_BUFFEREDMESH(rNode,data);
				} else if(rNode->name.compare("skeleton") == 0) {
					INTERNAL_LOAD_SKELETON(rNode,data);
				} else if(rNode->name.compare("float") == 0) {
					INTERNAL_LOAD_UNIFORMFLOAT(rNode,data);
				} else if(rNode->name.compare("double") == 0) {
					INTERNAL_LOAD_UNIFORMDOUBLE(rNode,data);
				} else if(rNode->name.compare("vector") == 0) {
					INTERNAL_LOAD_UNIFORMVECTOR(rNode,data);
				} else if(rNode->name.compare("dvector") == 0) {
					INTERNAL_LOAD_UNIFORMDVECTOR(rNode,data);
				} else if(rNode->name.compare("matrix") == 0) {
					INTERNAL_LOAD_UNIFORMMATRIX(rNode,data);
				} else if(rNode->name.compare("dmatrix") == 0) {
					INTERNAL_LOAD_UNIFORMDMATRIX(rNode,data);
				} else if(rNode->name.compare("texture") == 0) {
					INTERNAL_LOAD_TEXTURE(rNode,data);
				} else if(rNode->name.compare("volume") == 0) {
					INTERNAL_LOAD_VOLUME(rNode,data);
				} else if(rNode->name.compare("rendertarget") == 0) {
					INTERNAL_LOAD_RENDERTARGET(rNode,data);
				} else if(rNode->name.compare("object") == 0) {
					INTERNAL_LOAD_RENDEROBJECT(rNode,data);
				} else if(rNode->name.compare("pass") == 0) {
					INTERNAL_LOAD_PASS(rNode,data);
				} else if(rNode->name.compare("effect") == 0) {
					INTERNAL_LOAD_EFFECT(rNode,data);
				} else if(rNode->name.compare("audiobuffer") == 0) {
					INTERNAL_LOAD_AUDIOBUFFER(rNode,data);
				} else if(rNode->name.compare("audioobject") == 0) {
					INTERNAL_LOAD_AUDIOOBJECT(rNode,data);
				} else if(rNode->name.compare("audiolistener") == 0) {
					INTERNAL_LOAD_AUDIOLISTENER(rNode,data);
				} else if(rNode->name.compare("audiopass") == 0) {
					INTERNAL_LOAD_AUDIOPASS(rNode,data);
				} else {
					//incorrect tag
					delete data;
					stringstream errmsg;
					errmsg << "Error: wrong tag " << rNode->name << " detected";
					throw Exception(errmsg.str(),EX_COMPILE);
				}
			}
		}

		delete data;
	}

	void ShadeX::addResources(const std::string &path) {
		string data = readFile(path);
		addResourcesFromString(data);
	}

	void ShadeX::load() {
		bool loaded = true;
		stringstream res;
		for(unordered_map<string,SXResource *>::iterator iter = resources.begin() ; iter != resources.end() ; iter++) {
			(*iter).second->load();
			if(!(*iter).second->isLoaded()) {
				loaded = false;
				res << (*iter).first << " ";
			}
		}

		//adding uniforms to BufferedMeshes
		//precondition: to every string in bufferUniforms
		//a BufferedMesh with an id equal to the string exists
		stringstream failedBufferUniforms;
		stringstream uninitializedBuffers;
		for(unordered_map<string,BufferUniforms>::iterator iter = bufferUniforms.begin() ; iter != bufferUniforms.end() ; iter++) {
			const string &meshID = (*iter).first;
			BufferUniforms &bUniforms = (*iter).second;
			BufferedMesh *mesh = bufferedMeshes[meshID];
			if(!mesh->isLoaded()) {
				//each mesh must be loaded
				uninitializedBuffers << meshID << " ";
				loaded = false;
				continue;
			}
			for(map<unsigned int,vector<string> >::iterator attrNameIter = bUniforms.attributeNames.begin() ; attrNameIter != bUniforms.attributeNames.end() ; attrNameIter++) {
				unsigned int location = (*attrNameIter).first;
				vector<string> &attrNames = (*attrNameIter).second;
				vector<Uniform *> &unis = bUniforms.uniforms[location];

				//add uniforms to mesh
				//evaluate outcome
				bool added = mesh->setUniforms(unis, attrNames, location);
				if(!added) {
					//something went wrong
					failedBufferUniforms << meshID << " ";
					loaded = false;
				}
			}
		}
		bufferUniforms.clear();

		SXout(LogMarkup("ShadeX::load()/resources"));
		SXout(resources.size());
		SXout(LogMarkup("ShadeX::load()/shaders"));
		SXout(shaders.size());
		SXout(LogMarkup("ShadeX::load()/bufferedMeshes"));
		SXout(bufferedMeshes.size());
		SXout(LogMarkup("ShadeX::load()/skeletons"));
		SXout(skeletons.size());
		SXout(LogMarkup("ShadeX::load()/uniformFloats"));
		SXout(uniformFloats.size());
		SXout(LogMarkup("ShadeX::load()/uniformDoubles"));
		SXout(uniformDoubles.size());
		SXout(LogMarkup("ShadeX::load()/uniformVectors"));
		SXout(uniformVectors.size());
		SXout(LogMarkup("ShadeX::load()/uniformDVectors"));
		SXout(uniformDVectors.size());
		SXout(LogMarkup("ShadeX::load()/uniformMatrices"));
		SXout(uniformMatrices.size());
		SXout(LogMarkup("ShadeX::load()/uniformDMatrices"));
		SXout(uniformDMatrices.size());
		SXout(LogMarkup("ShadeX::load()/textures"));
		SXout(textures.size());
		SXout(LogMarkup("ShadeX::load()/volumes"));
		SXout(volumes.size());
		SXout(LogMarkup("ShadeX::load()/renderTargets"));
		SXout(renderTargets.size());
		SXout(LogMarkup("ShadeX::load()/renderObjects"));
		SXout(renderObjects.size());
		SXout(LogMarkup("ShadeX::load()/passes"));
		SXout(passes.size());
		SXout(LogMarkup("ShadeX::load()/effects"));
		SXout(effects.size());
		SXout(LogMarkup("ShadeX::load()/audioBuffers"));
		SXout(audioBuffers.size());
		SXout(LogMarkup("ShadeX::load()/audioObjects"));
		SXout(audioObjects.size());
		SXout(LogMarkup("ShadeX::load()/audioListeners"));
		SXout(audioListeners.size());
		SXout(LogMarkup("ShadeX::load()/audioPasses"));
		SXout(audioPasses.size());
		SXout(LogMarkup("ShadeX::load()/bufferUniforms"));
		SXout(bufferUniforms.size());

		if(!loaded) {
			string resStr = res.str();
			string ubStr = uninitializedBuffers.str();
			string fbStr = failedBufferUniforms.str();
			stringstream errmsg;
			if(resStr.size() > 0) {
				errmsg << "resources " << resStr << " could not be loaded";
			}
			if(ubStr.size() > 0) {
				errmsg << " - BufferedMeshes " << ubStr << " were not loaded, although needed for adding uniforms";
			}
			if(fbStr.size() > 0) {
				errmsg << " - not all uniforms could be added to BufferedMeshes " << fbStr;
			}
			throw Exception(errmsg.str(),EX_INIT);
		}
	}

	Shader &ShadeX::getShader(const std::string &id) {
		unordered_map<string, Shader *>::iterator iter = shaders.find(id);
		Shader *shader = 0;
		if(iter == shaders.end()) {
			//shader object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non shader object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create shader object, and insert
			shader = new Shader(id);
			shaders[id] = shader;
			resources[id] = shader;
		} else {
			//shader found
			shader = (*iter).second;
		}
		return shader [0];
	}

	void ShadeX::addResource(const std::string &id, sx::Shader &shader) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		shaders[id] = &shader;
		resources[id] = &shader;
	}

	BufferedMesh &ShadeX::getBufferedMesh(const std::string &id) {
		unordered_map<string, BufferedMesh *>::iterator iter = bufferedMeshes.find(id);
		BufferedMesh *mesh = 0;
		if(iter == bufferedMeshes.end()) {
			//BufferedMesh object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non BufferedMesh object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create shader object, and insert
			mesh = new BufferedMesh(id);
			bufferedMeshes[id] = mesh;
			resources[id] = mesh;
		} else {
			//mesh found
			mesh = (*iter).second;
		}
		return mesh [0];
	}

	void ShadeX::addResource(const std::string &id, sx::BufferedMesh &mesh) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		bufferedMeshes[id] = &mesh;
		resources[id] = &mesh;
	}

	Skeleton &ShadeX::getSkeleton(const string &id) {
		unordered_map<string, Skeleton *>::iterator iter = skeletons.find(id);
		Skeleton *skeleton = 0;
		if(iter == skeletons.end()) {
			//Skeleton object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string,SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non Skeleton object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create skeleton object, and insert
			skeleton = new Skeleton(id);
			skeletons[id] = skeleton;
			resources[id] = skeleton;
		} else {
			//skeleton found
			skeleton = (*iter).second;
		}
		return skeleton [0];
	}

	void ShadeX::addResource(const string &id, Skeleton &skeleton) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		skeletons[id] = &skeleton;
		resources[id] = &skeleton;
	}

	UniformFloat &ShadeX::getUniformFloat(const string &id) {
		unordered_map<string, UniformFloat *>::iterator iter = uniformFloats.find(id);
		UniformFloat *uFloat = 0;
		if(iter == uniformFloats.end()) {
			//UniformFloat object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non UniformFloat object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create uniform float object, and insert
			uFloat = new UniformFloat(id);
			uniformFloats[id] = uFloat;
			resources[id] = uFloat;
		} else {
			//mesh found
			uFloat = (*iter).second;
		}

		return uFloat [0];
	}

	void ShadeX::addResource(const string &id, UniformFloat &uFloat) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		uniformFloats[id] = &uFloat;
		resources[id] = &uFloat;
	}

	UniformDouble &ShadeX::getUniformDouble(const string &id) {
		unordered_map<string, UniformDouble *>::iterator iter = uniformDoubles.find(id);
		UniformDouble *uDouble = 0;
		if(iter == uniformDoubles.end()) {
			//UniformDouble object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non UniformDouble object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create uniform double object, and insert
			uDouble = new UniformDouble(id);
			uniformDoubles[id] = uDouble;
			resources[id] = uDouble;
		} else {
			//uniform double found
			uDouble = (*iter).second;
		}

		return uDouble [0];
	}

	void ShadeX::addResource(const string &id, UniformDouble &uDouble) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		uniformDoubles[id] = &uDouble;
		resources[id] = &uDouble;
	}

	UniformVector &ShadeX::getUniformVector(const string &id) {
		unordered_map<string, UniformVector *>::iterator iter = uniformVectors.find(id);
		UniformVector *uVector = 0;
		if(iter == uniformVectors.end()) {
			//UniformVector object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non UniformVector object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create shader object, and insert
			uVector = new UniformVector(id);
			uniformVectors[id] = uVector;
			resources[id] = uVector;
		} else {
			//mesh found
			uVector = (*iter).second;
		}

		return uVector [0];
	}

	void ShadeX::addResource(const string &id, UniformVector &uVector) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		uniformVectors[id] = &uVector;
		resources[id] = &uVector;
	}

	UniformDVector &ShadeX::getUniformDVector(const string &id) {
		unordered_map<string, UniformDVector *>::iterator iter = uniformDVectors.find(id);
		UniformDVector *uDVector = 0;
		if(iter == uniformDVectors.end()) {
			//UniformDVector object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non UniformDVector object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create uniform double vector object, and insert
			uDVector = new UniformDVector(id);
			uniformDVectors[id] = uDVector;
			resources[id] = uDVector;
		} else {
			//uniform double vector found
			uDVector = (*iter).second;
		}

		return uDVector [0];
	}

	void ShadeX::addResource(const string &id, UniformDVector &uDVector) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		uniformDVectors[id] = &uDVector;
		resources[id] = &uDVector;
	}

	UniformMatrix &ShadeX::getUniformMatrix(const string &id) {
		unordered_map<string, UniformMatrix *>::iterator iter = uniformMatrices.find(id);
		UniformMatrix *uMatrix = 0;
		if(iter == uniformMatrices.end()) {
			//UniformMatrix object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non UniformMatrix object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create shader object, and insert
			uMatrix = new UniformMatrix(id);
			uniformMatrices[id] = uMatrix;
			resources[id] = uMatrix;
		} else {
			//mesh found
			uMatrix = (*iter).second;
		}

		return uMatrix [0];
	}

	void ShadeX::addResource(const string &id, UniformMatrix &uMatrix) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		uniformMatrices[id] = &uMatrix;
		resources[id] = &uMatrix;
	}

	UniformDMatrix &ShadeX::getUniformDMatrix(const string &id) {
		unordered_map<string, UniformDMatrix *>::iterator iter = uniformDMatrices.find(id);
		UniformDMatrix *uDMatrix = 0;
		if(iter == uniformDMatrices.end()) {
			//UniformDMatrix object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non UniformDMatrix object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create uniform double matrix object, and insert
			uDMatrix = new UniformDMatrix(id);
			uniformDMatrices[id] = uDMatrix;
			resources[id] = uDMatrix;
		} else {
			//mesh found
			uDMatrix = (*iter).second;
		}

		return uDMatrix [0];
	}

	void ShadeX::addResource(const string &id, UniformDMatrix &uDMatrix) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		uniformDMatrices[id] = &uDMatrix;
		resources[id] = &uDMatrix;
	}

	Texture &ShadeX::getTexture(const string &id) {
		unordered_map<string, Texture *>::iterator iter = textures.find(id);
		Texture *texture = 0;
		if(iter == textures.end()) {
			//Texture object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-texture object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create texture object, and insert
			texture = new Texture(id);
			textures[id] = texture;
			resources[id] = texture;
		} else {
			//texture found
			texture = (*iter).second;
		}
		return texture [0];
	}

	void ShadeX::addResource(const string &id, Texture &texture) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		textures[id] = &texture;
		resources[id] = &texture;
	}

	Volume &ShadeX::getVolume(const string &id) {
		unordered_map<string, Volume *>::iterator iter = volumes.find(id);
		Volume *volume = 0;
		if(iter == volumes.end()) {
			//Volume object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-volume object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create volume object, and insert
			volume = new Volume(id);
			volumes[id] = volume;
			resources[id] = volume;
		} else {
			//volume found
			volume = (*iter).second;
		}
		return volume [0];
	}

	void ShadeX::addResource(const string &id, Volume &volume) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		volumes[id] = &volume;
		resources[id] = &volume;
	}

	RenderTarget &ShadeX::getRenderTarget(const string &id) {
		unordered_map<string, RenderTarget *>::iterator iter = renderTargets.find(id);
		RenderTarget *target = 0;
		if(iter == renderTargets.end()) {
			//target object with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-rendertarget object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create target object, and insert
			target = new RenderTarget(id);
			renderTargets[id] = target;
			resources[id] = target;
		} else {
			//target found
			target = (*iter).second;
		}

		return target [0];
	}

	void ShadeX::addResource(const string &id, RenderTarget &target) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		renderTargets[id] = &target;
		resources[id] = &target;
	}

	RenderObject &ShadeX::getRenderObject(const string &id) {
		unordered_map<string, RenderObject *>::iterator iter = renderObjects.find(id);
		RenderObject *object = 0;
		if(iter == renderObjects.end()) {
			//renderobject with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-renderobject object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create renderobject, and insert
			object = new RenderObject(id);
			renderObjects[id] = object;
			resources[id] = object;
		} else {
			//target found
			object = (*iter).second;
		}

		return object [0];
	}

	void ShadeX::addResource(const string &id, RenderObject &object) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		renderObjects[id] = &object;
		resources[id] = &object;
	}

	Pass &ShadeX::getPass(const string &id) {
		unordered_map<string, Pass *>::iterator iter = passes.find(id);
		Pass *pass = 0;
		if(iter == passes.end()) {
			//pass with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-pass object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create pass, and insert
			pass = new Pass(id);
			passes[id] = pass;
			resources[id] = pass;
		} else {
			//target found
			pass = (*iter).second;
		}

		return pass [0];
	}

	void ShadeX::addResource(const string &id, Pass &pass) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		passes[id] = &pass;
		resources[id] = &pass;
	}

	Effect &ShadeX::getEffect(const string &id) {
		unordered_map<string, Effect *>::iterator iter = effects.find(id);
		Effect *effect = 0;
		if(iter == effects.end()) {
			//effect with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-effect object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create pass, and insert
			effect = new Effect(id);
			effects[id] = effect;
			resources[id] = effect;
		} else {
			//target found
			effect = (*iter).second;
		}

		return effect [0];
	}

	void ShadeX::addResource(const string &id, Effect &effect) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		effects[id] = &effect;
		resources[id] = &effect;
	}

	AudioBuffer &ShadeX::getAudioBuffer(const string &id) {
		unordered_map<string, AudioBuffer *>::iterator iter = audioBuffers.find(id);
		AudioBuffer *buffer = 0;
		if(iter == audioBuffers.end()) {
			//AudioBuffer with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-AudioBuffer object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create AudioBuffer, and insert
			buffer = new AudioBuffer(id);
			audioBuffers[id] = buffer;
			resources[id] = buffer;
		} else {
			//target found
			buffer = (*iter).second;
		}

		return buffer [0];
	}

	void ShadeX::addResource(const string &id, AudioBuffer &buffer) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		audioBuffers[id] = &buffer;
		resources[id] = &buffer;
	}

	AudioObject &ShadeX::getAudioObject(const string &id) {
		unordered_map<string, AudioObject *>::iterator iter = audioObjects.find(id);
		AudioObject *object = 0;
		if(iter == audioObjects.end()) {
			//AudioObject with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-AudioObject object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create AudioObject, and insert
			object = new AudioObject(id);
			audioObjects[id] = object;
			resources[id] = object;
		} else {
			//target found
			object = (*iter).second;
		}

		return object [0];
	}

	void ShadeX::addResource(const string &id, AudioObject &object) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		audioObjects[id] = &object;
		resources[id] = &object;
	}

	AudioListener &ShadeX::getAudioListener(const string &id) {
		unordered_map<string, AudioListener *>::iterator iter = audioListeners.find(id);
		AudioListener *listener = 0;
		if(iter == audioListeners.end()) {
			//AudioListener with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-AudioListener object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create AudioListener, and insert
			listener = new AudioListener(id);
			audioListeners[id] = listener;
			resources[id] = listener;
		} else {
			//target found
			listener = (*iter).second;
		}

		return listener [0];
	}

	void ShadeX::addResource(const string &id, AudioListener &listener) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		audioListeners[id] = &listener;
		resources[id] = &listener;
	}

	AudioPass &ShadeX::getAudioPass(const string &id) {
		unordered_map<string, AudioPass *>::iterator iter = audioPasses.find(id);
		AudioPass *pass = 0;
		if(iter == audioPasses.end()) {
			//AudioPass with id does not exist yet
			//test if the id is not used yet
			unordered_map<string, SXResource *>::iterator iter2 = resources.find(id);
			if(iter2 != resources.end()) {
				//id already used for a non-AudioPass object
				//throw exception
				stringstream errmsg;
				errmsg << "Error: another resource already has identifyer " << id;
				throw Exception(errmsg.str(),EX_AMBIGUOUS);
			}
			//id still unused, create AudioPass, and insert
			pass = new AudioPass(id);
			audioPasses[id] = pass;
			resources[id] = pass;
		} else {
			//target found
			pass = (*iter).second;
		}

		return pass [0];
	}

	void ShadeX::addResource(const string &id, AudioPass &pass) {
		unordered_map<string, SXResource *>::iterator iter = resources.find(id);
		if(iter != resources.end()) {
			//another resource already uses the same identifyer
			//throw an exception
			stringstream errmsg;
			errmsg << "Error: another resource already has identifyer " << id;
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		audioPasses[id] = &pass;
		resources[id] = &pass;
	}

}

#endif