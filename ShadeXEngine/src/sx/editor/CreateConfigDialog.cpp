#ifndef _SX_EDITOR_CREATECONFIGDIALOG_CPP_
#define _SX_EDITOR_CREATECONFIGDIALOG_CPP_

/**
 * Create config dialog widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <QGridLayout>
#include <QPushButton>

namespace sx {

	void CreateConfigDialog::create() {
		const QString &t = input->text();
		if(t.size() == 0) {
			Warning *warning = new Warning("no input","You must input a configuration name.",this);
			warning->setModal(true);
			warning->show();
			return;
		}
		close();
	}

	void CreateConfigDialog::initGui() {
		input = new QLineEdit();
		QPushButton *createButton = new QPushButton("create configuration");
		connect(createButton,SIGNAL(clicked()),this,SLOT(create()));
		QGridLayout *layout = new QGridLayout();
		layout->addWidget(input,0,0);
		layout->addWidget(createButton,1,0);
		setLayout(layout);
	}

	CreateConfigDialog::CreateConfigDialog(QWidget *w): QDialog(w) {
		setWindowIcon(QIcon("../data/SX.png"));
		setWindowTitle("create configuration");
		initGui();
	}

	const QString CreateConfigDialog::getText() const {
		return input->text();
	}

}

#endif