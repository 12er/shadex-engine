#ifndef _SX_EDITOR_PASSWIDGET_CPP_
#define _SX_EDITOR_PASSWIDGET_CPP_

/**
 * Pass widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <QToolButton>

namespace sx {

	void PassWidget::initGui() {
		QGridLayout *layout = new QGridLayout();
		QToolButton *closeButton = new QToolButton();
		closeButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		closeButton->setStyleSheet(
			"QToolButton {"
			"   background-image: url(../data/editor/icons/close_unfocused.png);"
			"   border: 1px solid transparent;"
			"}"

			"QToolButton::hover {"
			"   background-image: url(../data/editor/icons/close_focused.png);"
			"   border: 1px solid transparent;"
			"}"

			"QToolButton::pressed {"
			"   background-image: url(../data/editor/icons/close_pressed.png);"
			"   border: 1px solid transparent;"
			"}"
			);
		connect(closeButton,SIGNAL(clicked()),this,SLOT(removeDiagramItem()));
		titleLabel = new ClickableLabel("<<Pass>>");
		connect(titleLabel,SIGNAL(clicked()),this,SLOT(startMove()));
		layout->addWidget(titleLabel,0,0,1,1);
		layout->addWidget(closeButton,0,1,1,1);
		layout->addWidget(IDgui,1,0,1,2);
		setLayout(layout);
	}

	PassWidget::PassWidget(const QString &ID, int guiID, GraphEditPane *scene, QWidget *parent): GraphicWidget(guiID, scene, parent) {
		IDgui = new ChangeIDInput(MODE_EDITPASS, ID, scene->getDiagramID(), guiID, scene);
		connect(IDgui,SIGNAL(changedGui()),this,SLOT(pack()));
		initGui();
	}

	void PassWidget::setID(const QString &ID) {
		IDgui->setID(ID);
	}

	const QString PassWidget::getID() const {
		return IDgui->getID();
	}

	void PassWidget::paintEvent(QPaintEvent *) {
		QPainter painter(this);
        painter.save();

        painter.setRenderHint(QPainter::Antialiasing);
        painter.setPen(Qt::gray);

        QRect widgetRect(QPoint(0,0),this->size());
        QPainterPath roundedRect;
        roundedRect.addRoundedRect(widgetRect,5,5);
        painter.setClipPath(roundedRect);
        QRegion maskRegion = painter.clipRegion();
        this->setMask(maskRegion);
        painter.drawRoundedRect(widgetRect,5,5);

        painter.restore();
	}

	void PassWidget::removeDiagramItem() {
		if(scene != 0) {
			string strID = IDgui->getID().toUtf8().constData();
			SXLogicObjects::removePass(strID,scene->getDiagramID(),guiID);
			scene->removeNode(guiID);
			scene = 0;
		}
	}

}

#endif