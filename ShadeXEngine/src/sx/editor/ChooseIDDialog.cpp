#ifndef _SX_EDITOR_CHOOSEIDDIALOG_CPP_
#define _SX_EDITOR_CHOOSEIDDIALOG_CPP_

/**
 * Choose ID dialog widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <QPushButton>
#include <QStringList>

namespace sx {

	void ChooseIDDialog::initGui() {
		QGridLayout *layout = new QGridLayout();
		input = new QLineEdit();
		QStringList itemList;
		if(editMode == MODE_ADDPASS) {
			for(unordered_map<string,SXLogicPass *>::iterator iter = SXLogicObjects::passes.begin() ; iter != SXLogicObjects::passes.end() ; iter++) {
				string strID = (*iter).first;
				QString qID = QString::fromUtf8(strID.c_str());
				itemList << qID;
			}
		}
		completer = new QCompleter(itemList);
		input->setCompleter(completer);
		QPushButton *button = new QPushButton("OK");
		button->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		connect(button,SIGNAL(clicked()),this,SLOT(choose()));
		layout->addWidget(input,0,0);
		layout->addWidget(button,1,0);
		setLayout(layout);
	}

	void ChooseIDDialog::choose() {
		if(editMode == MODE_ADDPASS) {
			const QString &t = input->text();
			string strID = t.toUtf8().constData();
			if(!SXLogicObjects::checkAddPass(strID)) {
				Warning *warning = new Warning("invalid input","The ID must be nonempty, and can't be used by another resource.",this);
				warning->setModal(true);
				warning->show();
				return;
			}
			ID = input->text();
			close();
		}
	}
	
	ChooseIDDialog::ChooseIDDialog(EditMode mode, QWidget *w): QDialog(w) {
		this->editMode = mode;
		setWindowIcon(QIcon("../data/SX.png"));
		setWindowTitle("ID");
		initGui();
	}
	
	const QString ChooseIDDialog::getID() const {
		return ID;
	}

}

#endif