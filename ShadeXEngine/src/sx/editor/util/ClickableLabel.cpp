#ifndef _SX_EDITOR_UTIL_CLICKABLELABEL_CPP_
#define _SX_EDITOR_UTIL_CLICKABLELABEL_CPP_

/**
 * Clickable label widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>

namespace sx {

	void ClickableLabel::mousePressEvent(QMouseEvent *ev) {
		QLabel::mousePressEvent(ev);
		clicked();
	}

	ClickableLabel::ClickableLabel(QWidget * parent, Qt::WindowFlags f): QLabel(parent,f) {
	}

	ClickableLabel::ClickableLabel(const QString & text, QWidget * parent, Qt::WindowFlags f): QLabel(text,parent,f) {
	}

}

#endif