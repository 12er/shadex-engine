#ifndef _SX_EDITOR_UTIL_SEQUENCELAYOUT_CPP_
#define _SX_EDITOR_UTIL_SEQUENCELAYOUT_CPP_

/**
 * Sequence layout class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>

namespace sx {

	SequenceLayout::SequenceLayout(LayoutExpansion expansion): QLayout() {
		this->expansion = expansion;
		this->margin_bottom = this->margin_left = this->margin_right = this->margin_top = 0;
	}

	void SequenceLayout::setMargin(int margin_bottom, int margin_top, int margin_left, int margin_right) {
		this->margin_bottom = margin_bottom;
		this->margin_top = margin_top;
		this->margin_left = margin_left;
		this->margin_right = margin_right;
	}
	
	void SequenceLayout::setMargin(int margin) {
		setMargin(margin,margin,margin,margin);
	}

	void SequenceLayout::addItem(QLayoutItem *item) {
		this->items.append(item);
	}

	int SequenceLayout::count() const {
		return this->items.size();
	}

	QLayoutItem *SequenceLayout::itemAt(int index) const {
		if(index < 0 || index >= this->items.size()) {
			return 0;
		}
		return this->items[index];
	}

	QLayoutItem *SequenceLayout::takeAt(int index) {
		if(index < 0 || index >= this->items.size()) {
			return 0;
		}
		QLayoutItem *item = this->items[index];
		this->items.removeAt(index);
		return item;
	}

	void SequenceLayout::setGeometry(const QRect &rect) {
		QLayout::setGeometry(rect);

		QListIterator<QLayoutItem *> iter = QListIterator<QLayoutItem *>(this->items);
		int xLocation = rect.x();
		int yLocation = rect.y();
		if(expansion == EXPAND_HORIZONTALLY) {
			int rowMaxHeight = 0;
			bool leftFirst = true;
			while(iter.hasNext()) {
				QLayoutItem *item = iter.next();

				if(leftFirst) {
					leftFirst = !leftFirst;
				} else {
					QSize itemSize = item->sizeHint();
					if(itemSize.width() + margin_left + margin_right + xLocation > rect.width() + rect.x()) {
						xLocation = rect.x();
						yLocation += rowMaxHeight;
						rowMaxHeight = 0;
					}
				}
				QSize itemSize = item->sizeHint();
				QPoint itemLocation(margin_left + xLocation,margin_top + yLocation);
				QRect itemRect(itemLocation,itemSize);
				item->setGeometry(itemRect);

				xLocation += itemSize.width() + margin_left + margin_right;
				rowMaxHeight = max(rowMaxHeight,itemSize.height() + margin_bottom + margin_top);
			}
		} else {
			int columnMaxWidth = 0;
			bool topFirst = true;
			while(iter.hasNext()) {
				QLayoutItem *item = iter.next();

				if(topFirst) {
					topFirst = !topFirst;
				} else {
					QSize itemSize = item->sizeHint();
					if(itemSize.height() + margin_bottom + margin_top + yLocation > rect.height() + rect.y()) {
						yLocation = rect.y();
						xLocation += columnMaxWidth;
						columnMaxWidth = 0;
					}
				}
				QSize itemSize = item->sizeHint();
				QPoint itemLocation(margin_left + xLocation,margin_top + yLocation);
				QRect itemRect(itemLocation,itemSize);
				item->setGeometry(itemRect);

				yLocation += itemSize.height() + margin_bottom + margin_top;
				columnMaxWidth = max(columnMaxWidth,itemSize.width() + margin_left + margin_right);
			}
		}
	}

	QSize SequenceLayout::minimumSize() const {
		QSize minSize(0,0);
		QListIterator<QLayoutItem *> iter(this->items);
		while(iter.hasNext()) {
			QSize iterSize = iter.next()->minimumSize();
			iterSize.setWidth(iterSize.width() + margin_left + margin_right);
			iterSize.setHeight(iterSize.height() + margin_bottom + margin_top);
			minSize = minSize.expandedTo(iterSize);
		}
		return minSize;
	}

	QSize SequenceLayout::sizeHint() const {
		QSize minSize(0,0);
		QListIterator<QLayoutItem *> iter(this->items);
		while(iter.hasNext()) {
			QSize iterSize = iter.next()->sizeHint();
			iterSize.setWidth(iterSize.width() + margin_left + margin_right);
			iterSize.setHeight(iterSize.height() + margin_bottom + margin_top);
			minSize = minSize.expandedTo(iterSize);
		}
		return minSize;
	}

	bool SequenceLayout::hasHeightForWidth() const {
		return true;
	}

	int SequenceLayout::heightForWidth(int wWidth) const {
		if(EXPAND_HORIZONTALLY) {
			QListIterator<QLayoutItem *> iter = QListIterator<QLayoutItem *>(this->items);
			int xLocation = 0;
			int yLocation = 0;
			int rowMaxHeight = 0;
			bool leftFirst = true;
			while(iter.hasNext()) {
				QLayoutItem *item = iter.next();

				if(leftFirst) {
					leftFirst = !leftFirst;
				} else {
					QSize itemSize = item->sizeHint();
					if(itemSize.width() + margin_left + margin_right + xLocation > wWidth) {
						xLocation = 0;
						yLocation += rowMaxHeight;
						rowMaxHeight = 0;
					}
				}
				QSize itemSize = item->sizeHint();

				xLocation += itemSize.width() + margin_left + margin_right;
				rowMaxHeight = max(rowMaxHeight,itemSize.height() + margin_bottom + margin_top);
			}

			return rowMaxHeight + yLocation;
		} else {
			int rHeight = 0;
			int rWidth = 0;
			do {
				rHeight += 20;
				QListIterator<QLayoutItem *> iter = QListIterator<QLayoutItem *>(this->items);
				int xLocation = 0;
				int yLocation = 0;
				int columnMaxWidth = 0;
				bool topFirst = true;
				while(iter.hasNext()) {
					QLayoutItem *item = iter.next();

					if(topFirst) {
						topFirst = !topFirst;
					} else {
						QSize itemSize = item->sizeHint();
						if(itemSize.height() + margin_bottom + margin_top + yLocation > rHeight) {
							yLocation = 0;
							xLocation += columnMaxWidth;
							columnMaxWidth = 0;
						}
					}
					QSize itemSize = item->sizeHint();

					yLocation += itemSize.height() + margin_bottom + margin_top;
					columnMaxWidth = max(columnMaxWidth,itemSize.width() + margin_left + margin_right);
				}
				rWidth = columnMaxWidth + xLocation;
			} while(rWidth > wWidth && rHeight < 2000);
			return rHeight;
		}
	}

}

#endif