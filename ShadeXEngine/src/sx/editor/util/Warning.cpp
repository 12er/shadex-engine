#ifndef _SX_EDITOR_UTIL_WARNING_CPP_
#define _SX_EDITOR_UTIL_WARNING_CPP_

/**
 * warning widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <QLabel>
#include <QPushButton>

namespace sx {

	void Warning::initGui(const QString &msg) {
		QLabel *warningIcon = new QLabel();
		warningIcon->setPixmap(QPixmap("../data/editor/icons/warning.png"));
		QLabel *messageLabel = new QLabel(msg);
		QPushButton *okButton = new QPushButton("OK");
		okButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		connect(okButton,SIGNAL(clicked()),this,SLOT(close()));
		QGridLayout *layout = new QGridLayout();
		layout->addWidget(warningIcon,0,0);
		layout->addWidget(messageLabel,0,1);
		layout->addWidget(okButton,1,0,1,2);
		setLayout(layout);
	}
	
	Warning::Warning(const QString &title, const QString &text, QWidget *w) {
		setWindowTitle(title);
		setWindowIcon(QIcon("../data/SX.png"));
		initGui(text);
		layout()->setSizeConstraint(QLayout::SetFixedSize);
	}

}

#endif