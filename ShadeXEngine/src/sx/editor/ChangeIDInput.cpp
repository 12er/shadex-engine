#ifndef _SX_EDITOR_CHANGEIDINPUT_CPP_
#define _SX_EDITOR_CHANGEIDINPUT_CPP_

/**
 * Change ID input widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <sx/Exception.h>
#include <QPushButton>
#include <QGraphicsView>
#include <QTransform>

namespace sx {

	void ChangeIDInput::initGui() {
		label = new ClickableLabel(ID);
		label->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		QGridLayout *labelLayout = new QGridLayout();
		labelLayout->addWidget(label);
		setLayout(labelLayout);
		inputWidget = new QDialog(0,Qt::FramelessWindowHint);
		input = new QLineEdit();
		QPushButton *inputButton = new QPushButton("OK");
		QPushButton *cancelButton = new QPushButton("Cancel");
		QGridLayout *layout = new QGridLayout();
		layout->addWidget(input,0,0);
		layout->addWidget(inputButton,0,1);
		layout->addWidget(cancelButton,0,2);
		inputWidget->setLayout(layout);
		showLabel();
		connect(label,SIGNAL(clicked()),this,SLOT(awaitInput()));
		connect(inputButton,SIGNAL(clicked()),this,SLOT(takeInput()));
		connect(cancelButton,SIGNAL(clicked()),this,SLOT(discardInput()));
	}
	
	void ChangeIDInput::awaitInput() {
		//show input area with current IDs
		QStringList itemList;
		if(editMode == MODE_EDITPASS) {
			for(unordered_map<string,SXLogicPass *>::iterator iter = SXLogicObjects::passes.begin() ; iter != SXLogicObjects::passes.end() ; iter++) {
				string strID = (*iter).first;
				QString qID = QString::fromUtf8(strID.c_str());
				itemList << qID;
			}
		}
		completer = new QCompleter(itemList);
		completer->popup();
		input->setText(ID);
		input->setCompleter(completer);
		showInput();
	}

	void ChangeIDInput::takeInput() {
		QString newID = input->text();
		try {
			string strID = ID.toUtf8().constData();
			string strNewID = newID.toUtf8().constData();
			if(editMode == MODE_EDITPASS) {
				SXLogicObjects::changePass(strID,strNewID,diagramID,guiID);
			}
			label->setText(newID);
			ID = newID;
		} catch(Exception &e) {
			QString msg = e.getMessage().c_str();
			Warning *w = new Warning("invalid input",msg,this);
			w->setModal(true);
			w->show();
		}
		showLabel();
	}
	
	void ChangeIDInput::discardInput() {
		showLabel();
	}

	ChangeIDInput::ChangeIDInput(EditMode mode, const QString &ID, int diagramID, int guiID, GraphEditPane *scene) : QWidget() {
		this->scene = scene;
		this->editMode = mode;
		this->ID = ID;
		this->diagramID = diagramID;
		this->guiID = guiID;
		setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		initGui();
	}

	void ChangeIDInput::setID(const QString &ID) {
		this->ID = ID;
		label->setText(ID);
	}

	const QString ChangeIDInput::getID() const {
		return ID;
	}

	void ChangeIDInput::showLabel() {
		inputWidget->hide();
		changedGui();
	}

	void ChangeIDInput::showInput() {
		if(scene != 0 && scene->getGraphicsView() != 0) {
			//QGraphicsScene has a bug
			//which causes mapToGlobal to work correctly
			//fix the glitch by deploying several transforms
			QPoint p = label->pos();
			p = p + QPoint(0,18);
			p = parentWidget()->mapToParent(p);
			QTransform t = scene->getGraphicsView()->viewportTransform();
			p = scene->getGraphicsView()->mapToGlobal(t.map(p));
			inputWidget->move(p);
		}
		input->setFocus();
		input->selectAll();
		inputWidget->setModal(true);
		inputWidget->show();
		changedGui();
	}

}

#endif