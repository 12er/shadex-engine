#ifndef _SX_EDITOR_GRAPHEDITPANE_CPP_
#define _SX_EDITOR_GRAPHEDITPANE_CPP_

/**
 * Graph edit pane
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneMouseEvent>

namespace sx {

	GraphEditPane::GraphEditPane(int diagramID, const QSize &size, QWidget *parentWidget): QGraphicsScene(0.0,0.0,(qreal)size.width(),(qreal)size.height(),parentWidget) {
		this->diagramID = diagramID;
		this->parentWidget = parentWidget;
		editMode = MODE_NONE;
	}

	int GraphEditPane::getDiagramID() const {
		return diagramID;
	}

	void GraphEditPane::setNextID(const QString &ID) {
		this->nextID = ID;
	}

	void GraphEditPane::setEditMode(EditMode mode) {
		this->editMode = mode;
	}

	void GraphEditPane::addNode(GraphicWidget &node) {
		nodes[node.getGuiID()] = &node;
	}

	void GraphEditPane::removeNode(int guiID) {
		//praecondition, not tested:
		//node with guiID has a widget associated with it
		//which is on the GraphEditPane
		unordered_map<int,GraphicWidget *>::iterator iter = nodes.find(guiID);
		if(iter != nodes.end()) {
			nodes.erase(iter);
			QGraphicsProxyWidget *proxy = (*iter).second->graphicsProxyWidget();
			if(proxy != 0) {
				removeItem(proxy);
			}
		}
	}

	void GraphEditPane::setGraphicsView(QGraphicsView *view) {
		this->view = view;
	}

	QGraphicsView *GraphEditPane::getGraphicsView() {
		return view;
	}

	void GraphEditPane::mousePressEvent(QGraphicsSceneMouseEvent *event) {
		QGraphicsScene::mousePressEvent(event);

		QPointF point = QPointF(event->scenePos());
		if(editMode == MODE_ADDPASS) {
			QString ID = nextID;
			if(ID.size() == 0) {
				//no ID given, hence do nothing
				editMode = MODE_NONE;
				return;
			}

			//get next guiID, and add pass
			int guiID = SXLogicObjects::getNextID(diagramID);
			addPass(point,ID,guiID);

			//add no further passes
			editMode = MODE_NONE;
		} else if(editMode == MODE_MOVE) {
			movePoint = point;
		}
	}

	void GraphEditPane::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
		QGraphicsScene::mouseMoveEvent(event);

		QPointF point = QPointF(event->scenePos());
		if(editMode == MODE_MOVE) {
			QGraphicsItem *moveItem = itemAt(movePoint);
			QPointF delta = point - movePoint;
			movePoint = point;
			moveItem->moveBy(delta.x(),delta.y());
		}
	}

	void GraphEditPane::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
		QGraphicsScene::mouseReleaseEvent(event);

		QPointF point = QPointF(event->scenePos());
		if(editMode == MODE_MOVE) {
			editMode = MODE_NONE;
		}
	}

	void GraphEditPane::keyPressEvent(QKeyEvent *keyEvent) {
		QGraphicsScene::keyPressEvent(keyEvent);
	}

	void GraphEditPane::addPass(const QPointF &point, const QString &ID, int guiID) {
		//add logic object
		string strID = ID.toUtf8().constData();
		SXLogicObjects::addPass(strID,diagramID,guiID);

		//create gui object
		PassWidget *pass = new PassWidget(ID,guiID,this);
		connect(this,SIGNAL(removingDiagramItems()),pass,SLOT(removeDiagramItem()));
		//add graphic item for easy access
		addNode(*pass);
		QGraphicsProxyWidget *passProxy = addWidget(pass);
		passProxy->setPos(point.x(),point.y());;
	}

	void GraphEditPane::removeDiagramItems() {
		removingDiagramItems();
	}

}

#endif