#ifndef _SX_EDITOR_SXLOGICOBJECTS_CPP_
#define _SX_EDITOR_SXLOGICOBJECTS_CPP_

/**
 * SX Logic objects
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <sx/Exception.h>
#include <sstream>
using namespace std;

namespace sx {

	unordered_map<int,int> SXLogicObjects::nextIDs;

	unordered_map<string,SXLogicObject *> SXLogicObjects::objects;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::effects;

	unordered_map<string,SXLogicPass *> SXLogicObjects::passes;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::renderobjects;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::meshes;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::shaders;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::rendertargets;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::textures;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::matrices;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::vectors;

	unordered_map<string,unordered_map<int,int> > SXLogicObjects::floats;

	bool SXLogicObjects::checkAddPass(const string &ID) {
		if(ID.size() == 0) {
			//empty IDs are not accepted
			return false;
		}
		unordered_map<string,SXLogicObject *>::iterator iter1 = SXLogicObjects::objects.find(ID);
		unordered_map<string,SXLogicPass *>::iterator iter2 = SXLogicObjects::passes.find(ID);
		//iff another object exists, which isn't a pass, ID can't be used for a pass
		return iter1 == SXLogicObjects::objects.end() || iter2 != SXLogicObjects::passes.end();
	}

	void SXLogicObjects::addPass(const string &ID, int diagramID, int guiID) {
		//precondition has to be checked outside method:
		//no object outside of the set of passes has the identifyer ID
		unordered_map<string,SXLogicObject *>::iterator iter1 = SXLogicObjects::objects.find(ID);
		if(iter1 == SXLogicObjects::objects.end()) {
			//pass does not exist yet, construct it
			SXLogicPass *pass = new SXLogicPass();
			pass->ID = ID;
			SXLogicObjects::objects[ID] = pass;
			SXLogicObjects::passes[ID] = pass;
		}
		//get pass
		unordered_map<string,SXLogicPass *>::iterator iter2 = SXLogicObjects::passes.find(ID);
		SXLogicPass *pass = (*iter2).second;
		//update passes' gui IDs
		unordered_map<int,int> &guiParts = pass->guiParts[diagramID];
		guiParts[guiID] = guiID;
	}

	void SXLogicObjects::changePass(const string &oldID, const string &newID, int diagramID, int guiID) {
		//precondition has to be checked outside method:
		//a gui element with oldID, diagramID and guiID exists
		if(newID.size() == 0) {
			//empty IDs are not accepted
			throw Exception("You must input an ID",EX_NODATA);
		}
		if(!checkAddPass(newID)) {
			//another resource already has identifyer newID
			stringstream errmsg;
			errmsg << "ID " << newID << " is used by another resource";
			throw Exception(errmsg.str(),EX_AMBIGUOUS);
		}
		//change oldID to newID
		SXLogicObjects::removePass(oldID,diagramID,guiID);
		SXLogicObjects::addPass(newID,diagramID,guiID);
	}

	void SXLogicObjects::removePass(const string &ID, int diagramID, int guiID) {
		//precondition has to be checked outside method:
		//a pass with the given ID, diagramID and guiID exists
		unordered_map<string,SXLogicObject *>::iterator objIter = SXLogicObjects::objects.find(ID);
		unordered_map<string,SXLogicPass *>::iterator passIter = SXLogicObjects::passes.find(ID);
		unordered_map<int,unordered_map<int,int> >::iterator guiPartsIter = (*passIter).second->guiParts.find(diagramID);
		unordered_map<int,int>::iterator guiPartIter = (*guiPartsIter).second.find(guiID);
		//erase gui part
		(*guiPartsIter).second.erase(guiPartIter);
		if((*guiPartsIter).second.size() == 0) {
			//this diagram has no gui parts anymore
			//remove diagram from ID
			(*passIter).second->guiParts.erase(guiPartsIter);
			if((*passIter).second->guiParts.size() == 0) {
				//no diagrams are associated with the ID
				//ID can be removed
				SXLogicObjects::objects.erase(objIter);
				SXLogicObjects::passes.erase(passIter);
			}
		}
	}

	void SXLogicObjects::updateNextID(int diagramID, int existingID) {
		nextIDs[diagramID] = max(nextIDs[diagramID], existingID + 1);
	}
	
	int SXLogicObjects::getNextID(int diagramID) {
		return nextIDs[diagramID]++;
	}

}

#endif