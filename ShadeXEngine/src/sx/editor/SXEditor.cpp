#ifndef _SX_EDITOR_SXEDITOR_CPP_
#define _SX_EDITOR_SXEDITOR_CPP_

/**
 * SX Editor widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>
#include <sx/Exception.h>
#include <QIcon>
#include <QRegExp>
#include <QStringList>
#include <QList>
#include <QSize>
#include <QGridLayout>
#include <QPixmap>
#include <QPalette>
#include <QColor>
#include <QBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QToolBar>
#include <QAction>
#include <QGraphicsView>

namespace sx {

	void SXEditor::createDialog() {
		CreateConfigDialog *dialog = new CreateConfigDialog(this);
		dialog->setModal(true);
		dialog->show();
		dialog->exec();
		QString name = dialog->getText();
		if(name.length() > 0) {
			addConfiguration(name);
			mainGuis->setCurrentIndex(1);
		}
	}

	void SXEditor::openDialog() {
		QString path = QFileDialog::getOpenFileName(this,"open configuration","","*.conf");
		if(!path.isNull()) {
			addConfiguration(path,true);
			mainGuis->setCurrentIndex(1);
		}
	}

	void SXEditor::setAddPass() {
		ChooseIDDialog *dialog = new ChooseIDDialog(MODE_ADDPASS, this);
		dialog->setModal(true);
		dialog->exec();
		QString ID = dialog->getID();
		if(ID.size() == 0) {
			return;
		}
		ConfigParts &part = currentConfig();
		part.graphEdit->setEditMode(MODE_ADDPASS);
		part.graphEdit->setNextID(ID);
	}

	void SXEditor::initGui() {
		//actions
		QAction *createAction = new QAction(QIcon("../data/editor/icons/new.png"),"create configuration",this);
		createAction->setShortcut(QKeySequence::New);
		createAction->setToolTip("create a new configuration");
		connect(createAction,SIGNAL(triggered()),this,SLOT(createDialog()));
		QAction *openAction = new QAction(QIcon("../data/editor/icons/load.png"),"open configuration",this);
		openAction->setShortcut(QKeySequence::Open);
		openAction->setToolTip("open an existing configuration");
		connect(openAction,SIGNAL(triggered()),this,SLOT(openDialog()));
		QAction *saveAction = new QAction(QIcon("../data/editor/icons/save.png"),"save configuration",this);
		saveAction->setShortcut(QKeySequence::Save);
		saveAction->setToolTip("save configuration of the currently active tab");
		QAction *compileAction = new QAction(QIcon("../data/editor/icons/run.png"),"compile configurations",this);
		compileAction->setShortcut(QKeySequence::Refresh);
		compileAction->setToolTip("compile opened configurations");
		
		addPassAction = new QAction(QIcon("../data/editor/icons/Pass.png"),"Pass",this);
		createAction->setToolTip("create a pass");
		connect(addPassAction,SIGNAL(triggered()),this,SLOT(setAddPass()));

		//changeable guis
		mainGuis = new QStackedWidget();

		//start screen
		start = new QWidget();
		start->setStyleSheet(
			".QWidget {"
			"	background-image: url(../data/editor/images/ShadeXEngine.png);"
			"	background-position: center center;"
			"	background-repeat: no-repeat;"
			"	background-color: #000000;"
			"}"
			);
		start->setAutoFillBackground(true);
		QPalette startBackgroundColoring;
		start->setPalette(startBackgroundColoring);

		QPushButton *createFile = new QPushButton("create configuration");
		createFile->setIcon(QIcon("../data/editor/icons/new.png"));
		connect(createFile,SIGNAL(clicked()),this,SLOT(createDialog()));
		QPushButton *openFile = new QPushButton("open configuration");
		openFile->setIcon(QIcon("../data/editor/icons/load.png"));
		connect(openFile,SIGNAL(clicked()),this,SLOT(openDialog()));
		SequenceLayout *startLayout = new SequenceLayout(EXPAND_VERTICALLY);
		startLayout->setMargin(5);
		startLayout->addWidget(createFile);
		startLayout->addWidget(openFile);
		start->setLayout(startLayout);
		start->addAction(createAction);
		start->addAction(openAction);

		//splitter of renderer and configuration tabs
		QWidget *mainWidget = new QWidget();
		QGridLayout *mainLayout = new QGridLayout();
		QToolBar *toolBar = new QToolBar();
		toolBar->setStyleSheet(
			"QToolBar {"
			"	border: 0px solid transparent;"
			"}"
			);
		toolBar->addAction(createAction);
		toolBar->addAction(openAction);
		toolBar->addAction(saveAction);
		toolBar->addSeparator();
		toolBar->addAction(compileAction);

		splitter = new QSplitter();
		nextConfigID = 0;
		configurations = new QTabWidget();
		configurations->setStyleSheet(
			"QTabBar::close-button {"
			"	image: url(../data/editor/icons/close_unfocused.png);"
			"}"

			"QTabBar::close-button:hover {"
			"	image: url(../data/editor/icons/close_focused.png);"
			"}"

			"QTabBar::close-button:pressed {"
			"	image: url(../data/editor/icons/close_pressed.png);"
			"}"
			);
		configurations->setTabPosition(QTabWidget::West);
		configurations->setTabsClosable(true);
		connect(configurations,SIGNAL(tabCloseRequested(int)),this,SLOT(removeConfiguration(int)));
		renderer = new SXWidget();
		splitter->addWidget(configurations);
		splitter->addWidget(renderer);
		splitter->setStretchFactor(0,1);
		splitter->setStretchFactor(1,1);
		QList<int> sizes;
		sizes << 680 << 400;
		splitter->setSizes(sizes);
		mainLayout->addWidget(toolBar,0,0);
		mainLayout->addWidget(splitter,1,0);
		mainWidget->setLayout(mainLayout);

		mainGuis->addWidget(start);
		mainGuis->addWidget(mainWidget);
		mainGuis->setCurrentIndex(0);

		QGridLayout *layout = new QGridLayout();
		layout->addWidget(mainGuis);
		layout->setContentsMargins(0,0,0,0);
		setLayout(layout);
	}

	SXEditor::SXEditor(): QWidget() {
		setWindowIcon(QIcon("../data/SX.png"));
		setWindowTitle("ShadeXEngine editor v1.0a");
		initGui();
		resize(1200,600);
	}

	SXEditor::~SXEditor() {
	}

	void SXEditor::addConfiguration(const QString &path, bool load) {
		bool usePath = path.size() > 0;
		QString name;
		if(usePath) {
			QStringList pathparts = path.split(QRegExp("(\\\\|/)"));
			name = pathparts.last();
		} else {
			name = "";
		}
		if(configParts.size() == 0) {
			//nothing loaded yet
			//change from start screen to work area
			mainGuis->setCurrentIndex(1);
		}
		configParts.push_back(ConfigParts());
		ConfigParts &part = configParts.last();
		part.ID = nextConfigID++;
		part.tools = new QWidget();
		SequenceLayout *toolLayout = new SequenceLayout(EXPAND_HORIZONTALLY);
		toolLayout->setMargin(10);
		QToolButton *addPassButton = new QToolButton();
		addPassButton->setDefaultAction(addPassAction);
		addPassButton->setIconSize(QSize(40,40));
		addPassButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		toolLayout->addWidget(addPassButton);
		part.tools->setLayout(toolLayout);
		part.graphEdit = new GraphEditPane(part.ID, QSize(2000,2000),this);
		QGraphicsView *graphEditView = new QGraphicsView(part.graphEdit);
		part.graphEdit->setGraphicsView(graphEditView);
		graphEditView->setStyleSheet(
			".QGraphicsView {"
			"   border: 1px solid transparent;"
			"}"
			);
		part.detailEdit = new QWidget();
		part.closeDetailButton = new QToolButton();
		part.closeDetailButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		part.closeDetailButton->setStyleSheet(
			"QToolButton {"
			"   background-image: url(../data/editor/icons/close_unfocused.png);"
			"   border: 1px solid transparent;"
			"}"

			"QToolButton::hover {"
			"   background-image: url(../data/editor/icons/close_focused.png);"
			"   border: 1px solid transparent;"
			"}"

			"QToolButton::pressed {"
			"   background-image: url(../data/editor/icons/close_pressed.png);"
			"   border: 1px solid transparent;"
			"}"
			);
		part.detailEditPane = new QWidget();
		connect(part.closeDetailButton,SIGNAL(clicked()),part.detailEdit,SLOT(hide()));
		QGridLayout *detailLayout = new QGridLayout();
		detailLayout->addWidget(part.closeDetailButton,0,0,1,1);
		detailLayout->addWidget(part.detailEditPane,1,0,1,2);
		part.detailEdit->setLayout(detailLayout);
		part.detailEdit->hide();
		QSplitter *configSplitter = new QSplitter();
		configSplitter->addWidget(part.tools);
		configSplitter->addWidget(graphEditView);
		configSplitter->addWidget(part.detailEdit);
		configSplitter->setStretchFactor(0,0);
		configSplitter->setStretchFactor(1,1);
		configSplitter->setStretchFactor(2,0);
		QList<int> sizes;
		sizes << 180 << 800 << 180;
		configSplitter->setSizes(sizes);
		configurations->addTab(configSplitter,name);
	}

	QSize SXEditor::sizeHint() const {
		QSize s;
		s.setWidth(1200);
		s.setHeight(600);
		return s;
	}

	ConfigParts &SXEditor::currentConfig() {
		if(configParts.size() == 0) {
			throw Exception("currently no configuration tab is present",EX_NODATA);
		}
		return configParts[configurations->currentIndex()];
	}

	void SXEditor::removeConfiguration(int index) {
		//send message to graphedit about removal of tab
		configParts[index].graphEdit->removeDiagramItems();
		//remove
		configurations->removeTab(index);
		configParts.erase(configParts.begin() + index);
		if(configParts.size() == 0) {
			mainGuis->setCurrentIndex(0);
		}
	}

}

#endif