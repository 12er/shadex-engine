#ifndef _SX_EDITOR_GRAPHICWIDGET_CPP_
#define _SX_EDITOR_GRAPHICWIDGET_CPP_

/**
 * Graphic widget
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXEditor.h>

namespace sx {

	GraphicWidget::GraphicWidget(int guiID, GraphEditPane *scene, QWidget *parent): QWidget(parent) {
		this->guiID = guiID;
		this->scene = scene;
	}

	void GraphicWidget::startMove() {
		scene->setEditMode(MODE_MOVE);
	}

	int GraphicWidget::getGuiID() const {
		return guiID;
	}

	void GraphicWidget::pack() {
		adjustSize();
	}

}

#endif