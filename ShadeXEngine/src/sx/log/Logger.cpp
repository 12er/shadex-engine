#ifndef _SX_LOG_LOGGER_CPP_
#define _SX_LOG_LOGGER_CPP_

/**
 * Logger class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>

namespace sx {

	map<string,Logger *> Logger::loggers = map<string,Logger *>();
	Logger *Logger::defaultLogger = Logger::initDefaultLogger();

	string Logger::newLine() {
		return "\n";
	}

	Logger *Logger::initDefaultLogger() {
		Logger *logger = new ConsoleLogger();
		loggers.insert(pair<string,Logger *>("SX",logger));
		return logger;
	}

	void Logger::addLogger(std::string ID, sx::Logger *logger) {
		if(logger == 0) {
			throw Exception("Adding null pointers instead of loggers is not allowed");
		}
		if(loggers.find(ID) != loggers.end()) {
			throw Exception("Another logger with the same ID exists already");
		}
		loggers.insert(pair<string,Logger *>(ID,logger));
	}

	Logger &Logger::get() {
		return defaultLogger [0];
	}

	void Logger::setDefaultLogger(std::string ID) {
		map<string,Logger *>::iterator iter = loggers.find(ID);
		if(iter == loggers.end()) {
			throw Exception("A logger with the specified identifyer does not exist");
		}
		defaultLogger = (*iter).second;
	}

	void Logger::deleteLogger(std::string ID) {
		map<string,Logger *>::iterator iter = loggers.find(ID);
		if(iter == loggers.end()) {
			throw Exception("A logger with the specified identifyer does not exist");
		}
		Logger *logger = (*iter).second;
		loggers.erase(iter);
		delete logger;
	}

	Logger::Logger() {
		this->minLevel = L_HARMLESS;
		this->level = L_HARMLESS;
	}

	Logger::~Logger() {
		
	}

	bool Logger::takeMessage() const {
		if(this->minLevel == L_NONE) {
			return false;
		}
		return this->minLevel <= this->level;
	}

	void Logger::setMinLevel(sx::LogLevel level) {
		this->minLevel = level;
	}
	
	LogLevel Logger::getMinLevel() const {
		return this->minLevel;
	}

	LogLevel Logger::getLevel() const {
		return this->level;
	}

	Logger &Logger::operator <<(const sx::Level l) {
		this->level = l.getLevel();
		return this [0];
	}

}

#endif