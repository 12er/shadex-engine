#ifndef _SX_LOG_LISTLOGGER_CPP_
#define _SX_LOG_LISTLOGGER_CPP_

/**
 * Logger storing input in lists
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>

namespace sx {

	ListLogger::ListLogger(): Logger() {
		markups.insert(pair<string,ListMarkup>("",ListMarkup("")));
		map<string,ListMarkup>::iterator iter = markups.find("");
		defaultMarkup = &(*iter).second;
	}

	ListLogger::~ListLogger() {
	}

	Logger &ListLogger::operator <<(const sx::Level l) {
		return Logger::operator <<(l);
	}

	Logger &ListLogger::operator <<(const sx::LogMarkup m) {
		map<string,ListMarkup>::iterator iter = markups.find(m.getMessage());
		if(iter == markups.end()) {
			markups.insert(pair<string,ListMarkup>(m.getMessage(),ListMarkup(m.getMessage())));
			iter = markups.find(m.getMessage());
		}
		defaultMarkup = &(*iter).second;

		return this [0];
	}

	Logger &ListLogger::operator <<(const sx::Annotation a) {
		defaultMarkup->getAnnotations().push_back(a);
		return this [0];
	}

	Logger &ListLogger::operator <<(char value) {
		if(takeMessage()) {
			defaultMarkup->getChars().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(unsigned char value) {
		if(takeMessage()) {
			defaultMarkup->getUchars().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(short value) {
		if(takeMessage()) {
			defaultMarkup->getShorts().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(unsigned short value) {
		if(takeMessage()) {
			defaultMarkup->getUshorts().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(int value) {
		if(takeMessage()) {
			defaultMarkup->getInts().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(unsigned int value) {
		if(takeMessage()) {
			defaultMarkup->getUints().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(long value) {
		if(takeMessage()) {
			defaultMarkup->getLongs().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(unsigned long value) {
		if(takeMessage()) {
			defaultMarkup->getUlongs().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(float value) {
		if(takeMessage()) {
			defaultMarkup->getFloats().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(double value) {
		if(takeMessage()) {
			defaultMarkup->getDoubles().push_back(value);
		}
		return this [0];
	}

	Logger &ListLogger::operator <<(std::string value) {
		if(takeMessage()) {
			defaultMarkup->getStrings().push_back(value);
		}
		return this [0];
	}

	map<string,ListMarkup> &ListLogger::getMarkups() {
		return markups;
	}

	Annotation ListLogger::getAnnotationEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<Annotation> &entries = (*iter).second.getAnnotations();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	char ListLogger::getCharEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<char> &entries = (*iter).second.getChars();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	unsigned char ListLogger::getUcharEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<unsigned char> &entries = (*iter).second.getUchars();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	short ListLogger::getShortEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<short> &entries = (*iter).second.getShorts();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	unsigned short ListLogger::getUshortEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<unsigned short> &entries = (*iter).second.getUshorts();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	int ListLogger::getIntEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<int> &entries = (*iter).second.getInts();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	unsigned int ListLogger::getUintEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<unsigned int> &entries = (*iter).second.getUints();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	long ListLogger::getLongEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<long> &entries = (*iter).second.getLongs();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	unsigned long ListLogger::getUlongEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<unsigned long> &entries = (*iter).second.getUlongs();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	float ListLogger::getFloatEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<float> &entries = (*iter).second.getFloats();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	double ListLogger::getDoubleEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<double> &entries = (*iter).second.getDoubles();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

	string ListLogger::getStringEntry(std::string ID, unsigned int index) {
		map<string,ListMarkup>::iterator iter = markups.find(ID);
		if(iter == markups.end()) {
			throw Exception("Invalid markup ID");
		}
		vector<string> &entries = (*iter).second.getStrings();
		if(index >= entries.size()) {
			throw Exception("Index out of bounds");
		}
		return entries[index];
	}

}

#endif