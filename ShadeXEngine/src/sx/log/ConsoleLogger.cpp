#ifndef _SX_LOG_CONSOLELOGGER_CPP_
#define _SX_LOG_CONSOLELOGGER_CPP_

/**
 * Logger printing log on the console
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>
#include <iostream>
using namespace std;

namespace sx {

	ConsoleLogger::ConsoleLogger(): Logger()  {	
	}

	ConsoleLogger::~ConsoleLogger() {
	}

	Logger &ConsoleLogger::operator <<(const sx::Level l) {
		return Logger::operator <<(l);
	}

	Logger &ConsoleLogger::operator <<(const sx::LogMarkup m) {
		cout << "---" << m.getMessage() << "---" << endl;
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(const sx::Annotation a) {
		cout << Logger::newLine() << a.getAnnotation();
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(char value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(unsigned char value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(short value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(unsigned short value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(int value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(unsigned int value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(long value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(unsigned long value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(float value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(double value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

	Logger &ConsoleLogger::operator <<(std::string value) {
		if(takeMessage()) {
			cout << value;
		}
		return this[0];
	}

}

#endif