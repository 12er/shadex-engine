#ifndef _SX_LOG_LOGMARKUP_CPP_
#define _SX_LOG_LOGMARKUP_CPP_

/**
 * Markup for Logs
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>

namespace sx {

	LogMarkup::LogMarkup(std::string message) {
		this->message = message;
	}

	LogMarkup::LogMarkup(const sx::LogMarkup &lm) {
		this->message = lm.message;
	}

	LogMarkup &LogMarkup::operator =(const sx::LogMarkup &lm) {
		if(this == &lm) {
			return this [0];
		}
		this->message = lm.message;
		return this [0];
	}

	LogMarkup::~LogMarkup() {
	}

	string LogMarkup::getMessage() const {
		return message;
	}

}

#endif