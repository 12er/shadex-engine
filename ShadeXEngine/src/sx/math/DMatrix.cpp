#ifndef _SX_MATH_DMATRIX_CPP_
#define _SX_MATH_DMATRIX_CPP_

/**
 * double precision matrix class
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXMath.h>
#include <sx/Log4SX.h>

namespace sx {

	DMatrix::DMatrix() {
		elements = new double[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = 0.0;
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i*5] = 1.0;
		}
	}

	DMatrix::DMatrix(double m) {
		elements = new double[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m;
		}
	}

	DMatrix::DMatrix(
		double m00, double m01,
		double m10, double m11) {
		elements = new double[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = 0.0;
		}
		elements[0] = m00; elements[4] = m01;
		elements[1] = m10; elements[5] = m11;
		elements[10] = 1.0;
		elements[15] = 1.0;
	}

	DMatrix::DMatrix(
		double m00, double m01, double m02,
		double m10, double m11, double m12,
		double m20, double m21, double m22) {
		elements = new double[16];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[3 + i*4] = 0.0;
		}
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[12 + i] = 0.0;
		}
		elements[0] = m00; elements[4] = m01; elements[8] = m02;
		elements[1] = m10; elements[5] = m11; elements[9] = m12;
		elements[2] = m20; elements[6] = m21; elements[10] = m22;
		elements[15] = 1.0;
	}

	DMatrix::DMatrix(
		double m00, double m01, double m02, double m03,
		double m10, double m11, double m12, double m13,
		double m20, double m21, double m22, double m23,
		double m30, double m31, double m32, double m33) {
		elements = new double[16];
		elements[0] = m00; elements[4] = m01; elements[8] = m02; elements[12] = m03;
		elements[1] = m10; elements[5] = m11; elements[9] = m12; elements[13] = m13;
		elements[2] = m20; elements[6] = m21; elements[10] = m22; elements[14] = m23;
		elements[3] = m30; elements[7] = m31; elements[11] = m32; elements[15] = m33;
	}

	DMatrix::DMatrix(const DVector &m0, const DVector &m1, const DVector &m2, const DVector &m3) {
		elements = new double[16];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = m0.elements[i];
			elements[i+4] = m1.elements[i];
			elements[i+8] = m2.elements[i];
			elements[i+12] = m3.elements[i];
		}
	}

	DMatrix::DMatrix(const double *m) {
		elements = new double[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m[i];
		}
	}

	DMatrix::DMatrix(const Matrix &m) {
		elements = new double[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = (float)m.elements[i];
		}
	}

	DMatrix::DMatrix(const DMatrix &m) {
		elements = new double[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
	}

	DMatrix &DMatrix::operator = (const DMatrix &m) {
		if(this == &m) {
			return this [0];
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
		return this [0];
	}

	DMatrix::~DMatrix() {
		SXout(LogMarkup("~DMatrix"));
		SXout("~DMatrix");
		delete [] elements;
	}

	double &DMatrix::operator [] (unsigned int index) {
		if(index >= 16) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements [index];
	}

	double DMatrix::operator [] (unsigned int index) const {
		if(index >= 16) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements [index];
	}

	DMatrix &DMatrix::operator << (const DMatrix &m) {
		if(this == &m) {
			return this [0];
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::operator << (const double *m) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m[i];
		}
		return this [0];
	}

	const DMatrix &DMatrix::operator >> (DMatrix &m) const {
		if(this == &m) {
			return this [0];
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			m.elements[i] = elements[i];
		}
		return this [0];
	}

	const DMatrix &DMatrix::operator >> (double *m) const {
		for(unsigned int i=0 ; i<16 ; i++) {
			m[i] = elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::add(const DMatrix &m) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] += m.elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::add(double m) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] += m;
		}
		return this [0];
	}

	DMatrix operator + (const DMatrix &m1, const DMatrix &m2) {
		DMatrix m = m1;
		for(unsigned int i=0 ; i<16 ; i++) {
			m.elements[i] += m2.elements[i];
		}
		return m;
	}

	DMatrix operator + (const DMatrix &m, double x) {
		DMatrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] += x;
		}
		return mo;
	}

	DMatrix operator + (double x, const DMatrix &m) {
		DMatrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] += x;
		}
		return mo;
	}

	DMatrix &DMatrix::leftmult(const DMatrix &m) {
		DMatrix mo;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < 4 ; r++) {
				mo.elements[r + c*4] = 0.0;
				for(unsigned int unite = 0 ; unite < 4 ; unite++) {
					mo.elements[r + c*4] += m.elements[r + unite*4] * elements[unite + c*4];
				}
			}
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = mo.elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::rightmult(const DMatrix &m) {
		DMatrix mo;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < 4 ; r++) {
				mo.elements[r + c*4] = 0.0f;
				for(unsigned int unite = 0 ; unite < 4 ; unite++) {
					mo.elements[r + c*4] += elements[r + unite*4] * m.elements[unite + c*4];
				}
			}
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = mo.elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::scalarmult(double s) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] *= s;
		}
		return this [0];
	}

	DMatrix operator * (const DMatrix &m1, const DMatrix &m2) {
		DMatrix m;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < 4 ; r++) {
				m.elements[r + c*4] = 0.0;
				for(unsigned int unite = 0 ; unite < 4 ; unite++) {
					m.elements[r + c*4] += m1.elements[r + unite*4] * m2.elements[unite + c*4];
				}
			}
		}
		return m;
	}

	DMatrix operator * (const DMatrix &m, double s) {
		DMatrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] *= s;
		}
		return mo;
	}

	DMatrix operator * (double s, const DMatrix &m) {
		DMatrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] *= s;
		}
		return mo;
	}

	DMatrix operator - (const DMatrix &m) {
		DMatrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] *= -1;
		}
		return mo;
	}

	DMatrix &DMatrix::transpose() {
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < c ; r++) {
				double temp = elements[r + c*4];
				elements[r + c*4] = elements[c + r*4];
				elements[c + r*4] = temp;
			}
		}
		return this [0];
	}

	DMatrix operator ! (const DMatrix &m) {
		DMatrix trans = m;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < c ; r++) {
				double temp = trans.elements[r + c*4];
				trans.elements[r + c*4] = trans.elements[c + r*4];
				trans.elements[c + r*4] = temp;
			}
		}
		return trans;
	}

	DMatrix &DMatrix::identity() {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = 0.0;
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i*5] = 1.0;
		}
		return this [0];
	}

	double DCOFACTOR(double *elements, unsigned int r, unsigned int c) {
		double sub[9];
		double sign = 1.0;
		if((r+c)%2 == 1) {
			//sign = (-1)^(r + c)
			sign = -1.0f;
		}
		unsigned int source = 0;
		unsigned int destination = 0;
		for(unsigned int column = 0 ; column < 4 ; column++) {
			for(unsigned int row = 0 ; row < 4 ; row++) {
				if(r != row && c != column) {
					sub[destination] = elements[source];
					destination++;
				}
				source++;
			}
		}

		return sign * (
			sub[0] * sub[4] * sub[8] - sub[0] * sub[5] * sub[7] +
			sub[1] * sub[6] * sub[5] - sub[1] * sub[3] * sub[8] +
			sub[2] * sub[3] * sub[7] - sub[2] * sub[4] * sub[6]
			);
	}

	DMatrix &DMatrix::inverse() {
		double det = determinant();
		if(det == 0.0) {
			//obviously the matrix is not regular
			//, hence it can't be inverted
			return this [0];
		}
		double inv = 1/det;
		DMatrix m;
		unsigned int index = 0;
		for(unsigned int column = 0 ; column < 4 ; column++) {
			for(unsigned int row = 0 ; row < 4 ; row++) {
				m.elements[index] = inv * DCOFACTOR(elements,column,row);
				index++;
			}
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
		return this [0];
	}

	DMatrix operator ^ (const DMatrix &m, int power) {
		if(power == 0) {
			return DMatrix();
		}
		DMatrix powm;
		DMatrix single = m;
		if(power < 0) {
			single.inverse();
		}
		int counter = 0;
		if(counter+1 <= abs(power)) {
			//calculate m^+-1
			counter++;
			powm = single;
		}
		while(counter * 2 <= abs(power)) {
			//try to reach potence exponentially
			//as long as possible
			counter *= 2;
			powm = powm * powm;
		}
		while(counter + 1 <= abs(power)) {
			//if power is not a power of two,
			//calculate the rest of the
			//multiplications in a linear way
			counter++;
			powm = powm * single;
		}

		return powm;
	}

	double DMatrix::determinant() const {
		return
			elements[0+4*0] * elements[1+4*1] * elements[2+4*2] * elements[3+4*3]	-	elements[0+4*0] * elements[1+4*1] * elements[3+4*2] * elements[2+4*3]
			+ elements[0+4*0] * elements[2+4*1] * elements[3+4*2] * elements[1+4*3]	-	elements[0+4*0] * elements[2+4*1] * elements[1+4*2] * elements[3+4*3]
			+ elements[0+4*0] * elements[3+4*1] * elements[1+4*2] * elements[2+4*3]	-	elements[0+4*0] * elements[3+4*1] * elements[2+4*2] * elements[1+4*3]

			- elements[1+4*0] * elements[0+4*1] * elements[2+4*2] * elements[3+4*3]	+	elements[1+4*0] * elements[0+4*1] * elements[3+4*2] * elements[2+4*3]
			- elements[1+4*0] * elements[2+4*1] * elements[3+4*2] * elements[0+4*3]	+	elements[1+4*0] * elements[2+4*1] * elements[0+4*2] * elements[3+4*3]
			- elements[1+4*0] * elements[3+4*1] * elements[0+4*2] * elements[2+4*3]	+	elements[1+4*0] * elements[3+4*1] * elements[2+4*2] * elements[0+4*3]

			+ elements[2+4*0] * elements[0+4*1] * elements[1+4*2] * elements[3+4*3]	-	elements[2+4*0] * elements[0+4*1] * elements[3+4*2] * elements[1+4*3]
			+ elements[2+4*0] * elements[1+4*1] * elements[3+4*2] * elements[0+4*3]	-	elements[2+4*0] * elements[1+4*1] * elements[0+4*2] * elements[3+4*3]
			+ elements[2+4*0] * elements[3+4*1] * elements[0+4*2] * elements[1+4*3]	-	elements[2+4*0] * elements[3+4*1] * elements[1+4*2] * elements[0+4*3]

			- elements[3+4*0] * elements[0+4*1] * elements[1+4*2] * elements[2+4*3]	+	elements[3+4*0] * elements[0+4*1] * elements[2+4*2] * elements[1+4*3]
			- elements[3+4*0] * elements[1+4*1] * elements[2+4*2] * elements[0+4*3]	+	elements[3+4*0] * elements[1+4*1] * elements[0+4*2] * elements[2+4*3]
			- elements[3+4*0] * elements[2+4*1] * elements[0+4*2] * elements[1+4*3]	+	elements[3+4*0] * elements[2+4*1] * elements[1+4*2] * elements[0+4*3]
		;
	}

	double DMatrix::cofactor(unsigned int row, unsigned int column) const {
		if(row >= 4 || column >= 4) {
			throw Exception("out of bounds", EX_BOUNDS);
		}
		return DCOFACTOR(elements,row,column);
	}

	DMatrix &DMatrix::random() {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = ((double)rand())/((double)RAND_MAX);
		}
		return this [0];
	}

	DMatrix &DMatrix::rotate(const DVector &m, double angle) {
		DVector n = m;
		n.normalize();
		double c = cos(angle);
		double s = sin(angle);
		double x = n.elements[0];
		double y = n.elements[1];
		double z = n.elements[2];
		elements[0] = x*x*(1-c)+c; elements[4] = x*y*(1-c)-z*s; elements[8] = x*z*(1-c)+y*s; elements[12] = 0;
		elements[1] = y*x*(1-c)+z*s; elements[5] = y*y*(1-c)+c; elements[9] = y*z*(1-c)-x*s; elements[13] = 0;
		elements[2] = x*z*(1-c)-y*s; elements[6] = y*z*(1-c)+x*s; elements[10] = z*z*(1-c)+c; elements[14] = 0;
		elements[3] = 0; elements[7] = 0; elements[11] = 0; elements[15] = 1;
		return this [0];
	}

	DMatrix &DMatrix::translate(const DVector &v) {
		identity();
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[12 + i] = v.elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::scale(const DVector &v) {
		identity();
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i*5] = v.elements[i];
		}
		return this [0];
	}

	DMatrix &DMatrix::shear(const DVector &v) {
		if(v.elements[2] == 0.0) {
			//can't make this matrix a shear matrix,
			//if the z component of v is zero
			return this [0];
		}
		identity();
		this->elements[8] = -v.elements[0]/v.elements[2];
		this->elements[9] = -v.elements[1]/v.elements[2]; 
		return this [0];
	}

	DMatrix &DMatrix::submatrix() {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i*4 + 3] = 0.0;
			elements[i + 12] = 0.0;
		}
		elements[15] = 1.0;
		return this [0];
	}

	DMatrix &DMatrix::normalMatrix() {
		submatrix();
		inverse();
		transpose();
		return this [0];
	}

	DMatrix &DMatrix::viewMatrix(const DVector &position, const DVector &view, const DVector &up) {
		if(view.length() == 0.0f || up.length() == 0.0) {
			return this [0];
		}
		DVector f = view;
		DVector u = up;
		f.normalize();
		u.normalize();
		double product = f.innerprod(u);
		if(abs(product) == 1.0) {
			//f and u don't span a plane
			//hence it's not possible to continue
			return this [0];
		}
		DVector s = f;
		s.crossmult(u);
		s.normalize();
		u = s;
		u.crossmult(f);
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i*4] = s.elements[i];
			elements[i*4 + 1] = u.elements[i];
			elements[i*4 + 2] = -f.elements[i];
		}
		submatrix();
		DVector eye = position;
		eye.scalarmult(-1.0);
		DMatrix trans;
		trans.translate(eye);
		rightmult(trans);
		return this [0];
	}

	DMatrix &DMatrix::perspectiveMatrix(double angle, double width, double height, double znear, double zfar) {
		if(angle == 0.0f || width == 0.0f || height == 0.0f || znear == zfar) {
			return this [0];
		}
		elements[0] = 1/(tan(angle/2.0)*(width/height));	elements[4] = 0.0;					elements[8] = 0.0;								elements[12] = 0.0;
		elements[1] = 0.0;									elements[5] = 1/tan(angle/2.0);	elements[9] = 0.0;								elements[13] = 0.0;
		elements[2] = 0.0;									elements[6] = 0.0;					elements[10] = (zfar + znear)/(znear - zfar);	elements[14] = (2.0 * zfar * znear)/(znear - zfar);
		elements[3] = 0.0;									elements[7] = 0.0;					elements[11] = -1.0;							elements[15] = 0.0;
		return this [0];
	}

	DMatrix &DMatrix::orthographicPerspeciveMatrix(double left, double right, double bottom, double top, double znear, double zfar) {
		if(left == right || bottom == top || znear == zfar) {
			return this [0];
		}
		elements[0] = 2.0/(right - left);	elements[4] = 0.0;					elements[8] = 0.0;						elements[12] = -(right+left)/(right - left);
		elements[1] = 0.0;					elements[5] = 2.0/(top - bottom);	elements[9] = 0.0;						elements[13] = -(top+bottom)/(top - bottom);
		elements[2] = 0.0;					elements[6] = 0.0;					elements[10] = -2.0/(zfar - znear);	elements[14] = -(zfar+znear)/(zfar - znear);
		elements[3] = 0.0;					elements[7] = 0.0;					elements[11] = 0.0;					elements[15] = 1.0;
		return this [0];
	}

	bool DMatrix::equals(const DMatrix &m) const {
		for(unsigned int i=0 ; i<16 ; i++) {
			if(elements[i] != m.elements[i]) {
				return false;
			}
		}
		return true;
	}

	bool DMatrix::equals(const DMatrix &m, double epsilon) const {
		for(unsigned int i=0 ; i<16 ; i++) {
			if(abs(elements[i] - m.elements[i]) > epsilon) {
				return false;
			}
		}
		return true;
	}

}

#endif