#ifndef _SX_MATH_VECTOR_CPP_
#define _SX_MATH_VECTOR_CPP_

/**
 * vector class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXMath.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <cmath>
using namespace std;

namespace sx {
	
	Vector::Vector() {
		elements = new float[4];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = 0.0f;
		}
		elements[3] = 1.0f;
	}

	Vector::Vector(float v) {
		elements = new float[4];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = v;
		}
		elements[3] = 1.0f;
	}

	Vector::Vector(float x,float y) {
		elements = new float[4];
		elements[0] = x;
		elements[1] = y;
		elements[2] = 0.0f;
		elements[3] = 1.0f;
	}

	Vector::Vector(float x,float y,float z) {
		elements = new float[4];
		elements[0] = x;
		elements[1] = y;
		elements[2] = z;
		elements[3] = 1.0f;
	}

	Vector::Vector(float x,float y,float z,float w) {
		elements = new float[4];
		elements[0] = x;
		elements[1] = y;
		elements[2] = z;
		elements[3] = w;
	}

	Vector::Vector(const float *v) {
		elements = new float[4];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v[i];
		}
	}

	Vector::Vector(const DVector &vector) {
		elements = new float[4];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = (float)vector.elements[i];
		}
	}

	Vector::Vector(const Vector &vector) {
		elements = new float[4];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = vector.elements[i];
		}
	}

	Vector &Vector::operator = (const Vector &v) {
		if(this == &v) {
			return this [0];
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v.elements[i];
		}
		return this [0];
	}

	Vector::~Vector() {
		SXout(LogMarkup("~Vector"));
		SXout("~Vector");
		delete [] elements;
	}

	float &Vector::operator [] (unsigned int index) {
		if(index >= 4) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements[index];
	}

	float Vector::operator [] (unsigned int index) const {
		if(index > 4) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements[index];
	}

	Vector &Vector::operator << (const Vector &v) {
		if(this == &v) {
			return this [0];
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v.elements[i];
		}
		return this [0];
	}

	Vector &Vector::operator << (const float *v) {
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v[i];
		}
		return this [0];
	}

	const Vector &Vector::operator >> (Vector &v) const {
		if(this == &v) {
			return this [0];
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			v.elements[i] = elements[i];
		}
		return this [0];
	}

	const Vector &Vector::operator >> (float *v) const {
		for(unsigned int i=0 ; i<4 ; i++) {
			v[i] = elements[i];
		}
		return this [0];
	}

	Vector &Vector::add(const Vector &v) {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] += v.elements[i];
		}
		return this [0];
	}

	Vector &Vector::add(float x) {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] += x;
		}
		return this [0];
	}

	Vector operator +(const Vector &v1, const Vector &v2) {
		Vector v;
		for(unsigned int i=0 ; i<3 ; i++) {
			v.elements[i] = v1.elements[i] + v2.elements[i];
		}
		return v;
	}

	Vector operator +(const Vector &v, float x) {
		Vector w;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] = v.elements[i] + x;
		}
		return w;
	}

	Vector operator +(float x, const Vector &v) {
		Vector w;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] = v.elements[i] + x;
		}
		return w;
	}

	Vector &Vector::crossmult(const Vector &v) {
		Vector w;
		w.elements[0] = elements[1] * v.elements[2] - elements[2] * v.elements[1];
		w.elements[1] = -(elements[0] * v.elements[2] - elements[2] * v.elements[0]);
		w.elements[2] = elements[0] * v.elements[1] - elements[1] * v.elements[0];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = w.elements[i];
		}
		return this [0];
	}

	Vector operator %(const Vector &v1, const Vector &v2) {
		Vector w;
		w.elements[0] = v1.elements[1] * v2.elements[2] - v1.elements[2] * v2.elements[1];
		w.elements[1] = -(v1.elements[0] * v2.elements[2] - v1.elements[2] * v2.elements[0]);
		w.elements[2] = v1.elements[0] * v2.elements[1] - v1.elements[1] * v2.elements[0];
		return w;
	}

	Vector &Vector::scalarmult(float s) {
		for(unsigned i=0 ; i<3 ; i++) {
			elements[i] *= s;
		}
		return this [0];
	}

	Vector operator *(const Vector &v, float x) {
		Vector w = v;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] *= x;
		}
		return w;
	}

	Vector operator *(float x, const Vector &v) {
		Vector w = v;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] *= x;
		}
		return w;
	}

	Vector operator --(const Vector &v) {
		Vector w = v;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] *= -1.0f;
		}
		return w;
	}

	float Vector::innerprod(const Vector &v) const {
		float ip = 0.0f;
		for(unsigned int i=0 ; i<3 ; i++) {
			ip += elements[i] * v.elements[i];
		}
		return ip;
	}

	float operator *(const Vector &v1, const Vector &v2) {
		float ip = 0.0f;
		for(unsigned int i=0 ; i<3 ; i++) {
			ip += v1.elements[i] * v2.elements[i];
		}
		return ip;
	}

	Vector &Vector::leftmult(const Matrix &m) {
		Vector w;
		w[3] = 0.0f;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += m.elements[outComponent + inComponent*4] * elements[inComponent];
			}
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = w.elements[i];
		}
		return this [0];
	}

	Vector &Vector::rightmult(const Matrix &m) {
		Vector w;
		w[3] = 0.0f;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += elements[inComponent] * m[inComponent + outComponent*4];
			}
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = w.elements[i];
		}
		return this [0];
	}

	Vector operator *(const Matrix &m, const Vector &v) {
		Vector w;
		w[3] = 0.0f;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += m.elements[outComponent + inComponent*4] * v.elements[inComponent];
			}
		}
		return w;
	}

	Vector operator *(const Vector &v, const Matrix &m) {
		Vector w;
		w[3] = 0.0f;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += v.elements[inComponent] * m.elements[inComponent + outComponent*4];
			}
		}
		return w;
	}

	Vector &Vector::normalize() {
		float size = 0.0f;
		for(unsigned int i=0 ; i<3 ; i++) {
			size += elements[i]*elements[i];
		}
		size = sqrt(size);
		if(size != 0) {
			for(unsigned i=0 ; i<3 ; i++) {
				elements[i] /= size;
			}
		}
		return this [0];
	}

	float Vector::distance(const Vector &v) const {
		Vector w;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] = elements[i] - v.elements[i];
		}
		float size = 0.0f;
		for(unsigned int i=0 ; i<3 ; i++) {
			size += w.elements[i]*w.elements[i];
		}
		return sqrt(size);
	}

	float Vector::length() const {
		float size = 0.0f;
		for(unsigned int i=0 ; i<3 ; i++) {
			size += elements[i]*elements[i];
		}
		return sqrt(size);
	}

	Vector &Vector::random() {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = ((float)rand())/((float)RAND_MAX);
		}
		elements[3] = 1.0f;
		return this [0];
	}

	Vector &Vector::homogenize() {
		if(elements[3] == 0.0f) {
			return this [0];
		}
		float hom = elements[3];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] /= hom;
		}
		return this [0];
	}

	bool Vector::equals(const Vector &v) const {
		for(unsigned int i=0 ; i<4 ; i++) {
			if(elements[i] != v.elements[i]) {
				return false;
			}
		}
		return true;
	}

	bool Vector::equals(const Vector &v, float epsilon) const {
		for(unsigned int i=0 ; i<4 ; i++) {
			if(abs(elements[i] - v.elements[i]) > epsilon) {
				return false;
			}
		}
		return true;
	}

}

#endif