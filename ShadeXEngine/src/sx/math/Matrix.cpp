#ifndef _SX_MATH_MATRIX_CPP_
#define _SX_MATH_MATRIX_CPP_

/**
 * matrix class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXMath.h>
#include <sx/Log4SX.h>

namespace sx {

	Matrix::Matrix() {
		elements = new float[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = 0.0f;
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i*5] = 1.0f;
		}
	}

	Matrix::Matrix(float m) {
		elements = new float[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m;
		}
	}

	Matrix::Matrix(
		float m00, float m01,
		float m10, float m11) {
		elements = new float[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = 0.0f;
		}
		elements[0] = m00; elements[4] = m01;
		elements[1] = m10; elements[5] = m11;
		elements[10] = 1.0f;
		elements[15] = 1.0f;
	}

	Matrix::Matrix(
		float m00, float m01, float m02,
		float m10, float m11, float m12,
		float m20, float m21, float m22) {
		elements = new float[16];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[3 + i*4] = 0.0f;
		}
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[12 + i] = 0.0f;
		}
		elements[0] = m00; elements[4] = m01; elements[8] = m02;
		elements[1] = m10; elements[5] = m11; elements[9] = m12;
		elements[2] = m20; elements[6] = m21; elements[10] = m22;
		elements[15] = 1.0f;
	}

	Matrix::Matrix(
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33) {
		elements = new float[16];
		elements[0] = m00; elements[4] = m01; elements[8] = m02; elements[12] = m03;
		elements[1] = m10; elements[5] = m11; elements[9] = m12; elements[13] = m13;
		elements[2] = m20; elements[6] = m21; elements[10] = m22; elements[14] = m23;
		elements[3] = m30; elements[7] = m31; elements[11] = m32; elements[15] = m33;
	}

	Matrix::Matrix(const Vector &m0, const Vector &m1, const Vector &m2, const Vector &m3) {
		elements = new float[16];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = m0.elements[i];
			elements[i+4] = m1.elements[i];
			elements[i+8] = m2.elements[i];
			elements[i+12] = m3.elements[i];
		}
	}

	Matrix::Matrix(const float *m) {
		elements = new float[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m[i];
		}
	}

	Matrix::Matrix(const DMatrix &m) {
		elements = new float[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = (float)m.elements[i];
		}
	}

	Matrix::Matrix(const Matrix &m) {
		elements = new float[16];
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
	}

	Matrix &Matrix::operator = (const Matrix &m) {
		if(this == &m) {
			return this [0];
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
		return this [0];
	}

	Matrix::~Matrix() {
		SXout(LogMarkup("~Matrix"));
		SXout("~Matrix");
		delete [] elements;
	}

	float &Matrix::operator [] (unsigned int index) {
		if(index >= 16) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements [index];
	}

	float Matrix::operator [] (unsigned int index) const {
		if(index >= 16) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements [index];
	}

	Matrix &Matrix::operator << (const Matrix &m) {
		if(this == &m) {
			return this [0];
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::operator << (const float *m) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m[i];
		}
		return this [0];
	}

	const Matrix &Matrix::operator >> (Matrix &m) const {
		if(this == &m) {
			return this [0];
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			m.elements[i] = elements[i];
		}
		return this [0];
	}

	const Matrix &Matrix::operator >> (float *m) const {
		for(unsigned int i=0 ; i<16 ; i++) {
			m[i] = elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::add(const Matrix &m) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] += m.elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::add(float m) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] += m;
		}
		return this [0];
	}

	Matrix operator + (const Matrix &m1, const Matrix &m2) {
		Matrix m = m1;
		for(unsigned int i=0 ; i<16 ; i++) {
			m.elements[i] += m2.elements[i];
		}
		return m;
	}

	Matrix operator + (const Matrix &m, float x) {
		Matrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] += x;
		}
		return mo;
	}

	Matrix operator + (float x, const Matrix &m) {
		Matrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] += x;
		}
		return mo;
	}

	Matrix &Matrix::leftmult(const Matrix &m) {
		Matrix mo;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < 4 ; r++) {
				mo.elements[r + c*4] = 0.0f;
				for(unsigned int unite = 0 ; unite < 4 ; unite++) {
					mo.elements[r + c*4] += m.elements[r + unite*4] * elements[unite + c*4];
				}
			}
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = mo.elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::rightmult(const Matrix &m) {
		Matrix mo;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < 4 ; r++) {
				mo.elements[r + c*4] = 0.0f;
				for(unsigned int unite = 0 ; unite < 4 ; unite++) {
					mo.elements[r + c*4] += elements[r + unite*4] * m.elements[unite + c*4];
				}
			}
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = mo.elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::scalarmult(float s) {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] *= s;
		}
		return this [0];
	}

	Matrix operator * (const Matrix &m1, const Matrix &m2) {
		Matrix m;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < 4 ; r++) {
				m.elements[r + c*4] = 0.0f;
				for(unsigned int unite = 0 ; unite < 4 ; unite++) {
					m.elements[r + c*4] += m1.elements[r + unite*4] * m2.elements[unite + c*4];
				}
			}
		}
		return m;
	}

	Matrix operator * (const Matrix &m, float s) {
		Matrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] *= s;
		}
		return mo;
	}

	Matrix operator * (float s, const Matrix &m) {
		Matrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] *= s;
		}
		return mo;
	}

	Matrix operator - (const Matrix &m) {
		Matrix mo = m;
		for(unsigned int i=0 ; i<16 ; i++) {
			mo.elements[i] *= -1;
		}
		return mo;
	}

	Matrix &Matrix::transpose() {
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < c ; r++) {
				float temp = elements[r + c*4];
				elements[r + c*4] = elements[c + r*4];
				elements[c + r*4] = temp;
			}
		}
		return this [0];
	}

	Matrix operator ! (const Matrix &m) {
		Matrix trans = m;
		for(unsigned int c = 0 ; c < 4 ; c++) {
			for(unsigned int r = 0 ; r < c ; r++) {
				float temp = trans.elements[r + c*4];
				trans.elements[r + c*4] = trans.elements[c + r*4];
				trans.elements[c + r*4] = temp;
			}
		}
		return trans;
	}

	Matrix &Matrix::identity() {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = 0.0f;
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i*5] = 1.0f;
		}
		return this [0];
	}

	float COFACTOR(float *elements, unsigned int r, unsigned int c) {
		float sub[9];
		float sign = 1.0f;
		if((r+c)%2 == 1) {
			//sign = (-1)^(r + c)
			sign = -1.0f;
		}
		unsigned int source = 0;
		unsigned int destination = 0;
		for(unsigned int column = 0 ; column < 4 ; column++) {
			for(unsigned int row = 0 ; row < 4 ; row++) {
				if(r != row && c != column) {
					sub[destination] = elements[source];
					destination++;
				}
				source++;
			}
		}

		return sign * (
			sub[0] * sub[4] * sub[8] - sub[0] * sub[5] * sub[7] +
			sub[1] * sub[6] * sub[5] - sub[1] * sub[3] * sub[8] +
			sub[2] * sub[3] * sub[7] - sub[2] * sub[4] * sub[6]
			);
	}

	Matrix &Matrix::inverse() {
		float det = determinant();
		if(det == 0.0f) {
			//obviously the matrix is not regular
			//, hence it can't be inverted
			return this [0];
		}
		float inv = 1/det;
		Matrix m;
		unsigned int index = 0;
		for(unsigned int column = 0 ; column < 4 ; column++) {
			for(unsigned int row = 0 ; row < 4 ; row++) {
				m.elements[index] = inv * COFACTOR(elements,column,row);
				index++;
			}
		}
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = m.elements[i];
		}
		return this [0];
	}

	Matrix operator ^ (const Matrix &m, int power) {
		if(power == 0) {
			return Matrix();
		}
		Matrix powm;
		Matrix single = m;
		if(power < 0) {
			single.inverse();
		}
		int counter = 0;
		if(counter+1 <= abs(power)) {
			//calculate m^+-1
			counter++;
			powm = single;
		}
		while(counter * 2 <= abs(power)) {
			//try to reach potence exponentially
			//as long as possible
			counter *= 2;
			powm = powm * powm;
		}
		while(counter + 1 <= abs(power)) {
			//if power is not a power of two,
			//calculate the rest of the
			//multiplications in a linear way
			counter++;
			powm = powm * single;
		}

		return powm;
	}

	float Matrix::determinant() const {
		return
			elements[0+4*0] * elements[1+4*1] * elements[2+4*2] * elements[3+4*3]	-	elements[0+4*0] * elements[1+4*1] * elements[3+4*2] * elements[2+4*3]
			+ elements[0+4*0] * elements[2+4*1] * elements[3+4*2] * elements[1+4*3]	-	elements[0+4*0] * elements[2+4*1] * elements[1+4*2] * elements[3+4*3]
			+ elements[0+4*0] * elements[3+4*1] * elements[1+4*2] * elements[2+4*3]	-	elements[0+4*0] * elements[3+4*1] * elements[2+4*2] * elements[1+4*3]

			- elements[1+4*0] * elements[0+4*1] * elements[2+4*2] * elements[3+4*3]	+	elements[1+4*0] * elements[0+4*1] * elements[3+4*2] * elements[2+4*3]
			- elements[1+4*0] * elements[2+4*1] * elements[3+4*2] * elements[0+4*3]	+	elements[1+4*0] * elements[2+4*1] * elements[0+4*2] * elements[3+4*3]
			- elements[1+4*0] * elements[3+4*1] * elements[0+4*2] * elements[2+4*3]	+	elements[1+4*0] * elements[3+4*1] * elements[2+4*2] * elements[0+4*3]

			+ elements[2+4*0] * elements[0+4*1] * elements[1+4*2] * elements[3+4*3]	-	elements[2+4*0] * elements[0+4*1] * elements[3+4*2] * elements[1+4*3]
			+ elements[2+4*0] * elements[1+4*1] * elements[3+4*2] * elements[0+4*3]	-	elements[2+4*0] * elements[1+4*1] * elements[0+4*2] * elements[3+4*3]
			+ elements[2+4*0] * elements[3+4*1] * elements[0+4*2] * elements[1+4*3]	-	elements[2+4*0] * elements[3+4*1] * elements[1+4*2] * elements[0+4*3]

			- elements[3+4*0] * elements[0+4*1] * elements[1+4*2] * elements[2+4*3]	+	elements[3+4*0] * elements[0+4*1] * elements[2+4*2] * elements[1+4*3]
			- elements[3+4*0] * elements[1+4*1] * elements[2+4*2] * elements[0+4*3]	+	elements[3+4*0] * elements[1+4*1] * elements[0+4*2] * elements[2+4*3]
			- elements[3+4*0] * elements[2+4*1] * elements[0+4*2] * elements[1+4*3]	+	elements[3+4*0] * elements[2+4*1] * elements[1+4*2] * elements[0+4*3]
		;
	}

	float Matrix::cofactor(unsigned int row, unsigned int column) const {
		if(row >= 4 || column >= 4) {
			throw Exception("out of bounds", EX_BOUNDS);
		}
		return COFACTOR(elements,row,column);
	}

	Matrix &Matrix::random() {
		for(unsigned int i=0 ; i<16 ; i++) {
			elements[i] = ((float)rand())/((float)RAND_MAX);
		}
		return this [0];
	}

	Matrix &Matrix::rotate(const Vector &m, float angle) {
		Vector n = m;
		n.normalize();
		float c = cos(angle);
		float s = sin(angle);
		float x = n.elements[0];
		float y = n.elements[1];
		float z = n.elements[2];
		elements[0] = x*x*(1-c)+c; elements[4] = x*y*(1-c)-z*s; elements[8] = x*z*(1-c)+y*s; elements[12] = 0;
		elements[1] = y*x*(1-c)+z*s; elements[5] = y*y*(1-c)+c; elements[9] = y*z*(1-c)-x*s; elements[13] = 0;
		elements[2] = x*z*(1-c)-y*s; elements[6] = y*z*(1-c)+x*s; elements[10] = z*z*(1-c)+c; elements[14] = 0;
		elements[3] = 0; elements[7] = 0; elements[11] = 0; elements[15] = 1;
		return this [0];
	}

	Matrix &Matrix::translate(const Vector &v) {
		identity();
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[12 + i] = v.elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::scale(const Vector &v) {
		identity();
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i*5] = v.elements[i];
		}
		return this [0];
	}

	Matrix &Matrix::shear(const Vector &v) {
		if(v.elements[2] == 0.0f) {
			//can't make this matrix a shear matrix,
			//if the z component of v is zero
			return this [0];
		}
		identity();
		this->elements[8] = -v.elements[0]/v.elements[2];
		this->elements[9] = -v.elements[1]/v.elements[2]; 
		return this [0];
	}

	Matrix &Matrix::submatrix() {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i*4 + 3] = 0.0f;
			elements[i + 12] = 0.0f;
		}
		elements[15] = 1.0f;
		return this [0];
	}

	Matrix &Matrix::normalMatrix() {
		submatrix();
		inverse();
		transpose();
		return this [0];
	}

	Matrix &Matrix::viewMatrix(const Vector &position, const Vector &view, const Vector &up) {
		if(view.length() == 0.0f || up.length() == 0.0f) {
			return this [0];
		}
		Vector f = view;
		Vector u = up;
		f.normalize();
		u.normalize();
		float product = f.innerprod(u);
		if(abs(product) == 1.0f) {
			//f and u don't span a plane
			//hence it's not possible to continue
			return this [0];
		}
		Vector s = f;
		s.crossmult(u);
		s.normalize();
		u = s;
		u.crossmult(f);
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i*4] = s.elements[i];
			elements[i*4 + 1] = u.elements[i];
			elements[i*4 + 2] = -f.elements[i];
		}
		submatrix();
		Vector eye = position;
		eye.scalarmult(-1.0f);
		Matrix trans;
		trans.translate(eye);
		rightmult(trans);
		return this [0];
	}

	Matrix &Matrix::perspectiveMatrix(float angle, float width, float height, float znear, float zfar) {
		if(angle == 0.0f || width == 0.0f || height == 0.0f || znear == zfar) {
			return this [0];
		}
		elements[0] = 1/(tan(angle/2.0f)*(width/height));	elements[4] = 0.0f;					elements[8] = 0.0f;								elements[12] = 0.0f;
		elements[1] = 0.0f;									elements[5] = 1/tan(angle/2.0f);	elements[9] = 0.0f;								elements[13] = 0.0f;
		elements[2] = 0.0f;									elements[6] = 0.0f;					elements[10] = (zfar + znear)/(znear - zfar);	elements[14] = (2.0f * zfar * znear)/(znear - zfar);
		elements[3] = 0.0f;									elements[7] = 0.0f;					elements[11] = -1.0f;							elements[15] = 0.0f;
		return this [0];
	}

	Matrix &Matrix::orthographicPerspeciveMatrix(float left, float right, float bottom, float top, float znear, float zfar) {
		if(left == right || bottom == top || znear == zfar) {
			return this [0];
		}
		elements[0] = 2.0f/(right - left);	elements[4] = 0.0f;					elements[8] = 0.0f;						elements[12] = -(right+left)/(right - left);
		elements[1] = 0.0f;					elements[5] = 2.0f/(top - bottom);	elements[9] = 0.0f;						elements[13] = -(top+bottom)/(top - bottom);
		elements[2] = 0.0f;					elements[6] = 0.0f;					elements[10] = -2.0f/(zfar - znear);	elements[14] = -(zfar+znear)/(zfar - znear);
		elements[3] = 0.0f;					elements[7] = 0.0f;					elements[11] = 0.0f;					elements[15] = 1.0f;
		return this [0];
	}

	bool Matrix::equals(const Matrix &m) const {
		for(unsigned int i=0 ; i<16 ; i++) {
			if(elements[i] != m.elements[i]) {
				return false;
			}
		}
		return true;
	}

	bool Matrix::equals(const Matrix &m, float epsilon) const {
		for(unsigned int i=0 ; i<16 ; i++) {
			if(abs(elements[i] - m.elements[i]) > epsilon) {
				return false;
			}
		}
		return true;
	}

}

#endif