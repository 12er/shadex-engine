#ifndef _STARTEDITOR_CPP_
#define _STARTEDITOR_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <sx/SXEditor.h>
#include <QApplication>

/**
 * Entry point of the editor
 */
int main(int argc, char **argv) {
	QApplication app(argc,argv);

	try {
		sx::Logger::addLogger("editor",new sx::FileLogger("ShadeXEngineEditor.html"));
		sx::Logger::setDefaultLogger("editor");
	} catch(sx::Exception &e) {
		sx::Logger::get() << e.getMessage();
	}

	sx::SXEditor editor;
	editor.show();

	return app.exec();
}

#endif