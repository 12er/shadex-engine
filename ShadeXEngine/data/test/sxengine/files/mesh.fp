#version 400 core

in vec3 vp_colors;
out vec4 frag;

void main() {
	frag = vec4(vp_colors,1);
}
