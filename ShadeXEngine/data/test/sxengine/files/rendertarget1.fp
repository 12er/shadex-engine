#version 400 core

in vec2 vp_texcoords;
out vec4 frag2;
out vec4 frag1;

uniform sampler2D logo;

void main() {

	vec4 soft = vec4(0);
	float count = 0;
	for(float x = -0.015 ; x <= 0.015 ; x=x+0.002) {
		for(float y = -0.015 ; y <= 0.015 ; y=y+0.002) {
			soft += texture2D(logo,vp_texcoords + vec2(x,y));
			count++;
		}
	}

	frag1 = soft/count;

	frag2 = 
	abs(texture2D(logo,vp_texcoords + vec2(0.003,0)) - texture2D(logo,vp_texcoords + vec2(-0.003,0)))
	+
	abs(texture2D(logo,vp_texcoords + vec2(0,0.003)) - texture2D(logo,vp_texcoords + vec2(0,-0.003)))
	;
}
