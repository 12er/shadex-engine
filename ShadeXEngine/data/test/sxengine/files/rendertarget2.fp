#version 400 core

in vec2 vp_texcoords;
out vec4 output;

uniform float useFrag1;
uniform sampler2D somefrag;
uniform sampler2D frag1;

void main() {
	output = texture2D(frag1,vp_texcoords) * useFrag1 + texture2D(somefrag,vp_texcoords) * (1 - useFrag1);
}

