#version 400 core

in vec2 vtc;
out vec4 target2;
out vec4 target1;
uniform vec4 weight;
uniform sampler2D v1;
uniform sampler2D v2;
uniform sampler2D v3;

void main() {
	target1 = texture2D(v1,vtc)*weight.x + texture2D(v2,vtc)*weight.y + texture2D(v3,vtc)*weight.z;
	target2 = texture2D(v1,vtc)*weight.z + texture2D(v2,vtc)*weight.x + texture2D(v3,vtc)*weight.y;
}
