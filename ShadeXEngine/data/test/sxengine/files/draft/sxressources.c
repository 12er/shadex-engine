(sxressources) {

	(texture) {
		id = "tex1";
		path = "../data/test/sxengine/files/ShadeXEngine.jpg";
	}

	(texture) {
		id = "tex2";
		width = 600;
		height = 400;
		format = "float";
	}

	(texture) {
		id = "init_on_load";
	}

	(rendertarget) {
		id = "target.init_on_load";
	}

	(rendertarget) {
		id = "target1";
		width = 600;
		height = 400;
	}

	(mesh) {
		id = "some.mesh";
		path = "../data/test/sxengine/files/mesh1.ply";
		(path) {
			"../data/test/sxengine/files/mesh2.ply"
			discardStandardBuffers = "true";
		}
	}
	
	(float) {
		id = "uniform1";
	}

	(float) {
		id = "uniform2";
		value = 15.332;
	}

	(vector) {
		id = "uniformvector";
		(value) {
			"10" "5" "0"
		}
	}

	(matrix) {
		id = "transforms.t2";
		(value) {
			"2" "0" "0" "0"
			"0" "2" "0" "0"
			"0" "0" "2" "0"
			"0" "0" "0" "1"
		}	
	}

	(shader) {
		id = "shader1";
		vertex = "../data/test/sxengine/files/shader1.vp";
		fragment = "../data/test/sxengine/files/shader1.fp";
	}

	(camera) {
		id = "cam1";
		
		(position) {
			"10" "10" "10"
		}

		(view) {
			"1" "1" "0"
		}

		(up) {
			"0" "0" "1"
		}

		(perspective) {
			angle = 0.7853;
			width = "target1.width";
			height = "target1.height";
			znear = 1;
			zfar = 1000;
		}
	}

	(object) {
		id = "obj1";
		shader = "someshader";
		mesh = "somemesh";
		visible = "true";
		(textures) {
			"tex1"
			somevar = "var.identifyer";
		}
		(floats) {

		}
		(vectors) {
		}
		(matrices) {
		}
		(uniformset) {
			id = 15;
			(floats) {
			}
		}
		(passes) {
			"pass1"
		}
	}

}
