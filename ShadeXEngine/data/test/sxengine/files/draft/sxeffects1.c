(sxeffects) {
	(pass) {
		id = "pass1";
		shader = "anothershader";

		(floats) {
			"somefloat"
			somevar = "var.identifyer";
		}
		(textures) {
			"sometexture"
		}
		(output) {
			clearDepthBuffer = "true";
			clearColorBuffer = "true";
			rendertarget = "target1";
			"anothertexture"
		}

	}

	(pass) {
		id = "somepass";
		visible = "false";
	}

	(effect) {
		id = "someeffect";
		visible = "true";

		(passes) {
			"pass1"
			"somepass"
		}
	}

}
