#version 400 core

in vec2 vp_texcoords;
out vec4 frag;

uniform sampler2D tex2;
uniform sampler2D tex1;

void main() {
	vec2 coord = vp_texcoords;
	if(coord.x < 0.5) {
		coord.x = coord.x * 2.0;
		float count = 0;
		vec4 color = vec4(0);
		for(float dx = -0.01 ; dx <= 0.01 ; dx = dx + 0.004) {
			for(float dy = -0.01 ; dy <= 0.01 ; dy = dy + 0.004) {
				color += texture2D(tex1,coord + vec2(dx,dy));
				count++;	
			}
		}
		frag = color/count;
	} else {
		coord.x = (coord.x - 0.5) * 2.0;
		vec2 s = vec2(0,-0.003);
		vec2 n = vec2(0,0.003);
		vec2 w = vec2(-0.003,0);
		vec2 e = vec2(0.003,0);
		frag = abs(texture2D(tex2,coord + s) - texture2D(tex2,coord + n)) + abs(texture2D(tex2,coord + w) - texture2D(tex2,coord + e));
	}
}
