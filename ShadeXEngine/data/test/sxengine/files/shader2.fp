#version 400 core

in vec2 gp_attr1;
out vec4 frag;

uniform sampler2D tex;

void main() {
	frag = texture2D(tex,gp_attr1);
}
