#version 400 core

in vec2 vtc;
out vec4 vol2;
out vec4 vol1;

uniform float layercoordinate;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;

void main() {
	vol1 = vec4(1,0,0,1);
	vol2 = vec4(0,1,0,1);

	//test of the layer coordinate
	//layer m out of n layers should be addressed by
	//(1/(2*n)) + (m/n)
	if(layercoordinate > 0.166665 && layercoordinate < 0.166667) {
		//render into layer 0 of 3 layers
		vol1 = texture2D(tex1,vtc);
		vol2 = texture2D(tex2,vtc);
	} else if(layercoordinate > 0.166665 + 0.33333333 && layercoordinate < 0.166667 + 0.33333333) {
		//render into layer 1 of 3 layers
		vol1 = texture2D(tex2,vtc);
		vol2 = texture2D(tex3,vtc);
	} else if(layercoordinate > 0.166665 + 0.66666666 && layercoordinate < 0.166667 + 0.66666666) {
		//render into layer 2 of 3 layers
		vol1 = texture2D(tex3,vtc);
		vol2 = texture2D(tex1,vtc);
	}
}
