#version 400 core

in vec2 coords;
out vec4 frag1;

uniform sampler2D logotex;
uniform sampler2D scenetex;

uniform float uselogo;
uniform float overlighten;

void main() {
	vec4 logocolor = texture2D(logotex,coords);
	vec4 scenecolor = texture2D(scenetex,coords);
	frag1 = (scenecolor * (1.0 - uselogo) + logocolor * uselogo) * max(overlighten*3.0 , 1.0);
}
