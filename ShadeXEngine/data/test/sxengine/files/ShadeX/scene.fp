#version 400 core

in vec2 vp_tex;
out vec4 frag1;
out vec4 frag2;

uniform sampler2D tex;

void main() {
	frag1 = texture2D(tex,vp_tex);
	frag2 = vec4(vec3(0.4),1);
}
