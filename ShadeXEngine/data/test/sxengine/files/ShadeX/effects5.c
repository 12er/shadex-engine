(effects) {

	(shader) {
		id = "quad.shader";
		
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 tcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				tcoords = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 tcoords;
			out vec4 frag1;

			void main() {
				frag1 = vec4(0,tcoords.x,tcoords.y,1);
			}
		"}

	}

	(rendertarget) {
		id = "display.rendertarget";
		renderToDisplay = "true";
		width = 600;
		height = 400;
	}

	(pass) {
		id = "quad.pass";
		shader = "quad.shader";
		backgroundQuad = "true";
		(output) {
			rendertarget = "display.rendertarget";
		}
	}

	(effect) {
		id = "quad.effect";
		(passes) {
			"quad.pass"
		}
	}

	(mesh) {
		id = "transform.m1.mesh";
		(attribute) {
			name = "vertices";
			outputName = "overtices";
			attributeSize = 4;
			(data) {
				"0.5""0""0""1" "0.4""0.4""0""1" "0.25""0.25""0""1"
				"0.25""0.25""0""1" "0.4""0.4""0""1" "0""0.5""0""1"
				"0""0.5""0""1" "-0.4""0.4""0""1" "-0.25""0.25""0""1"
				"-0.25""0.25""0""1" "-0.4""0.4""0""1" "-0.5""0""0""1"
				"-0.5""0""0""1" "-0.4""-0.4""0""1" "-0.25""-0.25""0""1"
				"-0.25""-0.25""0""1" "-0.4""-0.4""0""1" "0""-0.5""0""1"
				"0""-0.5""0""1" "0.4""-0.4""0""1" "0.25""-0.25""0""1"
				"0.25""-0.25""0""1" "0.4""-0.4""0""1" "0.5""0""0""1"
			}
		}
		(attribute) {
			name = "color";
			outputName = "ocolor";
			attributeSize = 3;
			(data) {
				"1""0""0" "0""1""0" "0""0""1"
				"0""0""1" "0""1""0" "1""0""1"
				"1""0""1" "0""1""1" "1""1""0"
				"1""1""0" "0""1""1" "1""0.5""1"
				"1""0.5""1" "0.5""1""0.1" "1""0.2""0.1"
				"1""0.2""0.1" "0.5""1""0.1" "0.3""0.2""1"
				"0.3""0.2""1" "0.1""1""0.3" "1""0.4""1"
				"1""0.4""1" "0.1""1""0.3" "1""0""0"
			}
		}
	}

	(matrix) {
		id = "transform.m.object.rotate";
		(value) {
			"0.9998"	"0.01745"	"0"	"0"
			"-0.01745"	"0.9998"	"0"	"0"
			"0"		"0"		"1"	"0"
			"0"		"0"		"0"	"1"
		}
	}

	(object) {
		id = "transform.m1.object";
		mesh = "transform.m1.mesh";
		(matrices) {
			transform = "transform.m.object.rotate";
		}
		(passes) {
			"transform.tf1.pass"
		}
	}

	(mesh) {
		id = "transform.m2.mesh";
		maxVertexCount = 35;
		(attribute) {
			name = "vertices";
			outputName = "overtices";
			attributeSize = 4;
		}
		(attribute) {
			name = "color";
			outputName = "ocolor";
			attributeSize = 3;
		}
	}

	(object) {
		id = "transform.m2.object";
		mesh = "transform.m2.mesh";
		(matrices) {
			transform = "transform.m.object.rotate";
		}
		(passes) {
			"transform.tf2.pass"
		}
	}


	(shader) {
		id = "transform.tf.shader";

		(transformfeedback) {
			"overtices"
			"ocolor"
		}

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec3 color;
			out vec4 overtices;
			out vec3 ocolor;

			uniform mat4 transform2;
			uniform mat4 transform;

			void main() {
				overtices = transform * transform2 * vertices;
				ocolor = color;
			}
		"}

	}

	(matrix) {
		id = "transform.tf.pass.scale";
		(value) {
			"0.998"	"0"	"0"	"0"
			"0"	"0.998"	"0"	"0"
			"0"	"0"	"0.998"	"0"
			"0"	"0"	"0"	"1"
		}
	}

	(pass) {
		id = "transform.tf1.pass";
		shader = "transform.tf.shader";
		(matrices) {
			transform2 = "transform.tf.pass.scale";
		}
		geometryoutput = "transform.m2.mesh";
	}

	(pass) {
		id = "transform.tf2.pass";
		shader = "transform.tf.shader";
		(matrices) {
			transform2 = "transform.tf.pass.scale";
		}
		geometryoutput = "transform.m1.mesh";
	}

	(shader) {
		id = "transform.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec3 color;
			in vec3 icolor;
			in mat4 itransform;
			out vec3 col;

			void main() {
				gl_Position = itransform * vertices;
				col = icolor*color;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec3 col;
			out vec4 frag1;

			void main() {
				frag1 = vec4(col,1);
			}
		"}

	}

	(mesh) {
		id = "transform.instance.mesh";
		faceSize = 1;
		(attribute) {
			name = "icolor";
			attributeSize = 3;
			(data) {
				"0.5 0.5 0.5 
				0 0 1 
				0 1 0 
				0 1 1 
				1 0 0 
				1 0 1 
				1 1 0 
				1 1 1 
				1 0 0"				
			}
		}
		(attribute) {
			name = "itransform";
			attributeSize = 16;
			(data) {
				"0.25 0 0 0	0 0.5 0 0	0 0 1 0		-1 -1 0 1 
				0.25 0 0 0	0 0.25 0 0	0 0 1 0		-0.8 -0.8 0.1 1
				0.5 0 0 0	0 0.25 0 0	0 0 1 0		-0.5 0 0.2 1
				0.1 0 0 0	0 0.1 0 0	0 0 1 0		-0.25 0.25 0.3 1
				0.25 0 0 0	0 0.25 0 0	0 0 1 0		0 0.5 0.4 1
				0.1 0 0 0	0 0.25 0 0	0 0 1 0		0.25 0 0.5 1
				0.5 0 0 0	0 0.5 0 0	0 0 1 0		0.5 0.5 0.6 1
				0.25 0 0 0	0 0.25 0 0	0 0 1 0		0.8 0.5 0.7 1
				0.1 0 0 0	0 0.1 0 0	0 0 1 0		0 -0.25 0.8 1"
			}
		}
	}

	(object) {
		id = "transform.instance.object";
		mesh = "transform.m1.mesh";
		(instancebuffers) {
			"transform.instance.mesh"
		}
		(passes) {
			"transform.pass"
		}
	}

	
	(shader) {
		id = "transform.noinfluencetest.render.shader";

		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 tcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				tcoords = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 tcoords;
			out vec4 frag1;

			void main() {
				frag1 = vec4(tcoords.x,1,tcoords.y,1);
			}
		"}
	
	}

	(shader) {
		id = "transform.noinfluencetest.visualize.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 tcoords;

			void main() {
				gl_Position = vertices;
				tcoords = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 tcoords;
			out vec4 frag1;

			uniform sampler2D displaypic;

			void main() {
				frag1 = texture2D(displaypic,vec2(1,1)-tcoords);
			}
		"}
	
	}

	(mesh) {
		id = "transform.noinfluencetest.mesh";
		faceSize = 3;
		(attribute) {
			name = "vertices";
			attributeSize = 4;
			(data) {
				"-1""-1""0.95""1" "1""-1""0.95""1" "1""1""0.95""1"
				"-1""-1""0.95""1" "1""1""0.95""1" "-1""1""0.95""1"
			}
		}
		(attribute) {
			name = "texcoords";
			attributeSize = 2;
			(data) {
				"0""0" "1""0" "1""1"
				"0""0" "1""1" "0""1"
			}
		}
	}

	(texture) {
		id = "transform.noinfluencetest.texture";
		width = 600;
		height = 400;
	}

	(rendertarget) {
		id = "transform.noinfluencetest.target";
		width = 600;
		height = 400;
	}

	(pass) {
		id = "transform.noinfluencetest.pass";
		shader = "transform.noinfluencetest.render.shader";
		backgroundQuad = "true";
		(output) {
			rendertarget = "transform.noinfluencetest.target";
			frag1 = "transform.noinfluencetest.texture";
		}
	}

	(object) {
		id = "transform.noinfluencetest.object";
		mesh = "transform.noinfluencetest.mesh";
		shader = "transform.noinfluencetest.visualize.shader";
		(textures) {
			displaypic = "transform.noinfluencetest.texture";
		}
		(passes) {
			"transform.pass"
		}
	}


	(pass) {
		id = "transform.pass";
		shader = "transform.shader";
		(output) {
			rendertarget = "display.rendertarget";
		}
	}

	(effect) {
		id = "transform.effect";
		(passes) {
			"transform.noinfluencetest.pass"
			"transform.tf1.pass"
			"transform.tf2.pass"
			"transform.pass"
		}
	}

}
