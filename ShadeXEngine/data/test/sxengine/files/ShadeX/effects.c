/**
 * some passes rendering some effects
 */
(effects) {

	/**
	 * effect to call all passes
	 * in an appropriate order
	 */

	(effect) {
		id = "test.effect";
		(passes) {
			"pass.scene"
			"pass.lightshaft"
			"pass.dowaves"
			"pass.display"
		}
	}

	/**
	 * quad for rendering post processing effects
	 */

	(mesh) {
		id = "mesh.postprocess";
		path = "../data/test/sxengine/files/ShadeX/target.ply";
	}

	/**
	 * object for rendering post processing effects
	 */

	(object) {
		id = "object.postprocess";
		mesh = "mesh.postprocess";
		(passes) {
			"pass.lightshaft"
			"pass.dowaves"
			"pass.display"
		}
	}

	/**
	 *  renders main scene
	 */

	(pass) {
		id = "pass.scene";
		shader = "shader.scene";

		(matrices) {
			viewprojection = "matrix.scene.viewprojection";
		}


		(output) {
			rendertarget = "target.scene";
			"frag1"
			"frag2"
		}
	}

	(shader) {
		id = "shader.scene";
		vertex = "../data/test/sxengine/files/ShadeX/scene.vp";
		fragment = "../data/test/sxengine/files/ShadeX/scene.fp";
	}

	(rendertarget) {
		id = "target.scene";
		width = 600;
		height = 400;
	}

	(texture) {
		id = "pass.scene.frag1";
		width = 600;
		height = 400;
		format = "byte";
	}

	(texture) {
		id = "pass.scene.frag2";
		width = 600;
		height = 400;
		format = "byte";
	}

	/**
	 * radial blur filter
	 */
	(pass) {
		id = "pass.lightshaft";
		shader = "shader.lightshaft";

		(textures) {
			tex = "pass.scene.frag2";
		}

		(output) {
			rendertarget = "target.lightshaft";
			"frag1"
		}
	}

	(shader) {
		id = "shader.lightshaft";
		vertex = "../data/test/sxengine/files/ShadeX/lightshaft.vp";
		fragment = "../data/test/sxengine/files/ShadeX/lightshaft.fp";
	}

	(rendertarget) {
		id = "target.lightshaft";
		width = 600;
		height = 400;
	}

	(texture) {
		id = "pass.lightshaft.frag1";
		width = 600;
		height = 400;
		format = "byte";
	}

	/**
	 * wave pass
	 */
	
	(pass) {
		id = "pass.dowaves";
		shader = "pass.dowaves.shader";

		(floats) {
			"wavefactor"
		}

		(vectors) {
			"trans"
		}

		(textures) {
			colored = "pass.scene.frag1";
			blurred = "pass.lightshaft.frag1";
			"waves"
		}

		(output) {
			rendertarget = "target.dowaves";
			"frag1"
		}
	}

	(shader) {
		id = "pass.dowaves.shader";
		vertex = "../data/test/sxengine/files/ShadeX/dowaves.vp";
		fragment = "../data/test/sxengine/files/ShadeX/dowaves.fp";
	}

	(rendertarget) {
		id = "target.dowaves";
		width = 600;
		height = 400;
	}

	(texture) {
		id = "pass.dowaves.frag1";
		width = 600;
		height = 400;
		format = "byte";
	}

	(texture) {
		id = "pass.dowaves.waves";
		path = "../data/test/sxengine/files/ShadeX/waves.jpg";
	}

	/**
	 * rendertext pass
	 */

	(pass) {
		id = "pass.display";
		shader = "pass.display.shader";
		(floats) {
			"uselogo"
			"overlighten"
		}
		(textures) {
			scenetex = "pass.dowaves.frag1";
			"logotex"
		}
		(output) {
			rendertarget = "displaytarget";
		}
	}

	(shader) {
		id = "pass.display.shader";
		vertex = "../data/test/sxengine/files/ShadeX/display.vp";
		fragment = "../data/test/sxengine/files/ShadeX/display.fp";
	}

	(rendertarget) {
		id = "displaytarget";
		width = 600;
		height = 400;
		renderToDisplay = "true";
	}

	(texture) {
		id = "pass.display.logotex";
		path = "../data/test/sxengine/files/ShadeX/ShadeXEngine.jpg";
	}

}
