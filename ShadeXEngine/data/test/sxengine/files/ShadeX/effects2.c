(effects) {

	(mesh) {
		id = "process.mesh";
		path = "../data/test/sxengine/files/ShadeX/target.ply";
	}

	(object) {
		id = "process.object";
		mesh = "process.mesh";
		(passes) {
			
		}
	}

	//testing loading volumes from files
	//and rendering them

	(effect) {
		id = "showvolume.effect";
		(passes) {
			"showvolume.pass"
		}
	}

	(volume) {
		id = "showvolume.volume1";
		(paths) {
			"../data/test/sxengine/files/vol1.jpg"
			"../data/test/sxengine/files/vol2.jpg"
			"../data/test/sxengine/files/vol3.jpg"
			"../data/test/sxengine/files/vol4.jpg"
			"../data/test/sxengine/files/vol5.jpg"
			"../data/test/sxengine/files/vol6.jpg"
		}
	}

	(volume) {
		id = "showvolume.volume2";
		(paths) {
			"../data/test/sxengine/files/ShadeXEngine.jpg"
			"../data/test/sxengine/files/tex4.jpg"
			"../data/test/sxengine/files/background.jpg"
		}
	}

	(shader) {
		id = "showvolume.shader";
		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}
		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 frag1;
			uniform sampler3D vol1;
			uniform sampler3D vol2;
			uniform float i;

			void main() {
				frag1 = texture(vol1,vec3(vtc,0.083333 + 0.166666 * i))* (1 - clamp(i*0.1 - 0.2,0,1))
					+ texture(vol2,vec3(vtc,0.083333 + 0.166666 * i)) * clamp(i*0.1 - 0.2,0,1);
			}
		"}
	}

	(object) {
		id = "showvolume.object";
		mesh = "process.mesh";
		(volumes) {
			vol2 = "showvolume.volume2";
		}
		(passes) {
			"showvolume.pass"
		}
	}

	(rendertarget) {
		id = "showvolume.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(pass) {
		id = "showvolume.pass";
		shader = "showvolume.shader";
		(floats) {
			i = "showvolume.index";
		}
		(volumes) {
			vol1 = "showvolume.volume1";
		}
		(output) {
			rendertarget = "showvolume.target";
		}
	}

}
