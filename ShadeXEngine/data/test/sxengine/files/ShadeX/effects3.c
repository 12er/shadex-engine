(effects) {

	(mesh) {
		id = "process.mesh";
		path = "../data/test/sxengine/files/ShadeX/target.ply";
	}

	(object) {
		id = "process.object";
		mesh = "process.mesh";
		(passes) {
			"clean.pass"
			"rendertovolume.pass"
		}
	}

	(effect) {
		id = "rendertovolume.effect";
		(passes) {
			"clean.pass"
			"rendertovolume.pass"
			"showvolume.pass"
		}
	}

	//render to volume

	(volume) {
		id = "volume.source1";
		(paths) {
			"../data/test/sxengine/files/vol1.jpg"
			"../data/test/sxengine/files/vol2.jpg"
			"../data/test/sxengine/files/vol3.jpg"
			"../data/test/sxengine/files/vol4.jpg"
			"../data/test/sxengine/files/vol5.jpg"
			"../data/test/sxengine/files/vol6.jpg"
		}
	}

	(volume) {
		id = "clean.pass.vol1";
		width = 500;
		height = 400;
		depth = 6;
		format = "float16";
	}
	
	(volume) {
		id = "rendertovolume.pass.vol2";
		width = 500;
		height = 400;
		depth = 6;
		format = "float";
	}

	(shader) {
		id = "clean.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 vol2;
			out vec4 vol1;

			void main() {
				vol1 = vec4(0.5,0.5,1,1);
				vol2 = vec4(1,0.5,1,1);
			}
		"}
	}

	(shader) {
		id = "rendertovolume.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 vol2;
			out vec4 vol1;

			uniform float depthcoord;
			uniform sampler3D volsource;

			void main() {
				vec4 f1 = texture(volsource,vec3(vtc,depthcoord));
				vec4 f2 = texture(volsource,vec3(vtc,depthcoord + 0.16666));
				vol2 = vec4(f2.r,f2.g*0.5,f2.b,1);
				vol1 = f1;
			}
		"}

	}

	(rendertarget) {
		id = "clean.target";
		width = 500;
		height = 400;
	}

	(pass) {
		id = "clean.pass";
		shader = "clean.shader";
		(volumeoutput) {
			rendertarget = "clean.target";
			"vol1"
			vol2 = "rendertovolume.pass.vol2";
		}
	}

	(rendertarget) {
		id = "volume.target";
		width = 500;
		height = 400;
	}

	(float) {
		id = "rendertovolume.pass.depthcoord";
	}

	(pass) {
		id = "rendertovolume.pass";
		shader = "rendertovolume.shader";
		(volumes) {
			volsource = "volume.source1";
		}
		(volumeoutput) {
			rendertarget = "volume.target";
			(layercoordinate) {
				"depthcoord"
			}
			(startlayer) {
				"startlayer"
				value = 2;
			}
			(endlayer) {
				"endlayer"
				value = 4;
			}
			vol1 = "clean.pass.vol1";
			"vol2"
		}
	}

	//render to display

	(float) {
		id = "objects.coord1";
		value = 0.083333;
	}

	(float) {
		id = "objects.coord2";
		value = 0.25;
	}

	(float) {
		id = "objects.coord3";
		value = 0.416666;
	}

	(float) {
		id = "objects.coord4";
		value = 0.583333;
	}

	(float) {
		id = "objects.coord5";
		value = 0.75;
	}

	(float) {
		id = "objects.coord6";
		value = 0.916666;
	}

	(matrix) {
		id = "objects.transform1";
		(value) {
			"0.15"	"0"		"0"		"-0.8333"
			"0"		"0.4"	"0"		"0"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(matrix) {
		id = "objects.transform2";
		(value) {
			"0.15"	"0"		"0"		"-0.5"
			"0"		"0.4"	"0"		"0"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(matrix) {
		id = "objects.transform3";
		(value) {
			"0.15"	"0"		"0"		"-0.1666"
			"0"		"0.4"	"0"		"0"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(matrix) {
		id = "objects.transform4";
		(value) {
			"0.15"	"0"		"0"		"0.1666"
			"0"		"0.4"	"0"		"0"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(matrix) {
		id = "objects.transform5";
		(value) {
			"0.15"	"0"		"0"		"0.5"
			"0"		"0.4"	"0"		"0"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(matrix) {
		id = "objects.transform6";
		(value) {
			"0.15"	"0"		"0"		"0.8333"
			"0"		"0.4"	"0"		"0"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(matrix) {
		id = "objects.obj1.shift";
		(value) {
			"1"		"0"		"0"		"0"
			"0"		"1"		"0"		"0.5"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(float) {
		id = "objects.obj1.weight";
		value = 1;
	}

	(object) {
		id = "objects.obj1";
		mesh = "process.mesh";
		(floats) {
			weight = "objects.obj1.weight";
		}
		(matrices) {
			shift = "objects.obj1.shift";
		}
		(uniformset) {
			id = 1;
			(floats) {
				coord = "objects.coord1";
			}
			(matrices) {
				transform = "objects.transform1";
			}
		}
		(uniformset) {
			id = 2;
			(floats) {
				coord = "objects.coord2";
			}
			(matrices) {
				transform = "objects.transform2";
			}
		}
		(uniformset) {
			id = 3;
			(floats) {
				coord = "objects.coord3";
			}
			(matrices) {
				transform = "objects.transform3";
			}
		}
		(uniformset) {
			id = 4;
			(floats) {
				coord = "objects.coord4";
			}
			(matrices) {
				transform = "objects.transform4";
			}
		}
		(uniformset) {
			id = 5;
			(floats) {
				coord = "objects.coord5";
			}
			(matrices) {
				transform = "objects.transform5";
			}
		}
		(uniformset) {
			id = 6;
			(floats) {
				coord = "objects.coord6";
			}
			(matrices) {
				transform = "objects.transform6";
			}
		}
		(passes) {
			"showvolume.pass"
		}
	}

	(matrix) {
		id = "objects.obj2.shift";
		(value) {
			"1"		"0"		"0"		"0"
			"0"		"1"		"0"		"-0.5"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(float) {
		id = "objects.obj2.weight";
		value = 0;
	}

	(object) {
		id = "objects.obj2";
		mesh = "process.mesh";
		(matrices) {
			shift = "objects.obj2.shift";
		}
		(floats) {
			weight = "objects.obj2.weight";
		}
		(uniformset) {
			id = 1;
			(floats) {
				coord = "objects.coord1";
			}
			(matrices) {
				transform = "objects.transform1";
			}
		}
		(uniformset) {
			id = 2;
			(floats) {
				coord = "objects.coord2";
			}
			(matrices) {
				transform = "objects.transform2";
			}
		}
		(uniformset) {
			id = 3;
			(floats) {
				coord = "objects.coord3";
			}
			(matrices) {
				transform = "objects.transform3";
			}
		}
		(uniformset) {
			id = 4;
			(floats) {
				coord = "objects.coord4";
			}
			(matrices) {
				transform = "objects.transform4";
			}
		}
		(uniformset) {
			id = 5;
			(floats) {
				coord = "objects.coord5";
			}
			(matrices) {
				transform = "objects.transform5";
			}
		}
		(uniformset) {
			id = 6;
			(floats) {
				coord = "objects.coord6";
			}
			(matrices) {
				transform = "objects.transform6";
			}
		}
		(passes) {
			"showvolume.pass"
		}
	}

	(shader) {
		id = "showvolume.shader";

		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			uniform mat4 transform;
			uniform mat4 shift;

			void main() {
				gl_Position = shift * transform * vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 frag1;

			uniform float coord;
			uniform float weight;
			uniform sampler3D v1;
			uniform sampler3D v2;

			void main() {
				frag1 = texture(v1,vec3(vtc,coord))*weight + texture(v2,vec3(vtc,coord))*(1-weight);
			}
		"}

	}

	(rendertarget) {
		id = "showvolume.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(pass) {
		id = "showvolume.pass";
		shader = "showvolume.shader";
		(volumes) {
			v1 = "clean.pass.vol1";
			v2 = "rendertovolume.pass.vol2";
		}
		(output) {
			rendertarget = "showvolume.target";
		}
	}

}