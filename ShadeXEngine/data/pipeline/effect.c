/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */


/**
 * defines the structure of the rendering pipeline
 */
(effects) {

	//rendering smoke with lightshafts, casting volume "medium.volume"
	(effect) {
		id = "maineffect.smoke.medium1.lightshaft";

		(passes) {
			"skygroundcolor.pass"
			"reflectionscene"
			"refractionscene"
			"mainscene"
			"raycast2.medium1.pass"
			"getblackscene2"
			"getlightshafts"
			"postscene.smoke.lightshaft"
		}
	}

	//rendering smoke with lightshafts, casting volume "medium2.volume"
	(effect) {
		id = "maineffect.smoke.medium2.lightshaft";

		(passes) {
			"skygroundcolor.pass"
			"reflectionscene"
			"refractionscene"
			"mainscene"
			"raycast2.medium2.pass"
			"getblackscene2"
			"getlightshafts"
			"postscene.smoke.lightshaft"
		}
	}

	//rendering smoke, casting volume "medium.volume"
	(effect) {
		id = "maineffect.smoke.medium1";

		(passes) {
			"skygroundcolor.pass"
			"reflectionscene"
			"refractionscene"
			"mainscene"
			"raycast.medium1.pass"
			"postscene.smoke"
		}
	}

	//rendering smoke, casting volume "medium2.volume"
	(effect) {
		id = "maineffect.smoke.medium2";

		(passes) {
			"skygroundcolor.pass"
			"reflectionscene"
			"refractionscene"
			"mainscene"
			"raycast.medium2.pass"
			"postscene.smoke"
		}
	}

	(effect) {
		id = "maineffect.lightshaft";

		(passes) {
			"skygroundcolor.pass"
			"reflectionscene"
			"refractionscene"
			"mainscene"
			"getblackscene"
			"getlightshafts"
			"postscene.lightshaft"
		}
	}

	(effect) {
		id = "maineffect";

		(passes) {
			"skygroundcolor.pass"
			"reflectionscene"
			"refractionscene"
			"mainscene"
			"postscene"
		}
	}

	//rendering foundation of the scene

	(pass) {
		id = "mainscene";
		(floats) {
			"ambient"
			maxbrightness = "mainscene.maxbrightness";
			"waterheight"
			"abovewater"
			"isRefractionscene"
			"isReflectioscene"
			"daytime"
		}
		(vectors) {
			"cam.position"
			"cam.view"
			"cam.up"

			"lightdir"
			skygroundcolor = "skygroundcolor";
		}
		(textures) {
			"sky"
		}
		(matrices) {
			"viewprojection"
			maincam_vp = "mainscene.main.viewprojection";
			"view"
			"proj2world"
			"normalv"
		}
		(output) {
			rendertarget = "mainscene.display";
			"frag1"
			dmap = "mainscene.dmap";
		}
	}

	(float) {
		id = "mainscene.isRefractionscene";
		value = 0;
	}

	(float) {
		id = "mainscene.isReflectioscene";
		value = 0;
	}

	(texture) {
		id = "mainscene.sky";
		path = "../data/texture/skymap.bmp";
	}

	(rendertarget) {
		id = "mainscene.display";
		width = 1024;
		height = 800;
	}

	(texture) {
		id = "mainscene.dmap";
		width = 1024;
		height = 800;
		format = "float16";
	}

	(texture) {
		id = "mainscene.dmap2";
		width = 1024;
		height = 800;
		format = "float16";
	}

	(texture) {
		id = "mainscene.frag1";
		width = 1024;
		height = 800;
		format = "byte";
	}

	(texture) {
		id = "mainscene2.frag1";
		width = 1024;
		height = 800;
		format = "byte";
	}

	(texture) {
		id = "mainscene3.frag1";
		width = 1024;
		height = 800;
		format = "byte";
	}

	(texture) {
		id = "mainscene4.frag1";
		width = 1024;
		height = 800;
		format = "byte";
	}

	//rendering scene behind water to refraction map
	
	(pass) {
		id = "refractionscene";
		(floats) {
			ambient = "mainscene.ambient";
			maxbrightness = "mainscene.maxbrightness";
			waterheight = "mainscene.waterheight";
			abovewater = "mainscene.abovewater";
			"isRefractionscene"
			"isReflectioscene"
			daytime = "mainscene.daytime";
		}
		(vectors) {
			position = "mainscene.cam.position";
			view = "mainscene.cam.view";
			up = "mainscene.cam.up";

			lightdir = "mainscene.lightdir";
			skygroundcolor = "skygroundcolor";
		}
		(matrices) {
			viewprojection = "mainscene.viewprojection";
			maincam_vp = "mainscene.main.viewprojection";
			view = "mainscene.view";
			normalv = "mainscene.normalv";
		}
		(textures) {
			sky = "mainscene.sky";
		}
		(output) {
			rendertarget = "refractionscene.refrag";
			frag1 = "refractionscene.refractionmap";
		}
	}

	(float) {
		id = "refractionscene.isRefractionscene";
		value = 1;
	}

	(float) {
		id = "refractionscene.isReflectioscene";
		value = 0;
	}

	(rendertarget) {
		id = "refractionscene.refrag";
		width = 1024;
		height = 800;
	}

	(texture) {
		id = "refractionscene.refractionmap";
		width = 1024;
		height = 800;
		format = "float16";
	}

	//rendering scene before water to reflection map
	
	(pass) {
		id = "reflectionscene";
		(floats) {
			ambient = "mainscene.ambient";
			maxbrightness = "mainscene.maxbrightness";
			waterheight = "mainscene.waterheight";
			abovewater = "mainscene.abovewater";
			"isRefractionscene"
			"isReflectioscene"
			daytime = "mainscene.daytime";
		}
		(vectors) {
			position = "mainscene.cam.position";
			view = "mainscene.cam.view";
			up = "mainscene.cam.up";

			lightdir = "mainscene.lightdir";
			skygroundcolor = "skygroundcolor";
		}
		(matrices) {
			"viewprojection"
			maincam_vp = "mainscene.main.viewprojection";
			"view"
			"normalv"
		}
		(textures) {
			sky = "mainscene.sky";
		}
		(output) {
			rendertarget = "reflectionscene.reflect";
			frag1 = "reflectionscene.reflectionmap";
		}
	}

	(float) {
		id = "reflectionscene.isRefractionscene";
		value = 0;
	}

	(float) {
		id = "reflectionscene.isReflectioscene";
		value = 1;
	}

	(rendertarget) {
		id = "reflectionscene.reflect";
		width = 1024;
		height = 800;
	}

	(texture) {
		id = "reflectionscene.reflectionmap";
		width = 1024;
		height = 800;
		format = "byte";
	}

	//light shafts

	//black map for lightshafts
	(pass) {
		id = "getblackscene";
		backgroundQuad = "true";
		shader = "getblackscene.shader";
		(vectors) {
			eye = "mainscene.cam.position";
			skygroundcolor = "skygroundcolor";
		}
		(textures) {
			colormap = "mainscene.frag1";
			dmap = "mainscene.dmap";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene3.frag1";
		}
	}

	//black map for lightshafts with smoke
	(pass) {
		id = "getblackscene2";
		backgroundQuad = "true";
		shader = "getblackscene2.shader";
		(vectors) {
			eye = "mainscene.cam.position";
			skygroundcolor = "skygroundcolor";
			smokeboxpos = "smokeboxpos";
		}
		(matrices) {
			viewprojection = "mainscene.viewprojection";
		}
		(textures) {
			colormap = "mainscene4.frag1";
			dmap = "mainscene.dmap";
			smokemap = "mainscene.dmap2";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene3.frag1";
		}
	}

	(pass) {
		id = "getlightshafts";
		backgroundQuad = "true";
		shader = "getlightshafts.shader";
		(floats) {
			sunlocaction = "sunlocaction";
		}
		(vectors) {
			sunposition = "sunposition";
		}
		(textures) {
			colormap = "mainscene3.frag1";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene2.frag1";
		}
	}

	//passes rendering scene to display
	
	(pass) {
		id = "postscene";
		backgroundQuad = "true";
		shader = "postscene.shader";
		(floats) {
			abovewater = "mainscene.abovewater";
			"showlogo"
			"glow"
		}
		(vectors) {
			"wave1"
			"wave2"
		}
		(textures) {
			maininput = "mainscene.frag1";
			"waves"
			"shadexlogo"
		}
		(output) {
			rendertarget = "displaytarget";
		}
	}

	(pass) {
		id = "postscene.smoke";
		backgroundQuad = "true";
		shader = "postscene.shader";
		(floats) {
			abovewater = "mainscene.abovewater";
			showlogo = "postscene.showlogo";
			glow = "postscene.glow";
		}
		(vectors) {
			wave1 = "postscene.wave1";
			wave2 = "postscene.wave2";
		}
		(textures) {
			maininput = "mainscene2.frag1";
			waves = "postscene.waves";
			shadexlogo = "postscene.shadexlogo";
		}
		(output) {
			rendertarget = "displaytarget";
		}
	}

	(pass) {
		id = "postscene.smoke.lightshaft";
		backgroundQuad = "true";
		shader = "postscene3.shader";
		(floats) {
			abovewater = "mainscene.abovewater";
			showlogo = "postscene.showlogo";
			glow = "postscene.glow";
			blendlightshafts = "blendlightshafts";
			daynightlightshafts = "daynightlightshafts";
		}
		(vectors) {
			sunposition = "sunposition";
			wave1 = "postscene.wave1";
			wave2 = "postscene.wave2";
		}
		(textures) {
			colormap = "mainscene4.frag1";
			shafts = "mainscene2.frag1";
			waves = "postscene.waves";
			shadexlogo = "postscene.shadexlogo";
		}
		(output) {
			rendertarget = "displaytarget";
		}
	}

	(pass) {
		id = "postscene.lightshaft";
		backgroundQuad = "true";
		shader = "postscene3.shader";
		(floats) {
			abovewater = "mainscene.abovewater";
			showlogo = "postscene.showlogo";
			glow = "postscene.glow";
			blendlightshafts = "blendlightshafts";
			daynightlightshafts = "daynightlightshafts";
		}
		(vectors) {
			sunposition = "sunposition";
			wave1 = "postscene.wave1";
			wave2 = "postscene.wave2";
		}
		(textures) {
			colormap = "mainscene.frag1";
			shafts = "mainscene2.frag1";
			waves = "postscene.waves";
			shadexlogo = "postscene.shadexlogo";
		}
		(output) {
			rendertarget = "displaytarget";
		}
	}

	(rendertarget) {
		id = "displaytarget";
		width = 1024;
		height = 800;
		renderToDisplay = "true";
	}

	(texture) {
		id = "postscene.waves";
		path = "../data/texture/waves.jpg";
	}

	(texture) {
		id = "postscene.shadexlogo";
		width = 1000;
		height = 600;
		format = "float16";
	}

	(rendertarget) {
		id = "postscene.shadexlogo.target";
		width = 1000;
		height = 600;
	}

	(texture) {
		id = "SXengine.texture";
		path = "../data/texture/SXE.jpg";
	}

	(texture) {
		id = "SXengine.X.texture";
		path = "../data/texture/X.jpg";
	}

	(float) {
		id = "postscene.glow";
		value = 1;
	}

	//reading the color of the sky's color at the horizon
	//this color is used to blend distant objects with the sky
	(pass) {
		id = "skygroundcolor.pass";
		shader = "skygroundcolor.shader";
		geometryoutput = "skygroundcolor2.mesh";
	}

	//passes generating grass geometry
	(pass) {
		id = "gen.grass.pass";
		shader = "gen.grass.shader";
		geometryoutput = "genobj.grass2.mesh";
	}

	(pass) {
		id = "gen.grass2.pass";
		shader = "gen.grass2.shader";
		(floats) {
			waterheight = "mainscene.waterheight";
		}
		(matrices) {
			"model"
			projheighmap = "mainobj.terrain.projheightmap";
		}
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
			heightmapNormals = "mainobj.terrain.normals";
			terroverlay = "terroverlay.texture";
		}
		geometryoutput = "mainobj.grass.mesh";
	}

	// precomputes the terrain normals
	(effect) {
		id = "calcterrnormals";

		(passes) {
			"terrnormalsscene"
		}
	}

	(pass) {
		id = "terrnormalsscene";
		backgroundQuad = "true";
		shader = "terrnormalscene.shader";
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
		}
		(output) {
			rendertarget = "terrnormalsscene.target";
			frag1 = "mainobj.terrain.normals";
		}
	}

	(rendertarget) {
		id = "terrnormalsscene.target";
		width = 1024;
		height = 1024;
	}

	(texture) {
		id = "mainobj.terrain.normals";
		width = 1024;
		height = 1024;
		format = "float";
	}

	// precompute overlay of terrain
	// depending on height and gradient
	// a certain point on the terrain is covered by
	// grass, rock or sand

	(texture) {
		id = "terroverlay.texture";
		width = 1024;
		height = 1024;
		format = "byte";
	}

	(rendertarget) {
		id = "terroverlay.target";
		width = 1024;
		height = 1024;
	}

	(pass) {
		id = "calcterroverlay.pass";
		backgroundQuad = "true";
		shader = "calcterroverlay.shader";
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
			normalmap = "mainobj.terrain.normals";
		}
		(floats) {
			waterheight = "mainscene.waterheight";
		}
		(output) {
			rendertarget = "terroverlay.target";
			terroverlay = "terroverlay.texture";
		}
	}

	(effect) {
		id = "calcterroverlay.effect";
		(passes) {
			"calcterroverlay.pass"
		}
	}

	//calculate terrain geometry
	(object) {
		id = "terrain.generategeometry.object";
		
		mesh = "mainobj.terrain.grid";
		(passes) {
			"terrain.generategeometry.pass"
		}
	}

	(pass) {
		id = "terrain.generategeometry.pass";
		shader = "terrain.generategeometry.shader";
		(matrices) {
			model = "mainobj.terrain.model";
			projheightmap = "mainobj.terrain.projheightmap";
		}
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
			normalmap = "mainobj.terrain.normals";
		}
		geometryoutput = "mainobj.terrain2.grid";
	}

	//reading terrain height
	(pass) {
		id = "terrain.getheight.pass";
		shader = "terrain.getheight.shader";
		(matrices) {
			projheightmap = "mainobj.terrain.projheightmap";
		}
		(vectors) {
			worldpos = "terrain.getheight.inputpoint.vector";
		}
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
		}
		geometryoutput = "terrain.outputpoint.mesh";
	}

	//reading terrain heights
	(pass) {
		id = "terrain.getheights.pass";
		shader = "terrain.getheights.shader";
		(matrices) {
			projheightmap = "mainobj.terrain.projheightmap";
		}
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
		}
		geometryoutput = "terrain.terrainpoints.mesh";
	}

	//smoke

	(rendertarget) {
		id = "fluiddynamic.volume.target";
		width = 160;
		height = 160;
	}

	//smoke initialization
	(effect) {
		id = "fluiddynamic.init.effect";
		(passes) {
			"fluiddynamic.volume.init.pass"
			"fluiddynamic.medium.init.pass"
		}
	}

	(pass) {
		id = "fluiddynamic.volume.init.pass";
		backgroundQuad = "true";
		shader = "fluiddynamic.init.shader";
		(volumeoutput) {
			rendertarget = "fluiddynamic.volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			destination = "velocitypressure.volume";
		}
	}

	(pass) {
		id = "fluiddynamic.medium.init.pass";
		backgroundQuad = "true";
		shader = "fluiddynamic.init.shader";
		(volumeoutput) {
			rendertarget = "fluiddynamic.volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			destination = "medium1.volume";
		}
	}

	//computation of smoke dynamics

	//invoced on even frames for smoke dynamics computation
	(effect) {
		id = "fluiddynamic1.effect";
		(passes) {
			"fluiddynamic.advect1.pass"
			"fluiddynamic.poisson.pass"
			"fluiddynamic.project.pass"
			"identifycastbox.pass"
		}
	}

	//invoced on odd frames for smoke dynamics computation
	(effect) {
		id = "fluiddynamic2.effect";
		(passes) {
			"fluiddynamic.advect2.pass"
			"fluiddynamic.poisson.pass"
			"fluiddynamic.project.pass"
			"identifycastbox.pass"
		}
	}

	//advection on even frames
	(pass) {
		id = "fluiddynamic.advect1.pass";
		backgroundQuad = "true";
		shader = "fluiddynamic.advect.shader";
		(volumes) {
			velpress = "velocitypressure.volume";
			medium = "medium1.volume";
		}
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
			emitsmoke = "fluiddynamic.emitsmoke";
			decay = "fluiddynamic.decay";
		}
		(vectors) {
			randacceleration = "fluiddynamic.randacceleration";
			p1 = "smoke.p1";
			p2 = "smoke.p2";
			p3 = "smoke.p3";
			p4 = "smoke.p4";
			p5 = "smoke.p5";
			v1 = "smoke.v1";
			v2 = "smoke.v2";
			v3 = "smoke.v3";
			v4 = "smoke.v4";
			v5 = "smoke.v5";
		}
		(volumeoutput) {
			rendertarget = "fluiddynamic.volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure2.volume";
			medium2 = "medium2.volume";
		}
	}

	//advection on odd frames
	(pass) {
		id = "fluiddynamic.advect2.pass";
		backgroundQuad = "true";
		shader = "fluiddynamic.advect.shader";
		(volumes) {
			velpress = "velocitypressure.volume";
			medium = "medium2.volume";
		}
		(floats) {
			dx = "dx.float";
			deltatime = "deltatime.float";
			emitsmoke = "fluiddynamic.emitsmoke";
			decay = "fluiddynamic.decay";
		}
		(vectors) {
			randacceleration = "fluiddynamic.randacceleration";
			p1 = "smoke.p1";
			p2 = "smoke.p2";
			p3 = "smoke.p3";
			p4 = "smoke.p4";
			p5 = "smoke.p5";
			v1 = "smoke.v1";
			v2 = "smoke.v2";
			v3 = "smoke.v3";
			v4 = "smoke.v4";
			v5 = "smoke.v5";
		}
		(volumeoutput) {
			rendertarget = "fluiddynamic.volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure2.volume";
			medium2 = "medium1.volume";
		}
	}

	//solving the poisson equation
	(pass) {
		id = "fluiddynamic.poisson.pass";
		backgroundQuad = "true";
		shader = "fluiddynamic.poisson.shader";
		(volumes) {
			velpress = "velocitypressure2.volume";
		}
		(floats) {
			dx = "dx.float";
		}
		(volumeoutput) {
			rendertarget = "fluiddynamic.volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure3.volume";
		}
	}

	//making the smoke divergence free
	(pass) {
		id = "fluiddynamic.project.pass";
		backgroundQuad = "true";
		shader = "fluiddynamic.project.shader";
		(volumes) {
			velpress = "velocitypressure3.volume";
		}
		(floats) {
			dx = "dx.float";
		}
		(volumeoutput) {
			rendertarget = "fluiddynamic.volume.target";
			(layercoordinate) {
				depthcoordinate = "layercoordinate";
			}
			velpress2 = "velocitypressure.volume";
		}
	}

	//visualize smoke

	(texture) {
		id = "identifycastbox.texture";
		width = 1024;
		height = 800;
		format = "float16";
	}

	(rendertarget) {
		id = "identifycastbox.target";
		width = 1024;
		height = 800;
	}

	(pass) {
		id = "identifycastbox.pass";
		(matrices) {
			viewprojection = "mainscene.viewprojection";
			box2tex = "castbox.box2tex";
			world2tex = "castbox.world2tex";
		}
		(vectors) {
			eye = "mainscene.cam.position";
		}
		(output) {
			rendertarget = "identifycastbox.target";
			frag1 = "identifycastbox.texture";
		}
	}

	//casting "medium1.volume", rendering to "mainscene2.frag1"
	(pass) {
		id = "raycast.medium1.pass";
		shader = "raycast.shader";
		backgroundQuad = "true";
		(matrices) {
			invmodel = "castbox.invtransform";
			box2tex = "castbox.box2tex";
			world2tex = "castbox.world2tex";
			proj2world = "mainscene.proj2world";
			viewprojection = "mainscene.viewprojection";
		}
		(vectors) {
			eye = "mainscene.cam.position";
			lightdir = "mainscene.lightdir";
			viewvector = "mainscene.cam.view";
			smokeboxpos = "smokeboxpos";
			skygroundcolor = "skygroundcolor";
		}
		(floats) {
			insidecastbox = "castbox.insidecastbox";
			maxbrightness = "mainscene.maxbrightness";
		}
		(textures) {
			raycaststencil = "identifycastbox.texture";
			dmap = "mainscene.dmap";
			backgroundscene = "mainscene.frag1";
		}
		(volumes) {
			medium = "medium1.volume";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene2.frag1";
			dmapout = "mainscene.dmap2";
		}
	}

	//casting "medium2.volume", rendering to "mainscene2.frag1"
	(pass) {
		id = "raycast.medium2.pass";
		shader = "raycast.shader";
		backgroundQuad = "true";
		(matrices) {
			invmodel = "castbox.invtransform";
			box2tex = "castbox.box2tex";
			world2tex = "castbox.world2tex";
			proj2world = "mainscene.proj2world";
			viewprojection = "mainscene.viewprojection";
		}
		(vectors) {
			eye = "mainscene.cam.position";
			lightdir = "mainscene.lightdir";
			viewvector = "mainscene.cam.view";
			smokeboxpos = "smokeboxpos";
			skygroundcolor = "skygroundcolor";
		}
		(floats) {
			insidecastbox = "castbox.insidecastbox";
			maxbrightness = "mainscene.maxbrightness";
		}
		(textures) {
			raycaststencil = "identifycastbox.texture";
			dmap = "mainscene.dmap";
			backgroundscene = "mainscene.frag1";
		}
		(volumes) {
			medium = "medium2.volume";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene2.frag1";
			dmapout = "mainscene.dmap2";
		}
	}

	//casting "medium1.volume", rendering to "mainscene4.frag1"
	(pass) {
		id = "raycast2.medium1.pass";
		shader = "raycast.shader";
		backgroundQuad = "true";
		(matrices) {
			invmodel = "castbox.invtransform";
			box2tex = "castbox.box2tex";
			world2tex = "castbox.world2tex";
			proj2world = "mainscene.proj2world";
			viewprojection = "mainscene.viewprojection";
		}
		(vectors) {
			eye = "mainscene.cam.position";
			lightdir = "mainscene.lightdir";
			viewvector = "mainscene.cam.view";
			smokeboxpos = "smokeboxpos";
			skygroundcolor = "skygroundcolor";
		}
		(floats) {
			insidecastbox = "castbox.insidecastbox";
			maxbrightness = "mainscene.maxbrightness";
		}
		(textures) {
			raycaststencil = "identifycastbox.texture";
			dmap = "mainscene.dmap";
			backgroundscene = "mainscene.frag1";
		}
		(volumes) {
			medium = "medium1.volume";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene4.frag1";
			dmapout = "mainscene.dmap2";
		}
	}

	//casting "medium2.volume", rendering to "mainscene4.frag1"
	(pass) {
		id = "raycast2.medium2.pass";
		shader = "raycast.shader";
		backgroundQuad = "true";
		(matrices) {
			invmodel = "castbox.invtransform";
			box2tex = "castbox.box2tex";
			world2tex = "castbox.world2tex";
			proj2world = "mainscene.proj2world";
			viewprojection = "mainscene.viewprojection";
		}
		(vectors) {
			eye = "mainscene.cam.position";
			lightdir = "mainscene.lightdir";
			viewvector = "mainscene.cam.view";
			smokeboxpos = "smokeboxpos";
			skygroundcolor = "skygroundcolor";
		}
		(floats) {
			insidecastbox = "castbox.insidecastbox";
			maxbrightness = "mainscene.maxbrightness";
		}
		(textures) {
			raycaststencil = "identifycastbox.texture";
			dmap = "mainscene.dmap";
			backgroundscene = "mainscene.frag1";
		}
		(volumes) {
			medium = "medium2.volume";
		}
		(output) {
			rendertarget = "mainscene.display";
			frag1 = "mainscene4.frag1";
			dmapout = "mainscene.dmap2";
		}
	}

	//shadex engine logo animation

	(pass) {
		id = "SXengine.pass";
		backgroundQuad = "true";
		shader = "SXengine.shader";
		(floats) {
			time = "SXengine.time";
			logo = "SXengine.logo";
		}
		(matrices) {
			SXengineCoordTransform = "SXengineCoord.transform";
		}
		(textures) {
			shadeXEngineTexture = "SXengine.texture";
			Xtexture = "SXengine.X.texture";
		}
		(output) {
			rendertarget = "postscene.shadexlogo.target";
			frag1 = "postscene.shadexlogo";
		}
	}

}
