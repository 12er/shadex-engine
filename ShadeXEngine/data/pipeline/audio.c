/**
 * ShadeX Engine Demo
 * (c) 2014 by Tristan Bauer
 */

(audio) {

	(float) {
		id = "backgroundmusic.volume";
		value = 1;
	}

	(audiobuffer) {
		id = "backgroundmusic.buffer";
		path = "../data/audio/empty.wav";
	}

	(audioobject) {
		id = "backgroundmusic.object";
		volume = "backgroundmusic.volume";
		buffer = "backgroundmusic.buffer";
		looping = "true";
	}

	(audiopass) {
		id = "backgroundmusic.pass";
		(objects) {
			"backgroundmusic.object"
		}
	}

}