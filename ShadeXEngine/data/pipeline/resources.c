/**
 * resource allocation
 */
(resources) {

	(float) {
		id = "deltatime.float";
	}

	(vector) {
		id = "mainscene.lightdir";
		(value) {"-1 -1 -1"}
	}

	(float) {
		id = "mainscene.ambient";
		value = 0.25;
	}

	(float) {
		id = "mainscene.maxbrightness";
		value = 1;
	}

	(float) {
		id = "mainscene.daytime";
		value = 0.5;
	}

	(float) {
		id = "mainscene.waterheight";
		value = 2.5;
	}

	//sky
	(object) {
		id = "mainobj.main.sky";
		mesh = "mainobj.sky.mesh";
		shader = "mainobj.main.sky.shader";
		(matrices) {
			model = "mainobj.sky.model";
		}
		(textures) {
			skymap = "mainscene.sky";
		}
		(passes) {
			"mainscene"
		}
	}

	(object) {
		id = "mainobj.sky";
		mesh = "mainobj.sky.mesh";
		shader = "mainobj.sky.shader";
		(matrices) {
			"model"
		}
		(textures) {
			skymap = "mainscene.sky";
		}
		(passes) {
			"refractionscene"
			"reflectionscene"
		}
	}

	(mesh) {
		id = "mainobj.sky.mesh";
		path = "../data/mesh/sparcedsphere.ply";
	}

	//terrain
	
	(object) {
		id = "mainobj.main.terrain";
		mesh = "mainobj.terrain2.grid";
		shader = "mainobj.main.terrain.shader";
		cullmode = "ccw";
		(matrices) {
			model = "mainobj.terrain.model";
			projheightmap = "mainobj.terrain.projheightmap";
		}
		(textures) {
			heightmap = "mainobj.terrain.heightmap";
			normalmap = "mainobj.terrain.normals";
			finemap = "mainobj.terrain.finemap";
			terroverlay = "terroverlay.texture";
			grass = "grassground.texture";
			rock = "rockground.texture";
			rock_heightmap = "rockground.heightmap";
			rock_normalmap = "rockground.normalmap";
		}
		(passes) {
			"mainscene"
		}
	}

	(object) {
		id = "mainobj.terrain";
		mesh = "mainobj.terrain2.grid";
		shader = "mainobj.terrain.shader";
		cullmode = "ccw";
		(matrices) {
			"model"
			"projheightmap"
		}
		(textures) {
			"heightmap"
			normalmap = "mainobj.terrain.normals";
			"finemap"
			terroverlay = "terroverlay.texture";
			grass = "grassground.texture";
			rock = "rockground.texture";
			rock_heightmap = "rockground.heightmap";
			rock_normalmap = "rockground.normalmap";
		}
		(passes) {
			"refractionscene"
			"reflectionscene"
		}
	}

	(mesh) {
		id = "mainobj.terrain.grid";
		path = "../data/mesh/grid.ply";
	}

	(mesh) {
		id = "mainobj.terrain2.grid";
		faceSize = 3;
		maxVertexCount = 536406;
		(attribute) {
			name = "vertices";
			outputName = "overtices";
			attributeSize = 3;
		}
		(attribute) {
			name = "texcoords";
			outputName = "otexcoords";
			attributeSize = 2;
		}
		(attribute) {
			name = "normals";
			outputName = "onormals";
			attributeSize = 3;
		}
	}

	(texture) {
		id = "mainobj.terrain.heightmap";
		path = "../data/texture/Terrain.png";
	}

	(texture) {
		id = "mainobj.terrain.finemap";
		path = "../data/texture/finemap.jpg";
	}

	(texture) {
		id = "grassground.texture";
		path = "../data/texture/grass.png";
	}

	(texture) {
		id = "rockground.texture";
		path = "../data/texture/rock.png";
	}

	(texture) {
		id = "rockground.heightmap";
		path = "../data/texture/rock_heightmap.png";
	}

	(texture) {
		id = "rockground.normalmap";
		path = "../data/texture/rock_normalmap.png";
	}

	//reading terrain height

	(object) {
		id = "terrain.getheight.inputpoint.obj";
		mesh = "terrain.getheight.inputpoint.mesh";
		(passes) {
			"terrain.getheight.pass"
		}
	}

	(mesh) {
		id = "terrain.getheight.inputpoint.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "positions";
			outputName = "opositions";
			attributeSize = 3;
			(data) {"0 0 0"}
		}
	}

	(mesh) {
		id = "terrain.outputpoint.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "positions";
			outputName = "opositions";
			attributeSize = 3;
			(data) {"0 0 0"}
		}
	}

	//reading terrain heights at multiple locations at once

	(object) {
		id = "terrain.getheights.inputpoints.obj";
		mesh = "terrain.inputpoints.mesh";
		(passes) {
			"terrain.getheights.pass"
		}
	}

	(mesh) {
		id = "terrain.inputpoints.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "positions";
			outputName = "opositions";
			attributeSize = 3;
			(data) {"0 0 0"}
		}
	}

	(mesh) {
		id = "terrain.terrainpoints.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "positions";
			outputName = "opositions";
			attributeSize = 3;
			(data) {"0 0 0"}
		}
	}
	

	//grass generation

	//vertex count = 10509
	(mesh) {
		id = "genobj.grass.mesh";
		path = "../data/mesh/grass.ply";
		(attribute) {
			name = "terrainNormal";
			outputName = "oterrainNormal";
			attributeSize = 3;
		}
		(attribute) {
			name = "midposition";
			outputName = "omidposition";
			attributeSize = 4;
		}
	}

	(object) {
		id = "genobj.grass.object";
		mesh = "genobj.grass.mesh";
		(passes) {
			"gen.grass.pass"
		}
	}

	//vertex count = 10509 * 9 = 94581
	(mesh) {
		id = "genobj.grass2.mesh";
		faceSize = 3;
		maxVertexCount = 94581;
		(attribute) {
			name = "vertices";
			outputName = "overtices";
			attributeSize = 3;
		}
		(attribute) {
			name = "texcoords";
			outputName = "otexcoords";
			attributeSize = 2;
		}
		(attribute) {
			name = "terrainNormal";
			outputName = "oterrainNormal";
			attributeSize = 3;
		}
		(attribute) {
			name = "midposition";
			outputName = "omidposition";
			attributeSize = 4;
		}
	}

	(object) {
		id = "genobj.grass2.object";
		mesh = "genobj.grass2.mesh";
		(passes) {
			"gen.grass2.pass"
		}
	}

	//vertex count = 10509 * 9 * 9 = 851229
	(mesh) {
		id = "mainobj.grass.mesh";
		faceSize = 3;
		maxVertexCount = 851229;
		(attribute) {
			name = "vertices";
			outputName = "overtices";
			attributeSize = 3;
		}
		(attribute) {
			name = "texcoords";
			outputName = "otexcoords";
			attributeSize = 2;
		}
		(attribute) {
			name = "terrainNormal";
			outputName = "oterrainNormal";
			attributeSize = 3;
		}
		(attribute) {
			name = "midposition";
			outputName = "omidposition";
			attributeSize = 4;
		}
	}

	//render grass

	(texture) {
		id = "grassform.texture";
		path = "../data/texture/grassthickness.png";
	}

	(object) {
		id = "mainobj.grass";
		mesh = "mainobj.grass.mesh";
		shader = "mainobj.grass.shader";
		(textures) {
			grassform = "grassform.texture";
			waves = "postscene.waves";
		}
		(vectors) {
			campos = "mainscene.cam.position";
			"wave1"
			"wave2"
		}
		(matrices) {
			"world2tex"
		}
		(passes) {
			"mainscene"
		}
	}

	(matrix) {
		id = "mainobj.grass.world2tex";
		(value) {
			"0.1 0 0 0
			0 0.1 0 0
			0 0 0.1 0
			0 0 0 1"
		}
	}

	//water
	
	(object) {
		id = "mainobj.water";
		mesh = "mainobj.water.grid";
		shader = "mainobj.water.shader";
		(vectors) {
			wave1 = "postscene.wave1";
			wave2 = "postscene.wave2";
		}
		(matrices) {
			"model"
			water_vp = "reflectionscene.viewprojection";
			"world2tex"
		}
		(textures) {
			refractionmap = "refractionscene.refractionmap";
			reflectionmap = "reflectionscene.reflectionmap";
			waves = "postscene.waves";
		}
		(passes) {
			"mainscene"
		}
	}

	(mesh) {
		id = "mainobj.water.grid";
		path = "../data/mesh/sparcegrid.ply";
	}

	(matrix) {
		id = "mainobj.water.world2tex";
		(value) {
			"0.1 0 0 0
			0 0.1 0 0
			0 0 0.1 0
			0 0 0 1"
		}
	}

	//firefly
	
	(object) {
		id = "mainobj.main.firefly";
		mesh = "mainobj.firefly.mesh";
		shader = "mainobj.main.firefly.shader";
		(textures) {
			tex = "mainobj.firefly.tex";
			normalmap = "mainobj.firefly.normalmap";
		}
		(passes) {
			"mainscene"
		}
	}

	(object) {
		id = "mainobj.firefly";
		mesh = "mainobj.firefly.mesh";
		shader = "mainobj.firefly.shader";
		(textures) {
			"tex"
			"normalmap"
		}
		(passes) {
			"refractionscene"
			"reflectionscene"
		}
	}

	(mesh) {
		id = "mainobj.firefly.mesh";
		path = "../data/mesh/firefly.ply";
	}

	(texture) {
		id = "mainobj.firefly.tex";
		path = "../data/texture/firefly.jpg";
	}

	(texture) {
		id = "mainobj.firefly.normalmap";
		path = "../data/texture/firefly_normalmap.jpg";
	}

	//fluid solver for smoke

	(float) {
		id = "dx.float";
		value = 0.01;
	}

	(float) {
		id = "castbox.insidecastbox";
		value = 0;
	}

	(float) {
		id = "fluiddynamic.emitsmoke";
		value = 1;
	}

	(float) {
		id = "fluiddynamic.decay";
		value = 0.02;
	}

	(vector) {
		id = "fluiddynamic.randacceleration";
		(value) {"0 0 4"}
	}

	(matrix) {
		id = "castbox.transform";
		(value) {
			(scale) {"1 1 1"}
		}
	}

	(matrix) {
		id = "castbox.invtransform";
		(value) {
			(scale) {"1 1 1"}
		}
	}

	(matrix) {
		id = "castbox.box2tex";
		(value) {
			(scale) {"1 1 1"}
		}
	}

	(matrix) {
		id = "castbox.world2tex";
		(value) {
			(scale) {"1 1 1"}
		}
	}

	(matrix) {
		id = "castbox.tex2world";
		(value) {
			(scale) {"1 1 1"}
		}
	}

	(volume) {
		id = "medium1.volume";
		width = 160;
		height = 160;
		depth = 100;
		format = "float16";
	}

	(volume) {
		id = "medium2.volume";
		width = 160;
		height = 160;
		depth = 100;
		format = "float16";
	}

	(volume) {
		id = "velocitypressure.volume";
		width = 160;
		height = 160;
		depth = 100;
		format = "float16";
	}

	(volume) {
		id = "velocitypressure2.volume";
		width = 160;
		height = 160;
		depth = 100;
		format = "float16";
	}

	(volume) {
		id = "velocitypressure3.volume";
		width = 160;
		height = 160;
		depth = 100;
		format = "float16";
	}

	//visualize smoke

	//castbox for identifying ray casting fragments (the fluid is bordered by this box)

	(mesh) {
		id = "fluiddynamic.castbox.mesh";
		faceSize = 3;
		maxVertexCount = 36;
		(attribute) {
			name = "vertices";
			outputName = "overtices";
			attributeSize = 3;
			(data) {"
				-1 -1 -1	1 1 -1		1 -1 -1		-1 -1 -1	-1 1 -1		1 1 -1
				1 -1 -1		1 1 1		1 -1 1		1 -1 -1		1 1 -1		1 1 1
				1  1 -1		-1 1 -1		-1 1 1		1 1 -1		-1 1 1		1 1 1
				-1 1 -1		-1 -1 -1	-1 -1 1		-1 1 -1		-1 -1 1		-1 1 1
				-1 -1 -1	1 -1 -1		1 -1 1		-1 -1 -1	1 -1 1		-1 -1 1
				-1 -1 1		1 -1 1		1 1 1		-1 -1 1		1 1 1		-1 1 1
				"
			}
		}
	}

	(object) {
		id = "castbox.object";
		shader = "identifycastbox.shader";
		mesh = "fluiddynamic.castbox.mesh";
		(matrices) {
			model = "castbox.transform";
			invmodel = "castbox.invtransform";
		}
		(passes) {
			"identifycastbox.pass"
		}
	}

	//read sky ground color

	(vector) {
		id = "skygroundcolor";
	}

	(mesh) {
		id = "skygroundcolor.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "positions";
			outputName = "opositions";
			attributeSize = 3;
			(data) {"0 0 0"}
		}
	}

	(mesh) {
		id = "skygroundcolor2.mesh";
		faceSize = 1;
		maxVertexCount = 1;
		(attribute) {
			name = "positions";
			outputName = "opositions";
			attributeSize = 3;
			(data) {"0 0 0"}
		}
	}

	(object) {
		id = "skygroundcolor.object";
		mesh = "skygroundcolor.mesh";
		(floats) {
			daytime = "mainscene.daytime";
		}
		(textures) {
			skymap = "mainscene.sky";
		}
		(passes) {
			"skygroundcolor.pass"
		}
	}

}
