(SXlogo) {

	(shader) {
		id = "SXengine.shader";

		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			uniform mat4 SXengineCoordTransform;

			void main()
			{
				vtexcoords = ( SXengineCoordTransform * vec4(texcoords,0,1) ).xy;
				gl_Position = vec4(vertices,1);
			}
			"
		}
		(fragment) {"
			#version 400 core
			/**
			 * logo of ShadeX Engine
			 */

			in vec2 vtexcoords;
			out vec4 frag1;

			/**
			 * texture of logo ShadeX Engine
			 */
			uniform sampler2D shadeXEngineTexture;

			/**
			 * texture of the X in ShadeX Engine
			 */
			uniform sampler2D Xtexture;

			/**
			 * time in seconds
			 */
			uniform float time;

			/**
			 * 0.0 iff logo is not rendered
			 */
			uniform float logo;

			/**
			 * Renders a logo animation, if logo is 0.0;
			 * shows a logo picture otherwise. Time describes
			 * the point of time in the animation in seconds.
			 * texcoord describes the location on the screen.
			 * The two textures Xtexture and shadeXEngineTexture
			 * are shown.
			 */
			void main() {
				if(time < 1.0) {
					//from second 0 to 1:
					//split logo in two halfes, and move them together
				
					float count = 200.0;
					float weight = 1.0;
					float decay = 0.98*clamp((exp(time)/1.25-1.0),0.0,1.0);
					float ftime = time*3.0;
					vec2 middle = vec2(0.479,0.51) + vec2(0.002,0.002);
					vec2 delta = (middle-vtexcoords)/count;
					
					vec2 tc = vtexcoords;
					vec4 color = vec4(0.0);
					if(vtexcoords.x < 0.523) {
						tc = tc + vec2(0.5,0.0)*(1.0-time);
						if(tc.x >= 0.523) {
							color = vec4(0.0);
						} else {
							color = clamp(texture2D(shadeXEngineTexture,clamp(tc,0,1))*2.0,0.0,1.0);
						}
					} else {
						tc = tc + vec2(-0.5,0.0)*(1.0-time);
						if(tc.x < 0.523) {
							color = vec4(0.0);
						} else {
							color = clamp(texture2D(shadeXEngineTexture,clamp(tc,0,1))*2.0,0.0,1.0);
						}
					}
					
					frag1 = color;
				} else if(logo == 0.0 || (time < (1.0 + 3.14159*2.5))) {
					//show no logo (logo == 0.0): from second 1 to infinity:
					//let the X in the logo glow by using a radial blur.
					//the center of the radial blur is rotating, and the
					//size of the rays is varying
					//
					//show logo (logo != 0.0): from second 1.0 to 1.0+3.14159*2.5:
					//let the X in the logo glow by using a radial blur.
					//the center of the radial blur is rotating, and the
					//size of the rays is varying 
				
					float etime = time - 1.0;
					float count = 200.0;
					float weight = 1.0;
					float decay = 0.98*clamp(1.0-1.0/exp(etime*10.0),0.0,1.0) + sin(etime)*0.01;
					float ftime = etime*3.0;
					float centerdistance = 1.0;
					if(logo != 0.0) {
						centerdistance = 1.0 - clamp((etime-3.14159*2.0)*0.5,0.0,1.0);
					}
					vec2 middle = vec2(0.479,0.51) + mat2(cos(ftime),sin(ftime),-sin(ftime),cos(ftime))*vec2(0.002,0.002)*centerdistance;
					vec2 delta = (middle-vtexcoords)/count;
					
					vec4 color = vec4(0.0);
					vec2 samplepoint = vtexcoords;
					float sumweight = 2.0*(decay-1.0)/(pow(decay,count+1.0)-1.0);
					for(float c = 0.0; c<=count ; c = c + 1.0) {
						color = color + texture2D(Xtexture,clamp(samplepoint,0,1))*weight*sumweight;
						weight = weight * decay;
						samplepoint = samplepoint + delta;
					}
					
					vec4 color2 = texture2D(shadeXEngineTexture,clamp(vtexcoords,0,1));
					frag1 = max(color,color2);
				} else {
					//show logo (logo != 0.0): from second 1.0+3.14159*2.5:
					//let the X in the logo glow by using a radial blur.
					//each rendered frame looks equal to the other frames.
				
					float count = 200.0;
					float weight = 1.0;
					float decay = 0.99;
					vec2 middle = vec2(0.479,0.51);
					vec2 delta = (middle-vtexcoords)/count;
					
					vec4 color = vec4(0.0);
					vec2 samplepoint = vtexcoords;
					float sumweight = 2.0*(decay-1.0)/(pow(decay,count+1.0)-1.0);
					for(float c = 0.0; c<=count ; c = c + 1.0) {
						color = color + texture2D(Xtexture,clamp(samplepoint,0,1))*weight*sumweight;
						weight = weight * decay;
						samplepoint = samplepoint + delta;
					}
					
					vec4 color2 = texture2D(shadeXEngineTexture,clamp(vtexcoords,0,1));
					frag1 = max(color,color2);
				}
			}
			"
		}
	}

}