(sky) {

	//reading the color of the sky's color at the horizon
	//this color is used to blend distant objects with the sky
	(shader) {
		id = "skygroundcolor.shader";
		(transformfeedback) {
			"opositions"
		}
		(vertex) {"
			#version 400 core
			in vec3 positions;
			out vec3 opositions;

			uniform sampler2D skymap;
			uniform float daytime;

			void main() {
				opositions = texture(skymap,vec2(daytime,0.0)).rgb;
			}
			"
		}
	}

	//render sky
	(shader) {
		id = "mainobj.sky.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			out vec4 world;
			out vec2 coords;
			out float projz;

			uniform mat4 model;
			uniform mat4 viewprojection;

			void main() {
				coords = vertices.xy;
				world = model * vec4(vertices,1);
				vec4 proj = viewprojection * world;
				projz = proj.z;
				gl_Position = proj;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			in vec4 world;
			in float projz;
			out vec4 frag1;

			uniform vec4 lightdir;
			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;
			uniform float abovewater;
			uniform vec4 position;
			uniform float daytime;
			uniform sampler2D skymap;

			void main() {
				if(isRefractionscene > 0.5 || isReflectioscene > 0.5) {
					bool cancelFrag = (world.z > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectioscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}
				vec3 fragdir = normalize(world.xyz - position.xyz);
				float height = max(0,dot(fragdir,vec3(0,0,1)) * 0.75);
				frag1 = texture2D(skymap,vec2(daytime,height));
				frag1.a = projz;
			}
			"
		}

	}

	(shader) {
		id = "mainobj.main.sky.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			out vec4 world;
			out vec2 coords;
			out vec3 dir;
			out float projz;

			uniform mat4 model;
			uniform mat4 viewprojection;

			void main() {
				coords = vertices.xy;
				dir = vertices.xyz;
				world = model * vec4(vertices,1);
				vec4 ppoint = viewprojection * world;
				projz = ppoint.z;
				gl_Position = ppoint;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			in vec4 world;
			in float projz;
			in vec3 dir;
			out vec4 frag1;
			out vec4 dmap;

			uniform vec4 lightdir;
			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;
			uniform float abovewater;
			uniform vec4 position;
			uniform float daytime;
			uniform sampler2D skymap;

			void main() {
				if(isRefractionscene > 0.5 || isReflectioscene > 0.5) {
					bool cancelFrag = (world.z > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectioscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}
				

				vec3 fragdir = normalize(world.xyz - position.xyz);
				float height = max(0,dot(fragdir,vec3(0,0,1)) * 0.75);

				vec3 vdir = normalize(dir.xyz);
				float hidefactor = clamp(height*100.0,0.0,1.0);
				float sunmetric = clamp(dot(vdir , -lightdir.xyz),0,1);
				float sunfactor = 0.0;
				float glowfactor = 0.0;
				float glowfactor2 = 0.0;
				if(sunmetric>0.99875)
				{
					sunfactor = 1.0;
				}
				else if(sunmetric>0.998)
				{
					sunfactor = (sunmetric - 0.998)*((1.0-0.8)/(0.99875-0.998)) + 0.8;
				}
				else if(sunmetric>0.995)
				{
					sunfactor = (sunmetric - 0.995)*(0.8/(0.998-0.995));
				}
				else
				{
					sunfactor = 0.0;
				}
				sunfactor = sunfactor * hidefactor;
				if(sunmetric>0.99875)
				{
					glowfactor =  pow((1.0-height),3.0);
				}
				else if(sunmetric>0.8)
				{
					glowfactor = (sunmetric - 0.8)*(1.0/(0.99875-0.8)) * pow((1.0-height),3.0);
				}
				else
				{
					glowfactor = 0.0;
				}
				if(sunmetric>0.99875)
				{
					glowfactor2 =  0.75*pow((1.0-height),4.0);
				}
				else if(sunmetric>0.99)
				{
					glowfactor2 = (sunmetric - 0.99)*(0.75/(0.99875-0.99)) * pow((1.0-height),4.0);
				}
				else
				{
					glowfactor2 = 0.0;
				}
				glowfactor = glowfactor*hidefactor;
				glowfactor2 = glowfactor2*hidefactor;

				frag1 = vec4(max(max(texture2D(skymap,vec2(daytime,height)) , vec4(sunfactor,sunfactor,sunfactor,0)),vec4(glowfactor,glowfactor2 + 0.25*glowfactor,0,0)).rgb,1.0);
				dmap = vec4(projz,world.xyz);
			}
			"
		}

	}
}