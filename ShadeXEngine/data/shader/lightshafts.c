(lightshafts) {

	//calculate black map for light shafts
	(shader) {
		id = "getblackscene.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			out vec4 frag1;

			uniform sampler2D colormap;
			uniform sampler2D dmap;
			uniform vec4 eye;
			uniform vec4 skygroundcolor;

			void main() {
				vec4 dmapfrag = texture2D(dmap,vtexcoords);
				vec3 worldpoint = dmapfrag.yzw;
				float fdistance = length(worldpoint - eye.xyz);
				frag1 = texture2D(colormap,vtexcoords);
				if(fdistance < 300.0) {
					float atmosphere = clamp((exp(dmapfrag.x / 80.0) - 1)/2.71828182,0,1);
					frag1 = atmosphere * skygroundcolor;
				}
			}
			"
		}
	}

	//calculate black map for light shafts with smoke
	(shader) {
		id = "getblackscene2.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out float vsmokeatmosphere;

			uniform vec4 smokeboxpos;
			uniform mat4 viewprojection;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
				vec4 smokeproj = viewprojection * smokeboxpos;
				vsmokeatmosphere = clamp((exp(smokeproj.z / 80.0) - 1)/2.71828182,0,1);
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in float vsmokeatmosphere;
			out vec4 frag1;

			uniform sampler2D colormap;
			uniform sampler2D dmap;
			uniform sampler2D smokemap;
			uniform vec4 eye;
			uniform vec4 skygroundcolor;

			void main() {
				vec4 dmapfrag = texture2D(dmap,vtexcoords);
				vec4 smokefrag = texture2D(smokemap,vtexcoords);
				vec3 worldpoint = dmapfrag.yzw;
				float fdistance = length(worldpoint - eye.xyz);
				frag1 = texture2D(colormap,vtexcoords) * smokefrag.r + skygroundcolor * vsmokeatmosphere * (1-smokefrag.r);
				if(fdistance < 300.0) {
					float atmosphere = clamp((exp(dmapfrag.x / 80.0) - 1)/2.71828182,0,1);
					frag1 = skygroundcolor * ( atmosphere * smokefrag.r + vsmokeatmosphere * (1-smokefrag.r) );
				}
			}
			"
		}
	}

	//create lightshafts by radially blurring the black map
	(shader) {
		id = "getlightshafts.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec3 vsunposition;

			uniform vec4 sunposition;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
				vsunposition = sunposition.xyz;
			}
			"
		}
		(fragment) {"
			#version 400 core
			in vec2 vtexcoords;
			in vec3 vsunposition;
			out vec4 frag1;

			uniform sampler2D colormap;
			uniform float sunlocaction;

			void main() {
				float samplecount = 80.0;
				float density = 0.5;
				float decay = 0.95;
				float weight = 0.0625;
				float exposure = 1.0;
				float maxsampledelta = 0.002;
				
				vec2 samplercoord = vtexcoords;
				float currentdecay = 1.0;
				vec2 sunray = vec2(0.0);
				
				sunray = (vsunposition.xy - vtexcoords)/(samplecount * density);
				if(sunlocaction < 0.0 && vsunposition.z > 0.0) {
					sunray = -sunray;
				}

				float sunraylength = length(vec3(sunray,0));
				sunray = normalize(vec3(sunray,0)).xy * clamp(sunraylength,0,maxsampledelta);

				vec4 color = texture2D(colormap,samplercoord);

				float sampleweight = 0.0;
				
				for(float i=0.0 ; i<samplecount ; ++i)
				{
					samplercoord += sunray;
					currentdecay *= decay;
					color += texture2D(colormap,clamp(samplercoord,0.01,0.99)) * currentdecay * weight;
					sampleweight += currentdecay * weight;
				}
				
				frag1 = color * exposure / sampleweight;
			}
			"
		}
	}


}